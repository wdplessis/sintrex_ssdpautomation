/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.View.PerformanceGraphs;

/**
 *
 * @author jseekoei
 */
public class PerformanceGraphsPageObject
{
    //static objects
    //----------------------
    public static String baselineStatusXpath()
	{
		return "//div[@id = 'contenttablerdGrid_grid']//div[4]";
	}

	public static String pageNumberXpath()
	{
		return "//td[@id = 'paddingTD-1']//input[1]";
	}
	public static String OperatorDropdown()
    {
        return "cboOperator";
    }

	public static String ThresholdValueBox()
    {
        return "txtThresholdValue";
    }

	public static String ThresholdViolationBox()
    {
        return "txtThresholdViolation";
    }

    public static String BandMaximumBox()
    {
        return "txtBandMax";
    }

    public static String DescriptionBox()
    {
        return "txtDescription";
    }

		  public static String ImpactDropdown()
    {
        return "cboImpact";
    }

    public static String FilterTextbox()
    {
        return "txtFilter";
    }
		 public static String AddRule()
    {
        return "//div[contains(@id, 'MultiLevelContextMenu')][contains(@style, 'display: block')]//td[contains(text(), 'Add')]";
    }

		  public static String AttributeButton()
    {
        return "btnClassKeyLookup";
    }
                  
    public static String FilterBoxFilterContext()
    {
        return "//td[@id='contextFilterView']//select[@id='cboFilterField']";
    }
    
    public static String TextBoxFilterContext()
    {
        return "//td[@id='contextFilterView']//input[@id='txtFilter']";
    }
    
    public static String SearchButtonFilterContext()
    {
        return "//td[@id='contextFilterView']//img[@id='btnGo'][contains(@src,'alarmSearch.png')]";
    }
    
    public static String CurrentValuesWindowXpath()
    {
        return "//div[@id='tdContent']//br/..";
    }
   
    
    public static String CurrentValuesWindowCloseButtonXpath()
    {
        return "//td[text()='Current Values']//..//..//td[text()='Close']";
    }
    public static String GetCurrentValueOptionButtonXpath()
    {
        return "//table[@id='_cmGetCurrentVal']//td[contains(text(),'Get Current Value')]";
    }
    public static String ReportingCodeBuilderIcon()
    {
        return "//td[@id='contextFilterView']//div[@id='dvReportCode']//img[@id='btnGo']";
    }
    public static String TimeRegionBrowseButton()
    {
        return "//td[@id='contextFilterView']//button[@id='btnFilterLookup']";
    }
        
    public static String PerformanceGraphsContextButton()
    {
        return "//td[@id='contextFilterView']//u[text()='Context']";
    }

    public static String PerformanceGraphsFilter()
    {
        return "//td[@id='contextFilterView']//u[text()='Filter']";
    }
    
    public static String TdWithIdWithImg(String textId)
    {
        return "//td[@id='"+textId+"']//img";
    }
    
    //----------------------
    
    
    //dynamic page objects
    //----------------------

	public static String rightClickOptionXpath (String option)
	{
            return "//div[@id = '_MultiLevelContextMenu_1']//td[contains(text(), '" + option + "')]";
	}

	public static String ColumnHeader(String ColumnTitle)
        {
            return "//div[contains(@id,'ColId')][text()='"+ColumnTitle+"']";
        }
          
        public static String MenuItemText(String text)
        {
            return "//font[contains(text(),'"+text+"')]//..//..//span";
        }
        public static String ServiceStatusViewXpath(String text)
        {
            return "//font[text()='"+text+"']//..//..//span";
        }
        public static String SaveGraphUtilizationButton()
        {
            return "//img[@id='saveday']";
        }
        public static String ContextMenuPerformanceGraphs()
        {
            return "//td[@id='contextFilterView']//select[@id='cboContext']";
        }
        public static String GraphBasedOnTextXpath(String text, int num)
        {
            return "//td[text()='"+text+"']//..//..//td["+num+"][contains(@id,'tdGraph')]";
        }
        public static String Context_or_FIlter_HyperLink(String text)
        {
            return "//td[@id='contextFilterView']//u[text()='"+text+"']";
        }
        public static String FilterResultXpath(String text)
        {
            return "//td[@id='attributesTreeCell']//font[contains(text(),'"+text+"')]//..//..//span";
        }
        
        public static String reportingCodeXpath(String text)
        {
            return "//select//option[contains(text(), '"+text+"')]";
        }
        public static String AutoUpdateCheckboxXpath(String text)
        {
            return "//tr[@id='"+text+"']//input[contains(@id,'chkAutoUpdate')]";
        }
        
        public static String AutoUpdateCheckBoxxpath2(String text)
        {
            return "//table[@id='graphoutput']//td[contains(text(),'"+text+"')]//..//input[contains(@id,'chkAutoUpdate')]";
        }
        
        public static String reportingCode( String text)
        {
            return "//option[contains(text(),'"+text+"')]";
        }
        public static String GraphNameContains( String text)
        {
            return "//*[name()='text' and contains(text(),'"+text+"')]";
        }
    
    //----------------------
        
        public static String MinMaxAvgLast(String text)
        {
            return "//div[@id='divGraph_hourSummary']//td[contains(@id,'"+text+"')]";
        }

}
