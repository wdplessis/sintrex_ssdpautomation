/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.View.Alarms;

import KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.ConfigConsole.*;

/**
 *
 * @author snkosi
 */
public class AlarmViewer_ContextMenuAndCustomPageObject 
{
   public static String HostGroupsContexMenuXpath(String menuName)
   {
       return "//font[text()='"+menuName+"'][not(contains(@style,'BOLD'))]";
   }
   
   public static String gridRowsXpath()
   {
       return "//div[contains(@id,'_grid_grid_Body')]/div";
   }
   
   public static String RowColDataXpath(String row,String col)
   {
       return "//div[@id='_grid_grid_RowId_"+row+"_CellId_"+col+"']/span";
   }
   
   public static String SideMenuExpandImageXpath(String menuName )
   {
       return "//font[text()='"+menuName+"'][not(contains(@style,'BOLD'))]//..//..//..//td[1]/img";
   }
   
   public static String sideMenuHostsListXpath(String menuName)
   {
        return "//font[text()='" + menuName + "'][not(contains(@style,'BOLD'))]//..//..//..//../tr[@treeview_type='sub-branches']//tr[@treeview_containerid='treeviewLeft']//font";
    }

    public static String sideMenuHostListImages(String menuName)
    {
        return "//font[text()='" + menuName + "'][not(contains(@style,'BOLD'))]//..//..//..//../tr[@treeview_type='sub-branches']//tr[@treeview_containerid='treeviewLeft']/td[1]/img[contains(@src,'treePlus')]";
    }

    public static String sideMenuHostListImagesNames(String menuName)
    {
        return "//font[text()='" + menuName + "'][not(contains(@style,'BOLD'))]//..//..//..//../tr[@treeview_type='sub-branches']//tr[@treeview_containerid='treeviewLeft']/td[1]/img//..//../td[4]//font";
    }

    public static String SideMenuList()
    {
        return "//div[@id='treeviewLeft']/table[@class='defaultTreeTable']/tbody//tr[@treeview_containerid='treeviewLeft']/td[4]//font";
    }

    public static String childContainerImage(String contName)
    {
        return "//font[text()='" + contName + "'][not(contains(@style,'BOLD'))]//..//..//..//td[1]/img";
    }

    public static String loadingImageXpath(String containerName)
    {
        return "//font[text()='" + containerName + "'][not(contains(@style,'BOLD'))]//..//..//..//..//img[contains(@src,'Animated')]";
    }
    
    public static String HostUnderContainerXpath(String contName,String hostName)
    {
        return "//font[text()='"+contName+"'][not(contains(@style,'BOLD'))]//..//..//..//../tr[@treeview_type='sub-branches']//tr[@treeview_containerid='treeviewLeft']//font[text()='"+hostName+"']";
    }
    public static String ExpandHostGroupXpath(String groupName)
    {
        return "//div[@id='alarmDashbord']/..//font[text()='"+ groupName+"']/../../..//img[contains(@src,'treePlus')]";
    }

    public static String GetHostAlarmNumber(String Hostname)
    {
        return "//span[text()='"+Hostname+"']/../..//div[@id='_grid_grid_RowId_0_CellId_0']//span";
    }
   
}
