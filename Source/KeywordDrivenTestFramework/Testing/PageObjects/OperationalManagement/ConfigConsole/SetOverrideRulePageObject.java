/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.ConfigConsole;

/**
 * @author nelson
 */
public class SetOverrideRulePageObject
{

    public static String extractedHostNameXpath(String hostIP)
    {
        return "//span[text() = '" + hostIP + "']/../..//div[3]//span";
    }

    public static String ipInterfaceOptionXpath()
    {
        return "//div[@class = 'jqx-grid-groups-row']//span[contains(text(), 'IP Interface')]";
    }

    public static String filteringDropDownXpath()
    {
        return "//div[@id='dropdownlistWrappercboTreeFilter']//div[@class='jqx-icon-arrow-down jqx-icon']";
    }

    public static String arrayObjectNames()
    {
        return "//div[contains(@class,'qx-grid-group-expand')]//..//span[@class='jqx-grid-groups-row-details']";
    }

    public static String baselineXpath()
    {
        return "//li[text() = 'Tasks']/../../../../../..//div[contains(@class, 'jqx-rc-t-expanded')]//li[text() = 'Create Baseline']";
    }
    
    public static String HostCPUUtilXpath(String text)
    {
      //  return "//span[text()='"+text+"']//..//..//div[8]//span[contains(text(),'CPU Util')]";
        return "//span[contains(text(),'"+text+"')]";
    }
    
    public static String scrollingDown()
    {
        return "//div[@id='jqxScrollBtnDownverticalScrollBardvObjects']//div[@class='jqx-reset jqx-icon-arrow-down']";
    }

}
