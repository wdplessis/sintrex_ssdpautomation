/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.ConfigConsole.Locations;


/**
 *
 * @author snkosi
 */
public class CreateBaseLinePageObjects
{

    public static String AllRoutersXpath()
    {
        return "//tr[contains(@data-key,'100001_L_6242_L_')]//span[contains(text(),'-')]";
    }

    public static String AllSwitchesXpath()
    {
        return "//tr[contains(@data-key,'100001_L')]//span[contains(text(),'-')]";
    }

    public static String trWithDatakey(String Datakey)
    {
        return "//tr[@data-key='" + Datakey + "']";
    }

    public static String DivStatusColumnSpan()
    {
        return "//div[contains(@class, 'c2')]//span";
    }
    
    public static String DivStatusColumn()
    {
        return "//div[@role='row']//div[4]//div";
    }
    //Added
    public static String DivStatusColumnXpath()
    {
        return "//div[contains(@class, 'jqx-grid-cell jqx-item column')]/div";
    }

    public static String tdWithIdImg()
    {
        return "//td[@id='requestSettings']//img";
    }
        public static String trWithID(String text)
    {
        return "//tr[@id='"+text+"']";
    }
    
    
}
