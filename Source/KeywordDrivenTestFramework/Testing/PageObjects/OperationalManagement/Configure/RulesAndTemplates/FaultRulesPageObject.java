/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.Configure.RulesAndTemplates;

/**
 * This is the page object class of SIN-58-Maintain Remove from containers
 *
 * @author nelson
 */
public class FaultRulesPageObject
{

    public static String FaultRulesRightClickOptionXpath(String option)
    {
        return "//div[@id = '_MultiLevelContextMenu_1']//td[contains(text(), '" + option + "')]";
    }

    public static String InputWithId(String text)
    {
        return "//input[@id='" + text + "']";
    }

    public static String descriptionFromFRListXpath()
    {
        return "//div[@class = ' gridRowDefault']//div[3]//span";
    }

    public static String MenuItemDropDownXpath(String text)
    {
        return "//li[@item-label = '" + text + "']";
    }

    public static String AWithIdXpath(String text)
    {
        return "//a[@id = '" + text + "']";
    }

    public static String BaselineNumberOfElement()
    {
        return "//div[@id = 'undefined_recordsLabel']/span//span";
    }

    public static String DivStatusColumn()
    {
        return "//div[@role='row']//div[3]//div";
    }

    public static String DivWithUnsectable(String dropList)
    {
        return "//li[@id = '" + dropList + "']";
    }

    public static String HostGraphXpath()
    {
        return "//div[@id = 'dateGraphContainer']//canvas[1]";
    }

    public static String GraphPerformanceXpath(String text)
    {
        return "//td[text() = '" + text + "']/..//div[contains(@id, 'divGraphIUtilization')]/canvas[2]";
    }

    public static String PerformanceHomeIconXpath()
    {
        return "//div[@id = 'Performance']//a[text() = 'Home']";
    }

    public static String WhiteSpacefromDropDownbelowContextDropDownXpath()
    {
        return "//span[text() = 'Location']/../..//div[@id = 'listitem0innerListBoxcboTreeFilter']//span";
    }

    public static String MainHomeIconXpath()
    {
        return "//div[@id = 'mainSplitter1']//a[text()='Home']";
    }

    public static String DivStatusColumnSpan()
    {
        return "//div[contains(@class, 'c2')]//span";
    }

    public static String messageFaultRuleList()
    {
        return "//div[text()='Alarm']//..//..//..//div[@id='_grid_grid_Body']//div[contains(@id, '_grid_grid_RowId')]//div[8]";
    }

    public static String ValidateAlarmViewerPage()
    {
        return "//table[@id= 'mainTable'][@style = 'overflow: hidden; table-layout: fixed; height: 98%;']//div[@id = '_grid_grid_Header']";
    }

    public static String exportTemplateFirstGrid()
    {
        return "//div[@id = '_grid_gridReportViewer_RowId_0_CellId_0']//span";
    }

    public static String listOfPreviousCsvNames()
    {
        return "//div[contains(@id, '_grid_gridReportViewer_RowId_')][@class = ' gridRowDefault']";
    }

    public static String baselineStatusXpath()
    {
        return "//div[@id = 'contenttablerdGrid_grid']//div[@role = 'gridcell'][4]//div";
    }

    public static String columnTextXpath(String text, String number)
    {
        return "//div[text()='" + text + "']//..//..//..//div[@id='_grid_grid_Body']//div//div[contains(@class,'grid_grid_c" + number + " gridCell')]//span";
    }

    public static String validateRuleImported()
    {
        return "//div[@id = 'fileInfo']//td[contains(text(), 'Total Number of Rules')]";
    }

    public static String createBaselineDropDownXpath()
    {
        return "//div[@class = 'jqx-menu-popup jqx-menu-popup jqx-rc-t-expanded']//li[text() = 'Create Baseline']";
    }
}
