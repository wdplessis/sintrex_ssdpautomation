/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.Configure.RulesAndTemplates;

/**
 *
 * @author abassier
 */
public class PerformanceRulesPageObject {
    
                //----------Dynamic Objects----------//
    
    public static String PerformanceRulesColumnID(int IDNum)
    {
        return "//div[contains(@id, 'CellId_"+IDNum+"')][not(contains(@id,'gridResults'))]//span";
    }
    
    public static String ColumnHeader(String ColumnTitle)
    {
        return "//div[contains(@id,'ColId')][text()='"+ColumnTitle+"']";
    }
    
    public static String PerformanceRuleDeactivatedImgXpath(String text)
    {
        return "//span[contains(text(),'"+text+"')]//..//..//div[7]//img[@src='resources/images/checkboxNoChk.png']";
    }
    
    public static String PerformanceRuleActivatedImgXpath(String text)
    {
        return "//span[contains(text(),'"+text+"')]//..//..//div[7]//img[@src='resources/images/checkboxChk.png']";
    }
    
    public static String PerformanceRulesActiveColumnCheckTicks(String rowNum)
    {
        return "//div[contains(@id, '_grid_grid_RowId_"+rowNum+"')][contains(@id, '_CellId_6')]//img[@src='resources/images/checkboxChk.png']";
    }
    
    public static String SortColumnHeadingsXpath(int num)        
    {
        return "//div[@id='_grid_grid_ColId_"+num+"_Sort']";
    }
    
                //----------Dynamic Objects----------//
    
    public static String AddRule()
    {
        return "//div[contains(@id, 'MultiLevelContextMenu')][contains(@style, 'display: block')]//td[contains(text(), 'Add')]";
    }
    
    public static String AttributeKeyClassColumn()
    {
        return "//div[contains(@id, '_grid_grid_RowId_')][contains(@id, 'CellId_0')]";
    }
    
    public static String AttributeDescriptionColumn()
    {
        return "//div[contains(@id, '_grid_grid_RowId_')][contains(@id, 'CellId_1')]";
    }
    
    public static String GenerateBaseline()
    {
        return "//td[@name='createBaseLine_applyButton']";
    }    
    
    public static String OKPopUpWindow()
    {
        return "//td[@id='alertOK'][contains(text(),'OK')]";
    }
    
    public static String BaselineResultsStatusColumn()
    {
        return "//div[contains(@class, 'jqx-grid-cell jqx-item column')]/div";
    }
    
    public static String ColumnsDropdown()
    {
        return "//li[@id='Columns_id']";
    }
    
    public static String ActivatePerformanceRule()
    {
        return "//table[@id='_Activate']";
    }
    
    public static String DeactivatePerformanceRule()
    {
        return "//table[@id='_Deactivate']";
    }
    
    public static String DeletePerformanceRule()
    {
        return "//table[@id='_Delete']";
    }
    
    public static String ImportTemplate()
    {
        return "//table[@id='_Import']";
    }
    
    public static String ExportTemplate()
    {
        return "//table[@id='_Export_Template']";
    }
    
    public static String ChooseFileButton()
    {
        return "pathFrom";
    }
    
    public static String DownloadTemplate()
    {
        return "//table[@id='_contextDownload']";
    }
    
    public static String DeleteGeneratedTemplate()
    {
        return "//table[@id='_contextDelete']";
    }
    
    public static String ExportTemplateExtension()
    {
        return "E_ExportPerformanceGlobalRulesReport_";
    }
            
    public static String AttributeButton()
    {
        return "btnClassKeyLookup";
    }
    
    public static String OperatorDropdown()
    {
        return "cboOperator";
    }
    
    public static String ThresholdValueBox()
    {
        return "txtThresholdValue";
    }
    
    public static String ThresholdViolationBox()
    {
        return "txtThresholdViolation";
    }
    
    public static String BandMaximumBox()
    {
        return "txtBandMax";
    }    
    
    public static String DescriptionBox()
    {
        return "txtDescription";
    }
    
    public static String ImpactDropdown()
    {
        return "cboImpact";
    }
    
    public static String FilterTextbox()
    {
        return "txtFilter";
    }
        
    public static String MainPageCSVButton()
    {
        return "performanceGlobalRuleMaintenance_btnExportGrid";
    }
    
    public static String AttributeKeyCSVButton()
    {
        return "attributeClassKeyLookup_btnExportGrid";
    }
    
    public static String PerformanceRulesCSVExtension()
    {
        return "performanceGlobalRuleMaintenance_GridExport";
    }
    
    public static String AttibuteKeyCSVExtension()
    {
        return "attributeClassKeyLookup_GridExport";
    }
        
    public static String PageRight()
    {
        return "resources/images/next.png";
    }
    
    public static String PageLeft()
    {
        return "resources/images/previous.png";
    }
        
    public static String AlarmViewerDropdown()
    {
        return "cboContext";
    }
    
    public static String GenerateTemplateButton()
    {
        return "//td[@id='applyButton'][@name='reportParameterValues_applyButton']";
    }
    
    public static String ExportTemplateRefreshButton()
    {
        return "reportViewer_Filter";
    }
    
    public static String ExportTemplateTitle()
    {
        return "ExportPerformanceGlobalRulesReport";
    }
    
    public static String switchFramemodalIframeId1()
    {
        return "modalIframeId1";
    }
    
}
