/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.Configure;

/**
 *
 * @author abassier
 */
public class ConfigConsolePageObjects {
    
    public static String FilterDropdown()
    {
        return "dropdownlistContentcboTreeFilter";
    }
    
    public static String SearchBox()
    {
        return "inputFilterValue";
    }
    
    public static String PredictiveTextSelect(String text)
    {
        return "//li[@data-name='"+text+"']";
    }
    
    public static String SetDropdownBlank()
    {
        return "listitem0innerListBoxcboTreeFilter";
    }
    
    public static String DatePickerMonthID()
    {
        return "//td[contains(@id,'calendarTitleHeader')]";
    }
    
    public static String DatePickerCurrentDay()
    {
        return "//td[contains(@class,'calendar-cell-today')]";
    }
     
    public static String PreviousMonth()
    {
        return "//div[contains(@class,'arrow-left')][not(contains(@class,'reset'))]";
    }
    
    public static String NextMonth()
    {
        return "//div[contains(@class,'arrow-right')][not(contains(@class,'reset'))]";
    }
    
    public static String HostAddedDateCalendarButton()
    {
        return "//div[contains(@class, 'button')]/div[contains(@class, 'calendar')]";
    }
    
    public static String DateHostWasAdded(String day)
    {
        return "//td[contains(@class, 'calendar-cell-month')][text()='"+day+"'][ not(contains(@class,'othermonth'))]";
    }
    
    public static String Calendar()
    {
        return "inputinputFilterValue";
    }
    //div[@id='inputFilterValue']//div[@class='jqx-icon jqx-icon-calendar']
    public static String CalendarIcon()
    {
        return "//div[@id='inputFilterValue']//div[@class='jqx-icon jqx-icon-calendar']";
    }
    
    public static String FilterOptionResultsWindowExcludingSubnetsAndUnlinkedLocations()
    {
        return "//span[@class='jqx-tree-grid-title jqx-grid-cell-wrap'][not(contains(text(),'.'))][not(contains(text(),'Unlinked Devices'))]";
    }
    
    public static String FilterOptionResultsWindow()
    {
        return "//span[@class='jqx-tree-grid-title jqx-grid-cell-wrap']";
    }
    
    public static String FilterOptionResultsWindowWithoutSubnets()
    {
        return "//span[@class='jqx-tree-grid-title jqx-grid-cell-wrap'][not(contains(text(),'.'))]";
    }
    
    public static String ContextDropdownMenu()
    {
        return "dropdownlistContentcboContext";
    }
    
    public static String BlankSpotClick()
    {
        return "contenttreeGrid";
    }
    
    public static String ObjectInterfaceXpath()
    {
        return "//span[contains(text(),'Object: ')]//..//span[contains(text(),'Interface')]";
    }
    
    public static String CellsWithinRowContainsFirstCellConfigTab(String text)
    {
        return "//span[text()='"+text+"']/../..//td[not(contains(@style, 'none'))]";
    }
    
    public static String InputWithCanchange(String text)
    {
        return "//input[@canchange='"+text+"']";
    }
    
    //--------------------Edit Host---------------------------------------
    public static String EditHostFilterDropdown(String tdclass, String idtext)
    {
        return "//td[@class='"+tdclass+"']//select[@id='"+idtext+"']";
    }
    
    public static String EditHostFilterTextbox(String tdclass, String idtext)
    {
        return "//td[@class='"+tdclass+"']//input[@id='"+idtext+"']";
    }
    
    public static String EditHostFilterSearchButton(String tdclass, String idtext)
    {
        return "//td[@class='"+tdclass+"']//img[@id='"+idtext+"']";
    }
    
    public static String EditHostFilterColumnXpath(String divId, String coltext)
    {
        return "//div[@id='"+divId+"']//div[contains(@class,' "+coltext+"')]//span";
    }
    
    public static String DivCotainsWithSpan(String text)
    {
        return "//div[contains(@class,' "+text+"')]//span";
    }
    
    
    public static String SpanWithinRowContainingText(String text)
    {
        return "//span[contains(text(), '"+text+"')]//..//../div//span";
    }
    
    public static String ScrollArrowXpath(String textid, String textClass)
    {
        return "//div[@id='"+textid+"']//div[@class='"+textClass+"']";
    }
    
    public static String FilterWorklistBottomWindow()
    {
        return "jqx-widget jqx-input jqx-rc-all jqx-widget-content";
    }
    
    public static String DivContainsClassWithSpan(String text)
    {
        return "//div[contains(@class,'"+text+"')]//span";
    }
    
    public static String ColumnNumXpath(String colNum)
    {
        return "//div[contains(@id, 'CellId_"+colNum+"')]//span";
    }
    
    public static String TdWithId_AWithText(String textId,String text)
    {
        return "//td[@id='"+textId+"']//a[text()='"+text+"']";
    }
    
    public static String DivWithId_Span(String textId)
    {
        return "//div[@id='"+textId+"']//span";
    }
    
    public static String HostCheckBox(String text)
    {
        return "//span[contains(text(),'"+text+"')]//..//span[contains(@class,'jqx-tree-grid-checkbox')]";
    }
    
    public static String TdWithNameContainsStyle(String textName, String textStyle)
    {
        return "//td[@name='"+textName+"'][contains(@style,'"+textStyle+"')]";
    }
    
    public static String ThirdCellInTable(String textName)
    {
        return "//span[text()='"+textName+"']//..//..//div[3]//span";
    }
    
    public static String CloseButtonSSH()
    {
        return "//div[@id='buttomDiv1']//div[@id='divSubmitBtns']//td[text()='Close']";
    }
    
    public static String ExpandedContainers()
    {
        return "//span[contains(@class, 'expand-button')]";
    }
     public static String ExpandAll()
    {
        return "//a[@href= 'javascript:expandAll()']";
    }
    
    
    public static String CollapsedContainers()
    {
        return "//span[contains(@class, 'collapse-button')]";
    }
    
    public static String ContainerIcons()
    {
        return "//img[@class='jqx-tree-grid-icon-size jqx-tree-grid-indent']";
    }
    
    public static String WinAgentUpdatesStatusColumn()
    {
        return "//div[contains(@id,'_CellId_3')]//span";
    }
    
    public static String WorkListDialogStatusColumnUsingIPXpah(String textIP)
    {
        return "//div[text()='"+textIP+"']/../..//div[4]/div";
    }
    

    
    //------------------config tab---------------------------------------------
    public static String SpanWithinTableXpath(String tableId, String textSpan)
    {
        return "//table[@id='"+tableId+"']//span[contains(text(),'"+textSpan+"')]";
    }
    
    public static String ConfigTabDescriptionFilterTextField()
    {
        return "//div[@class='jqx-grid-cell jqx-grid-cell-pinned jqx-grid-cell-filter-row']//input";
    }
    
    public static String ObjectsWithinObjectGroups(String text)
    {
        return "//div[contains(@role,'gridcell')]//div[contains(text(),'"+text+"')]";
    }
    
    public static String DisableUiWindow()
    {
        return "//div[@class='disableUI'][@id='aeContent'][contains(@style,'block')]";
    }
    
    public static String ObjectGroupsXpath(String text)
    {
        return "//span[contains(text(),'Object:')]//..//span[contains(text(),'"+text+"')]";
    }
    
    public static String menuArrowButton(String text)
    {
        return "//span[contains(text(),'"+text+"')]//..//span[contains(@class,'collapse-button')]";
    }
    
    public static String hostCheckBox(String text)
    {
        return "//span[contains(text(),'"+text+"')]//..//span[contains(@class,'checkbox-default')]";
    }
    
    public static String menuArrowButtonCollapse(String text)
    {
        return "//span[contains(text(),'"+text+"')]//..//span[contains(@class,'group-expand')]";
    }
    
    public static String bUnitCheckBox(String text)
    {
        return "//span[contains(text(),'"+text+"')]//..//span[contains(@class,'grid-checkbox')]";
    }
    
    public static String contactULDropDown(String text)
    {
        return "//div[contains(@id,'listitem2innerListBoxselFilterCriteriaUS')]//span[contains(text(),'"+text+"')]";
    }
    
    public static String contactHGDropDownList(String text)
    {
        return "//div[contains(@id,'listitem2innerListBoxselFilterCriteriaHG')]//span[contains(text(),'"+text+"')]";
    }
    
    public static String contactUsersDropDown()
    {
        return "//td//span[contains(text(),'Users')]//..//span[contains(@class, 'arrow-right')]";
    }
    //-----------------end-of-oonfig------------------------------------------ 
    
    
    
}