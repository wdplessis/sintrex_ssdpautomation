/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author szeuch
 */
public class SintelligentSmokeTestPageObject
{
    public static String MainMenuButtonXpath()
    {
        return "//table[@id='tableMenuContainer']//img[@src='resources/images/menuLabel.png']";
    }
    
    public static String CloseTabXpath(String tabName)
    {
        return "//a[text()='"+tabName+"']/..//span";
    }
    
    public static String InterrogationStatusXpath(String text)
    {
        return "//div[text()='"+text+"']/../..//div[4]/div";
    }
    
    public static String AlarmViewerAlarmCounterXpath()
    {
        return "//div[@id='_grid_grid_Body']/div";
    }
    
    public static String AlarmViewerBodyXpath()
    {
        return "_grid_grid_Body";
    }
}
