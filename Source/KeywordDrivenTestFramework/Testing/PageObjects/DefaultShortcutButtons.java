/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;


/**
 *
 * @author trichardson
 */

public class DefaultShortcutButtons {

    // -- found in default iframe -- //
    
    public static String UserPortalButton() 
    {
        return "//td[@title='User Portal']";
    }

    public static String UserRefreshButton() 
    {
        return "//td[@title='Refresh']";
    }

    public static String UserConsoleButton() 
    {
        return "//td[@title='Console']";
    }

    public static String UserAlarmViewerButton() 
    {
        return "//td[@title='Alarm Viewer']";
    }

    public static String UserPerformanceGraphsButton() 
    {
        return "//td[@title='Performance Graphs']";
    }

    public static String UserReportsButton() 
    {
        return "//td[@title='Reports']";
    }

    public static String UserWANDashboardButton() 
    {
        return "//td[@title='WAN Dashboard']";
    }

    public static String UserRequestButton() 
    {
        return "//td[@title='User Request']";
    }

    public static String UserHealthCheckButton() 
    {
        return "//td[@title='Health Check']";
    }

    public static String UserPreferenceButton() 
    {
        return "//td[@title='User Preference']";
    }

    public static String UserHelpButton() 
    {
        return "//td[@title='Help']";
    }

    public static String UserLogoutButton() 
    {
        return "//td[@title='Logout']";
    }

}
