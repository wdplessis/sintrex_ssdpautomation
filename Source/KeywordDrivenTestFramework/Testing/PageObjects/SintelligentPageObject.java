/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import static KeywordDrivenTestFramework.Core.BaseClass.currentEnvironment;
import org.openqa.selenium.By;

/**sadf
 *
 * @author szeuch
 */
public class SintelligentPageObject
{

    //-------------------URL's-------------------//
    public static String SintelligentURL()
    {
        return currentEnvironment.PageUrl;
    }

    public static String BackendManagementPortalURL()
    {
        return "http://10.0.4.195/csp/sys/UtilHome.csp";
    }
    //-------------------URL's-------------------//

    //-------------------Generic Page Objects-------------------//
    public static String SpanWithTextXpath(String text)
    {
        return "//span[text()='" + text + "']";
    }
    
    
          
    public static String DivWithIDSpanXpath(String text)
    {
        return "//Div[@id='" + text + "']//span";
    }

    public static String DivWithIDSpanWithText(String text, String Description)
    {
        return "//Div[@id='" + text + "']//span[text()='" + Description + "']";
    }

    public static String DivWithIdDivXpath(String text)
    {
        return "//Div[@id='" + text + "']//div";
    }

    public static String BWithTextXpath(String text)
    {
        return "//b[text()='" + text + "']";
    }

    public static String DivWithTextSpanXpath(String text)
    {
        return "//div[text()='" + text + "']//div";
    }

    public static String frameTemplateMaintenance()
    {
        return "iframetemplateMaintenance";
    }

    public static String DownloadFileNameXpath(String text)
    {
        return "//span[text()='" + text + "']/../..//div[5]//span";
    }

    public static String SpanContainsTextXpath(String text)
    {
        return "//span[contains(text(), '"+text+"')]";
    }

    public static String TdContainsInput(String text)
    {
        return "//td[contains(text(),'" + text + "')]/..//input";
    }

    public static String SpanContainsClassXpath(String text)
    {
        return "//span[contains(@class, '" + text + "')]";
    }

    public static String AWithTextXpath(String text)
    {
        return "//a[text()='" + text + "']";
    }

   
    public static String TdWithIDWithA(String text)
    {
        return "//td[@id='" + text + "']//a";
    }
    
    public static String TdContainsID(String text)
    {
        return "//td[contains(@id,'"+text+"')]";
    }

    public static String ReportsIframe()
    {
        return "iframereportMaintenance";
    }

    public static String ReportsIframe2()
    {
        return "iframereportMaintenance2";
    }

    public static String TdWithName(String text)
    {
        return "//td[@name='" + text + "']";
    }

    public static String AContainsOnclickTextXpath(String text)
    {
        return "//a[contains(@onclick,'" + text + "')]";
    }

    public static String AContainsTextXpath(String text)
    {
        return "//a[contains(text(), '" + text + "')]";
    }
    
     public static String AWithClassXpath(String text)
    {
        return "//a[@class= '" + text + "']";
    }

       public static String AWithHref(String text)
    {
        return "//a[@href= '" + text + "']";
    }

    public static String LiContainsTextXpath(String text)
    {
        return "//li[contains(text(), '" + text + "')]";
    }

    public static String LiWithIdwithAXpath(String text)
    {
        return "//li[@id='" + text + "']//a";
    }

    public static String LiContainsTextXpath2(String text)
    {
        return "//li[contains(text(), '" + text + "')]";
    }

    public static String LiWithTextXpath(String text)
    {
        return "//li[text()= '" + text + "']";
    }

    public static String LiWithIdXpath(String text)
    {
        return "//li[@id= '" + text + "']";
    }

    public static String LiWithClassXpath(String text)
    {
        return "//li[@class= '" + text + "']";
    }

    public static String LiContainsClassXpath(String text)
    {
        return "//li[contains(@class, '" + text + "')]";
    }

    public static String MarqueeContainsTextXpath(String text)
    {
        return "//marquee[contains(text(),'" + text + "')]";
    }

    public static String OptionWithValueXpath(String text)
    {
        return "//option[@value='" + text + "']";
    }
    
    public static String OptionWithIDXpath(String text)
    {
        return "//option[@id='" + text + "']";
    }
    
    public static String TdWithTextXpath(String text)
    {
        return "//td[text()='" + text + "']";
    }

    public static String OptionWithTextXpath(String text)
    {
        return "//option[text()='" + text + "']";
    }  

    public static String OptionWithTitleXpath(String text)
    {
        return "//option[@title='" + text + "']";
    }
    
    public static String OptionContinasTitleXpath(String text)
    {
        return "//option[contains(@title,'" + text + "')]";
    }

    public static String OptionContainsTextXpath(String text)
    {
        return "//option[contains(text(),'" + text + "')]";
    }

    public static String TdContainsTextXpath(String text)
    {
        return "//td[contains(text(), '" + text + "')]";
    }

    public static String ImgWithIdXpath(String text)
    {
        return "//img[@id='" + text + "']";
    }

    public static String ImgWithTitlepath(String text)
    {
        return "//img[@title='" + text + "']";
    }

    public static String TdContainsTitleXpath(String text)
    {
        return "//td[contains(@title,'" + text + "')]";
    }

    public static String ImgWithNameXpath(String text)
    {
        return "//img[@name='" + text + "']";
    }

    public static String ColumnSortStatusIcon(String text)
    {
        return "//div[text()='" + text + "']/..//div[3]";
    }

    public static String ImgContainsSrcAndNameXpath(String src, String name)
    {
        return "//img[contains(@src,'" + src + "')][contains(@name,'" + name + "')]";
    }

    public static String ImgContainsTitle(String text)
    {
        return "//img[contains(@title,'" + text + "')]";
    }

    public static String ImgWithSrc(String text)
    {
        return "//img[@src='" + text + "']";
    }

    public static String ImgContainsSrc(String text)
    {
        return "//img[contains(@src, '" + text + "')]";
    }

    public static String InputWithValueXpath(String text)
    {
        return "//input[@value='" + text + "']";
    }

    public static String InputWithIdXpath(String text)
    {
        return "//input[@id='" + text + "']";
    }

    public static String InputWithClassXpath(String text)
    {
        return "//input[@class='" + text + "']";
    }

    public static String InputContainsValueXpath(String text)
    {
        return "//input[contains(@value, '" + text + "')]";
    }

    public static String InputContainsIdXpath(String text)
    {
        return "//input[contains(@id, '" + text + "')]";
    }

    public static String InputWithName(String text)
    {
        return "//input[@name='" + text + "']";
    }

    public static String InputWithType(String text)
    {
        return "//input[@type='" + text + "']";
    }

    public static String NobrContainsTextXpath(String text)
    {
        return "//nobr[contains(text(), '" + text + "')]";
    }

    public static String PContainsTextXpath(String text)
    {
        return "//p[contains(text(), '" + text + "')]";
    }

    public static String SelectWithIdXpath(String text)
    {
        return "//select[@id='" + text + "']";
    }

    public static String SelectWithIdOptionXpath(String text)
    {
        return "//select[@id='" + text + "']//option";
    }

    public static String SelectWithIDByoptionTextXpath(String DDownXpath, String txtToSelect)
    {
        return "//select[@id = '" + DDownXpath + "']//option[text() = '" + txtToSelect + "']";
    }

    public static String SelectWithIDByoptionContainsTextXpath(String DDownXpath, String txtToSelect)
    {
        return "//select[@id = '" + DDownXpath + "']//option[contains(text(),'" + txtToSelect + "')]";
    }

    public static String SelectWithNameXpath(String text)
    {
        return "//select[@name='" + text + "']";
    }

    public static String ListItemWithIdToAXpath(String text)
    {
        return "//li[@id='" + text + "']//a";
    }

    public static String AWithTitleXpath(String text)
    {
        return "//a[@title='" + text + "']";
    }

    public static String DivWithTextXpath(String text)
    {
        return "//div[text()='" + text + "']";
    }

    public static String DivContainsTextXpath(String text)
    {
        return "//div[contains(text(), '" + text + "')]";
    }

    public static String DivWithClassDivWithText(String tabclass, String text)
    {
        return "//div[@class='" + tabclass + "']//div[text()= '" + text + "']";
    }

    public static String ButtonWithTextXpath(String text)
    {
        return "//button[text()='" + text + "']";
    }

    public static String ButtonWithIDXpath(String text)
    {
        return "//button[@id='" + text + "']";
    }

      public static String ButtonWithTypeXpath(String text)
    {
        return "//button[@type='" + text + "']";
    }
      
      
    public static String ButtonWithTitleXpath(String text)
    {
        return "//button[@title='" + text + "']";
    }

    public static String ButtonContainsOnClickXpath(String text)
    {
        return "//button[contains(@onclick,'" + text + "')]";
    }

    public static String FontContainsTextXpath(String text)
    {
        return "//font[contains(text(), '" + text + "')]";
    }

    public static String FontWithTextXpath(String text)
    {
        return "//font[text()='" + text + "']";
    }
    public static String FontWithIdXpath(String text)
    {
        return "//font[@id='" + text + "']";
    }
    public static String TdWithId(String text)
    {
        return "//td[@id='" + text + "']";//font
    }

    public static String TdWithClass(String text)
    {
        return "//td[@class='" + text + "']";
    }

    public static String TdWithTitle(String text)
    {
        return "//td[@title='" + text + "']";
    }

    public static String CompleteReport_ReportNameTextXpath(String text)
    {
        return "//div[@id='" + text + "']/span";
    }

    public static String DivWithId(String text)
    {
        return "//div[@id='" + text + "']";
    }
    public static String DivWithId2(String text)
    {
        return "//div[@id='_grid_grid_ColId_1_Desc']";  
    }
    
    public static String DivContainsId(String text)
    {
        return "//div[contains(@id, '" + text + "')]";
    }
    
    public static String DivContainsIdSpanText(String text, String text2)
    {
        return "//div[contains(@id, '" + text + "')]//span[contains(text(),'"+text2+"')]";
    }
    
    public static String DivContainsIdSpan(String text)
    {
        return "//div[contains(@id, '" + text + "')]//span";
    }

    public static String DivWithClass(String text)
    {
        return "//div[@class='" + text + "']";
    }

    public static String DivContainsClass(String text)
    {
        return "//div[contains(@class, '" + text + "')]";
    }

    public static String DivContainsClassSpan(String text)
    {
        return "//div[contains(@class, '" + text + "')]//span";
    }

    public static String bContainsText(String text)
    {
        return "//b[contains(text(), '" + text + "')]";
    }

    public static String TextAreaWithId(String text)
    {
        return "//textarea[@id='" + text + "']";
    }

    public static String IframeContainsNameXpath(String text)
    {
        return "//iframe[contains(@name , '" + text + "')]";
    }

    public static String TagWithTwoContainsConditions(String tag, String conditionOneAttribute, String conditionOneAttributeValue, String conditionTwoAttribute, String conditionTwoAttributeValue)
    {
        return "//" + tag + "[contains(" + conditionOneAttribute + ",'" + conditionOneAttributeValue + "')][contains(" + conditionTwoAttribute + ",'" + conditionTwoAttributeValue + "')]";
    }

    public static String FilterButtonXpath(String srcName, String frameName)
    {

        return "//img[contains(@src,'" + srcName + "')]//..//img[contains(@name,'" + frameName + "')]";
    }

    public static String MainTabs(String text)
    {
        return "//li[@tabname='" + text + "']//a";
    }

    public static String MainTabsClose(String text)
    {
        return "//li[@tabname='" + text + "']//a//..//span";
    }

    public static String MainTabClose(String text)
    {
        return "//li[@tabname = '" + text + "']//span";
    }

    public static String LiWithTabname(String text)
    {
        return "//li[@tabname = '" + text + "']";
    }

    public static String tablewithIdXpath(String text)
    {
        return "//table[@id='" + text + "']";
    }

    public static String tableWithRole(String text)
    {
        return "//table[@role='" + text + "']";
    }
    public static String tablewithClassXpath(String text)
    {
        return "//table[@class='" + text + "']";
    }
    //-------------------Generic Page Objects-------------------//
    //-------------------Login Page Objects-------------------//
    public static String UsernameTextBoxXpath()
    {
        return "//input[@name='CacheUserName']";
    }

    public static String PasswordTextBoxXpath()
    {
        return "//input[@name='CachePassword']";
    }

    public static String LoginButtonXpath()
    {
        return "//div[@id='divLoginButton']";
    }

    public static String LoginButtonBackendXpath()
    {
        return "//input[@name='CacheLogin']";
    }

    public static String IncorrectLoginCredentialsXpath()
    {
        return "//div[text()='Incorrect login details specified. Please try again.']";
    }
    //-------------------Login Page Objects-------------------//

    //-------------------Cross-Page Page Objects-------------------//
    public static String ColumnGridItemSpans(String columnNum)
    {
        return "//div[contains(@id, '_grid_grid_RowId_')][contains(@id, '_CellId_" + columnNum + "')]//span";
    }

    public static String ColumnGridItemSpans(String gridPartialId, String columnNum)
    {
        return "//div[contains(@id, '" + gridPartialId + "')][contains(@id, '_CellId_" + columnNum + "')]//span";
    }

    public static String MainMenuButtonXpath()
    {
        return "//img[@src='resources/images/menuLabel.png']";
    }

    public static String LogOutButtonXpath()
    {
        return "//img[@id='btnLogout']";
    }

    public static String HeaderTabsXpath(String text)
    {
        return "//li[@tabname='" + text + "']//a[text()='" + text + "']";
    }

    public static String ChooseFileButton()
    {
        return "//td[contains(text(), 'Select a file')]//..//..//input";
    }

    public static String SubnetsMainPageRefreshSrc()
    {
        return "../resources/images/filter.png";
    }

    public static String OpenHostGroupDropdown(String text)
    {
        return "//span[contains(text(), '" + text + "')]//..//span[contains(@class, 'button')]";
    }

    public static String OpenHostGroupCheckBox(String text)
    {
        return "//span[contains(text(), '" + text + "')]//..//span[contains(@class, 'checkbox')]";
    }
    public static String TableWithIdandTdContainsText(String Id,String text)
    {
        return "//table[@id='"+ Id+"']//td[contains(text(),'"+text+"')]";
    }
    public static String ReportingCodeGroupDropdown(String text)
    {
        return "//td[text()= '" + text + "']//..//..//..//..//..//div[contains(@class,'tree-plus')]";

    }

    public static String AllVisibleMainTabCloseButtons()
    {
        return "//ul[@id='ulTabbedList']//span";
    }

    public static String SpecificVisibleMainTabCloseButton(String tabNumber)
    {
        return "//ul[@id='ulTabbedList']//li[" + tabNumber + "]/span";
    }

    public static String EditObjectBottomGridSaveEditRow(String text)
    {
        return "//div[contains(@id, 'saveCancel')]/../../..//td[not(contains(@style, 'display: none'))][" + text + "]";
    }

    public static String OpenSpecificRouter(String text)
    {
        return "//span[contains(text(), '" + text + "')]";
    }

    public static String ContactContextHostGroupCheckbox(String text)
    {
        return "//table[@id='tablelcHostGroupTree']//span[contains(text(), '" + text + "')]//..//span[contains(@class, 'checkbox')]";
    }

    public static String RightClickContextMenuItems(String text)
    {
        return "//div[@id='jqContextMenu']//span[contains(text(), '" + text + "')]";
    }

    public static String RouterAttributeObject()
    {
        return "//div[contains(text(), 'Host - GNS-Router-CPT1')]";
    }

    public static String ShowRunningConfigOption()
    {
        return "//a[contains(text(), 'Show Running Config')]";
    }

    public static String CopyToClipBoard()
    {
        return "//span[contains(text(), 'Copy to Clipboard')]";
    }

    public static String CloseCopyToClipboard()
    {
        return "//img[@id = \"closeCurrentValue\"]";
    }

    public static String ContactCheckBox(String text)
    {
        return "//span[text()='" + text + "']//..//span[contains(@class, 'checkbox')]";
    }

    public static String HostGroupCheckBoxChecked(String text)
    {
        return "//span[contains(text(), '" + text + "')]//..//div[contains(@class, 'checked')]";
    }

    public static String HostCheckBox(String text)
    {
        return "//span[contains(text(), '" + text + "')]//..//span[contains(@class, 'jqx-tree-grid-checkbox')]";
    }

    public static String UserRequestScreenItems(String text)
    {
        return "//div[@id='_grid_normalGrid']//span[contains(text(), '" + text + "')]";
    }

    public static String UserRequestScreenItemCompleted(String text)
    {
        return "//div[@id='_grid_normalGrid']//span[contains(text(), '" + text + "')]//..//..//span[text()='Completed']";
    }

    public static String UserRequestsIconSrc()
    {
        return "resources/images/requestScreen.png";
    }

    public static String smallLoaderIcon()
    {
        return "//div[contains(@class, 'sintrex-loader')]";
    }

    public static String smallLoaderIconBottomWindow()
    {
        return "//div[contains(@class, 'sintrex-loader')]//div//div";
    }

    public static String ConfigConsoleHostsDownScrollArrow()
    {
        return "//div[@id='jqxScrollBtnDownverticalScrollBartreeGrid']//div";
    }

    public static String ConfigConsoleHostsDownScrollArrowNew(String IdText)
    {
        return "//div[@id='" + IdText + "']//div";
    }

    public static String ConfigConsoleHostsUpScrollArrow()
    {
        return "//div[@id='jqxScrollBtnUpverticalScrollBartreeGrid']//div";
    }

    public static String ConfigConsoleConfigObjectsScrollDownArrow()
    {
        return "//div[@id='jqxScrollWrapverticalScrollBarobjectContainer_grid']//div[@id='jqxScrollBtnDownverticalScrollBarobjectContainer_grid']//div";
    }

    public static String ConfigConsoleAttributesScrollDownButton()
    {
        return "//div[@id='wrapperdvObjects']//div[@id='jqxScrollBtnDownverticalScrollBardvObjects']//div";
    }

    public static String ConfigConsoleCollapseArrow()
    {
        return "//span[contains(@class, 'collapse')]";
    }

    public static String treeGridtXpath()
    {
        return "//table[@id='tabletreeGrid']//tbody//tr[not(contains(@data-key,'unlinkeddevices'))]//span[contains(@class,'-wrap')]";
    }

    public static String ConfigConsoleExpandArrow()
    {
        return "//span[contains(@class, 'expand')]";
    }

    public static String DivSelectedRowXpath(String text)
    {
        return "//span[contains(text(),'" + text + "')]/../..";
    }

    public static String objectWithIdXpath(String text)
    {
        return "//object[@id='" + text + "']";
    }

    public static String objectContainsIdXpath(String text)
    {
        return "//object[contains(@id,'" + text + "')]";
    }

    public static String AllHostsCheckBox()
    {
        return "//font[contains(text(), 'All Hosts')]//..//..//../td[2]/img";
    }

    public static String EditHostMainTabs(String text)
    {
        return "//tr[@id='tabmainHost']//a[text()= '" + text + "']";
    }

    //-------------------Cross-Page Page Objects-------------------//
    //-------------------Alarm Viewer-------------------//
    public static String AlarmViewerButtonXpath()
    {
        return "//img[@id='btnAlarmViewer']";
    }

    public static String CloseButtonXpath(String text)
    {
        return "//td[4][@id='" + text + "']";
    }

    public static String AlarmViewerFirstDataRowXpath()
    {
        return "//div[@id='_grid_grid_RowId_0']//div[1]";
    }

    public static String AlarmViewerFirstDataRowAlertAcknowledgedXpath()
    {
        return "//div[@id='_grid_grid_Body']//div//div[2]//img[contains(@src, 'checkboxChk')]";
    }

    public static String AlarmViewerToolItems(String itemType, String toolId)
    {
        return "//b[contains(text(), 'Last Updated')]//..//..//..//..//..//..//..//" + itemType + "[@id='" + toolId + "']";
    }
    //-------------------Alarm Viewer-------------------//

    //-------------------Config Console-------------------//
    
  
    
     public static String ExpandAll()
    {
        return "//a[@href='javascript:expandAll()']";
    }

 
     public static String LeftMenuPane_Router_Xpath()
    {
        return "//span[contains(text(), 'Routers')]";
    }

    public static String EntireColoumnSpanXpath(String textClass)
    {
        return "//div[contains(@class,'" + textClass + "')]//span";
    }

    public static String ContextDropDownValidationXpath()
    {
        return "//div[@id='dropdownlistContentcboContext']";
    }

    public static String HostIPAddressTextBoxXpath()
    {
        return "//input[@id='txtUid']";
    }

    public static String GetButtonXpath()
    {
        return "//input[@value='Get']";
    }

    public static String DisabledWindowXpath()
    {
        return "//form//div[@class='disableUI']";
    }

    public static String ContextMenuItemsExpandXpath(String text)
    {
        return "//span[text()='" + text + "']/..//span[1]";
    }
    
    public static String ContextMenuItemsExpandXpathUsingSpanNumber(String text, Integer spanNumber)
    {
        return "//span[text()='" + text + "']/..//span["+spanNumber+"]";
    }

    public static String LinkWindowExpandXpath(String text)
    {
        return "//span[text()='" + text + "']//..//span[contains(@class,'button')]";
    }

    public static String ContextMenuItemsExpandIndexedXpath(String text)
    {
        return "//span[text()='" + text + "']/..//span[2]";
    }

    public static String HostTypeDropDownXpath()
    {
        return "//select[@id='cboCiClassKey']";
    }

    public static String HostTestResultsTableXpath()
    {
        return "//div[@id='_grid_gridResults_RowId_0']";
    }

    public static String HostTestResultsTableRowsXpath()
    {
        return "//div[@id='_grid_gridResults_RowId_0']//div//span";
    }

    public static String InterrogationTableGridXpath()
    {
        return "//div[@id='_grid_normalGrid_RowId_0']";
    }

    public static String GridColumnXpath(String id, String text)
    {
        return "//div[@id='" + id + "']//div[contains(@id,'_CellId_" + text + "')]//span";
    }

    public static String sortingColumnHeader(String text)
    {
        return "//div[contains(@id,'ColId_" + text + "_Sort')]";
    }

    public static String GridDownloadFileNameXpath(String text)
    {
        return "//span[text()='" + text + "']/../..//div[5]//span";
    }

    public static String InterrogationStatusXpath()
    {
        return "//div[@id='_grid_normalGrid_RowId_0']//div[3]//span";
    }

    public static String InterrogationTableRowsXpath()
    {
        return "//div[@id='_grid_normalGrid_RowId_0']//div//span";
    }

    public static String InterrogationStatusForHost(String Hostname, String text)
    {
        return "//div[text()='" + Hostname + "']/../..//div[text()='" + text + "']";
    }

    public static String MenuItemSubItemXpath(String menuItem, String text)
    {
        return "//span[text()='" + menuItem + "']/../../..//span[contains(text(),'" + text + "')]";
    }

    public static String DeviceInfoTableDataXpath()
    {
        return "//div[text()='Device Info']/../..//table//tr//td";
    }

    public static String HostConfigDetailsTableXpath()
    {
        return "//table[@id='tabledvObjects']//tr//td[6]//span[2]//div//span";
    }

    public static String frameXp(String text)
    {
        return "//iframe[contains(@name, '" + text + "')]";
    }

    public static String HostConfigDetailsTableNameRowXpath()
    {
        return "//table[@id='tabledvObjects']//tr//td[6]//span//div//span";
    }

    public static String SpanTextMenuItemExpandXpath(String text)
    {
        return "//span[text()='" + text + "']/..//span[contains(@class, 'jqx-icon-arrow-right')]";
    }

    public static String UnlinkedDevicesHostName(String text)
    {
        return "//table[@id='tabletreeGrid']//span[contains(text(), '" + text + "')]";
    }

    public static String ResolveDuplicatesModalDialogValidationXpath()
    {
        return "//div[@id='headerHeading'][contains(text(), 'Resolve Duplicates')]";
    }

    public static String ResolveDuplicateModalDialogCloseButtonXpath()
    {
        return "//table[@name='resolveDuplicateHosts_tblSubmitBtns']//td[text()='Close']";
    }

    public static String DiscoveryLogFilterButtonXpath(String srcName, String frameName)
    {

        return "//img[contains(@src,'" + srcName + "')]//..//img[contains(@name,'" + frameName + "')]";
    }

    public static String trWithIdXpath(String text)
    {
        return "//tr[@id='" + text + "']";
    }

    public static String LiWithItemLabel(String text)
    {
        return "//li[@item-label='" + text + "']";
    }

    public static String BaselineStatusNoXpath()
    {
        return "//div[contains(@class, 'jqx-grid-cell jqx-item column')]/div";
    }

    public static String LiContainsItemLabel(String text)
    {
        return "//li[@item-label='" + text + "']";
    }

    public static String SNMPTopRow(String text)
    {
        return "//div[@id='row0rdGrid_grid']//div[text()='" + text + "']";
    }

    public static String WorklistTopRow(String host, String text)
    {
        return "//div[@id='_grid_normalGrid_RowId_0']//span[text()='" + host + "']//..//..//span[contains(text(),'" + text + "')]";
    }

    public static String TdContainsClass(String text)
    {
        return "//td[contains(@class,'" + text + "')]";
    }

    public static String tabContainsText(String text)
    {
        return "//li[@id='liTabConsole']//a[contains(text(), '" + text + "')]";
    }

    public static String InputWithIdWithImgContainsSrc(String textId, String textSrc)
    {
        return "//input[@id='" + textId + "']//..//img[contains(@src,'" + textSrc + "')]";
    }

    public static String UserRequestSubmenuSettingsButtonSrc()
    {
        return "../../../../operationalManagement/resources/images/dashboard_settings.png";
    }

    public static String UserRequestSubmenuSettingsCloseButtonSrc()
    {
        return "./../../../resources/images/closeDash.png";
    }

    public static String ScrollComplete()
    {
        return "//div[@id='jqxScrollAreaDownverticalScrollBartreeGrid'][contains(@style, 'height: 0px')]";
    }

    public static String ExpandAnyContainerVisible(String text)
    {
        return "//span[text()='" + text + "']//..//span[contains(@class,'button')]";
    }

    public static String SubnetExpanded()
    {
        return "//span[@class='jqx-tree-grid-indent']/..";
    }

    //------------------Selected Alarm Added---------------------//
    public static String DivIdAndTdText(String divId, String tdText)
    {
        return "//div[@id='" + divId + "']//td[contains(text(), '" + tdText + "')]";
    }

    public static String DivIdAndDivText(String divId, String divText)
    {
        return "//div[@id='" + divId + "']/div[contains(text(), '" + divText + "')]";
    }

    public static String DivNameAndTdText(String divName, String tdText)
    {
        return "//div[@name='" + divName + "']/div[contains(text(), '" + tdText + "')]";
    }

    //-------------------Config Console-------------------//
    public static String AllHostsWithInContainerXpath()
    {
        return "//span[contains(text(),'10.')]";
    }

    public static String MultiToolWindowTextCheckbox(String text)
    {
        return "//span[text()='" + text + "']//..//span[contains(@class,'checkbox')]";
    }

    //-------------------Purge Host-------------------//
    public static String HostLocationsValidationXpath(String text)
    {
        return "//td[@role='gridcell']//span[contains(text(), '" + text + "')]";
    }

    public static String HostLocationsRemovalValidationXpath(String text)
    {
        return "//span[contains(@class, 'tree')]//span[contains(text(), '" + text + "')]";
    }

    public static String DeletedHostsExpandMenuItemXpath()
    {
        return "//span[text()='Deleted Hosts'][contains(@class, 'grid-title')]/..//span[1]";
    }
    //-------------------Purge Host-------------------//

    //-------------------Backend Management-------------------//
    public static String NameSpaceSelectorTextXpath(String text)
    {
        return "//div[@class='listBox']//div[contains(text(), '" + text + "')]";
    }

    public static String NameSpaceSelectedValidationXpath(String text)
    {
        return "//td[text()='Namespace:']/..//td[contains(text(), '" + text + "')]";
    }

    public static String QueryTextAreaXpath()
    {
        return "//textarea";
    }
    //-------------------Backend Management-------------------//

    public static String MainSubnetsFilterTextField()
    {
        return "//option[contains(text(), 'Report Key')]//..//..//select//..//..//div[@id='divFilterText']//input";
    }

    public static String MainSubnetsFilter()
    {
        return "//option[contains(text(), 'Report Key')]//..//..//select";
    }

    public static String CSVButtonID()
    {
        return "btnExportGrid";
    }

    public static String ExpandSubnetXpath(String text)
    {
        return "//b[contains(text(), '" + text + "')]/../../..//div[contains(@class, 'tree-plus')]";
    }

    //-------------------Frames-------------------//
    public static String InnerTimeRegionIFrame()
    {
        return "iframetimeRegionMaintenance";
    }

    public static String BuildPageIFrame()
    {
        return "iframebuild";
    }

    public static String SystemHealthIFrame()
    {
        return "iframesystemHealth";
    }

    public static String iframeEscalationRulesMaintenance()
    {
        return "iframeescalationRulesMaintenance";
    }
    
    public static String iframecustomLocationSubnet(){
        return "iframecustomLocationSubnet";
    }

    public static String ManagerRedirectFrame()
    {
        return "iframehostManagerRedirect";
    }

    
    public static String ReportScheduleMaintenanceFrame()
    {
        return "iframereportScheduleMaintenance";
    }

    public static String InvestigateMaintenanceFram()
    {
        return "iframeinvestigateMaintenance";
    }
    public static String WinEventLogTemplateFrame()
    {
        return "iframewinEventlogTemplateMaintenance";
    }
    public static String WinAgentUpdatesFrame()
    {
        return "iframeviewWinLog";
    }

    public static String AlarmViewerFrame()
    {
        return "iframealarmViewer";
    }
    
    public static String iframealarmViewer2()
    {
        return "iframealarmViewer2";
    }
    
    public static String iframealarmViewer4()
    {
        return "iframealarmViewer4";
    }

    public static String AlarmViewerFrameNew()
    {
        return "iframealarmViewer";
    }

   
    public static String PerformanceGraphsFrame()
    {
        return "iframeviewPerformanceGraphs";
    }

    public static String InvestigationMaintenanceFrame()
    {
        return "iframeinvestigationMaintenance";
    }

    public static String WANDashboardFrame()
    {
        return "iframewanDashboard";
    }

    public static String ViewNetVizMapFrame()
    {
        return "iframeviewNetVizMaps";
    }

    public static String ifrNetVizFrame()
    {
        return "ifrNetViz";
    }

    public static String UserRequestsFrame()
    {
        return "iframeuserRequests";
    }

    public static String SystemHealthFrame()
    {
        return "iframesystemHealth";
    }

    public static String UserPreferencesFrame()
    {
        return "iframeuserPreferences";
    }

    public static String logMaintenanceFrame()
    {
        return "iframelogMaintenance";
    }

    public static String ConsoleMaintenanceFrame()
    {
        return "iframeconsoleMaintenance";
    }

    public static String UserMaintenaceFrame()
    {
        return "iframeuserMaintenance";
    }

    public static String operationalLogMaintenanceFrame()
    {
        return "iframeoperationalLogMaintenance";
    }
    
    public static String iframeoperationalLogMaintenance4()
    {
        return "iframeoperationalLogMaintenance4";
    }

    public static String serviceRollupMonitorFrame()
    {
        return "iframeserviceRollupMonitor";
    }
    
    public static String serviceRollupMonitorFrame2()
    {
        return "iframeserviceRollupMonitor2";
    }

    public static String IfToolBarFrame()
    {
        return "ifToolBar";
    }

    public static String ToolbarFrame()
    {
        return "toolbar";
    }

    public static String toolbarFrameFrame()
    {
        return "toolbarFrame";
    }

    //
    public static String mainFramesFrame()
    {
        return "mainFrames";
    }

    public static String Frame47Frame()
    {
        return "frame_47";
    }

    public static String GridFrameFrame()
    {
        return "gridFrame";
    }

    public static String ModalIframeId1Frame()
    {
        return "modalIframeId1";
    }

    public static String ModalIframeId2Frame()
    {
        return "modalIframeId2";
    }

    public static String ModalIframeIdFrame()
    {
        return "modalIframeId";
    }

    public static String ModalIframeId3Frame()
    {
        return "modalIframeId3";
    }

    public static String ModalIframeId4Frame()
    {
        return "modalIframeId4";
    }

    public static String PerformanceGraphGroupsFrame()
    {
        return "iframerrdGraphGroupMaintenance";
    }

    public static String ScreenFrame()
    {
        return "screen";
    }

    public static String SubnetsFrameXpath()
    {
        return "iframediscoveredSubnets";
    }

    public static String CompletedReports()
    {
        return "iframecompletedReportMaintenance";
    }

    public static String iframeReportsSchedule()
    {
        return "iframepublishLogMaintenance";
    }

    public static String PublishReportLogMaintenanceFrame()
    {
        return "iframepublishLogMaintenance";
    }

    public static String CompletedReportIframe()
    {
        return "iframecompletedReportMaintenance";
    }

    //new Frame
    public static String DefaultMaintenaceFrame()
    {
        return "iframedefaultMaintenance";
    }

    public static String ReportingCodeIframe()
    {
        return "iframereportingCodeMaintenance";
    }

    public static String DiscoveryLogFrame()
    {
        return "iframediscoveryLog";
    }

    public static String WorkListFrame()
    {
        return "iframesubnetWorklistMaintenance";
    }

    public static String SubnetScheduleFrame()
    {
        return "iframesubnetScheduleMaintenance2";
    }

    public static String DeviceRulesMaintenanceFrame()
    {
        return "iframedeviceRulesMaintenance";
    }

    public static String DeviceRulesIframeName()
    {
        return "iframedeviceRulesMaintenance";
    }

    public static String PrefixMaintenanceIframeName()
    {
        return "iframeprefixMaintenance";
    }

    public static String workSpaceFrameName()
    {
        return "workSpace";
    }

    public static String InterrogationWorklistFrame()
    {
        return "iframeworklistMaintenance";
    }

    public static String PerformanceRulesFrame()
    {
        return "iframeperformanceGlobalRuleMaintenance";
    }

    public static String RoleMaintenanceFrame()
    {
        return "iframeroleMaintenance";
    }

    public static String GlobalRuleMaintemanceFrame()
    {
        return "iframeglobalRuleMaintenance";
    }

    public static String maintenanceSlotsFrame()
    {
        return "iframemaintenanceSlotMaintenance";
    }
    public static String reportFilterFrame()
    {
        return "iframefilterMaintenance";
    }
    public static String categoryMaintenance()
    {
        return "iframecategoryMaintance";
    }

    public static String Layers()
    {
        return "iframelayerMaintenance";
    }

    public static String SelectedNotesFrame()
    {
        return "ifrSelectNotes";
    }

    public static String EscalationRolesFrame()
    {
        return "iframeescalationRoles";
    }

    public static String EscalationRulesFrame()
    {
        return "iframeescalationRulesMaintenance";
    }

    public static String EscalationExclutionListFrame()
    {
        return "iframeescalationExclusionList";
    }

    public static String iframeWinEventlogTemplateMaintenanceFrame()
    {
        return "iframewinEventlogTemplateMaintenance";
    }

    public static String EscalationProceduresFrame()
    {
        return "iframeescalationProceduresMaintenance";
    }

    public static String PrioritiesFrame()
    {
        return "iframehdPriorityMaintenance";
    }

    public static String QueuesFrame()
    {
        return "iframehdQueueMaintenance";
    }

    public static String KnownProblemsFrame()
    {
        return "iframehdKnownProblemMaintenance";
    }

    //-------------------Frames-------------------//
    public static String userRequestTableCellByClass(String text)
    {
        return "//div[@id='_grid_normalGrid_Body']/div/div[contains(@class, '" + text + "')]/span";
    }
     
    public static String AlarmIframeWithID(String text){
        
    return "//iframe[@id='"+ text +"']";
    }
    
    
    public static String userRequestTableCellByClassAndByText(String Class, String text)
    {
        return "//div[@id='_grid_normalGrid_Body']/div/div[contains(@class, '" + Class + "')]/span[contains(text(), '" + text + "')]";
    }

    public static String reportTableCellByClass(String text)
    {
        return "//div[@id='_grid_grid_Body']/div/div[contains(@class, '" + text + "')]/span";
    }

    public static String reportTableCellContainsTexAndClass(String Class, String text)
    {
        return "//div[@id='_grid_grid_Body']/div/div[contains(@class, '" + Class + "')]/span[contains(text(), '" + text + "')]";
    }

    public static String hostPropertyDescription()
    {
        return "//div[@id='_grid_detailGrid_Body']/div/div[contains(@class, 'grid_detailGrid_c1 ')]/span";
    }

    public static String generateButtonWithId(String text)
    {
        return "//td[contains(text(), 'Generate')][@id='" + text + "']";
    }

    public static String reportViewerTableCellByClass(String text)
    {
        return "//div[@id='_grid_gridReportViewer_Body']/div/div[contains(@class, '" + text + "')]/span[contains(text(), 'HostListReport')]";
    }

    public static String reportViewerTableCellByClassTwo(String text)
    {
        return "//div[@id='_grid_gridReportViewer_Body']/div/div[contains(@class, '" + text + "')]/span";
    }

    public static String reportDescription(String text)
    {
        return "//div[@id='_grid_normalGrid_Body']/div/div[contains(@class, 'grid_normalGrid_c1 ')]/span[contains(text(), '" + text + "')]";
    }

    public static String paginationButtonWithOnClick(String text)
    {
        return "//div[@id='pagingDiv']//td[@onclick='" + text + "']";
    }

    public static String PagingTdWithOnclick(String text)
    {
        return "//td[@onclick='" + text + "']";
    }

    public static String imgWithIDContainsName(String idText, String nameText)
    {
        return "//img[@id='" + idText + "'][contains(@name,'" + nameText + "')]";
    }

    public static String ImgContainsOnClickTextXpath(String text)
    {
        return "//img[contains(@onclick, '" + text + "')]";
    }

    public static String AlarmViewerLastUpdatedTime()
    {
        return "//font[@id='fntRefreshText']//b";
    }

    public static String FaultRulesFrame()
    {
        return "iframeglobalRuleMaintenance";
    }

    public static By InputWithIdXpath()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static String EscalationDestinationFrame()
    {
        return "iframeescalationDestinationsMaintenance";
    }

    public static String AddOption()
    {
        return "//table[@id = '_btnAdd']//td[contains(text(), 'Add')]";
    }

    public static String DeleteOption()
    {
        return "//table[@id = '_btnDelete']//td[contains(text(), 'Delete')]";
    }

    public static String ManagerRedirectFrame3()
    {
        return "//iframe[@name= 'iframereportMaintenance3']";
    }
    
    public static String rebuildAlarms()
    {
        return "//div[contains(@id,'_grid_grid_Body')]//div[contains(@id,'_grid_grid_RowId_0')]";
    }
    public static String alertOk()
    {
        return "//td[@id='alertOK']";
    }
    
    public static String reportManagerIframe()
    {
        return "iframereportManagerMaintenance3";
    }
    
    public static String menuItemFrame()
    {
        return "iframemenuItemsMaintenance";
    }    
    
    public static String menuItemFound()
    {
        return "//div[@id='_grid_grid_RowId_0_CellId_0']//span[contains(text(),'Config Console')]";
    }
    
    public static String checkActiveMenu()
    {
        return "//label[text()='Active']//input[@tabindex='7']";
    }
    
    public static String userMenuTab()
    {
        return "//a[text()='User Access']//..//a[text()='Users']";
    }
    
    public static String reportManagerDirectoryFrame()
    {
        return "iframereportManagerDirectory";
    }
    
    public static String adminReportMenu()
    {
        return "//a[contains(text(),'Administration')][not(contains(text(),'Logs'))]//..//ul//li//a[text()='Report']//span";
    }
    
    public static String addEditServiceFrame()
    {
        return "//iframe[@id='modalIframeId1'][contains(@src,'addEditService')]";
    }
    
    public static String incidentEskomExpand()
    {
        return "//font[contains(text(),'Eskom')]//..//..//..//td//img[@src='../resources/controls/treeview/images/treePlus.png']";
    }
    
    public static String defaultSNMPCommNum2()
    {
       return "//a[text()='SNMP Community']//..//..//td//input"; 
    }
    
    public static String defaultContainerAutoNumbering(String text)
    {
        return "//div[contains(@id,'"+text+"')]//span";
    }
    
    public static String tabName()
    {
        return "//li[contains(@id,'li')]//a";
    }
    
    public static String removeTabxpath(String tabName)
    {
        return "//li//a[text()='"+tabName+"']//..//span[text()='Remove Tab']";
    }
    
    public static String iframeimportMetadata()
    {
        return "iframeimportMetadata";
    }
    
    public static String iframeopenLayerMapView()
    {
        return "iframeopenLayerMapView";
    }
    
    public static String menuView_Dashboard()
    {
        return "//a[contains(text(),'View')]//..//a[text()='Dashboards']";
    }
    
    public static String mapViewRightClickXpath()
    {
        return "//*[@id=\"OpenLayers_Layer_Vector_71_svgRoot\"]";
    }
    
    public static String filterOptions(String text)
    {
        return "//div[contains(@id, '"+text+"')]//span[not(contains(@style,'visibility: hidden'))]";
    }
    
    public static String MapViewerFrame()
    {
        return "iframeopenLayerMapView";
    }
     public static String RolesMaintenanceFrame()
    {
        return "iframeroleMaintenance";
    }
    
    public static String AlarmBuilderMaintenance()
    {
        return "iframessAlarmBuilderMaintenance";
    }
    
    public static String imgConainsIDxpath(String text)
    {
        return "//img[contains(@id,'"+text+"')]";
    }
    
    public static String loaderDisplay(String text)
    {
        return "//div[@id='aeContent'][contains(@style,'"+text+"')]";
    }
    
    public static String workListValidation(String text)
    {
        return "//td[@id='"+text+"']//a[@class='progress-button orange']";
    }
    
    
    public static String workListValidation2(String text)
    {
        return "//td[@id='"+text+"']//a[@class='progress-button blue']";
    }
    
    public static String AccountingWorkstationsFrame() 
    {
        return "iframemacAddressMaintenance";
    }    
    
    public static String spanWithClassFont(String text)
    {
        return "//span[@class='"+text+"']//font";
    }
    
    public static String DivWithTitle(String text)
    {
        return "//div[@title='"+text+"']";
    }
        
    public static String LiWithTitle (String text)
    {
        return "//li[@title='"+text+"']";
    }
    
    public static String aWithSpanClass (String text)
    {
        return "//a//span[@class='"+text+"']";
    }
    
    public static String SpanUpOneSpanContainsClass(String text1, String text2)
    {
        return "//span[text()='"+text1+"']//..//span[contains(@class,'"+text2+"')]";
    }
    
    public static String TdContainsStyle(String text1)
    {
        return "//td[contains(@style,'"+text1+"')]";
    }    
            
    public static String ConsoleContainerPlusIcon(String ContainerName) 
    {
        return "//font[text()='"+ ContainerName +"']/../../..//img[contains(@src,'treePlus')]"; 
    }
    public static String ConsoleContainsTextContainerPlusIcon(String ContainerName) 
    {
        return "//font[contains(text(),'"+ ContainerName +"')]/../../..//img[contains(@src,'treePlus')]";             
    }
    public static String ConsoleContainerMinusIcon(String ContainerName) 
    {
        return "//font[text()='"+ ContainerName +"']/../../..//img[contains(@src,'treeMinus')]"; 
    }
     public static String ConsoleContainerCheckbox(String ContainerName) 
    {
        return "//font[text()='"+ ContainerName +"']/../../..//img[contains(@src,'Check')]"; 
    }
     
     public static String hostTreeDrilldown(String text)
    {
        return "//font[text()='"+text+"']/../..//..//td//img[@src='../resources/controls/treeview/images/treePlus.png']";
    }
            
    public static String DivContainsIDDivContainsIDSpan(String text1, String text2)
    {
        return "//div[contains(@id,'"+text1+"')]//div[contains(@id,'"+text2+"')]//span";
    }
  
    public static String CustomContextAlarmcolumnTextXpath(String text,String number)
    {
       return "//div[text()='"+text+"']//..//..//..//div[@id='_grid_grid_Body']//div//div[contains(@class,'grid_grid_c"+number+" gridCell')]//span";
    }
    public static String AlarmViewerSettingsCheckBox(String text)
    {
        return "//td[text()='"+text+"']//..//td//input";
    }
    
    public static String imgWithTitleXpath(String text)
    {
        return "//img[@title='"+text+"']";
    }
    
    public static String selectContainerHost(String text)
    {
        return "//font[text()='Linux']//..//..//..//..//tr//td//table//tbody//tr//td//table//tbody//tr//td//span//font[text()='"+text+"']";
    }
}
