/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import static KeywordDrivenTestFramework.Core.BaseClass.currentEnvironment;
/**
 *
 * @author szeuch
 */
public class NavigateToPageObject 
{
    //-------------------URL's-------------------//
    public static String SintelligentURL() 
    {
        return currentEnvironment.PageUrl;
    }
    
    public static String BackendManagementPortalURL()
    {
        return "http://10.0.4.195/csp/sys/UtilHome.csp";
    }
    //-------------------URL's-------------------//
    
    //-------------------Generic Page Objects-------------------//
    public static String SecondLayerMenuItemContainsText(String firstLayerMenuItem, String secondLayerMenuItem)
    {
        return "//div[@id= 'divMenuContainer']//a[contains(text(), '"+firstLayerMenuItem+"')]//..//ul//a[contains(text(), '"+secondLayerMenuItem+"')]";
    }
    
    public static String thirdLayerMenuItemContainsText(String firstLayerMenuItem, String secondLayerMenuItem, String thirdLayerMenuItem)
    {
        return "//div[@id= 'divMenuContainer']//a[contains(text(), '"+firstLayerMenuItem+"')]//..//ul//a[contains(text(), '"+secondLayerMenuItem+"')]//..//li//a[contains(text(), '"+thirdLayerMenuItem+"')]";
    }
    
    public static String fourthLayerMenuItemContainsText(String firstLayerMenuItem, String secondLayerMenuItem, String thirdLayerMenuItem, String fourthLayerMenuItem)
    {
        return "//div[@id= 'divMenuContainer']//a[contains(text(), '"+firstLayerMenuItem+"')]//..//ul//a[contains(text(), '"+secondLayerMenuItem+"')]//..//li//a[contains(text(), '"+thirdLayerMenuItem+"')]//..//ul//li//a[contains(text(), '"+fourthLayerMenuItem+"')]";
    }
    
    public static String UserAccessRolesLink()
    {
        return "//a[contains(text(), 'User Access')]//..//a[text()='Roles']";
    }
    
    public static String AdministratiobLink()
    {
        return "//a[contains(text(), 'User Access')]//..//..//..//a[text()='Administration']";
    }

}