package KeywordDrivenTestFramework.Testing.TestClasses.OperationalManagement.View.Alarms.AlarmViewer;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.SintelligentPageObject;

/**
 *
 * @author Willem
 */
public class Acknowledge_All_Alarms extends BaseClass
{
    Narrator narrator;
    private String error;

    public Acknowledge_All_Alarms(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }

    public TestResult executeTest()
    {
        if (!Alarm_Scheduler()) 
        {
            error = "Failed to start Alarm Scheduler";            
        }     
        return narrator.finalizeTest("Successfully closed alarms", false, testData);        
    }
    
    public boolean Alarm_Scheduler()
    {   
        //Navigate to Alarm Viewer        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to the default context";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnAlarmViewer")))
        {
            error = "Failed to wait for Alarm Viewer button ";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnAlarmViewer")))
        {
            error = "Failed to wait for Alarm Viewer button visibility";
            return false;              
        }        
       
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnAlarmViewer"))) 
        {
            error = "Failed to click on Alarm Viewer button ";
            return false;
        } 
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            error = "Failed to wait for Refresh button ";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            error = "Failed to wait for Refresh button visibility";
            return false;              
        }        
        
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab"))) 
        {
            error = "Failed to click on Refresh button ";
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to the default context";
            return false;
        }

        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrameNew(), "workSpace"))
        {
            error = "Could not switch to the '" + SintelligentPageObject.AlarmViewerFrameNew() + "'";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByPartialId("gridFrame"))
        {
            error = "Failed to switch to Iframe with Id gridFrame";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.DivContainsId("_grid_grid_RowId_0")))
        {
            error = "Failed to wait for Alarm";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivContainsId("_grid_grid_RowId_0")))
        {
            error = "Failed to wait for Alarm visibility";
            return false;              
        }          
        
        if (!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivContainsId("_grid_grid_RowId_0"))) 
        {
            error = "Failed to right click on Alarm";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.DivContainsId("acAcknowledgeAll")))
        {
            error = "Failed to wait for Acknowledge All button";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivContainsId("acAcknowledgeAll")))
        {
            error = "Failed to wait for Acknowledge All visibility";
            return false;              
        }         

        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.DivContainsId("acAcknowledgeAll"))) 
        {
            error = "Failed to click on Acknowledge All button ";
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ModalIframeId1Frame()))
        {
            error = "Failed to switch to screen frame";
            return false;
        }              

        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtNoteText")))
        {
            error = "Failed to wait for Note field";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputContainsIdXpath("txtNoteText")))
        {
            error = "Failed to wait for Note field visibility";
            return false;              
        }         
        
        if (!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputContainsIdXpath("txtNoteText"), "Acknowledge all Alarms")) 
        {
            error = "Failed to enter 'Acknowledge all Alarms' in Note field";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.TdWithTextXpath("Apply")))
        {
            error = "Failed to wait for Apply button";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithTextXpath("Apply")))
        {
            error = "Failed to wait for Apply button visibility";
            return false;              
        }         
        
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithTextXpath("Apply")))
        {
            error = "Failed to click on Apply button";
            return false;
        }         
        
        return true;
    }
    
}