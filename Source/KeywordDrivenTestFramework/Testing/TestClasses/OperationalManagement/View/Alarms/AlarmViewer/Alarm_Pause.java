package KeywordDrivenTestFramework.Testing.TestClasses.OperationalManagement.View.Alarms.AlarmViewer;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;

/**
 *
 * @author Willem
 */
public class Alarm_Pause extends BaseClass
{
    Narrator narrator;
    private String error;

    public Alarm_Pause(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }

    public TestResult executeTest()
    {
        if (!Alarm_Scheduler()) 
        {
            error = "Failed to start Alarm Scheduler";            
        }     
        return narrator.finalizeTest("Successfully closed alarms", false, testData);        
    }
    
    public boolean Alarm_Scheduler()
    {   
        int Pause;
        Pause= (Integer.parseInt(testData.getData("Pause"))*60);
        SeleniumDriverInstance.pause(Pause*1000);    
            
        return true;
    }
    
}
