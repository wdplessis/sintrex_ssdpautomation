package KeywordDrivenTestFramework.Testing.TestClasses.OperationalManagement.View.Alarms.AlarmViewer;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.ConfigConsole.SetOverrideRulePageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.Configure.RulesAndTemplates.FaultRulesPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.View.Alarms.AlarmViewer_ContextMenuAndCustomPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.SintelligentPageObject;
import static KeywordDrivenTestFramework.Testing.TestMarshall.down;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Willem
 */
public class Create_Alarm_Down extends BaseClass
{
    Narrator narrator;
    private String error; 

    public boolean CreateAlarm(String Host, String Impact, String AlarmMessage)
    {
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to default content";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.LiWithIdXpath("liTab2924")))
        {
            error = "Failed to wait for 'Console Config' tab";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiWithIdXpath("liTab2924")))
        {
            error = "Failed to wait for 'Console Config' tab visibility";
            return false;              
        }         
              
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.LiWithIdXpath("liTab2924")))
        {
            error = "Failed to click on 'Console Config' tab";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            error = "Failed to wait for Refresh button";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            error = "Failed to wait for Refresh button visibility";
            return false;              
        }         
        
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab"))) 
        {
            error = "Failed to click on Refresh button";
            return false;
        }         

        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame(), SintelligentPageObject.workSpaceFrameName()))
        {
            error = "Failed to switch to manager frame";
            return false;
        }

        //Navigate to Ping Hosts
        if (!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
        {
            error = "Failed to Switch to Host Groups context";
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.OpenHostGroupDropdown("Servers")))
        {
            error = "Failed to wait for 'Servers' container";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OpenHostGroupDropdown("Servers")))
        {
            error = "Failed to wait for 'Servers' container visibility";
            return false;              
        }         
        
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.OpenHostGroupDropdown("Servers")))
        {
            error = "Failed to expand the 'Servers' container";
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.OpenHostGroupDropdown("Ping Hosts")))
        {
            error = "Failed to wait for 'Ping Host' container button";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OpenHostGroupDropdown("Ping Hosts")))
        {
            error = "Failed to wait for 'Ping Host' container visibility";
            return false;              
        }         
        
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.OpenHostGroupDropdown("Ping Hosts")))
        {
            error = "Failed to expand the 'Ping Host' container";
            return false;
        }

        //Create Down Alarms 
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.SpanContainsTextXpath(Host)))
        {
            error = "Failed to wait for '" + (Host) + "'";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath(Host)))
        {
            error = "Failed to wait for '" + (Host) + "' visibility";
            return false;              
        }         
        
        if (!SeleniumDriverInstance.stableDoubleClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath(Host)))
        {
            error = "Failed to double-click on host '" + (Host) + "'";
            return false;
        }           
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.DivWithTextXpath("Config")))
        {
            error = "Failed to wait for 'Config' tab";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivWithTextXpath("Config")))
        {
            error = "Failed to wait for 'Config' tab visibility";
            return false;              
        }             

        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.DivWithTextXpath("Config")))
        {
            error = "Failed to click on 'Config' tab";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.SpanContainsTextXpath("IP Interface (1)")))
        {
            error = "Failed to wait for 'IP Interface (1)'";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("IP Interface (1)")))
        {
            error = "Failed to wait for 'IP Interface (1)' visibility";
            return false;              
        }             

        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("IP Interface (1)")))
        {
            error = "Failed to click on 'IP Interface (1)'";
            return false;
        }         
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.trWithIdXpath("row0dvObjectsb")))
        {
            error = "Failed to wait for IP Address - " + (Host) + "";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.trWithIdXpath("row0dvObjectsb")))
        {
            error = "Failed to wait for IP Address - " + (Host) + " visibility";
            return false;              
        }              

        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.trWithIdXpath("row0dvObjectsb")))
        {
            error = "Failed to click on IP Address - " + (Host) + "";
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.AContainsTextXpath("Edit Object")))
        {
            error = "Failed to wait for 'Edit Object'";
            return false;              
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Edit Object")))
        {
            error = "Failed to wait for 'Edit Object' visibility";
            return false;              
        }           
        
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Edit Object")))
        {
            error = "Failed to click on 'Edit Object'";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            error = "Failed to switch to screen frame";
            return false;
        }       

        if (!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.DivWithId("containingDiv")+ SintelligentPageObject.SelectWithIdXpath("cboImpact") , (Impact))) 
        {
            error = "Failed to select :" + (Impact) + " from impact dropdown";
            return false;            
        }

        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Fault Rules")))
        {
            error = "Failed to click on 'Fault Rules'";
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkDisableDown"), false))
        {
            error = "Failed to check if checkbox 'Disable Down Monitoring' is unclicked";
            return false;
        }

        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithTextXpath("Apply")))
        {
            error = "Failed to click on 'Apply' button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to default content";
            return false;
        }

        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame(), SintelligentPageObject.workSpaceFrameName()))
        {
            error = "Failed to switch to manager frame";
            return false;
        }                 
        
        //Create Baseline
        if (!createBaseline()) 
        {
            return false;
        }
        
        //Validate Baseline
        if (!validateBaseline()) 
        {
            return false;
        }
        
        //Navigate to Alarm Viewer        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to the default context";
            return false;
        }
       
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnAlarmViewer"))) 
        {
            error = "Failed to click on Alarm Viewer button ";
            return false;
        }        
        
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab"))) 
        {
            error = "Failed to click on Alarm Viewer button ";
            return false;
        }                 
       
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrameNew(), "workSpace"))
        {
            error = "Could not switch to the '" + SintelligentPageObject.AlarmViewerFrameNew() + "'";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.IframeContainsNameXpath("gridFrame")))
        {
            error = "Failed to wait for iframe 'gridFrame'";
            return false;
        }        

        //Navigate to created Down Alarms        
        if (!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboContext"), "Host Groups"))
        {
            error = "Failed to select 'Host Groups' from the context dropdown menu on the Alarm Viewer page";
            return false;
        }      
        
        if (!SeleniumDriverInstance.stableClickElementByXpath(AlarmViewer_ContextMenuAndCustomPageObject.ExpandHostGroupXpath("Servers"))) 
        {           
            error = "Failed to expand host group: "+ ("Servers");
            return false;
        }
        
        if (!waitHostGroup()) 
        {
            return false;
        }
        
        if (!SeleniumDriverInstance.stableClickElementByXpath(AlarmViewer_ContextMenuAndCustomPageObject.ExpandHostGroupXpath("Ping Hosts"))) 
        {
            error = "Failed to expand host group: "+ ("Ping Hosts");
            return false;
        }

        if (!waitHost(Host)) 
        {
            return false;
        }
             
        if (!SeleniumDriverInstance.stableDoubleClickElementByXpath(SintelligentPageObject.FontContainsTextXpath(Host)))
        {
            error = "Failed to double click on container '" + (Host) + "' on the 'Host Groups' context ";
            return false;
        }            

        if (!SeleniumDriverInstance.switchToFrameByPartialId("ifToolBar"))
        {
            error = "Failed to switch to iframe ifToolBar";
            return false;
        }                        

        if (!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown"), "Impact"))
        {
            error = "Failed to select Impact on the Filter dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboFilterValue"), (Impact)))
        {
            error = "Failed to select "+ (Impact) +" - on the filter value drop down";
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to the default context";
            return false;
        }

        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrameNew(), "workSpace"))
        {
            error = "Could not switch to the '" + SintelligentPageObject.AlarmViewerFrameNew() + "'";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.IframeContainsNameXpath("gridFrame")))
        {
            error = "Failed to wait for iframe 'gridFrame'";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByPartialId("gridFrame"))
        {
            error = "Failed to switch to Iframe with Id gridFrame";
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!searchAlarm(AlarmMessage, Host, Impact)) 
        {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to the default context";
            return false;
        }

        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrameNew(), "workSpace"))
        {
            error = "Could not switch to the '" + SintelligentPageObject.AlarmViewerFrameNew() + "'";
            return false;
        }            

        if (!SeleniumDriverInstance.stableClickElementByXpath(AlarmViewer_ContextMenuAndCustomPageObject.ExpandHostGroupXpath("Routers"))) 
        {
            error = "Failed to expand host group: "+ ("Routers");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AlarmViewer_ContextMenuAndCustomPageObject.ExpandHostGroupXpath("Switches"), 5)) 
        {     
            return true;
        }  
        else
        {    
            if (!SeleniumDriverInstance.stableClickElementByXpath(AlarmViewer_ContextMenuAndCustomPageObject.ExpandHostGroupXpath("Switches"))) 
            {
                error = "Failed to expand host group: "+ ("Switches");
                return false;
            }    
        }    
            
        return true; 
    }

// <editor-fold desc="Close Alarm">    
public boolean CloseAlarm(String impact)
    {                                  
        //Retieves Host Name and Alarm Message according to Index
        int index=0;

            for (int j = 0; j < down.length; j++) 
            {
                if (impact.equals(down[1][j])) 
                {
                    index= j;
                    down[1][j]="Dummy";
                    break;
                }
            }
   
            String host = down[0][index];
            String message = down[2][index];

        //
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to the default context";
            return false;
        }       

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrameNew(), "workSpace"))
        {
            error = "Could not switch to the '" + SintelligentPageObject.AlarmViewerFrameNew() + "'";
            return false;
        }

        //Navigate to created Down Alarms
        if (!SeleniumDriverInstance.stableDoubleClickElementByXpath(SintelligentPageObject.FontContainsTextXpath(host)))
        {
            error = "Failed to double-click on host '" + (host) + "'";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByPartialId("ifToolBar"))
        {
            error = "Failed to switch to iframe ifToolBar";
            return false;
        }        
        
        if (!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithNameXpath("_cboFilterDropdown"), "Impact"))
        {
            error = "Failed to select 'Impact' on the filter value drop down";
            return false;
        }
         
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboFilterValue"), (impact)))
        {
            error = "Failed to select "+ (impact) +" - on the filter value drop down";
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to the default context";
            return false;
        }

        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrameNew(), "workSpace"))
        {
            error = "Could not switch to the '" + SintelligentPageObject.AlarmViewerFrameNew() + "'";
            return false;
        }            

        if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.IframeContainsNameXpath("gridFrame")))
        {
            error = "Failed to wait for iframe 'gridFrame'";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByPartialId("gridFrame"))
        {
            error = "Failed to switch to Iframe with Id gridFrame";
            return false;
        }                

        if (!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithIdDivXpath("_grid_grid_RowId_0")))
        {
            error = "Could not switch to the ";
            return false;
        }                      

        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.tablewithIdXpath("_acSetOverrideRule")))
        {
            error = "Failed to expand host group ";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ModalIframeId1Frame()))
        {
            error = "Failed to switch to screen frame";
            return false;
        }            

        if (!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkDisableDown"), true))
        {
            error = "Failed to check if checkbox 'Disable Down Monitoring' is unclicked";
            return false;
        }                       

        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithTextXpath("Apply")))
        {
            error = "Failed to click on 'Apply' button";
            return false;
        }        
       
        return true;       
    }
// </editor-fold>

// <editor-fold desc="Create Baseline">
 public boolean createBaseline()
    {
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to sitch to default content - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame(), SintelligentPageObject.workSpaceFrameName()))
        {
            error = "Failed to switch to inner manager frame - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiWithItemLabel("Tasks")))
        {
            error = "Failed to wait for 'Tasks' drop down to be visible - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiWithItemLabel("Tasks")))
        {
            error = "Failed to wait for 'Tasks' drop down to be clickable - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiWithItemLabel("Tasks")))
        {
            error = "Failed to hover Over 'Tasks' drop down - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SetOverrideRulePageObject.baselineXpath()))
        {
            error = "Failed to wait for 'Create Baseline' option to be visible - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SetOverrideRulePageObject.baselineXpath()))
        {
            error = "Failed to wait for 'Create Baseline' option to be visible - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementByXpath_Actions(SintelligentPageObject.LiWithItemLabel("Create Baseline")))
        {
            error = "Failed to click on 'Create Baseline' - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            error = "Failed to switch to screen frame - Create baseline";
            return false;
        }
        
        if (!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkClearLog"), true))
        {
            error = "Failed to check 'Clear log' checkbox - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkSkipShutInterfaces"), true))
        {
            error = "Failed to check 'Skip Shut Interfaces' checkbox  - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkSkipShutSNMPInterfaces"), true))
        {
            error = "Failed to check 'Skip Shut SNMP Monitored Interfaces' checkbox  - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkMaintainVirtualInterfaces"), true))
        {
            error = "Failed to check 'Maintain Virtual Interfaces' checkbox - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementByXpath_Actions(SintelligentPageObject.TdWithTextXpath("Generate")))
        {
            error = "Failed to click on 'Generate' button - Create baseline";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementByXpath_Actions(SintelligentPageObject.TdWithTextXpath("OK")))
        {
            error = "Failed to click on 'Alert OK' button - Create baseline";
            return false;
        }

        return true;
    }
// </editor-fold>

// <editor-fold desc="Validate Baseline"> 
public boolean validateBaseline()
    {
        // Switch to the Global mantenance frame screen iframe
        int maxWaitingTime = 0;        
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame(), SintelligentPageObject.workSpaceFrameName()))
        {
            error = "Failed to switch to the maintenance container page";
            return false;
        }

        List<WebElement> status;

        // loop for max 3 min trying validate baseline
        while (maxWaitingTime != 180)
        {
            // counter to keep track of element completed in the baseline
            int doneCounter = 0;
            try
            {
                status = SeleniumDriverInstance.Driver.findElements(By.xpath(FaultRulesPageObject.baselineStatusXpath()));
            }
            catch (Exception e)
            {
                error = "Failed to retrieve baseline status' - Validate baseline";
                return false;
            }

            // Get text of all status
            for (int i = 0; i < status.size(); i++)
            {
                if (status.get(i).getText().equals("Completed"))
                {
                    doneCounter++;
                }
            }

            // if the correct number of element has completed then break the loop
            if (doneCounter == Integer.parseInt(SeleniumDriverInstance.retrieveTextByXpath(FaultRulesPageObject.BaselineNumberOfElement())))
            {
                break;
            }
            else // refresh page to wait again
            {
                SeleniumDriverInstance.pause(2000);
                
                SeleniumDriverInstance.pause(1000);

                if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.trWithIdXpath("BL")))
                {
                    error = "Failed to click on Baseline in the right bottom panel - Validate baseline";
                    return false;
                }
            }

            // Delay by one second before clicking again
            SeleniumDriverInstance.pause(1000);
            maxWaitingTime++;
        }

//        narrator.stepMessage("Successfully validated Baseline completion in 'Host Groups'");
        return true;
    }
// </editor-fold>

// <editor-fold desc="Wait HostGroup"> 
public boolean waitHostGroup()
    {
        int maxWaitingTime = 0;        
        while (maxWaitingTime != 30)
            if (!SeleniumDriverInstance.waitForElementByXpath(AlarmViewer_ContextMenuAndCustomPageObject.ExpandHostGroupXpath("Ping Hosts")))
            {
                if (!SeleniumDriverInstance.switchToDefaultContent())
                {
                    error = "Failed to switch to the default context";
                    return false;
                }                

                if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab"))) 
                {
                    error = "Failed to click on Alarm Viewer button ";
                    return false;
                }
                
                SeleniumDriverInstance.pause(5000);                
                
                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrameNew(), "workSpace"))
                {
                    error = "Could not switch to the '" + SintelligentPageObject.AlarmViewerFrameNew() + "'";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.IframeContainsNameXpath("gridFrame")))
                {
                    error = "Failed to wait for iframe 'gridFrame'";
                    return false;
                }        
       
                if (!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboContext"), "Host Groups"))
                {
                    error = "Failed to select 'Host Groups' from the context dropdown menu on the Alarm Viewer page";
                    return false;
                }
                
                if (!SeleniumDriverInstance.stableClickElementByXpath(AlarmViewer_ContextMenuAndCustomPageObject.ExpandHostGroupXpath("Servers"))) 
                {

                    error = "Failed to expand host group: "+ ("Servers");
                    return false;
                }
                
                maxWaitingTime++; 
            }
            else
            {
                break;  
            }       

        return true;
    }
// </editor-fold>

// <editor-fold desc="Wait Host"> 
public boolean waitHost(String Host)
    {
        int maxWaitingTime = 0;        
        while (maxWaitingTime != 25)
            if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.FontContainsTextXpath(Host)))                
            {
                if (!SeleniumDriverInstance.switchToDefaultContent())
                {
                    error = "Failed to switch to the default context";
                    return false;
                }                

                if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab"))) 
                {
                    error = "Failed to click on Alarm Viewer button ";
                    return false;
                }
                
                SeleniumDriverInstance.pause(5000);                
                
                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrameNew(), "workSpace"))
                {
                    error = "Could not switch to the '" + SintelligentPageObject.AlarmViewerFrameNew() + "'";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.IframeContainsNameXpath("gridFrame")))
                {
                    error = "Failed to wait for iframe 'gridFrame'";
                    return false;
                }        
       
                if (!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboContext"), "Host Groups"))
                {
                    error = "Failed to select 'Host Groups' from the context dropdown menu on the Alarm Viewer page";
                    return false;
                }
                
                if (!SeleniumDriverInstance.stableClickElementByXpath(AlarmViewer_ContextMenuAndCustomPageObject.ExpandHostGroupXpath("Servers"))) 
                {

                    error = "Failed to expand host group: "+ ("Servers");
                    return false;
                }
                
                if (!SeleniumDriverInstance.stableClickElementByXpath(AlarmViewer_ContextMenuAndCustomPageObject.ExpandHostGroupXpath("Ping Hosts"))) 
                {

                    error = "Failed to expand host group: "+ ("Ping Hosts");
                    return false;
                }
                
                maxWaitingTime++; 
            }
            else
            {
                break;  
            }       

        return true;
    }
// </editor-fold>

// <editor-fold desc="Search Alarm"> 
public boolean searchAlarm(String AlarmMessage, String Host, String Impact)
    {
        int maxWaitingTime = 0;        
        while (maxWaitingTime != 25)
            if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.SpanContainsTextXpath(AlarmMessage)))                
            {                
                if (!SeleniumDriverInstance.switchToDefaultContent())
                {
                    error = "Failed to switch to the default context";
                    return false;
                }                

                if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab"))) 
                {
                    error = "Failed to click on Alarm Viewer button ";
                    return false;
                }
                
                SeleniumDriverInstance.pause(15000);                
                
                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrameNew(), "workSpace"))
                {
                    error = "Could not switch to the '" + SintelligentPageObject.AlarmViewerFrameNew() + "'";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.IframeContainsNameXpath("gridFrame")))
                {
                    error = "Failed to wait for iframe 'gridFrame'";
                    return false;
                }        
       
                if (!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboContext"), "Host Groups"))
                {
                    error = "Failed to select 'Host Groups' from the context dropdown menu on the Alarm Viewer page";
                    return false;
                }
                
                if (!SeleniumDriverInstance.stableClickElementByXpath(AlarmViewer_ContextMenuAndCustomPageObject.ExpandHostGroupXpath("Servers"))) 
                {
                    error = "Failed to expand host group: "+ ("Servers");
                    return false;
                }
                
                if (!SeleniumDriverInstance.stableClickElementByXpath(AlarmViewer_ContextMenuAndCustomPageObject.ExpandHostGroupXpath("Ping Hosts"))) 
                {
                    error = "Failed to expand host group: "+ ("Ping Hosts");
                    return false;
                }                
                
                if (!SeleniumDriverInstance.stableDoubleClickElementByXpath(SintelligentPageObject.FontContainsTextXpath(Host)))
                {
                    error = "Failed to double click on container '" + (Host) + "' on the 'Host Groups' context ";
                    return false;
                }                       

                if (!SeleniumDriverInstance.switchToFrameByPartialId("ifToolBar"))
                {
                    error = "Failed to switch to iframe ifToolBar";
                    return false;
                }            

                if (!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown"), "Impact"))
                {
                    error = "Failed to select Impact on the Filter dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboFilterValue"), (Impact)))
                {
                    error = "Failed to select "+ (Impact) +" - on the filter value drop down";
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToDefaultContent())
                {
                    error = "Failed to switch to the default context";
                    return false;
                }

                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrameNew(), "workSpace"))
                {
                    error = "Could not switch to the '" + SintelligentPageObject.AlarmViewerFrameNew() + "'";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.IframeContainsNameXpath("gridFrame")))
                {
                    error = "Failed to wait for iframe 'gridFrame'";
                    return false;
                }

                if (!SeleniumDriverInstance.switchToFrameByPartialId("gridFrame"))
                {
                    error = "Failed to switch to Iframe with Id gridFrame";
                    return false;
                }                
                
                maxWaitingTime++; 
            }
            else
            {
                break;  
            }       

        return true;
    }
// </editor-fold>
    
}