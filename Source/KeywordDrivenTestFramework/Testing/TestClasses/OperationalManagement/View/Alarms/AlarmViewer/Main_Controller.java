package KeywordDrivenTestFramework.Testing.TestClasses.OperationalManagement.View.Alarms.AlarmViewer;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Willem
 */
public class Main_Controller extends BaseClass
{
    Narrator narrator;
    private String error;   
    int aCount = 1;
       
    public Main_Controller(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }

    public TestResult executeTest()
    {               
        String countAmount = testData.getData("AlarmAmount");
        int amountCountAlarms = Integer.parseInt(countAmount);    
        
        int count = 0;
        
        while (count == 0) 
        {                 
            String timeStamp = new SimpleDateFormat("HHmm").format(Calendar.getInstance().getTime());               
            
            if(timeStamp.equals(testData.getData("TimeStart")))
            {
                if (testData.getData("Method").equals("Create_Alarm_Down"))
                {                   
                    for (int j = 0; j < amountCountAlarms; j++) 
                    {
                        if (!CreateAlarmDown()) 
                        {
                            error = "Failed to start 'CreateAlarm'";            
                        }         
                        aCount ++;
                    }  
                    count = 1;
                }
                else if (testData.getData("Method").equals("Create_Alarm_Threshold"))
                {                   
                    for (int j = 0; j < amountCountAlarms; j++) 
                    {
                        if (!CreateAlarmThreshold()) 
                        {
                            error = "Failed to start 'CreateAlarm'";            
                        }
                        aCount ++;
                    }
                    count = 1;
                }
                else if (testData.getData("Method").equals("Create_Alarm_Up"))
                {
                    for (int j = 0; j < amountCountAlarms; j++) 
                    {                    
                        if (!CreateAlarmUp()) 
                        {
                            error = "Failed to start 'CreateAlarm'";            
                        }
                        aCount ++;
                    }    
                    count = 1;
                }
            }
        }
            
        return narrator.finalizeTest("Successfully created down alarm", false, testData);
    }
    
    public boolean CreateAlarmDown()
    {
        Create_Alarm_Down createDownAlarm = new Create_Alarm_Down();
        
        createDownAlarm.CreateAlarm(testData.getData("Host"+aCount), testData.getData("Impact"+aCount), testData.getData("AlarmMessage"+aCount));
        
//        createDownAlarm.CloseAlarm(error);
  
        return true;
    }
    
      
    public boolean CreateAlarmThreshold()
    {
        Create_Alarm_Threshold createThresholdAlarm = new Create_Alarm_Threshold();
        
        createThresholdAlarm.CreateAlarm(testData.getData("Host"+aCount), testData.getData("Impact"+aCount), testData.getData("AlarmMessage"+aCount));
        
//        createThresholdAlarm.CloseAlarm(error);
        
        return true;
    }

    public boolean CreateAlarmUp()
    {
        Create_Alarm_Up createUpAlarm = new Create_Alarm_Up();
        
        createUpAlarm.CreateAlarm(testData.getData("Host"+aCount), testData.getData("Impact"+aCount), testData.getData("AlarmMessage"+aCount));
        
//        createUpAlarm.CloseAlarm(error);
        
        return true;
    }           
    
}
