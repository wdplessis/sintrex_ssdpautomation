package KeywordDrivenTestFramework.Testing.TestClasses.OperationalManagement.View.Alarms.AlarmViewer;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;

/**
 *
 * @author Willem
 */
public class Alarm_Time extends BaseClass
{
    Narrator narrator;
    private String error;

    public Alarm_Time(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }

    public TestResult executeTest()
    {
        if (!Alarm_Scheduler()) 
        {
            error = "Failed to start Alarm Scheduler";            
        }     
        return narrator.finalizeTest("Successfully closed alarms", false, testData);        
    }
    
    public boolean Alarm_Scheduler()
    {                
        String Parameter_Value;

        int alarms=0;
        
        for (int i = 1; i < 1000; i++) 
        {
            Parameter_Value = testData.getData("Alarm"+i);
           if (Parameter_Value.contains("Parameter not found")) 
            {
                break;
            }
           alarms= i;
        }
        String[] alarmTimes = new String[alarms];
        String[] alarmtype = new String[alarms];
        String[] alarmImpact = new String[alarms];
        for (int i = 1; i <= alarms; i++) 
        {
            Parameter_Value =testData.getData("Alarm"+i).split(",")[0];
            alarmtype[i-1]=(Parameter_Value);
            
            Parameter_Value =testData.getData("Alarm"+i).split(",")[1];
            alarmImpact[i-1]=(Parameter_Value);
            
            Parameter_Value =testData.getData("Alarm"+i).split(",")[2];
            alarmTimes[i-1]=(Parameter_Value);
        }

        int maxTime=0;
        for (int i = 0; i < alarmTimes.length; i++) 
        {
            if (Integer.parseInt(alarmTimes[i])> maxTime) 
            {
                maxTime = Integer.parseInt(alarmTimes[i]);
            }
        }
        
        int lowestTime=5000;
        for (int i = 0; i < alarmTimes.length; i++) 
        {
            if (Integer.parseInt(alarmTimes[i])< lowestTime) 
            {
                lowestTime = Integer.parseInt(alarmTimes[i]);
            }
        }
             Create_Alarm_Threshold thresholds = new Create_Alarm_Threshold(); 
             Create_Alarm_Down Down = new Create_Alarm_Down();
             Create_Alarm_Up Up = new Create_Alarm_Up();
        
        int i = 0;
        int value;
        boolean firstTime=false;
        while (i < maxTime)
        {
            firstTime=true;
            for ( int j= 0;j<alarmTimes.length;j++) 
            {
               
               if(Integer.parseInt(alarmTimes[j])== lowestTime)
                { 
                    
                    if (firstTime) 
                    {
                      firstTime= false;
                      value= (Integer.parseInt(alarmTimes[j])-i)*60;
                      SeleniumDriverInstance.pause((value)*1000);
                    }

                    if (alarmtype[j].equals("Down")) 
                    {
                        Down.CloseAlarm(alarmImpact[j]);                        
                    }
                    else if(alarmtype[j].equals("Up"))
                    {
                        Up.CloseAlarm(alarmImpact[j]);
                    }
                    else
                    {
                        thresholds.CloseAlarm(alarmImpact[j]);
                    }    
                    alarmTimes[j]="50000";
                    
                }
            }
            i=lowestTime;
            
            lowestTime=5000;
            for (int k = 0; k < alarmTimes.length; k++) 
            {
                if (Integer.parseInt(alarmTimes[k])< lowestTime) 
                {
                    lowestTime = Integer.parseInt(alarmTimes[k]);
                }
            }

        } 
                
        return true;
    }
}