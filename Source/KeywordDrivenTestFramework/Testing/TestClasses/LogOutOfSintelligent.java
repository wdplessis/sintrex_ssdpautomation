/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.SintelligentPageObject;

/**
 *
 * @author szeuch
 */
public class LogOutOfSintelligent extends BaseClass 
{
    Narrator narrator;
    
    String error = "";

    public LogOutOfSintelligent(TestEntity testData) 
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }
    
    public TestResult executeTest()
    {
        if(!LogOut())
        {
            return narrator.testFailed("Failed to log out of Sintelligent - " + error, false);
        }
        
        if(!ValidateLogOut())
        {
            return narrator.testFailed("Failed to validate the user was successfully logged out - " + error, false);
        }
        
        return narrator.finalizeTest("Successfully logged out of Sintelligent", false, testData);
    }
    
    public boolean LogOut()
    {
        //Switches to default frame to be able to access the logout button
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to the default content";
            return false;
        }
        
        //Waits for logout button to be clickable
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LogOutButtonXpath())) 
        {
            error = "Failed to wait for the logout button to be clickable";
            return false;
        }
        
        //Clicks the logout button
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LogOutButtonXpath()))
        {
            error = "Failed to click the logout button";
            return false;
        }
        
        narrator.stepPassed("Successfully clicked the log out button", false);
        return true;
    }
    
    public boolean ValidateLogOut()
    {
        //Validate username, password and login button are visible after the logout
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.UsernameTextBoxXpath())) 
        {
            error = "Failed to wait for the username text box";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.PasswordTextBoxXpath())) 
        {
            error = "Failed to wait for the password text box";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LoginButtonXpath())) 
        {
            error = "Failed to wait for the login button";
            return false;
        }
        
        narrator.stepPassed("Successfully validated the log out was successful", false);
        return true;
    }
}
