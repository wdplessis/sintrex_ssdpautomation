/*
    Rebuilds data on the Sintelligent system for regression purposes
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.ConfigConsole.Locations.CreateBaseLinePageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.Configure.ConfigConsolePageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.Configure.RulesAndTemplates.FaultRulesPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.OperationalManagement.Configure.RulesAndTemplates.PerformanceRulesPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.SintelligentPageObject;
import KeywordDrivenTestFramework.Utilities.APICallUtility;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Reinhardt
 */

/*
    PERFORMS THE FOLLOWING ACTIONS
        - TRUNCS SYSTEM
        - IMPORT HOST GROUP LAYOUT
        - *IMPORT BUSINESS UNITS
        - IMPORT HOSTS
        - IMPORT PING + VIRTUAL
        - IMPORT LOCATIONS
        - CHANGE HOST CREDENTIALS - VIA SSH + TELNET + WMI for windows hosts
        - INTERROGATE HOSTS
        - *IMPORTS HOST ATTRIBUTES
        - CREATE PERFORMANCE + FAULT RULE
        - RUNS BASELINE
        - RUNS UPDATE WMI AGENT FOR WINDOWS HOSTS
*/

public class SintelligentRebuild extends BaseClass 
{
    Narrator narrator;
    
    int numHostGroupItemsBeforeImport = 0;
    
    String CSVLocation =System.getProperty("user.dir")+"\\TestDependencies\\Startup Imports\\";
    String unzippedFilePath = "";
    String error = "";
    
    int tryCounter = 0;

    int maxTryCounter = 0;
    
    int maxTryCounter2 = 10;
    int menuDrillDownAmount;
    public boolean test = false;

    public SintelligentRebuild(TestEntity testData) 
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }
    
    public TestResult executeTest()
    {
        maxTryCounter = Integer.parseInt(testData.getData("MaxTryCounter"));
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Rebuild Start: " + timeStamp);
        //validate correct page has loaded
        System.out.println("Validate page");
        test = ValidatePage();
        while(!test && (tryCounter < maxTryCounter))
        {
            test = ValidatePage();
        }
        tryCounter = 0;
        
//        System.out.println("Checking if a licence is active if not import one");
//        test = checkSintLicence(CSVLocation);
//        while(!test && (tryCounter < maxTryCounter))
//        {
//            test = checkSintLicence(CSVLocation);
//        }
//        tryCounter = 0;
        
//        System.out.println("Importing language Metadata");
//        test = importLanguageMetaData(CSVLocation);
//        while(!test && (tryCounter < maxTryCounter))
//        {
//            test = importLanguageMetaData(CSVLocation);
//        }
//        tryCounter = 0;
//        
//        //set up default credentials
//        System.out.println("Setting up default credentials...");
//        test = setDefaultCredentials();
//        while (!test && (tryCounter < maxTryCounter))
//        {
//            test = setDefaultCredentials();
//        }
//        tryCounter = 0;
        
//        //Enable Config console Menu Item
//        System.out.println("Enable Config Console menu Item...");
//        test = enableMenuItem();
//        while (!test && (tryCounter < maxTryCounter))
//        {
//            test = enableMenuItem();
//        }
//        tryCounter = 0;
//        
//        //Report manager - Add directory 
//        
//        System.out.println("Create Report Manager Directory - 'Directory'");
//        test = createDir();
//        while (!test && (tryCounter < maxTryCounter))
//        {
//            test = createDir();
//        }
//        tryCounter = 0;
//        
//        //Add Reporting code prefix 
//        System.out.println("Set Reporting codes...");
//        test = reportingCodePrefix();
//        while (!test && (tryCounter < maxTryCounter))
//        {
//            test = reportingCodePrefix();
//        }
//        tryCounter = 0;
//        
//        //Services Rollup - Setup Escalation Email 
//        System.out.println("Set up email settings - Sintrex Escalation Email...");
//        test = escEmailSetup();
//        while (!test && (tryCounter < maxTryCounter))
//        {
//            test = escEmailSetup();
//        }
//        tryCounter = 0;
//        
        
        
        
//        //******************************************************************************
//        //currently being blocked by KB-401
//        //****************************************************************************** 
//        //Import user list
//        System.out.println("Importing User List...");
//        test = importUserList(CSVLocation);
//        while (!test && (tryCounter < maxTryCounter))
//        {
//            test = importUserList(CSVLocation);
//        }
//        tryCounter = 0;
        
        
        //******************************************************************************
        //currently being blocked by KB-402
        //******************************************************************************
        //Import Category Types
//        System.out.println("Importing Category Types...");
//        test = importCategoryTypes(CSVLocation);
//        while (!test && (tryCounter < maxTryCounter))
//        {
//            test = importCategoryTypes(CSVLocation);
//        }
//        tryCounter = 0;
        
        //extend map view menu
//        System.out.println("Importing Category Types...");
//        test = mapViewExtendMenu();
//        while (!test && (tryCounter < maxTryCounter))
//        {
//            test = mapViewExtendMenu();
//        }
//        tryCounter = 0;
        
        
        //Add Incidents known problems
        
//        System.out.println("Add Known Problems directories...");
//        test = knownProblemDir();
//        while (!test && (tryCounter < maxTryCounter))
//        {
//            test = knownProblemDir();
//        }
//        tryCounter = 0;
        
        //*****************************************************************************************************************************************************************************************
        //set the test startup by deleting all hosts and host groups in the system
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Test Startup - Truncate Data: " + timeStamp);
        test = TestStartUp();
        while(!test && (tryCounter < maxTryCounter))
        {
            test = TestStartUp();
        }
        tryCounter = 0;
        
        //import the Host Group Layout csv file
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Import group layout: " + timeStamp);
        System.out.println("Import group layout");
        test = ImportGroupLayout(CSVLocation);
        while(!test && (tryCounter < maxTryCounter))
        {
            test = ImportGroupLayout(CSVLocation);
        }
        tryCounter = 0;       
                    
        //import the host list csv file
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Import host list: " + timeStamp);
        test = ImportHostList(CSVLocation);
        while(!test && (tryCounter < maxTryCounter))
        {
            test = ImportHostList(CSVLocation);
        }
        tryCounter = 0;
        
        //import the Location Layout csv file
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Import ping and virtual: " + timeStamp);
        test = ImportPingAndVirtualList(CSVLocation);
        while(!test && (tryCounter < maxTryCounter))
        {
            test = ImportPingAndVirtualList(CSVLocation);
        }        
        tryCounter = 0;
        
        //Move Ping Hosts
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Move ping hosts: " + timeStamp);
        test = MovePingHosts();
        while(!test && (tryCounter < maxTryCounter))
        {
            test = MovePingHosts();
        }
        tryCounter = 0;
        
         //import the Location Layout csv file
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Import locaiton layout: " + timeStamp);
        test = ImportLocationLayout(CSVLocation);
        while(!test && (tryCounter < maxTryCounter))
        {
            test = ImportLocationLayout(CSVLocation);
        }        
        tryCounter = 0;

        /******************************************************************************************************************************************************************************
        /*
        //SSH the Hosts so that the Credentials get set to the Defaults
        System.out.println("Perform SSH");
        test = PerformSSH();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = PerformSSH();
        }
        tryCounter = 0;
        
        if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE )
        {  
                if(!RefreshPage())
               {
                   return narrator.testFailed("Failed to Refresh page " + error, false);
               }
        }
        */
         //Telnet the Hosts so that the Credentials get set to the Defaults
         System.out.println("Perform telnet");
        test = PerformTelnet();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = PerformTelnet();
        }
        tryCounter = 0;
        
         if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE )
        {  
                if(!RefreshPage())
               {
                   return narrator.testFailed("Failed to Refresh page " + error, false);
               }
        }
         
         
         
        //WMI the Windows Hosts so that the Credentials get set to the Defaults 
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Perform WMI: " + timeStamp);
        test = PerformWMI();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = PerformWMI();
        }
        tryCounter = 0;

        if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE )
        {  
                if(!RefreshPage())
               {
                   return narrator.testFailed("Failed to Refresh page " + error, false);
               }
        }
         
        
        
        
        //Import the Fault Rules
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Import fault rules: " + timeStamp); 
        test = PerformFaultRuleImport(CSVLocation);
        while (!test && (tryCounter < maxTryCounter))
        {
            test = PerformFaultRuleImport(CSVLocation);
        }
        tryCounter = 0;
        

        
        
        //Import the Performance Rules
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Import performance rules: " + timeStamp); 
        test = PerformPerformanceRuleImport(CSVLocation);
        while (!test && (tryCounter < maxTryCounter))
        {
            test = PerformPerformanceRuleImport(CSVLocation);
        }
        tryCounter = 0;
        
        
       if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE )
        {  
                if(!RefreshPage())
               {
                   return narrator.testFailed("Failed to Refresh page " + error, false);
               }
        } 
        
        //Interrogate the Hosts so their detail can be populated
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Perform interrogation: " + timeStamp); 
        test = PerformInterrogations();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = PerformInterrogations();
        }
        tryCounter = 0;
         
        if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE )
        {  
                if(!RefreshPage())
               {
                   return narrator.testFailed("Failed to Refresh page " + error, false);
               }
        }
        
        //Create a Baseline so that properties can be monitored
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Create baseline: " + timeStamp); 
        test = CreateBaseline();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = CreateBaseline();
        }
        tryCounter = 0;
               
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Validate baseline: " + timeStamp); 
        test = ValidateCreateBaseline();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = ValidateCreateBaseline();
        }
        tryCounter = 0;
        
        if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE)
        {  
                if(!RefreshPage())
                {
                    return narrator.testFailed("Failed to Refresh page " + error, false);
                }
        }
        
        if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
        {  
                if(!RefreshPage())
                {
                    return narrator.testFailed("Failed to Refresh page " + error, false);
                }
        }
       
        if (Narrator.Testpack != null) 
        {
            
       
            if(Narrator.Testpack.equals("Test Pack 1") || Narrator.Testpack.equals("Test Pack 8") || Narrator.Testpack.equals("Test Pack 9") || Narrator.Testpack.equals("Test Pack 10") || Narrator.Testpack.equals("Test Pack 11"))
            {
                //Runs the Update WMI Agent so that the Window's properties can be monitored
                System.out.println("Update WMI agents");
                test = UpdateWMIAgents();
                while (!test && (tryCounter < maxTryCounter))
                {
                    test = UpdateWMIAgents();
                }
                tryCounter = 0;
            }

         }
        else
        {
               System.out.println("Test Pack: " + Narrator.Testpack);
               System.out.println("Update WMI agents");
                test = UpdateWMIAgents();
                while (!test && (tryCounter < maxTryCounter))
                {
                    test = UpdateWMIAgents();
                }
                tryCounter = 0;

        }
        
         if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE )
        {  
                if(!RefreshPage())
               {
                   return narrator.testFailed("Failed to Refresh page " + error, false);
               }
        }
        
        
        //Checking for alarms to start appearing before finishing the rebuild
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        System.out.println("Waiting for alarms to come in: " + timeStamp); 
        test = checkAlarmStatus();
        while (!test && (tryCounter < maxTryCounter2))
        {
            test = checkAlarmStatus();
        }
        System.out.println("Alarms are now present");
          
        
        return narrator.finalizeTest("Successfully performed the Sintelligent system rebuild", false, testData);
    }
    
    public boolean ValidatePage()
    {
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to validate the Config Console page was loaded successfully");
            return true;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Home")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AContainsTextXpath("Home")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
//        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false; 
//        }
//        
//        //Validate the main page is active with the menu button
//        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainMenuButtonXpath())) 
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//        
//        //switch iframes
//        if(!SeleniumDriverInstance.switchToDefaultContent())
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//        
//        //switch context
//        if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
////        }
        
        narrator.stepPassed("Successfully validated the correct SINTelligent page was loaded", false);
        return true;
    }
     
    //do test startup by deleting and purging all hosts
    public boolean TestStartUp()
    { 
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to validate the Config Console page was loaded successfully");
            return true;
        }
        
        //clear the system
        APICallUtility api = new APICallUtility();
        try
        {
            //NO NOT EDIT THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //NO NOT EDIT THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //NO NOT EDIT THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //NO NOT EDIT THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//            api.sendPost("http://10.0.4.185/sintrex_api/api/test/query",
//                "{\n" +
//                "\"type\":\"SELECT\",\n" +
//                "\"statement\":\"CALL TMP.Packaging_PackageCleanSQLProc()\"\n" +
//                "}");
        
            String apiUrl = "http://" + currentEnvironment.Ip + "/sintrex_api/api/test/query";
            api.sendPost(apiUrl,
                "{\n" +
                "\"type\":\"SELECT\",\n" +
                "\"statement\":\"CALL TMP.Packaging_PackageCleanSQLProc()\"\n" +
                "}");
        } 
        catch (Exception ex)
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
      
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //click refresh
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Refresh All")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AContainsTextXpath("Refresh All")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
           if(!SeleniumDriverInstance.clickElementByXpathFireFox(SintelligentPageObject.AContainsTextXpath("Refresh All")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            } 
        }
        else
        {
           if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AContainsTextXpath("Refresh All")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            } 
        }
        
        //validate there are no hosts in the system
        String test = SeleniumDriverInstance.CustomQuickExpandAllConfigConsoleVisibleItems();
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //validate no hosts
        List <WebElement> hostItems;
        try
        {
            hostItems = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.SpanContainsTextXpath(" - ")));
            
            if(hostItems.size() != 0)
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
        }
        catch(Exception e)
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        narrator.stepPassed("Successfully prepared the page for test startup by ensuring there were no Hosts or Host Groups found in the system on the Config Console page", true);
        return true;
    }
    
    
    //import host group layout
    public boolean ImportGroupLayout(String pathToFileDirectory)
    {
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to import the Host Group layout file '"+testData.getData("HostGroupLayoutCSVName")+"'");
            return true;
        }
              
        
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //extract number of items before import for future validation
        String test = SeleniumDriverInstance.CustomQuickExpandAllConfigConsoleVisibleItems();
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //hover over the 'Tasks' dropdown
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //hover 'Import'
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //click 'Group Layout'
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Group Layout")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Group Layout")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("Group Layout")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("pathFrom")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.InputWithIdXpath("pathFrom")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithIdXpath("pathFrom"), pathToFileDirectory+testData.getData("HostGroupLayoutCSVName")+".csv");
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Total Number of Errors: 0")))
        {
            tryCounter = 10000;
            System.out.println(tryCounter);
            return false;
        }
        
            
            
        narrator.stepPassed("Successfully imported the host group layout csv file", true);
        return true;
    }
    
    //import the locations layout
    public boolean ImportLocationLayout(String pathToFileDirectory)
    {
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to import the locations layout file '"+testData.getData("LocationsLayoutCSVName")+"'");
            return true;
        }
        
        //updates host type 
        APICallUtility api = new APICallUtility();
        try
        {
            String apiUrl = "http://" + currentEnvironment.Ip + "/sintrex_api/api/test/query";
            api.sendPost(apiUrl,
                "{\n" +
                "\"type\":\"UPDATE\",\n" +
                "\"statement\":\"UPDATE Config.Ci SET ciClassKey = 1010002 WHERE isContainer = 1 AND ofType <> 'N' AND isActive = 1 AND uid = '"+currentEnvironment.Ip+"'\"\n" +
                "}");
        } 
        catch (Exception ex)
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.SwitchContext("Locations", ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //extract number of items before import for future validation
        String test = SeleniumDriverInstance.CustomQuickExpandAllConfigConsoleVisibleItems();
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //hover over the 'Tasks' dropdown
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //hover 'Import'
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //click 'Group Layout'
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Location Layout")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Location Layout")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("Location Layout")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("pathFrom")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.InputWithIdXpath("pathFrom")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithIdXpath("pathFrom"), pathToFileDirectory+testData.getData("LocationsLayoutCSVName")+".csv");
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Total Number of Errors: 0")))
        {
            tryCounter = 10000;
            System.out.println(tryCounter);
            return false;
        }
        
        //click close
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Cancel")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        narrator.stepPassed("Successfully imported the location layout csv file", true);
        return true;
    }        
    
    //import the host list from the [testDependencies] directory
    public boolean ImportHostList(String pathToFileDirectory)
    {
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to import the Host List file '"+testData.getData("HostListCSVName")+"'");
            return true;
        } 
        
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //extract number of items before import for future validation
        String test = SeleniumDriverInstance.CustomQuickExpandAllConfigConsoleVisibleItems();
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //extract number of items before import
        List <WebElement> hostGroupsItems;
        try
        {
            hostGroupsItems = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivWithId("contenttabletreeGrid")+SintelligentPageObject.OpenHostGroupCheckBox("")));
            
            //+1 for 'unlinked devices'
            numHostGroupItemsBeforeImport = hostGroupsItems.size() + 1;
        }
        catch(Exception e)
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //hover over the 'Tasks' dropdown
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //hover 'Import'
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //click 'Host Lists'
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Host List")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Host List")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("Host List")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("uploadFileOpener")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.InputWithIdXpath("uploadFileOpener")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.InputWithIdXpath("uploadFileOpener")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //upload the csv file
        test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithType("file"), pathToFileDirectory+testData.getData("HostListCSVName")+".csv");
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //click 'Start Upload'
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Start")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Start")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //wait for upload
        String uploadWait = SeleniumDriverInstance.WaitForLoaderIconNotVisible(SintelligentPageObject.tableWithRole("presentation")+SintelligentPageObject.DivWithClass("progress progress-striped active"));
        if(!uploadWait.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //click close
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Close")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //click the 'reference meta data' checkbox
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("chkReference")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.InputWithIdXpath("chkReference")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.InputWithIdXpath("chkReference")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //click 'Upload'
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdContainsTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        String wait = SeleniumDriverInstance.WaitForLoaderIconNotVisible();
        if(!wait.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        SeleniumDriverInstance.pause(500);
        
        //validate there were no errors:
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Total Number of Errors: 0")))
        {
           tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //validate some hosts were imported
        if(SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.TdContainsTextXpath("Total Number of Hosts Imported: 0"), 3))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //updates host type 
        APICallUtility api = new APICallUtility();
        try
        {
            String apiUrl = "http://" + currentEnvironment.Ip + "/sintrex_api/api/test/query";
            api.sendPost(apiUrl,
                "{\n" +
                "\"type\":\"UPDATE\",\n" +
                "\"statement\":\"UPDATE Config.Ci SET ciClassKey = 1010002 WHERE isContainer = 1 AND ofType <> 'N' AND isActive = 1 AND uid = '"+currentEnvironment.Ip+"'\"\n" +
                "}");
        } 
        catch (Exception ex)
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.SpanUpOneSpanContainsClass(testData.getData("WLCContainer"), "collapse-button")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.SpanWithTextXpath(testData.getData("WLCHost"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Edit")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(1500);
        
        //switch to 'screen' iframe
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(1500);
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Scripts")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_scriptsGrid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.tablewithIdXpath("_scriptsAdd")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown"), "Script Key"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtFilterTextbox"), "66"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
            
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithIdXpath("Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.SpanWithTextXpath("Cisco WLC SSH Running Config")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(1500);
        
        //switch to 'screen' iframe
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(1500);
        
        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.SpanWithTextXpath("Cisco WLC SSH Running Config")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.tablewithIdXpath("_scriptsDisable")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("alertOK")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        narrator.stepPassedWithScreenshot("Done Importing Hosts", true);
        
        narrator.stepPassed("Successfully uploaded the Host List csv file ["+testData.getData("HostListCSVName")+"] on the 'Import New Hosts' screen", true);
        return true;
    }

    //import the Ping and Virtual Host list from the [testDependencies] directory
    public boolean ImportPingAndVirtualList(String pathToFileDirectory)
    {
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to import the Ping and Virtual Host List file '"+testData.getData("PingAndVirtualListCSVName")+"'");
            return true;
        }
        
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //extract number of items before import for future validation
        String test = SeleniumDriverInstance.CustomQuickExpandAllConfigConsoleVisibleItems();
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //hover over the 'Tasks' dropdown
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //hover 'Import'
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //click 'Host Lists'
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Virtual and Ping")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Virtual and Ping")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("Virtual and Ping")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("uploadFileOpener")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.InputWithIdXpath("uploadFileOpener")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.InputWithIdXpath("uploadFileOpener")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //upload the csv file
        test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithType("file"), pathToFileDirectory+testData.getData("PingAndVirtualListCSVName")+".csv");
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //click 'Start Upload'
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Start")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Start")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //wait for upload
        String uploadWait = SeleniumDriverInstance.WaitForLoaderIconNotVisible(SintelligentPageObject.tableWithRole("presentation")+SintelligentPageObject.DivWithClass("progress progress-striped active"));
        if(!uploadWait.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //click close
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Close")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        /*
        //click the 'reference meta data' checkbox
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("chkReference")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.InputWithIdXpath("chkReference")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.InputWithIdXpath("chkReference")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        */
        //click 'Upload'
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdContainsTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //validate there were no errors:
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Total Number of Errors: 0")))
        {
           tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //validate some hosts were imported
        if(SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.TdContainsTextXpath("Total Number of Hosts Imported: 0"), 3))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        narrator.stepPassed("Successfully uploaded the Host List csv file ["+testData.getData("PingAndVirtualListCSVName")+"] on the 'Import Ping and Virtual Hosts' screen", true);
        return true;
    }

    //moves Ping hosts to the correct containers
    public boolean MovePingHosts()
    {
        if(tryCounter > 10)
        {
            System.out.println("Failed to Move Ping Hosts");
            return true;
        }
        
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Refresh All")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AContainsTextXpath("Refresh All")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
            if(!SeleniumDriverInstance.clickElementByXpathFireFox(SintelligentPageObject.AContainsTextXpath("Refresh All")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
        }
        
        else
        {
           if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AContainsTextXpath("Refresh All")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            } 
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OpenHostGroupDropdown("Import Hosts")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.OpenHostGroupDropdown("Import Hosts")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupDropdown("Import Hosts")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        String wait = SeleniumDriverInstance.WaitForLoaderIconNotVisible();
        if(!wait.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        SeleniumDriverInstance.pause(500);
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OpenHostGroupCheckBox("Import Hosts")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.OpenHostGroupCheckBox("Import Hosts")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupCheckBox("Import Hosts")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Maintain")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Maintain")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Maintain")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Move Tagged Hosts")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Move Tagged Hosts")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("Move Tagged Hosts")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("cboContainers")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        
        //if(!SeleniumDriverInstance.selectByPartialTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboContainers"), "Ping Hosts"))
        if(!SeleniumDriverInstance.selectByTextFromDropDownList_UsingXpathNew(SintelligentPageObject.SelectWithIdXpath("cboContainers"), "Ping Hosts"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Apply")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdContainsTextXpath("Apply")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Apply")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        narrator.stepPassed("Successfully moved the imported Ping Hosts to the correct containers", true);
        return true;
    }
    
    //initiates SSH for all the hosts in Host Groups
    public boolean PerformSSH()
    { 
        //clear the system
        APICallUtility api = new APICallUtility();
        try
        {
            String apiUrl = "http://" + currentEnvironment.Ip + "/sintrex_api/api/test/query";
            api.sendPost(apiUrl,
                "{\n" +
                "\"type\":\"UPDATE\",\n" +
                "\"statement\":\"UPDATE Common.RequestDetail SET status = 'F' WHERE status = 'Q'\"\n" +
                "}");
        } 
        catch (Exception ex)
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        } 
        
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to run SSH tests on the Hosts");
            return true;
        }
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        
        //Check all the boxes for Host Groups
        List <WebElement> hostGroupCheckBoxes;
        ArrayList <String> hostGroupNames = new ArrayList();
        try
        {
            hostGroupCheckBoxes = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivWithId("contenttreeGrid")+SintelligentPageObject.SpanContainsClassXpath("tree-grid-title")));
            for (int x = 0; x < hostGroupCheckBoxes.size() ; x++)
            {
                String hostGroupDesc = hostGroupCheckBoxes.get(x).getText();
                if (!hostGroupDesc.equals("Unlinked Devices")&&!hostGroupDesc.equals("Spare Decommissioned")&&!hostGroupDesc.equals("Discover"))
                {    
                    hostGroupNames.add(hostGroupDesc);
                }
            }
            
            for (int x = 0; x < hostGroupNames.size() ; x++)
            {
                String groupName = hostGroupNames.get(x);
                SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupCheckBox(groupName));
            }

        }
        catch(Exception e)
        {
            tryCounter++;
            System.out.println(tryCounter + "-" + e);
            return false;
        }
        
         if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Diagnostics")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Diagnostics")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Diagnostics")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //select SSH 
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("SSH")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("SSH")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        SeleniumDriverInstance.pause(500);
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("SSH")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgContainsSrc("dashboard_settings.png")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgContainsSrc("dashboard_settings.png")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgContainsSrc("dashboard_settings.png")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown"),"5 seconds"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnReqClose")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnReqClose")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnReqClose")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //switch frames
        if (!SeleniumDriverInstance.switchToDefaultContent()) 
        {
            error = "Failed to switch back to the default content";
            return false;
        }
        //switches to the config console iframe
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame(), "workSpace"))
        {
            error = "Could not switch to the '"+SintelligentPageObject.ManagerRedirectFrame()+"'";
            return false;
        }
        
        //wait for loader to not be present
        String waiter = SeleniumDriverInstance.WaitForLoaderIconNotVisible();
        if(!waiter.isEmpty())
        {
            error = waiter;
            return false;
        }
        
        //validate page has loaded
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Request Command")))
        {
            error = "Could not validate the 'SSH' request screen has loaded";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Request Command")))
        {
            error = "Could not validate the 'SSH' request screen has loaded";
            return false;
        }
        
        //if error, fail test
        if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("No data to display"),1))
        {
            error = "The data was not generated properly: received the 'No data to display error message'";
            return false;
        }
       /*
         //filter by status
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //dropdown animation
        SeleniumDriverInstance.pause(1000);
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("(Select All)")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("(Select All)")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("(Select All)")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Busy")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Busy")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Busy")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
         if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Queued")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Queued")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Queued")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        */
        //wait for busy items to resolve
        int stopCounter = 0;
        //boolean itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Q"),1) && SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("B"),1);
        boolean itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation("SSH-buttons"),1) || SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation2("SSH-buttons"),1);
        while(itemsBusy)
        {
            //wait maximum of 30 seconds for busy items to resolve
            SeleniumDriverInstance.pause(1000);
            if(stopCounter > 1500)
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation("SSH-buttons"),1) || SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation2("SSH-buttons"),1);
            //itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Q"),1) && SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("B"),1);
            stopCounter++;
        }
        
        return true;
    }   
    
    //initiates Telnet for all the hosts in Host Groups
    public boolean PerformTelnet()
    { 
        //clear the system
        APICallUtility api = new APICallUtility();
        try
        {
            String apiUrl = "http://" + currentEnvironment.Ip + "/sintrex_api/api/test/query";
            api.sendPost(apiUrl,
                "{\n" +
                "\"type\":\"UPDATE\",\n" +
                "\"statement\":\"UPDATE Common.RequestDetail SET status = 'F' WHERE status = 'Q'\"\n" +
                "}");
        } 
        catch (Exception ex)
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        } 
        
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to run Telnet tests on the Hosts");
            return true;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
      
        
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
       
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        
        //Check all the boxes for Host Groups
        List <WebElement> hostGroupCheckBoxes;
        ArrayList <String> hostGroupNames = new ArrayList();
        try
        {
            hostGroupCheckBoxes = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivWithId("contenttreeGrid")+SintelligentPageObject.SpanContainsClassXpath("tree-grid-title")));
            for (int x = 0; x < hostGroupCheckBoxes.size() ; x++)
            {
                String hostGroupDesc = hostGroupCheckBoxes.get(x).getText();
                if (!hostGroupDesc.equals("Servers")&&!hostGroupDesc.equals("Unlinked Devices")&&!hostGroupDesc.equals("Spare Decommissioned")&&!hostGroupDesc.equals("Discover"))
                {    
                    hostGroupNames.add(hostGroupDesc);
                }
            }
            
            for (int x = 0; x < hostGroupNames.size() ; x++)
            {
                String groupName = hostGroupNames.get(x);
                SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupCheckBox(groupName));
            }

        }
        catch(Exception e)
        {
            tryCounter++;
            System.out.println(tryCounter + "-" + e);
            return false;
        }
        
         if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Diagnostics")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Diagnostics")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Diagnostics")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //select Telnet 
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Telnet")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Telnet")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        SeleniumDriverInstance.pause(500);
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("Telnet")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgContainsSrc("dashboard_settings.png")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgContainsSrc("dashboard_settings.png")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgContainsSrc("dashboard_settings.png")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown"),"5 seconds"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnReqClose")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnReqClose")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnReqClose")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //switch frames
        if (!SeleniumDriverInstance.switchToDefaultContent()) 
        {
            error = "Failed to switch back to the default content";
            return false;
        }
        //switches to the config console iframe
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame(), "workSpace"))
        {
            error = "Could not switch to the '"+SintelligentPageObject.ManagerRedirectFrame()+"'";
            return false;
        }
        
        //wait for loader to not be present
        String waiter = SeleniumDriverInstance.WaitForLoaderIconNotVisible();
        if(!waiter.isEmpty())
        {
            error = waiter;
            return false;
        }
        
        //validate page has loaded
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Request Command")))
        {
            error = "Could not validate the 'Telnet' request screen has loaded";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Request Command")))
        {
            error = "Could not validate the 'Telnet' request screen has loaded";
            return false;
        }
        
        //if error, fail test
        if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("No data to display"),1))
        {
            error = "The data was not generated properly: received the 'No data to display error message'";
            return false;
        }
        /*
         //filter by status
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //dropdown animation
        SeleniumDriverInstance.pause(1000);
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("(Select All)")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("(Select All)")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("(Select All)")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Busy")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Busy")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Busy")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
         if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Queued")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Queued")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Queued")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        */
        //wait for busy items to resolve
        int stopCounter = 0;
        //boolean itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Q"),1) && SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("B"),1);
        boolean itemsBusy = (SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation("Telnet-buttons"),1) || SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation2("Telnet-buttons"),1));
        while(itemsBusy)
        {
            SeleniumDriverInstance.pause(1000);
            //wait maximum of 300 seconds for busy items to resolve
            if(stopCounter > 1500)
            {               
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation("Telnet-buttons"),1) || SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation2("Telnet-buttons"),1);
            //itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Q"),1) && SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("B"),1);
            stopCounter++;
        }
        
        return true;
    }        
    
//initiates WMI query
    public boolean PerformWMI()
    {
      
        if(tryCounter > 10)
        {
            return true;
        }
        
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        //expand to server
        int containerCounter = 1;
        while(testData.getData("Container"+containerCounter) != "Parameter not found")
        {
            //expand to specific server
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OpenHostGroupDropdown(testData.getData("Container"+containerCounter))))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.OpenHostGroupDropdown(testData.getData("Container"+containerCounter))))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupDropdown(testData.getData("Container"+containerCounter))))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            String test = SeleniumDriverInstance.WaitForLoaderIconNotVisible();
            if(!test.isEmpty())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            containerCounter++;
        }
        containerCounter = 0;

        //tag the server
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OpenHostGroupCheckBox(testData.getData("WindowsHostToTest1"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.OpenHostGroupCheckBox(testData.getData("WindowsHostToTest1"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupCheckBox(testData.getData("WindowsHostToTest1"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //tag the server
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OpenHostGroupCheckBox(testData.getData("WindowsHostToTest2"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.OpenHostGroupCheckBox(testData.getData("WindowsHostToTest2"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupCheckBox(testData.getData("WindowsHostToTest2"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Diagnostics")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Diagnostics")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Diagnostics")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //select WMI 
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("WMI")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("WMI")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("WMI")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //change submenu user request
        
        //filter by status
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //dropdown animation
        SeleniumDriverInstance.pause(1000);
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("(Select All)")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("(Select All)")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("(Select All)")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Completed")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Completed")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Completed")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
         if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("requestSettings")+"//img"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.tablewithIdXpath("request")+ SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown"),"5 seconds"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
       
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnReqClose")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        //wait for busy items to resolve
        int stopCounter = 0;
        boolean itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation("WMI-buttons"),1) || SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation2("WMI-buttons"),1);
        while(itemsBusy)
        {
            SeleniumDriverInstance.pause(1000);
            //wait maximum of 100 seconds for busy items to resolve
            if(stopCounter > 100)
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation("WMI-buttons"),1) || SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation2("WMI-buttons"),1);
            stopCounter++;
        }
        
        List <WebElement> doneItems;
        boolean itemsPassed = false;
        stopCounter = 0;
        do
        {
            SeleniumDriverInstance.pause(1000);
            if(stopCounter > 3)
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            try
            {
                doneItems = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsTextXpath("Completed")));
                //two windows hosts imported
                if(doneItems.size() > 0)
                {
                    itemsPassed = true;
                }
            }
            catch(Exception e)
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            stopCounter++;
        }
        while(!itemsPassed);
        
        narrator.stepPassed("Succesfully submitted windows servers '"+testData.getData("WindowsHostToTest1")+"' and '"+testData.getData("WindowsHostToTest2")+"' for WMI query", true);
        return true;
    }
    /*
    //waits for completion
    public boolean ValidateWMIQuery()
    {
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Could not switch to the default iframe";
            return false;
        }
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            error = "Could not switch to the '"+SintelligentPageObject.ManagerRedirectFrame()+"' iframe";
            return false;
        }
        
        //sort the requests
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Start Time")))
        {
            error = "Could not wait for the 'Start Time' heading to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Start Time")))
        {
            error = "Could not wait for the 'Start Time' heading to be clickable";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Start Time")))
        {
            error = "Could not click the 'Start Time' heading ";
            return false;
        }
        String test = SeleniumDriverInstance.WaitForLoaderIconNotVisible(SintelligentPageObject.DivContainsClass("loader10"));
        if(!test.isEmpty())
        {
            error = test;
            return false;
        }
        //sort the requests
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Start Time")))
        {
            error = "Could not wait for the 'Start Time' heading to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Start Time")))
        {
            error = "Could not wait for the 'Start Time' heading to be clickable";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Start Time")))
        {
            error = "Could not click the 'Start Time' heading '";
            return false;
        }
        test = SeleniumDriverInstance.WaitForLoaderIconNotVisible(SintelligentPageObject.DivContainsClass("loader10"));
        if(!test.isEmpty())
        {
            error = test;
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility("//div[@id='contenttablerdGrid_grid']//div[contains(text(), '10.15.0.112')]//..//..//div[2]",60))
        {
            error = "Could not wait for the 'Start Time' heading to be visible";
            return false;
        }
        //wait for status to complete for specified host
        //create list of all requests with specified IP - extract unique id [status time]
        List <WebElement> WMIStartTimeItems;
        String startTime = "";
        try 
        {
            WMIStartTimeItems = SeleniumDriverInstance.Driver.findElements(By.xpath("//div[@id='contenttablerdGrid_grid']//div[contains(text(), '10.15.0.112')]//..//..//div[2]"));
            
            //extract start time
            startTime = WMIStartTimeItems.get(0).getText().toString();
        }
        catch (Exception e) 
        {
            System.out.println("[ERROR] - " + e.getMessage());
            error = "Could not extract the WMI query items";
            return false;
        }
        
        String timeStamp;
        //Could be 1 second off due to timing - Stefan
        timeStamp = timeStamp.substring(0, timeStamp.length()-11);
        
        if (!startTime.contains(timeStamp)) 
        {
            error = "Failed to validate the start time was as expected Expected: '"+startTime+"' - Actual: '"+timeStamp+"' ";
            return false;
        }
        
        testData.extractParameter("Start time for initiated WMI query:", ""+startTime, "");
        
        //use the start time to id the most recent query and wait for its completion
        boolean isComplete = SeleniumDriverInstance.waitForElementByXpathVisibility(DiagnosticsMenuWMIPageObject.WMICompleteStatusItem(startTime));
        int stopCounter = 0;
        if(!isComplete)
        {
            if(stopCounter== 60)
            {
                error = "Could not wait for the WMI query for host '"+testData.getData("WindowsHostToTest1")+"' with start time '"+startTime+"' to be completed";
                return false;
            }
            
            //waits a maximum of 60 seconds for completion
            SeleniumDriverInstance.pause(1000);
            
            isComplete = SeleniumDriverInstance.waitForElementByXpathVisibility(DiagnosticsMenuWMIPageObject.WMICompleteStatusItem(startTime));
            stopCounter++;
        }
        
        narrator.stepPassed("Successfully validated that the WMI query for the host '"+testData.getData("WindowsHostToTest1")+"' completed successfully", isComplete);
        return true;
    }    
    */
    
    //initiates Interrogations for all the hosts in Host Groups
    public boolean PerformInterrogations()
    { 
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to run the Interrogations successfully");
            return true;
        }

        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
      
        
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
       
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        
        //Check all the boxes for Host Groups
        List <WebElement> hostGroupCheckBoxes;
        ArrayList <String> hostGroupNames = new ArrayList();
        try
        {
            hostGroupCheckBoxes = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivWithId("contenttreeGrid")+SintelligentPageObject.SpanContainsClassXpath("tree-grid-title")));
            for (int x = 0; x < hostGroupCheckBoxes.size() ; x++)
            {
                String hostGroupDesc = hostGroupCheckBoxes.get(x).getText();
                if (!hostGroupDesc.equals("Unlinked Devices")&&!hostGroupDesc.equals("Spare Decommissioned")&&!hostGroupDesc.equals("Discover"))
                {    
                    hostGroupNames.add(hostGroupDesc);
                }
            }
            
            for (int x = 0; x < hostGroupNames.size() ; x++)
            {
                String groupName = hostGroupNames.get(x);
                SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupCheckBox(groupName));
            }

        }
        catch(Exception e)
        {
            tryCounter++;
            System.out.println(tryCounter + "-" + e);
            return false;
        }
        
         if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //select Telnet 
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Interrogate")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Interrogate")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        SeleniumDriverInstance.pause(500);
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("Interrogate")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgContainsSrc("dashboard_settings.png")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgContainsSrc("dashboard_settings.png")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgContainsSrc("dashboard_settings.png")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown"),"5 seconds"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnReqClose")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnReqClose")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnReqClose")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //switch frames
        if (!SeleniumDriverInstance.switchToDefaultContent()) 
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        //switches to the config console iframe
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame(), "workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //wait for loader to not be present
        String waiter = SeleniumDriverInstance.WaitForLoaderIconNotVisible();
        if(!waiter.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //validate page has loaded
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Request Command")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Request Command")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //if error, fail test
        if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("No data to display"),1))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        /*
         //filter by status
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //dropdown animation
        SeleniumDriverInstance.pause(1000);
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("(Select All)")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("(Select All)")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("(Select All)")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Busy")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Busy")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Busy")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
         if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Queued")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Queued")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Queued")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Select Filter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        */
        //wait for busy items to resolve
        int stopCounter = 0;
        //boolean itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Q"),1) && SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("B"),1);
        boolean itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation("Work List-buttons"),1) || SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation2("Work List-buttons"),1);
        while(itemsBusy)
        {
            SeleniumDriverInstance.pause(1000);
            //wait maximum of 300 seconds for busy items to resolve
            if(stopCounter > 1800)
            {               
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation("Work List-buttons"),1) || SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation2("Work List-buttons"),1);
            //itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Q"),1) && SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("B"),1);
            stopCounter++;
        }
        
        return true;
    }  
    
    //initiates the Baseline
    public boolean CreateBaseline()
    {
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to run the Baseline successfully");
            return true;
        }
      
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        
        //Validate the main page is active with the menu button
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainMenuButtonXpath())) 
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.hoverOverElementActionsByXpath(SintelligentPageObject.LiWithTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiWithTextXpath("Create Baseline")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
//------------------------------------------Validate that the tickboxes are indeed ticked-------------------------------------------------
        if (!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkClearLog"), true))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkSkipShutInterfaces"), true))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkSkipShutSNMPInterfaces"), false))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkMaintainVirtualInterfaces"), true))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        //-------------------------------------------------------------------------------------------------------------------------------------------
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        narrator.stepMessage("Successfully started the Create Baseline");
//using an if statment to see if firefox is being used, if true then run a normal if statment instead of a not if statment
//REASON: when Trying a normal not if statement the action gets performed but the action returns an error aswell and fails the test
// When using a normal if statement and returning true instead of falss the action gets perfomed and the test continues as normal
        String browser = ApplicationConfig.SelectedBrowser().toString();

        //Possibility of a false positive
        if (browser.equalsIgnoreCase("Firefox"))
        {
            if (SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("alertOK")))
            {
                return true;
            }
        } else if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("alertOK")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        //failed to switch frame due to not clicking on the 'OK' button
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
           //failed to switch frame due to not clicking on the 'OK' button
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(5000);
        
      if(!SeleniumDriverInstance.clickElementByXpath(CreateBaseLinePageObjects.trWithID("BL")))
      {
        error = "Failed to switch to default content";
            return false;
      }
        List<WebElement> statusVal;
        try
        {
            statusVal = SeleniumDriverInstance.Driver.findElements(By.xpath(CreateBaseLinePageObjects.DivStatusColumn()));
        } catch (Exception e)
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if (statusVal.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        //declaring veribles
        boolean isDone = false;
        int rowDoneAmount = 0;

        //starting a while loop
        while (!isDone)
        {
            //checking the size of webElements list

            for (int i = 0; i < statusVal.size(); i++)
            {
                //declare a empty string
                String temp = "";
                //try catch
                try
                {
                    statusVal = SeleniumDriverInstance.Driver.findElements(By.xpath(CreateBaseLinePageObjects.DivStatusColumn()));
                    //assighning the temp  extracted text  from (i) and converting to string
                    temp = statusVal.get(i).getText().toString();
                } catch (Exception e)
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                //if value of temp == 'Done' then add 1 to rowDoneAmount verible
                if (temp.equals("Completed"))
                {
                    rowDoneAmount++;
                }
            }
            //iff rowDoneAmount is exacly the same size as webElement List the isDone boolean = true
            if (rowDoneAmount == statusVal.size())
            {
                isDone = true;
                break;
            } else
            {
                rowDoneAmount = 0;
            }

            try
            {
                statusVal = SeleniumDriverInstance.Driver.findElements(By.xpath(CreateBaseLinePageObjects.DivStatusColumn()));
            } catch (Exception e)
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if (statusVal.isEmpty())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithTextXpath("Baseline")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithTextXpath("Baseline")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            
        }

//        if (!SeleniumDriverInstance.switchToDefaultContent())
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnRequest")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRequest")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.UserRequestsFrame()))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
        return true;
    }

    public boolean ValidateCreateBaseline()
    {
        int ignoredCheckBoxes = 0;
//        if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
//        {
            // Validate Baseline
            // Switch to the Switch to the Global mantenance frame screen iframe
            if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame(), SintelligentPageObject.workSpaceFrameName()))
            {
                error = "Failed to switch to the maintenance container page";
            }

            List<WebElement> status;
            int count = 0;
            // loop for max 3 min trying validate baseline
            while (count != 180)
            {
                // counter to keep track of element completed in the baseline
                int doneCounter = 0;
                try
                {
                    status = SeleniumDriverInstance.Driver.findElements(By.xpath(FaultRulesPageObject.baselineStatusXpath()));

                }
                catch (Exception e)
                {
                    error = "Failed to retrieve baseline status'";
                    return false;
                }

                // Get text of all status
                for (int i = 0; i < status.size(); i++)
                {
                    if (status.get(i).getText().toString().equals("Completed"))
                    {
                        doneCounter++;
                    }
                }

                // if the correct number of element has completed then break the loop
                if (doneCounter == Integer.parseInt(SeleniumDriverInstance.retrieveTextByXpath(FaultRulesPageObject.BaselineNumberOfElement())))
                {
                    break;
                }
                else // refresh page to wait again
                {
                    if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.TdWithTextXpath("Baseline")))
                    {
                        error = "Failed to wait for Baseline buttons in the right bottom panel";
                        return false;
                    }

                    if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithTextXpath("Baseline")))
                    {
                        error = "Failed to wait for Baseline buttons in the right bottom panel to be visible";
                        return false;
                    }

                    if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithTextXpath("Baseline")))
                    {
                        error = "Failed to wait for Baseline button in the right bottom panel to be clickable";
                        return false;
                    }

                    if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithTextXpath("Baseline")))
                    {
                        SeleniumDriverInstance.pause(60000);
                        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithTextXpath("Baseline")))
                        {
                        error = "Failed to click on Baseline in the right bottom panel";
                        return false;
                        }
                    }
                }

                // Delay by one second before clicking again
                SeleniumDriverInstance.pause(1000);
                count++;
            }

            if (ignoredCheckBoxes == 2)
            {
                narrator.stepMessage("Successfully created baseline with 'Skip Shut Interfaces' and 'Skip Shut SNMP Monitored Interfaces' unchecked");
            }
            else
            {
                narrator.stepMessage("Successfully created a normal baseline");
            }
            return true;
//        }
//        else
//        {
//            if(tryCounter > maxTryCounter)
//            {
//                System.out.println("Failed to Validate that the Baseline was successfully run");
//                return true;
//            }
//
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(CreateBaseLinePageObjects.DivStatusColumnSpan()))
//            {
//                tryCounter++;
//                System.out.println(tryCounter);
//                return false;
//            }
//            //--------------------------------------------Validate all Request comands are complete----------------------------------------------  
//            List<WebElement> RequestCommandVal;
//            try
//            {
//                RequestCommandVal = SeleniumDriverInstance.Driver.findElements(By.xpath(CreateBaseLinePageObjects.DivStatusColumnSpan()));
//            } catch (Exception e)
//            {
//                tryCounter++;
//                System.out.println(tryCounter);
//                return false;
//            }
//
//            if (RequestCommandVal.isEmpty())
//            {
//                tryCounter++;
//                System.out.println(tryCounter);
//                return false;
//            }
//
//            //waits for all to say completed
//            for (int i = 0; i < RequestCommandVal.size(); i++)
//            {
//                while (!RequestCommandVal.get(i).getText().toString().equals("Completed"))
//                {
//
//                    if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("searchIcon")))
//                    {
//                        tryCounter++;
//                        System.out.println(tryCounter);
//                        return false;
//                    } else if (!(!RequestCommandVal.get(i).getText().toString().equals("Failed")))
//                    {
//                        tryCounter++;
//                        System.out.println(tryCounter);
//                        return false;
//                    }
//                    //pausing to get the give the server time to process the requests
//                    SeleniumDriverInstance.pause(500);
//                    try
//                    {
//                        RequestCommandVal = SeleniumDriverInstance.Driver.findElements(By.xpath(CreateBaseLinePageObjects.DivStatusColumnSpan()));
//                    } catch (Exception e)
//                    {
//                        tryCounter++;
//                        System.out.println(tryCounter);
//                        return false;
//                    }
//                }
//            }
//
//            return true;
//        }
        
    }
    
    //Runs the Update WMI Agent and Validates that it completes
    public boolean UpdateWMIAgents()
    {
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to run the Update WMI Agent successfully");
            return true;
        }
                     
        if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
        {  
                if(!RefreshPage())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
        }
        
        
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
//        SeleniumDriverInstance.pause(2500);
//        
//        
//        //extract number of open tabs
//        List<WebElement> visibleTabs;
//        try
//        {
//            visibleTabs = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.tabName()));
//        }
//        catch (Exception e)
//        {
//            System.out.println("[ERROR] - " + e.getMessage());
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//
//        //start at position 2 - skip 'Home'
//        for (int i = 0; i < visibleTabs.size(); i++)
//        {
//            String nameOfTab = visibleTabs.get(i).getText();
//
//            if(!nameOfTab.equals("Home") && !nameOfTab.equals("Config Console"))
//            {
//                if (!SeleniumDriverInstance.clickElementByXpathFireFox(SintelligentPageObject.removeTabxpath(nameOfTab)))
//                {
//                    tryCounter++;
//                    System.out.println(tryCounter);
//                    return false;
//                }
//
//                SeleniumDriverInstance.pause(1000);
//            }
//
//        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainTabs("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false; 
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        //expand to server
        int containerCounter = 1;
        while(testData.getData("Container"+containerCounter) != "Parameter not found")
        {
            //expand to specific server
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OpenHostGroupDropdown(testData.getData("Container"+containerCounter))))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.OpenHostGroupDropdown(testData.getData("Container"+containerCounter))))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupDropdown(testData.getData("Container"+containerCounter))))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            String test = SeleniumDriverInstance.WaitForLoaderIconNotVisible();
            if(!test.isEmpty())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            containerCounter++;
        }
        containerCounter = 0;
        
        //tag the server
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OpenHostGroupCheckBox(testData.getData("WindowsHostToTest1"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.OpenHostGroupCheckBox(testData.getData("WindowsHostToTest1"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupCheckBox(testData.getData("WindowsHostToTest1"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //tag the server
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OpenHostGroupCheckBox(testData.getData("WindowsHostToTest2"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.OpenHostGroupCheckBox(testData.getData("WindowsHostToTest2"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupCheckBox(testData.getData("WindowsHostToTest2"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //select WMI 
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Update Windows Agent")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Update Windows Agent")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("Update Windows Agent")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameById("screen"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivContainsTextXpath("Currently busy with Update WMI Agent. "), 5))
        {
            if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.TdWithId("alertOK")))
            {
                error = "inable to wait for the error message xpath";
                return false;
            }
            
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("alertOK")))
            {
                error = "Unable to wait for the error message xpath xpath to be visible";
                return false;
            }
            
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("alertOK")))
            {
                error = "Unable to wait for the error message xpath to be clickable";
                return false;
            }
            
            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("alertOK")))
            {
                error = "Unable to click on the error message ok button";
                return false;
            }
            System.out.println("Update WMI agent is not working, skipping this step");
            return true;
        }
        if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivContainsTextXpath("Invalid status code structure"), 5))
        {
            if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.TdWithId("alertOK")))
            {
                error = "inable to wait for the error message xpath";
                return false;
            }
            
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("alertOK")))
            {
                error = "Unable to wait for the error message xpath xpath to be visible";
                return false;
            }
            
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("alertOK")))
            {
                error = "Unable to wait for the error message xpath to be clickable";
                return false;
            }
            
            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("alertOK")))
            {
                error = "Unable to click on the error message ok button";
                return false;
            }
            
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Tasks")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Tasks")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Tasks")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            //select Telnet 
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Interrogate")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Interrogate")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            SeleniumDriverInstance.pause(500);
            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("Interrogate")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            int stopCounter = 0;
            //boolean itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Q"),1) && SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("B"),1);
            boolean itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation("WMI_Completed"),1) || SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation2("WMI_Completed"),1);
            while(itemsBusy)
            {
                SeleniumDriverInstance.pause(1000);
                //wait maximum of 300 seconds for busy items to resolve
                if(stopCounter > 1800)
                {               
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation("WMI_Completed"),1) || SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.workListValidation2("WMI_Completed"),1);
                //itemsBusy = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Q"),1) && SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("B"),1);
                stopCounter++;
            }
            
            UpdateWMIAgents();
        }

        if (!CustomMenuNavigation("View", "Win Agent Updates"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
  
//        if (!SeleniumDriverInstance.switchToDefaultContent())
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.WinAgentUpdatesFrame(),"toolbar"))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown"), "Host Name"))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//
//        //if (!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtFilterTextbox"), testData.getData("WindowsHostToTest1")))
//        if (!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtFilterTextbox"), testData.getData("WinAgentsUpdateFilter")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnGo")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
     if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.WinAgentUpdatesFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
     //waits for all to equal doneFHosT
        List <WebElement> winAgentUpdateStatus;
        try
        {
            winAgentUpdateStatus = SeleniumDriverInstance.Driver.findElements(By.xpath(ConfigConsolePageObjects.ColumnNumXpath("3")));
        }
        catch(Exception e)
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        //refreshes until values appear. should be "queued"
        Integer checkWMIUpdate = 0;
        while((winAgentUpdateStatus.isEmpty()) || (checkWMIUpdate < 120))
        {
            if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.WinAgentUpdatesFrame(), "toolbar"))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithNameXpath("viewWinLog_btnGo")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            SeleniumDriverInstance.pause(1000);//so that the refresh button doesnt get spammed

            if(!SeleniumDriverInstance.switchToDefaultContent())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.WinAgentUpdatesFrame()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            try
            {
                winAgentUpdateStatus = SeleniumDriverInstance.Driver.findElements(By.xpath(ConfigConsolePageObjects.ColumnNumXpath("3")));
                if (!winAgentUpdateStatus.isEmpty())
                {
                    checkWMIUpdate = 130;
                }
                checkWMIUpdate++;
            }
            catch(Exception e)
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
        }

        try
        {
            winAgentUpdateStatus = SeleniumDriverInstance.Driver.findElements(By.xpath(ConfigConsolePageObjects.ColumnNumXpath("3")));
        }
        catch(Exception e)
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (winAgentUpdateStatus.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //waits for all to equal done
        for(int i = 0; i < winAgentUpdateStatus.size(); i++)
        {
            int timeOut = 0;
            while(!winAgentUpdateStatus.get(i).getText().contains("Done")&&timeOut<60)
            {
                if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.WinAgentUpdatesFrame(), "toolbar"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithNameXpath("viewWinLog_btnGo")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                SeleniumDriverInstance.pause(1000);//so that the refresh button doesnt get spammed
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.WinAgentUpdatesFrame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                try
                {
                    winAgentUpdateStatus = SeleniumDriverInstance.Driver.findElements(By.xpath(ConfigConsolePageObjects.ColumnNumXpath("3")));
                }
                catch(Exception e)
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                timeOut++;  
            }   
            if(!winAgentUpdateStatus.get(i).getText().contains("Done")&&timeOut>=60)
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
        }
        
        return true;
       
    }
    
    public boolean closeAllTabs()
    {
        if (!SeleniumDriverInstance.switchToDefaultContent())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            //extract number of open tabs
            List<WebElement> visibleTabs;
            try
            {
                visibleTabs = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.tabName()));
            }
            catch (Exception e)
            {
                System.out.println("[ERROR] - " + e.getMessage());
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            //start at position 2 - skip 'Home'
            for (int i = 0; i < visibleTabs.size(); i++)
            {
                String nameOfTab = visibleTabs.get(i).getText();

                if(!nameOfTab.equals("Home"))
                {
                    if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.removeTabxpath(nameOfTab)))
                    {
                        tryCounter++;
                        System.out.println(tryCounter);
                        return false;
                    }

                    SeleniumDriverInstance.pause(1000);
                }

            }
        
        return true;
    }
            
    public boolean CustomMenuNavigation(String Name1, String Name2)
    {
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to navigate the menus");
            return true;
        }
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        //Using a pause to give the menue bar enough time to expand
        //Helps with stability using firefox and Chrome
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.AWithTextXpath(Name1)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AWithTextXpath(Name1)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AWithTextXpath(Name1)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
            if (!SeleniumDriverInstance.clickElementByXpathFireFox(SintelligentPageObject.AWithTextXpath(Name1)))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
           }   
        }
        else
        {
            if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AWithTextXpath(Name1)))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            } 
        }
       

        //Using a pause to give the menue bar enough time to expand
        //Helps with stability using firefox and Chrome
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.AWithTextXpath(Name2)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AWithTextXpath(Name2)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AWithTextXpath(Name2)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
            if (!SeleniumDriverInstance.clickElementByXpathFireFox(SintelligentPageObject.AWithTextXpath(Name2)))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
           }   
        }
        else
        {
            if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AWithTextXpath(Name2)))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            } 
        }
        
        return true;
    }
    
    public boolean PerformFaultRuleImport(String pathToFileDirectory)
    {
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to Import the Fault Rules successfully");
            return true;
        }
        
         //clear the Fault Rules
        APICallUtility api = new APICallUtility();
        try
        {
            String apiUrl = "http://" + currentEnvironment.Ip + "/sintrex_api/api/test/query";
            api.sendPost(apiUrl,
                "{\n" +
                "\"type\":\"DELETE\",\n" +
                "\"statement\":\"DELETE FROM Fault.GlobalRule\"\n" +
                "}");
        } 
        catch (Exception ex)
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!CustomMenuNavigation("Rules and Templates", "Fault Rules"))
        {
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Configure")))
            {
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Operational Management")))
                {
                    if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
                    {
                        tryCounter++;
                        System.out.println(tryCounter);
                        return false;
                    }

                    if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Operational Management")))
                    {
                        tryCounter++;
                        System.out.println(tryCounter);
                        return false;
                    }
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Configure")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
            }

            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if (!CustomMenuNavigation("Rules and Templates", "Fault Rules"))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
        }
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.FaultRulesFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(PerformanceRulesPageObject.ColumnHeader("Description")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(PerformanceRulesPageObject.ColumnHeader("Description")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.rightClickElementByXpath(PerformanceRulesPageObject.ColumnHeader("Description")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(PerformanceRulesPageObject.ImportTemplate()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(PerformanceRulesPageObject.ImportTemplate()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
            if(!SeleniumDriverInstance.clickElementByXpathFireFox(PerformanceRulesPageObject.ImportTemplate()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
        }
        else
        {
            if(!SeleniumDriverInstance.clickElementByXpath(PerformanceRulesPageObject.ImportTemplate()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
        }
        
        SeleniumDriverInstance.pause(5000);
         //switches into edit frame 
        if(!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("pathFrom")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.InputWithIdXpath("pathFrom")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        String test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithIdXpath("pathFrom"), pathToFileDirectory+testData.getData("FaultRulesCSVName")+".csv");
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Total Number of Errors: 0")))
        {
            tryCounter = 10000;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
            //switch iframes
            if(!SeleniumDriverInstance.switchToDefaultContent())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            SeleniumDriverInstance.pause(2500);


            //extract number of open tabs
            List<WebElement> visibleTabs;
            try
            {
                visibleTabs = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.tabName()));
            }
            catch (Exception e)
            {
                System.out.println("[ERROR] - " + e.getMessage());
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            //start at position 2 - skip 'Home'
            for (int i = 0; i < visibleTabs.size(); i++)
            {
                String nameOfTab = visibleTabs.get(i).getText();

                if(!nameOfTab.equals("Home") && !nameOfTab.equals("Config Console"))
                {
                    if (!SeleniumDriverInstance.clickElementByXpathFireFox(SintelligentPageObject.removeTabxpath(nameOfTab)))
                    {
                        tryCounter++;
                        System.out.println(tryCounter);
                        return false;
                    }

                    SeleniumDriverInstance.pause(1000);
                }

            }
        }
        
        return true;
    }
    
    //Import the Performance Rules
    public boolean PerformPerformanceRuleImport(String pathToFileDirectory)
    {
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to Import the Performance Rules successfully");
            return true;
        }
        
        //clear the Performance Rules
        APICallUtility api = new APICallUtility();
        try
        {
            String apiUrl = "http://" + currentEnvironment.Ip + "/sintrex_api/api/test/query";
            api.sendPost(apiUrl,
                "{\n" +
                "\"type\":\"DELETE\",\n" +
                "\"statement\":\"DELETE FROM Performance.GlobalRule\"\n" +
                "}");
        } 
        catch (Exception ex)
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
//        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Performance Rules")))
//        {
//            if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
//            {
//                tryCounter++;
//                System.out.println(tryCounter);
//                return false;
//            }
//            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Performance Rules")))
//            {
//                tryCounter++;
//                System.out.println(tryCounter);
//                return false;
//            }
//        }
        if(!RefreshPage())
       {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
       }
        if (!CustomMenuNavigation("Rules and Templates", "Performance Rules"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
         if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PerformanceRulesFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(PerformanceRulesPageObject.ColumnHeader("Description")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(PerformanceRulesPageObject.ColumnHeader("Description")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.rightClickElementByXpath(PerformanceRulesPageObject.ColumnHeader("Description")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(PerformanceRulesPageObject.ImportTemplate()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(PerformanceRulesPageObject.ImportTemplate()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
            if(!SeleniumDriverInstance.clickElementByXpathFireFox(PerformanceRulesPageObject.ImportTemplate()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
        }
        else
        {
            if(!SeleniumDriverInstance.clickElementByXpath(PerformanceRulesPageObject.ImportTemplate()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
        }
        
        
          SeleniumDriverInstance.pause(5000);
         //switches into edit frame 
        if(!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("pathFrom")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.InputWithIdXpath("pathFrom")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        String test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithIdXpath("pathFrom"), pathToFileDirectory+testData.getData("PerformanceRulesCSVName")+".csv");
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Total Number of Rules:"), 5))
        {
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }      
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
          if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
            //switch iframes
            if(!SeleniumDriverInstance.switchToDefaultContent())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            SeleniumDriverInstance.pause(2500);


            //extract number of open tabs
            List<WebElement> visibleTabs;
            try
            {
                visibleTabs = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.tabName()));
            }
            catch (Exception e)
            {
                System.out.println("[ERROR] - " + e.getMessage());
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            //start at position 2 - skip 'Home'
            for (int i = 0; i < visibleTabs.size(); i++)
            {
                String nameOfTab = visibleTabs.get(i).getText();

                if(!nameOfTab.equals("Home") && !nameOfTab.equals("Config Console"))
                {
                    if (!SeleniumDriverInstance.clickElementByXpathFireFox(SintelligentPageObject.removeTabxpath(nameOfTab)))
                    {
                        tryCounter++;
                        System.out.println(tryCounter);
                        return false;
                    }

                    SeleniumDriverInstance.pause(1000);
                }

            }
        }
        
        return true;
    }
    
    //import user list
    public boolean importUserList(String pathToFileDirectory)
    {
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
//        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Administration")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("User Access")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.userMenuTab()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.UserMaintenaceFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.rightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        //switches into edit frame 
        if(!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        String test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithIdXpath("pathFrom"), pathToFileDirectory+testData.getData("userListImport")+".csv");
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Total Number of Errors: 0"),5))
        {
            tryCounter = 10000;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.UserMaintenaceFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        List <WebElement> userList;
        
        try 
        {
            userList = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsId("CellId_2")));
            
        } 
        catch (Exception e) 
        {
            tryCounter++;
            System.out.println(tryCounter);
            System.out.println(e);
            return false;
        }
        
        for (int i = 0; i < userList.size(); i++) 
        {
            String findUserDvt = userList.get(i).getText();
            
            if(findUserDvt.equals("dvt"))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath(findUserDvt)))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Edit")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPassword"), testData.getData("dvtUserPass")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtConfirmPassword"), testData.getData("dvtUserPass")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                i = userList.size() + 1;
            }
        }
        
//        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
        
        return true;
    }
    
    public boolean importCategoryTypes(String pathToFileDirectory)
    {
        if (!CustomMenuNavigation("Metadata", "Category Types"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.categoryMaintenance()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.rightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        //switches into edit frame 
        if(!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        String test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithIdXpath("pathFrom"), pathToFileDirectory+testData.getData("categoryTypeImport")+".csv");
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Total Number of Errors: 0"), 5))
        {
            tryCounter = 10000;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        return true;
    }
    
    public boolean enableMenuItem()
    {
        if (!CustomMenuNavigation("User Access", "Menu Items"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
           

        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.menuItemFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        } 
        
        
        
                String filter =SeleniumDriverInstance.filteringMethodFrameSwitchingByFrameNamesWaitForLoader(
                 "Menu Name"
                ,"Config Console"
                ,"1"
                ,SintelligentPageObject.SelectWithNameXpath("menuItemsMaintenance_cboFilterDropdown")
                ,SintelligentPageObject.InputWithName("menuItemsMaintenance_txtFilterTextbox")
                ,SintelligentPageObject.ImgWithNameXpath("menuItemsMaintenance_btnGo")
                ,SintelligentPageObject.menuItemFound()
                ,true
                ,SintelligentPageObject.menuItemFrame()
                ,SintelligentPageObject.ToolbarFrame()
                ,SintelligentPageObject.menuItemFrame()
                ,SintelligentPageObject.workSpaceFrameName());
                
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.menuItemFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }        
        
        if (filter != "") 
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.menuItemFound()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        
        if(SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.ImgWithSrc("resources/images/checkboxNoChk.png"), 5))
        {
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.menuItemFound()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.doubleClickElementbyActionXpath(SintelligentPageObject.menuItemFound()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }  

            if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.checkActiveMenu()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
        }
        
        System.out.println("Menu Item 'Config Console' is active");
        
        return true;
    }
    
    public boolean createDir()
    {
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.adminReportMenu()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Report Manager Directories")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
//        if (!CustomMenuNavigation("Report", "Report Manager Directories"))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.reportManagerDirectoryFrame(), SintelligentPageObject.workSpaceFrameName()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanWithTextXpath(testData.getData("reportManagerDir")), 5))
        {
            if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDirectory"), testData.getData("reportManagerDir")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            System.out.println("Added 'Directory' to the Report Manager Directories");
        }
        else
        {
            System.out.println("The 'Directory' already exists in Report Manager Directories");
        }
    
        return true;
    }
    
    public boolean escEmailSetup()
    {
        
        if (!CustomMenuNavigation("System", "Services Rollup"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.serviceRollupMonitorFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //Filter for the Escalation Process
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.SelectWithIdXpath("cboContext")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboContext")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboContext"), "Service"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        //enter Escalation Process into the filter textbox

        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.InputWithIdXpath("txtFilter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.InputWithIdXpath("txtFilter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtFilter"), testData.getData("SinEscEmail")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithSrc("../resources/images/filter.png")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.doubleClickElementbyActionXpath(SintelligentPageObject.TdContainsTextXpath(testData.getData("SinEscEmail"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("tabParameters")))
        {
           tryCounter++;
           System.out.println(tryCounter);
           return false; 
        }
        
//        List <WebElement> currentValue;
//        try 
//        {
//            currentValue = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsId("_CellId_2")));
//        }
//        catch (Exception e) 
//        {
//           tryCounter++;
//           System.out.println(tryCounter);
//           System.out.println(e);
//           return false;
//        }
//        
//        if(currentValue.equals(0))
//        {
//           tryCounter++;
//           System.out.println(tryCounter);
//           return false;
//        }
        
        List <WebElement> emailCategories;
        
        try 
        {
            emailCategories = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsId("_CellId_0")));
        } 
        catch (Exception e) 
        {
           tryCounter++;
           System.out.println(tryCounter);
           System.out.println(e);
           return false;
        }
        
//        for (int i = 0; i < currentValue.size(); i++) 
//        {
//            String currentValueToChange = currentValue.get(i).getText();
//            
//            if(currentValueToChange.equals("devmailauth@sintrex.com"))
//            {
//                if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.SpanContainsTextXpath(emailCategories.get(i).toString())))
//                {
//                    tryCounter++;
//                    System.out.println(tryCounter);
//                    return false;
//                }
//                if(!SeleniumDriverInstance.doubleClickElementbyActionXpath(SintelligentPageObject.SpanContainsTextXpath(emailCategories.get(i).toString())))
//                {
//                    tryCounter++;
//                    System.out.println(tryCounter);
//                    return false;
//                }
//                
//               if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtValue"), testData.getData("escForm")))
//                {
//                    tryCounter++;
//                    System.out.println(tryCounter);
//                    return false;
//                }
//                
//                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
//                {
//                    tryCounter++;
//                    System.out.println(tryCounter);
//                    return false;
//                }
//
//                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
//                {
//                    tryCounter++;
//                    System.out.println(tryCounter);
//                    return false;
//                }
//            }
//        }
        

       
        
        for (int i = 0; i < emailCategories.size(); i++) 
        {
            String catName = emailCategories.get(i).getText();
            
            if(!SeleniumDriverInstance.doubleClickElementbyActionXpath(SintelligentPageObject.SpanContainsTextXpath(catName)))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.switchToDefaultContent())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.serviceRollupMonitorFrame(), "workSpace"))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId2Frame()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(catName.equals("From"))
            {
                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("txtValue")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtValue"), testData.getData("escForm")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.serviceRollupMonitorFrame(), "workSpace"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
            }
            
           if(catName.equals("Port"))
            {
                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("txtValue")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtValue"), testData.getData("escPort")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.serviceRollupMonitorFrame(), "workSpace"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
            }
           
           if(catName.equals("SmtpServer"))
            {
                
                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("txtValue")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtValue"), testData.getData("escSMTPServer")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.serviceRollupMonitorFrame(), "workSpace"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
            }
           
           if(catName.equals("UseSMTPServerCredentials"))
            {
                
                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("txtValue")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtValue"), testData.getData("escServerCredentials")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.serviceRollupMonitorFrame(), "workSpace"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
            }
          
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
            {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        System.out.println("Sintrex Escalation Email has been set up");
        
        
        return true;
    }
    
    public boolean mapViewExtendMenu()
    {
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!CustomMenuNavigation("Operational Management", "View"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.menuView_Dashboard()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Map View")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.iframeopenLayerMapView(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("trAlarmList"), 5))
        {
           System.out.println("Menu has been expanded");
           return true;
        }
        else
        {
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithSrc("../resources/images/sliderBtn.png")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("divMap")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Set Default View")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            
            
            if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("trAlarmList"), 5))
            {
               System.out.println("Menu has been expanded");
               return true;
            }
        }
        return true;
    }
    
    public boolean knownProblemDir()
    {
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!CustomMenuNavigation("IT Service Management", "Incident Management"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Known Problems")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.KnownProblemsFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbParent")), 5))
        {
            
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.incidentEskomExpand()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbChild")), 5))
            {
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbChild"))))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivWithId("_grid_grid_RowId_0"), 5))
                {
                    List <WebElement> problem;
                    List <WebElement> fix;
                    try 
                    {
                        problem = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsId("CellId_0")));
                        fix = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsId("CellId_1")));
                    } 
                    catch (Exception e) 
                    {
                        tryCounter++;
                        System.out.println(tryCounter);
                        System.out.println(e);
                        return false;
                    }

                    if(problem.equals(0))
                    {
                        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtDescription")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtDescription"), testData.getData("knowProbDisciption")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtFix")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtFix"), testData.getData("knowProbFix")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        
                        if (SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        
                        System.out.println("SUCCESS - Added the correct text to Probelm and Fix");
                        
                        
                        
                        System.out.println("Closing open tabs");

                        if(!closeAllTabs())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        
                        SeleniumDriverInstance.pressKey(Keys.F5);

                        if (!CustomMenuNavigation("Operational Manegment", "Configure"))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Config Console")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        
                        return true;
                    }
                    
                    int i = 0;
                    for (int x = 0; x <= problem.size(); x++) 
                    {

                        String validateProblemTxt = problem.get(i).getText();
                        String validateFixTxt = fix.get(i).getText();

                        i++;

                        if(validateProblemTxt.equals(testData.getData("knowProbDisciption")))
                        {
                            System.out.println("Problem text field is correct");
                        }

                        if(validateFixTxt.equals(testData.getData("knowProbFix")))
                        {
                            System.out.println("Fix text field is correct");
                            x = problem.size() + 1;
                        }
                        
                        if(x > problem.size())
                        {
                            System.out.println("SUCCESS - Added the correct text to Probelm and Fix");
                            
                            System.out.println("Closing open tabs");
                            
                            
        
                            if(!closeAllTabs())
                            {
                                tryCounter++;
                                System.out.println(tryCounter);
                                return false;
                            }

                            SeleniumDriverInstance.pressKey(Keys.F5);

                            if (!CustomMenuNavigation("Operational Management", "Configure"))
                            {
                                tryCounter++;
                                System.out.println(tryCounter);
                                return false;
                            }

                            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Config Console")))
                            {
                                tryCounter++;
                                System.out.println(tryCounter);
                                return false;
                            }
                            
                            return true;
                        }

                    }
                }
                
                else
                {
                        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtDescription")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtDescription"), testData.getData("knowProbDisciption")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtFix")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtFix"), testData.getData("knowProbFix")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        
                        System.out.println("SUCCESS - Added the correct text to Probelm and Fix");
                        
                        System.out.println("Closing open tabs");
        
                        if(!closeAllTabs())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        SeleniumDriverInstance.pressKey(Keys.F5);

                        if (!CustomMenuNavigation("Operational Manegment", "Configure"))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Config Console")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        
                        return true;
                }
            }
            else
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbParent"))))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Create Category")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputContainsIdXpath("txtDescription")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputContainsIdXpath("txtDescription"), testData.getData("knownProbChild")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.KnownProblemsFrame(),"workSpace"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbChild"))))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtDescription")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtDescription"), testData.getData("knowProbDisciption")))
                {     
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtFix")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtFix"), testData.getData("knowProbFix")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                
                System.out.println("SUCCESS - Known Problems directories added.");
        
                System.out.println("Closing open tabs");

                if(!closeAllTabs())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                SeleniumDriverInstance.pressKey(Keys.F5);

                if (!CustomMenuNavigation("Operational Manegment", "Configure"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Config Console")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                        
                return true;
            }
        }
            

        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("treeview")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Create Category")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputContainsIdXpath("txtDescription")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputContainsIdXpath("txtDescription"), testData.getData("knownProbParent")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.KnownProblemsFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbParent"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Create Category")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputContainsIdXpath("txtDescription")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputContainsIdXpath("txtDescription"), testData.getData("knownProbChild")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        
        System.out.println("SUCCESS - Known Problems directories added.");
        
        System.out.println("Closing open tabs");
        
        if(!closeAllTabs())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pressKey(Keys.F5);
       
        if (!CustomMenuNavigation("Operational Manegment", "Configure"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        return true;
    }
    
    public boolean reportingCodePrefix()
    {
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Reporting Code Prefix")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        List<WebElement>currentReportingPrefix;
        List<WebElement>currentReportingDesc;
        try 
        {
            currentReportingPrefix = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsIdSpan("CellId_0")));
            currentReportingDesc = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsIdSpan("CellId_1")));
                    
        }
        catch (Exception e) 
        {
            tryCounter++;
            System.out.println(tryCounter);
            System.out.println(e);
            return false;
        }
        
        if(currentReportingPrefix.isEmpty())
        {
            for(int i = 1; i < 10; i++)
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix"+i)))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc"+i)))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }


                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }       

                if (!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
            }
        }

            int m = 0;
            ArrayList<String> nameOfRepPre = new ArrayList<>();
            for (int i = 0; i < currentReportingPrefix.size(); i++) 
            {
                String nameOfReportingPrefix = currentReportingPrefix.get(m).getText();
                m++;

                nameOfRepPre.add(nameOfReportingPrefix);

            }
            if(!nameOfRepPre.contains(testData.getData("prefix1")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix1")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc1")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
             if(!nameOfRepPre.contains(testData.getData("prefix2")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix2")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc2")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
             if(!nameOfRepPre.contains(testData.getData("prefix3")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix3")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc3")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
             if(!nameOfRepPre.contains(testData.getData("prefix4")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix4")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc4")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
             if(!nameOfRepPre.contains(testData.getData("prefix5")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix5")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc5")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
             if(!nameOfRepPre.contains(testData.getData("prefix6")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix6")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc6")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
             if(!nameOfRepPre.contains(testData.getData("prefix7")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix7")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc7")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
            if(!nameOfRepPre.contains(testData.getData("prefix8")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix8")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc8")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
            if(!nameOfRepPre.contains(testData.getData("prefix9")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix9")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc9")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
            if(!nameOfRepPre.contains(testData.getData("prefix10")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix10")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc10")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
            
        try 
        {
            currentReportingPrefix = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsIdSpan("CellId_0")));
            
                    
        }
        catch (Exception e) 
        {
            tryCounter++;
            System.out.println(tryCounter);
            System.out.println(e);
            return false;
        }

        int z = 0;
        int y = 1;
        for (int x = 0; x < currentReportingPrefix.size(); x++) 
        {
            
            String nameOfReportingPrefix = currentReportingPrefix.get(z).getText();
            z++;
            
            if(nameOfReportingPrefix.equals(testData.getData("prefix1"))  || 
               nameOfReportingPrefix.equals(testData.getData("prefix2"))  || 
               nameOfReportingPrefix.equals(testData.getData("prefix3"))  ||
               nameOfReportingPrefix.equals(testData.getData("prefix4"))  || 
               nameOfReportingPrefix.equals(testData.getData("prefix5"))  ||
               nameOfReportingPrefix.equals(testData.getData("prefix6"))  || 
               nameOfReportingPrefix.equals(testData.getData("prefix7"))  || 
               nameOfReportingPrefix.equals(testData.getData("prefix8"))  ||  
               nameOfReportingPrefix.equals(testData.getData("prefix9"))  || 
               nameOfReportingPrefix.equals(testData.getData("prefix10")) )
            {
                System.out.println("Reporting code - " + testData.getData("prefix"+y) +" is present");
                SeleniumDriverInstance.pause(800);
                y++;
            }
            else
            {
                        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix"+x)))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc"+x)))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }        
            }
        }
        
        
        
        
        
        System.out.println("Reporting code prefix has been set up");
            
        return true;
    }
    
    public boolean setDefaultCredentials()
    {

        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!CustomMenuNavigation("System", "Defaults"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
                
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtCiClassKey"), testData.getData("CIClassKey")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtReportingCode"), testData.getData("5020PortNumber")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboTimeZoneKey"), testData.getData("TimeZone")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboAssetLocationType"), testData.getData("Assetlocation")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtWMIUsername1"), testData.getData("adminUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtWMIUsername2"), testData.getData("adminUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
      ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMPCommunity1"), testData.getData("adminUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMPCommunity1"), testData.getData("SNMPCommunity1")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMPCommunity2"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMPCommunity3"), testData.getData("monUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtWMIPassword1"), testData.getData("wmiPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtWMIPassword2"), testData.getData("wmiPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtmsgBrokerPort"), testData.getData("msgBrokerPort")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtsftpHostIP"), testData.getData("sftpHostIP")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtsftpHostPort"), testData.getData("5020PortNumber")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtWMIPassword1Confirm"), testData.getData("wmiPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtWMIPassword2Confirm"), testData.getData("wmiPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("SNMP3")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //////////////////////////////////////
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3Username1"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3Username2"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3Username3"), testData.getData("monUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3AuthPassword1"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3AuthPassword1Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3PrivPassword1"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3PrivPassword1Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3AuthPassword2"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3AuthPassword2Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3PrivPassword2"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3PrivPassword2Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3AuthPassword3"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3AuthPassword3Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3PrivPassword3"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3PrivPassword3Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboSNMP3AuthProtocol1"), "MD5"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboSNMP3PrivProtocol1"), "AES"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboSNMP3AuthProtocol2"), "MD5"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboSNMP3PrivProtocol2"), "AES"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Telnet / SSH")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtTelnetUsername1"), testData.getData("monUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSSHUsername1"), testData.getData("monUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtTelnetUsername2"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSSHUsername2"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtEnablePassword1"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtTelnetPassword1"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSSHPassword1"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtEnablePassword2"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtTelnetPassword2"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSSHPassword2"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtEnablePassword1Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtTelnetPassword1Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSSHPassword1Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtEnablePassword2Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtTelnetPassword2Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSSHPassword2Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Service Credentials")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableDoubleClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("SNMP Read Community"), ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtPassword"), testData.getData("SNMPCommunity1")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("nextpage")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableDoubleClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("SSH"), ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtUserName"), testData.getData("monUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtPassword"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtConfirmPassword"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableDoubleClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Telnet"), ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtUserName"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtPassword"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtConfirmPassword"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableDoubleClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Win32 Agent Port"), ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtUserName"), testData.getData("winAgentPort")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Escalation Defaults")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboEmailDefaultService"), testData.getData("SinEscEmail")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboSMSDefaultService"), testData.getData("SMSDefaultService")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboTypeOfSMS"), testData.getData("TypeOfSMS")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboSMSProvider"), testData.getData("SMSProvider")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Distributed Pollers")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Asset")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputWithIdXpath("txtExternalAssetIp"), testData.getData("ExternalAssetIp")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputWithIdXpath("txtAssetPort"), testData.getData("AssetPort")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Container Auto Numbering")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        List <WebElement> currentContainerAutoNumbering;
        try 
        {
            currentContainerAutoNumbering = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.defaultContainerAutoNumbering("_CellId_2")));
        } 
        catch (Exception e) 
        {
            tryCounter++;
            System.out.println(tryCounter);
            System.out.println(e);
            return false;
        }
        
                String containterType0 = "2000002 - Physical Locations";
                String idPrefex0 = "GHTest";
                String startAt0 = "01010";
                
                String containterType1 = "2000003 - Regions";
                String idPrefex1 = "14987";
                String startAt1 = "2";
                
                String containterType2 = "2000007 - Countries";
                String idPrefex2 = "test";
                String startAt2 = "999";
                
                String containterType3 = "2000012 - Continent";
                String idPrefex3 = "";
                String startAt3 = "12";
        
        if(currentContainerAutoNumbering.size() == 0)
        {     
            for (int i = 0; i < 4; i++) 
            {

                String containerToEnter = "";
                String idPrefexToEnter = "";
                String StartAtToEnter = "";
                
                if( i == 0 )
                {
                    containerToEnter = containterType0;
                    idPrefexToEnter = idPrefex0;
                    StartAtToEnter = startAt0;        
                            
                }
                
                if( i == 1 )
                {
                    containerToEnter = containterType1;
                    idPrefexToEnter = idPrefex1;
                    StartAtToEnter = startAt1;        
                            
                }
                
                if( i == 2 )
                {
                    containerToEnter = containterType2;
                    idPrefexToEnter = idPrefex2;
                    StartAtToEnter = startAt2;        
                            
                }
                
                if( i == 3 )
                {
                    containerToEnter = containterType3;
                    idPrefexToEnter = idPrefex3;
                    StartAtToEnter = startAt3;        
                            
                }
                
                
                
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivContainsId("_grid_plocNumberGrid")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("ciClassKey"), containerToEnter))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("idPrefix"), idPrefexToEnter))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("startAt"), StartAtToEnter))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                    
            }
            
        }
        else
        {
            for (int i = 0; i < currentContainerAutoNumbering.size(); i++) 
            {
                String validateContainer = currentContainerAutoNumbering.get(i).getText();
                
                if(validateContainer.equals(startAt0) || validateContainer.equals(startAt1) || validateContainer.equals(startAt2) || validateContainer.equals(startAt3))
                {
                    System.out.println("Container"+i+" Auto Numbering is correct");
                }
                
                else
                {
                    for (i = 0; i < 4; i++) 
                    {


                        String containerToEnter = "";
                        String idPrefexToEnter = "";
                        String StartAtToEnter = "";

                        if( i == 0 )
                        {
                            containerToEnter = containterType0;
                            idPrefexToEnter = idPrefex0;
                            StartAtToEnter = startAt0;        

                        }

                        if( i == 1 )
                        {
                            containerToEnter = containterType1;
                            idPrefexToEnter = idPrefex1;
                            StartAtToEnter = startAt1;        

                        }

                        if( i == 2 )
                        {
                            containerToEnter = containterType2;
                            idPrefexToEnter = idPrefex2;
                            StartAtToEnter = startAt2;        

                        }

                        if( i == 3 )
                        {
                            containerToEnter = containterType3;
                            idPrefexToEnter = idPrefex3;
                            StartAtToEnter = startAt3;        

                        }



                        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivContainsId("_grid_plocNumberGrid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("ciClassKey"), containerToEnter))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("idPrefix"), idPrefexToEnter))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("startAt"), StartAtToEnter))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                    }   
                }
            }
        }
                        if(!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
//        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Earth Station")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//        
//        
//        
//        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
        
        
        System.out.println("Finished - Default credentials in now entered.");
        return true;
    }
    
    public boolean checkSintLicence(String CSVLocation)
    {
        
        int i = 0;
        
        if(currentEnvironment == Enums.Environment.AutomationTesting1_131)
        {
            i = 1;
        }
        
        if(currentEnvironment == Enums.Environment.AutomationTesting2_132)
        {
            i = 2;
        }
        
        if(currentEnvironment == Enums.Environment.AutomationTesting3_133)
        {
            i = 3;
        }
        
        if(currentEnvironment == Enums.Environment.SSDPAutomation1_63)
        {
            i = 4;
        } 
        
        if(currentEnvironment == Enums.Environment.SSDPAutomation2_71)
        {
            i = 5;
        }         
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        List<WebElement>menuList;
        try 
        {
            menuList = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.LiWithClassXpath("dcjq-parent-li")));
        } 
        catch (Exception e) 
        {
            tryCounter++;
            System.out.println(tryCounter);
            System.out.println(e);
            return false;
        }
        
        if(menuList.size() != 6)
        {
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Administration")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("System")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Build")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.switchToDefaultContent())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.BuildPageIFrame(),"workSpace"))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Import")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            String test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithIdXpath("pathFrom"), CSVLocation+testData.getData("licence"+i)+".zip");
            if(!test.isEmpty())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
        }
        
        
        return true;
    }
    
    public boolean importLanguageMetaData(String CSVLocation)
    {
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!CustomMenuNavigation("Administration", "Metadata"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Metadata Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.iframeimportMetadata(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //upload the csv file
        String test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithIdXpath("pathFrom"), CSVLocation+testData.getData("LangueageMetadata")+".zip");
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        for(int i = 0; i < 10; i++)
        {
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithTextXpath("Import Done"), 5))
            {
                i = 0;
            }
            else
            {
                i = 11;
            }
        }
        
        System.out.println("Language metadata imported.");
        return true;
    }
    
    public boolean checkAlarmStatus()
    {
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnAlarmViewer")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnAlarmViewer")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnAlarmViewer")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnAlarmViewer")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrame(), SintelligentPageObject.workSpaceFrameName()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.GridFrameFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        List <WebElement> alarmList;
        
        alarmList = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.rebuildAlarms()));
        
        int counter = 0;
        
        while(alarmList.isEmpty() || counter == 180)
        {
            SeleniumDriverInstance.pause(5000);
            
            if(!SeleniumDriverInstance.switchToDefaultContent())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnGo")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnGo")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnGo")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnGo")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if (!SeleniumDriverInstance.switchToDefaultContent())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrame()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.AlarmViewerFrame(), SintelligentPageObject.workSpaceFrameName()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.GridFrameFrame()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            alarmList = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.rebuildAlarms()));
            
            SeleniumDriverInstance.pause(2000);
            
            counter ++;
        }
        
        
        return true;
    }
    
     public boolean ValidatePage2()
    {
        if (!SeleniumDriverInstance.switchToDefaultContent()) 
        {
            error = "Failed to switch to default content";
            return false;
        }
        
        //Validate page is still logged in - left main menu button
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainMenuButtonXpath())) 
        {
            error = "Failed to wait for the main menu side bar";
            return false;
        }
        
        narrator.stepPassed("Successfully validate the correct SINTelligent page was loaded", false);
        return true;
    }
     
     public boolean NavigateToPage()
    {
        //Opens the left main menu
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainMenuButtonXpath())) 
        {
            error = "Failed to click the main menu bar";
            return false;
        }
        
        SeleniumDriverInstance.pause(100);
        
        //How many menu items it must drill down
        menuDrillDownAmount = Integer.parseInt(testData.getData("MenuDrillDownAmount"));
        //a[text()='Configure']
        SeleniumDriverInstance.clickElementByXpath("//a[text()='Operational Management']");
        for (int i = 1; i < menuDrillDownAmount + 1; i++) 
        {
            SeleniumDriverInstance.pause(100);
            
            //Expands the menu item
            if (!SeleniumDriverInstance.expandNavigationMenuItemByXpath(SintelligentPageObject.AWithTextXpath(testData.getData("Menu"+i))))
            {
                error = "Failed to click the menu item '"+testData.getData("Menu"+i)+"'";
                return false;
            }
            
            SeleniumDriverInstance.pause(100);
        }
          
        SeleniumDriverInstance.pause(100);
        
        //Switches back to the default window
        if (!SeleniumDriverInstance.switchToDefaultContent()) 
        {
            error = "Failed to switch back to the default content";
            return false;
        }
        
        narrator.stepPassed("Successfully validated the SINTelligent page loaded successfully ", false);
        return true;
    }
     
     
     public boolean RefreshPage()
     {
        SeleniumDriverInstance.Driver.navigate().refresh();
        if(!ValidatePage2())
        {
           return false; 
        }
        
        if(!NavigateToPage())
        {
            return false;
        }
        ValidatePage();
        return true;
     }
     
    public boolean addPrefex()
    {
        return true;
    }
    
}