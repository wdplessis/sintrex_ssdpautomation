/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.reportDirectory;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.SintelligentPageObject;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;


/**
 *
 * @author Reinhardt
 */
public class Initialbuildstart extends BaseClass
{

    Narrator narrator;
    boolean isLoading = false;
    String error = "";
    String downloadfilePath = System.getProperty("user.home") + "\\Downloads\\";
    String outputDir = "C:\\sintrex_keyworddriventestautomation\\" + reportDirectory;
    String ReportName = "";
    
    String CSVLocation =System.getProperty("user.dir")+"\\TestDependencies\\Startup Imports\\";
    String unzippedFilePath = "";
    
    int tryCounter = 0;

    int maxTryCounter = 0;
    
    int maxTryCounter2 = 10;
    int menuDrillDownAmount;
    boolean test = false;
    
    public Initialbuildstart(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }
    
    public TestResult executeTest()
    {
        if (!ValidatePage())
        {
            return narrator.testFailed("Failed to validate the correct SINTelligent page was loaded - " + error, false);
        }
        
        /*System.out.println("Importing language Metadata");
        test = importLanguageMetaData(CSVLocation);
        while(!test && (tryCounter < maxTryCounter))
        {
            test = importLanguageMetaData(CSVLocation);
        }
        tryCounter = 0;
        */
        //set up default credentials
        System.out.println("Setting up default credentials...");
        test = setDefaultCredentials();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = setDefaultCredentials();
        }
        tryCounter = 0;
        
        //Enable Config console Menu Item
        System.out.println("Enable Config Console menu Item...");
        test = enableMenuItem();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = enableMenuItem();
        }
        tryCounter = 0;
        
        //Report manager - Add directory 
        
        System.out.println("Create Report Manager Directory - 'Directory'");
        test = createDir();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = createDir();
        }
        tryCounter = 0;
        
        //Add Reporting code prefix 
        System.out.println("Set Reporting codes...");
        test = reportingCodePrefix();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = reportingCodePrefix();
        }
        tryCounter = 0;
        
        //Services Rollup - Setup Escalation Email 
        System.out.println("Set up email settings - Sintrex Escalation Email...");
        test = escEmailSetup();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = escEmailSetup();
        }
        tryCounter = 0;
        
        
        
        
        //******************************************************************************
        //currently being blocked by KB-401
        //****************************************************************************** 
        //Import user list
        System.out.println("Importing User List...");
        test = importUserList(CSVLocation);
        while (!test && (tryCounter < maxTryCounter))
        {
            test = importUserList(CSVLocation);
        }
        tryCounter = 0;
        
        
        //******************************************************************************
        //currently being blocked by KB-402
        //******************************************************************************
        //Import Category Types
//        System.out.println("Importing Category Types...");
//        test = importCategoryTypes(CSVLocation);
//        while (!test && (tryCounter < maxTryCounter))
//        {
//            test = importCategoryTypes(CSVLocation);
//        }
//        tryCounter = 0;
        
        //extend map view menu
        System.out.println("Extend map view menu...");
        test = mapViewExtendMenu();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = mapViewExtendMenu();
        }
        tryCounter = 0;
        
        
        //Add Incidents known problems
        
        System.out.println("Add Known Problems directories...");
        test = knownProblemDir();
        while (!test && (tryCounter < maxTryCounter))
        {
            test = knownProblemDir();
        }
        tryCounter = 0;
         
        return narrator.finalizeTest("Successfully Exported and validated the CSV in the Custom Context of the Alarm Viewer", false, testData);

    }
    
    public boolean ValidatePage()
    {
        //Validate the main page is active with the menu button
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainMenuButtonXpath())) 
        {
            error = "Failed to wait for the main menu side bar";
            return false;
        }

        //switch to default context
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to the default context";
            return false;
        }
        //switches to the Config Console iframe
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame(), "workSpace"))
        {
            if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ConsoleMaintenanceFrame(), "workSpace"))
            {
                error = "Could not switch to the '"+SintelligentPageObject.ConsoleMaintenanceFrame()+"'";
                return false;
            }

            
            
        }
        
        //wait for system services status to be visible
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithClass("contextDivTd"),10))
        {
            
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivWithId("treeviewLeft")))
            {
                error = "Failed to wait for the 'System Service Status' to be visible";
                return false;
            }

        }
    
        return true;
    }
    
    public boolean importLanguageMetaData(String CSVLocation)
    {
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!CustomMenuNavigation("Administration", "Metadata"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Metadata Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.iframeimportMetadata(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //upload the csv file
        String test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithIdXpath("pathFrom"), CSVLocation+testData.getData("LangueageMetadata")+".zip");
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        for(int i = 0; i < 10; i++)
        {
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithTextXpath("Import Done"), 5))
            {
                i = 0;
            }
            else
            {
                i = 11;
            }
        }
        
        System.out.println("Language metadata imported.");
        return true;
    }
    
    public boolean setDefaultCredentials()
    {

        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!CustomMenuNavigation("Administration", "System"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Defaults")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
                
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtCiClassKey"), testData.getData("CIClassKey")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
//        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtReportingCode"), testData.getData("5020PortNumber")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboTimeZoneKey"), testData.getData("TimeZone")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboAssetLocationType"), testData.getData("Assetlocation")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtWMIUsername1"), testData.getData("adminUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtWMIUsername2"), testData.getData("adminUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
      ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMPCommunity1"), testData.getData("adminUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMPCommunity1"), testData.getData("SNMPCommunity1")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMPCommunity2"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMPCommunity3"), testData.getData("monUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtWMIPassword1"), testData.getData("wmiPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtWMIPassword2"), testData.getData("wmiPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtmsgBrokerPort"), testData.getData("msgBrokerPort")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtsftpHostIP"), currentEnvironment.Ip))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtsftpHostPort"), testData.getData("5020PortNumber")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtWMIPassword1Confirm"), testData.getData("wmiPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtWMIPassword2Confirm"), testData.getData("wmiPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("SNMP3")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //////////////////////////////////////
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3Username1"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3Username2"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3Username3"), testData.getData("monUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3AuthPassword1"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3AuthPassword1Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3PrivPassword1"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3PrivPassword1Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3AuthPassword2"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3AuthPassword2Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3PrivPassword2"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3PrivPassword2Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3AuthPassword3"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3AuthPassword3Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3PrivPassword3"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSNMP3PrivPassword3Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboSNMP3AuthProtocol1"), "MD5"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboSNMP3PrivProtocol1"), "AES"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboSNMP3AuthProtocol2"), "MD5"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboSNMP3PrivProtocol2"), "AES"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Telnet / SSH")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtTelnetUsername1"), testData.getData("monUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSSHUsername1"), testData.getData("monUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtTelnetUsername2"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSSHUsername2"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtEnablePassword1"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtTelnetPassword1"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSSHPassword1"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtEnablePassword2"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtTelnetPassword2"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSSHPassword2"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtEnablePassword1Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtTelnetPassword1Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSSHPassword1Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtEnablePassword2Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtTelnetPassword2Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtSSHPassword2Confirm"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Service Credentials")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableDoubleClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("SNMP Read Community"), ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtPassword"), testData.getData("SNMPCommunity1")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("nextpage")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableDoubleClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("SSH"), ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtUserName"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtPassword"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtConfirmPassword"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableDoubleClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Telnet"), ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtUserName"), testData.getData("sintrexUsername")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtPassword"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtConfirmPassword"), testData.getData("defaultPassword")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableDoubleClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Win32 Agent Port"), ""))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputContainsIdXpath("txtUserName"), testData.getData("winAgentPort")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Escalation Defaults")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboEmailDefaultService"), testData.getData("SinEscEmail")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboSMSDefaultService"), testData.getData("SMSDefaultService")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboTypeOfSMS"), testData.getData("TypeOfSMS")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboSMSProvider"), testData.getData("SMSProvider")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkUseExternalHelpdesk"),true))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(1000);
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboExternalHelpDesk"), testData.getData("HelpDeskType")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkUpdateHDNewIncident"),true))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkUpdateHDAlarmNotes"),true))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionByXpath(SintelligentPageObject.InputWithIdXpath("chkUpdateHDAlarmClose"),true))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Distributed Pollers")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Asset")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputWithIdXpath("txtExternalAssetIp"), testData.getData("ExternalAssetIp")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterElementByXpath(SintelligentPageObject.InputWithIdXpath("txtAssetPort"), testData.getData("AssetPort")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Container Auto Numbering")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        List <WebElement> currentContainerAutoNumbering;
        try 
        {
            currentContainerAutoNumbering = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.defaultContainerAutoNumbering("_CellId_2")));
        } 
        catch (Exception e) 
        {
            tryCounter++;
            System.out.println(tryCounter);
            System.out.println(e);
            return false;
        }
        
                String containterType0 = "2000002 - Physical Locations";
                String idPrefex0 = "GHTest";
                String startAt0 = "01010";
                
                String containterType1 = "2000003 - Regions";
                String idPrefex1 = "14987";
                String startAt1 = "2";
                
                String containterType2 = "2000007 - Countries";
                String idPrefex2 = "test";
                String startAt2 = "999";
                
                String containterType3 = "2000012 - Continent";
                String idPrefex3 = "";
                String startAt3 = "12";
        
        if(currentContainerAutoNumbering.size() == 0)
        {     
            for (int i = 0; i < 4; i++) 
            {

                String containerToEnter = "";
                String idPrefexToEnter = "";
                String StartAtToEnter = "";
                
                if( i == 0 )
                {
                    containerToEnter = containterType0;
                    idPrefexToEnter = idPrefex0;
                    StartAtToEnter = startAt0;        
                            
                }
                
                if( i == 1 )
                {
                    containerToEnter = containterType1;
                    idPrefexToEnter = idPrefex1;
                    StartAtToEnter = startAt1;        
                            
                }
                
                if( i == 2 )
                {
                    containerToEnter = containterType2;
                    idPrefexToEnter = idPrefex2;
                    StartAtToEnter = startAt2;        
                            
                }
                
                if( i == 3 )
                {
                    containerToEnter = containterType3;
                    idPrefexToEnter = idPrefex3;
                    StartAtToEnter = startAt3;        
                            
                }
                
                
                
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivContainsId("_grid_plocNumberGrid")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("ciClassKey"), containerToEnter))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("idPrefix"), idPrefexToEnter))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("startAt"), StartAtToEnter))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                    
            }
            
        }
        else
        {
            for (int i = 0; i < currentContainerAutoNumbering.size(); i++) 
            {
                String validateContainer = currentContainerAutoNumbering.get(i).getText();
                
                if(validateContainer.equals(startAt0) || validateContainer.equals(startAt1) || validateContainer.equals(startAt2) || validateContainer.equals(startAt3))
                {
                    System.out.println("Container"+i+" Auto Numbering is correct");
                }
                
                else
                {
                    for (i = 0; i < 4; i++) 
                    {


                        String containerToEnter = "";
                        String idPrefexToEnter = "";
                        String StartAtToEnter = "";

                        if( i == 0 )
                        {
                            containerToEnter = containterType0;
                            idPrefexToEnter = idPrefex0;
                            StartAtToEnter = startAt0;        

                        }

                        if( i == 1 )
                        {
                            containerToEnter = containterType1;
                            idPrefexToEnter = idPrefex1;
                            StartAtToEnter = startAt1;        

                        }

                        if( i == 2 )
                        {
                            containerToEnter = containterType2;
                            idPrefexToEnter = idPrefex2;
                            StartAtToEnter = startAt2;        

                        }

                        if( i == 3 )
                        {
                            containerToEnter = containterType3;
                            idPrefexToEnter = idPrefex3;
                            StartAtToEnter = startAt3;        

                        }



                        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivContainsId("_grid_plocNumberGrid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("ciClassKey"), containerToEnter))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("idPrefix"), idPrefexToEnter))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("startAt"), StartAtToEnter))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                    }   
                }
            }
        }
                        if(!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.DefaultMaintenaceFrame(),"workSpace"))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
//        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Earth Station")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
//        
//        
//        
//        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
        
        narrator.stepPassed("Finished - Default credentials in now entered.", false);
        System.out.println("Finished - Default credentials in now entered.");
        return true;
    }
    
    public boolean enableMenuItem()
    {
        if (!CustomMenuNavigation("User Access", "Menu Items"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
           

        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.menuItemFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        } 
        
        
        
                String filter =SeleniumDriverInstance.filteringMethodFrameSwitchingByFrameNamesWaitForLoader(
                 "Menu Name"
                ,"Config Console"
                ,"1"
                ,SintelligentPageObject.SelectWithNameXpath("menuItemsMaintenance_cboFilterDropdown")
                ,SintelligentPageObject.InputWithName("menuItemsMaintenance_txtFilterTextbox")
                ,SintelligentPageObject.ImgWithNameXpath("menuItemsMaintenance_btnGo")
                ,SintelligentPageObject.menuItemFound()
                ,true
                ,SintelligentPageObject.menuItemFrame()
                ,SintelligentPageObject.ToolbarFrame()
                ,SintelligentPageObject.menuItemFrame()
                ,SintelligentPageObject.workSpaceFrameName());
                
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.menuItemFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }        
        
        if (filter != "") 
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.menuItemFound()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        
        if(SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.ImgWithSrc("resources/images/checkboxNoChk.png"), 5))
        {
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.menuItemFound()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.doubleClickElementbyActionXpath(SintelligentPageObject.menuItemFound()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }  

            if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.checkActiveMenu()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
        }
        narrator.stepPassed("Menu Item 'Config Console' is active.", false);
        System.out.println("Menu Item 'Config Console' is active");
        
        return true;
    }
    
    public boolean createDir()
    {
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.adminReportMenu()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Report Manager Directories")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
//        if (!CustomMenuNavigation("Report", "Report Manager Directories"))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.reportManagerDirectoryFrame(), SintelligentPageObject.workSpaceFrameName()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanWithTextXpath(testData.getData("reportManagerDir")), 5))
        {
            if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDirectory"), testData.getData("reportManagerDir")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }

            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            System.out.println("Added 'Directory' to the Report Manager Directories");
        }
        else
        {
            System.out.println("The 'Directory' already exists in Report Manager Directories");
        }
    
        narrator.stepPassed("Created Report Manager Directories.", false);
        return true;
    }
    
    public boolean reportingCodePrefix()
    {
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Reporting Code Prefix")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        List<WebElement>currentReportingPrefix;
        List<WebElement>currentReportingDesc;
        try 
        {
            currentReportingPrefix = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsIdSpan("CellId_0")));
            currentReportingDesc = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsIdSpan("CellId_1")));
                    
        }
        catch (Exception e) 
        {
            tryCounter++;
            System.out.println(tryCounter);
            System.out.println(e);
            return false;
        }
        
        if(currentReportingPrefix.isEmpty())
        {
            for(int i = 1; i < 10; i++)
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix"+i)))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc"+i)))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }


                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }       

                if (!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
            }
        }
        
        
        try 
        {
            currentReportingPrefix = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsIdSpan("CellId_0")));
            currentReportingDesc = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsIdSpan("CellId_1")));
                    
        }
        catch (Exception e) 
        {
            tryCounter++;
            System.out.println(tryCounter);
            System.out.println(e);
            return false;
        }

            int m = 0;
            ArrayList<String> nameOfRepPre = new ArrayList<>();
            for (int i = 0; i < currentReportingPrefix.size(); i++) 
            {
                String nameOfReportingPrefix = currentReportingPrefix.get(m).getText();
                m++;

                nameOfRepPre.add(nameOfReportingPrefix);

            }
            if(!nameOfRepPre.contains(testData.getData("prefix1")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix1")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc1")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
             if(!nameOfRepPre.contains(testData.getData("prefix2")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix2")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc2")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
             if(!nameOfRepPre.contains(testData.getData("prefix3")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix3")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc3")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
             if(!nameOfRepPre.contains(testData.getData("prefix4")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix4")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc4")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
             if(!nameOfRepPre.contains(testData.getData("prefix5")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix5")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc5")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
             if(!nameOfRepPre.contains(testData.getData("prefix6")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix6")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc6")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
             if(!nameOfRepPre.contains(testData.getData("prefix7")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix7")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc7")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
            if(!nameOfRepPre.contains(testData.getData("prefix8")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix8")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc8")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
            if(!nameOfRepPre.contains(testData.getData("prefix9")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix9")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc9")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
            if(!nameOfRepPre.contains(testData.getData("prefix10")))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix10")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc10")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
            }
            
        try 
        {
            currentReportingPrefix = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsIdSpan("CellId_0")));
            
                    
        }
        catch (Exception e) 
        {
            tryCounter++;
            System.out.println(tryCounter);
            System.out.println(e);
            return false;
        }

        int z = 0;
        int y = 1;
        for (int x = 0; x < currentReportingPrefix.size(); x++) 
        {
            
            String nameOfReportingPrefix = currentReportingPrefix.get(z).getText();
            z++;
            
            if(nameOfReportingPrefix.equals(testData.getData("prefix1"))  || 
               nameOfReportingPrefix.equals(testData.getData("prefix2"))  || 
               nameOfReportingPrefix.equals(testData.getData("prefix3"))  ||
               nameOfReportingPrefix.equals(testData.getData("prefix4"))  || 
               nameOfReportingPrefix.equals(testData.getData("prefix5"))  ||
               nameOfReportingPrefix.equals(testData.getData("prefix6"))  || 
               nameOfReportingPrefix.equals(testData.getData("prefix7"))  || 
               nameOfReportingPrefix.equals(testData.getData("prefix8"))  ||  
               nameOfReportingPrefix.equals(testData.getData("prefix9"))  || 
               nameOfReportingPrefix.equals(testData.getData("prefix10")) )
            {
                System.out.println("Reporting code - " + testData.getData("prefix"+y) +" is present");
                SeleniumDriverInstance.pause(800);
                y++;
            }
            else
            {
                        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPrefix"), testData.getData("prefix"+x)))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtDescription"), testData.getData("desc"+x)))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }


                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }       

                        if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.PrefixMaintenanceIframeName(), SintelligentPageObject.workSpaceFrameName()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }        
            }
        }
        
        
        
        
        
        System.out.println("Reporting code prefix has been set up");
            
        return true;
    }
    
    public boolean escEmailSetup()
    {
        
        if (!CustomMenuNavigation("System", "Services Rollup"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.serviceRollupMonitorFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        //Filter for the Escalation Process
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.SelectWithIdXpath("cboContext")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboContext")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboContext"), "Service"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        //enter Escalation Process into the filter textbox

        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.InputWithIdXpath("txtFilter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.InputWithIdXpath("txtFilter")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtFilter"), testData.getData("SinEscEmail")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithSrc("../resources/images/filter.png")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.doubleClickElementbyActionXpath(SintelligentPageObject.TdContainsTextXpath(testData.getData("SinEscEmail"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("tabParameters")))
        {
           tryCounter++;
           System.out.println(tryCounter);
           return false; 
        }
        
//        List <WebElement> currentValue;
//        try 
//        {
//            currentValue = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsId("_CellId_2")));
//        }
//        catch (Exception e) 
//        {
//           tryCounter++;
//           System.out.println(tryCounter);
//           System.out.println(e);
//           return false;
//        }
//        
//        if(currentValue.equals(0))
//        {
//           tryCounter++;
//           System.out.println(tryCounter);
//           return false;
//        }
        
        List <WebElement> emailCategories;
        
        try 
        {
            emailCategories = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsId("_CellId_0")));
        } 
        catch (Exception e) 
        {
           tryCounter++;
           System.out.println(tryCounter);
           System.out.println(e);
           return false;
        }
        
//        for (int i = 0; i < currentValue.size(); i++) 
//        {
//            String currentValueToChange = currentValue.get(i).getText();
//            
//            if(currentValueToChange.equals("devmailauth@sintrex.com"))
//            {
//                if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.SpanContainsTextXpath(emailCategories.get(i).toString())))
//                {
//                    tryCounter++;
//                    System.out.println(tryCounter);
//                    return false;
//                }
//                if(!SeleniumDriverInstance.doubleClickElementbyActionXpath(SintelligentPageObject.SpanContainsTextXpath(emailCategories.get(i).toString())))
//                {
//                    tryCounter++;
//                    System.out.println(tryCounter);
//                    return false;
//                }
//                
//               if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtValue"), testData.getData("escForm")))
//                {
//                    tryCounter++;
//                    System.out.println(tryCounter);
//                    return false;
//                }
//                
//                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
//                {
//                    tryCounter++;
//                    System.out.println(tryCounter);
//                    return false;
//                }
//
//                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
//                {
//                    tryCounter++;
//                    System.out.println(tryCounter);
//                    return false;
//                }
//            }
//        }
        

       
        
        for (int i = 0; i < emailCategories.size(); i++) 
        {
            String catName = emailCategories.get(i).getText();
            
            if(!SeleniumDriverInstance.doubleClickElementbyActionXpath(SintelligentPageObject.SpanContainsTextXpath(catName)))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.switchToDefaultContent())
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.serviceRollupMonitorFrame(), "workSpace"))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId2Frame()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(catName.equals("From"))
            {
                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("txtValue")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtValue"), testData.getData("escForm")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.serviceRollupMonitorFrame(), "workSpace"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
            }
            
           if(catName.equals("Port"))
            {
                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("txtValue")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtValue"), testData.getData("escPort")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.serviceRollupMonitorFrame(), "workSpace"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
            }
           
           if(catName.equals("SmtpServer"))
            {
                
                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("txtValue")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtValue"), testData.getData("escSMTPServer")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.serviceRollupMonitorFrame(), "workSpace"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
            }
           
           if(catName.equals("UseSMTPServerCredentials"))
            {
                
                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("txtValue")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtValue"), testData.getData("escServerCredentials")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.serviceRollupMonitorFrame(), "workSpace"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
            }
          
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
            {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        System.out.println("Sintrex Escalation Email has been set up");
        
        
        return true;
    }
    
    public boolean importUserList(String pathToFileDirectory)
    {
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
//        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("Administration")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }

        SeleniumDriverInstance.pause(3000);
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AWithTextXpath("User Access")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.userMenuTab()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.UserMaintenaceFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.rightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        //switches into edit frame 
        if(!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        String test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithIdXpath("pathFrom"), pathToFileDirectory+testData.getData("userListImport")+".csv");
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Total Number of Errors: 0"),5))
        {
            tryCounter = 10000;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        System.out.println("Imported the user list");
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.UserMaintenaceFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        List <WebElement> userList;
        
        try 
        {
            userList = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsId("CellId_2")));
            
        } 
        catch (Exception e) 
        {
            tryCounter++;
            System.out.println(tryCounter);
            System.out.println(e);
            return false;
        }
        
        for (int i = 0; i < userList.size(); i++) 
        {
            String findUserDvt = userList.get(i).getText();
            
            if(findUserDvt.equals("dvt"))
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.SpanContainsTextXpath(findUserDvt)))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Edit")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtPassword"), testData.getData("dvtUserPass")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableEnterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtConfirmPassword"), testData.getData("dvtUserPass")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                i = userList.size() + 1;
            }
        }
        System.out.println("Changed the user password for DVT");
//        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
//        {
//            tryCounter++;
//            System.out.println(tryCounter);
//            return false;
//        }
        
        return true;
    }
    
    public boolean importCategoryTypes(String pathToFileDirectory)
    {
        if (!CustomMenuNavigation("Metadata", "Category Types"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.categoryMaintenance()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.rightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Import")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        //switches into edit frame 
        if(!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeIdFrame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        String test = SeleniumDriverInstance.uploadCSVNew(SintelligentPageObject.InputWithIdXpath("pathFrom"), pathToFileDirectory+testData.getData("categoryTypeImport")+".csv");
        if(!test.isEmpty())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithTextXpath("Upload")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Total Number of Errors: 0"), 5))
        {
            tryCounter = 10000;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        return true;
    }
    
     public boolean mapViewExtendMenu()
    {
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!CustomMenuNavigation("Operational Management", "View"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.menuView_Dashboard()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Map View")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.iframeopenLayerMapView(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("trAlarmList"), 5))
        {
           System.out.println("Menu has been expanded");
           return true;
        }
        else
        {
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithSrc("../resources/images/sliderBtn.png")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("divMap")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Set Default View")))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            
            
            if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("trAlarmList"), 5))
            {
               System.out.println("Menu has been expanded");
               return true;
            }
        }
        return true;
    }
     
     public boolean knownProblemDir()
    {
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!CustomMenuNavigation("IT Service Management", "Incident Management"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Known Problems")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.KnownProblemsFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbParent")), 5))
        {
            
            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.incidentEskomExpand()))
            {
                tryCounter++;
                System.out.println(tryCounter);
                return false;
            }
            
            if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbChild")), 5))
            {
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbChild"))))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivWithId("_grid_grid_RowId_0"), 5))
                {
                    List <WebElement> problem;
                    List <WebElement> fix;
                    try 
                    {
                        problem = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsId("CellId_0")));
                        fix = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.DivContainsId("CellId_1")));
                    } 
                    catch (Exception e) 
                    {
                        tryCounter++;
                        System.out.println(tryCounter);
                        System.out.println(e);
                        return false;
                    }

                    if(problem.equals(0))
                    {
                        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtDescription")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtDescription"), testData.getData("knowProbDisciption")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtFix")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtFix"), testData.getData("knowProbFix")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        
                        if (SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        
                        System.out.println("SUCCESS - Added the correct text to Probelm and Fix");
                        
                        
                        
                        System.out.println("Closing open tabs");

                        if(!closeAllTabs())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        
                        SeleniumDriverInstance.pressKey(Keys.F5);

                        if (!CustomMenuNavigation("Operational Manegment", "Configure"))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Config Console")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        
                        return true;
                    }
                    
                    int i = 0;
                    for (int x = 0; x <= problem.size(); x++) 
                    {

                        String validateProblemTxt = problem.get(i).getText();
                        String validateFixTxt = fix.get(i).getText();

                        i++;

                        if(validateProblemTxt.equals(testData.getData("knowProbDisciption")))
                        {
                            System.out.println("Problem text field is correct");
                        }

                        if(validateFixTxt.equals(testData.getData("knowProbFix")))
                        {
                            System.out.println("Fix text field is correct");
                            x = problem.size() + 1;
                        }
                        
                        if(x > problem.size())
                        {
                            System.out.println("SUCCESS - Added the correct text to Probelm and Fix");
                            
                            System.out.println("Closing open tabs");
                            
                            
        
                            if(!closeAllTabs())
                            {
                                tryCounter++;
                                System.out.println(tryCounter);
                                return false;
                            }

                            SeleniumDriverInstance.pressKey(Keys.F5);

                            if (!CustomMenuNavigation("Operational Management", "Configure"))
                            {
                                tryCounter++;
                                System.out.println(tryCounter);
                                return false;
                            }

                            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Config Console")))
                            {
                                tryCounter++;
                                System.out.println(tryCounter);
                                return false;
                            }
                            
                            return true;
                        }

                    }
                }
                
                else
                {
                        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtDescription")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtDescription"), testData.getData("knowProbDisciption")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtFix")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtFix"), testData.getData("knowProbFix")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        
                        System.out.println("SUCCESS - Added the correct text to Probelm and Fix");
                        
                        System.out.println("Closing open tabs");
        
                        if(!closeAllTabs())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        SeleniumDriverInstance.pressKey(Keys.F5);

                        if (!CustomMenuNavigation("Operational Manegment", "Configure"))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Config Console")))
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }
                        
                        return true;
                }
            }
            else
            {
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbParent"))))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Create Category")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputContainsIdXpath("txtDescription")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputContainsIdXpath("txtDescription"), testData.getData("knownProbChild")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.switchToDefaultContent())
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.KnownProblemsFrame(),"workSpace"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbChild"))))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                
                if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtDescription")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtDescription"), testData.getData("knowProbDisciption")))
                {     
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtFix")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtFix"), testData.getData("knowProbFix")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                
                
                System.out.println("SUCCESS - Known Problems directories added.");
        
                System.out.println("Closing open tabs");

                if(!closeAllTabs())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                SeleniumDriverInstance.pressKey(Keys.F5);

                if (!CustomMenuNavigation("Operational Manegment", "Configure"))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }

                if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Config Console")))
                {
                    tryCounter++;
                    System.out.println(tryCounter);
                    return false;
                }
                        
                return true;
            }
        }
            

        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("treeview")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Create Category")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputContainsIdXpath("txtDescription")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputContainsIdXpath("txtDescription"), testData.getData("knownProbParent")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.KnownProblemsFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbParent"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Create Category")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputContainsIdXpath("txtDescription")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputContainsIdXpath("txtDescription"), testData.getData("knownProbChild")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.KnownProblemsFrame(),"workSpace"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.FontContainsTextXpath(testData.getData("knownProbChild"))))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }


        if(!SeleniumDriverInstance.stableRightClickElementByXpath(SintelligentPageObject.DivWithId("_grid_grid")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Add")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId1Frame()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtDescription")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtDescription"), testData.getData("knowProbDisciption")))
        {     
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TextAreaWithId("txtFix")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.TextAreaWithId("txtFix"), testData.getData("knowProbFix")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("applyButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.TdWithId("closeButton")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        
        System.out.println("SUCCESS - Known Problems directories added.");
        
        System.out.println("Closing open tabs");
        
        if(!closeAllTabs())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        SeleniumDriverInstance.pressKey(Keys.F5);
       
        if (!CustomMenuNavigation("Operational Manegment", "Configure"))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.AContainsTextXpath("Config Console")))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        return true;
    }
    
    public boolean CustomMenuNavigation(String Name1, String Name2)
    {
        if(tryCounter > maxTryCounter)
        {
            System.out.println("Failed to navigate the menus");
            return true;
        }
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        //Using a pause to give the menue bar enough time to expand
        //Helps with stability using firefox and Chrome
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.AWithTextXpath(Name1)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AWithTextXpath(Name1)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AWithTextXpath(Name1)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AWithTextXpath(Name1)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }

        //Using a pause to give the menue bar enough time to expand
        //Helps with stability using firefox and Chrome
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.AWithTextXpath(Name2)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AWithTextXpath(Name2)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AWithTextXpath(Name2)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AWithTextXpath(Name2)))
        {
            tryCounter++;
            System.out.println(tryCounter);
            return false;
        }
        
        return true;
    }
    
    public boolean closeAllTabs()
    {
                    if (!SeleniumDriverInstance.switchToDefaultContent())
                        {
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        //extract number of open tabs
                        List<WebElement> visibleTabs;
                        try
                        {
                            visibleTabs = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.tabName()));
                        }
                        catch (Exception e)
                        {
                            System.out.println("[ERROR] - " + e.getMessage());
                            tryCounter++;
                            System.out.println(tryCounter);
                            return false;
                        }

                        //start at position 2 - skip 'Home'
                        for (int i = 0; i < visibleTabs.size(); i++)
                        {
                            String nameOfTab = visibleTabs.get(i).getText();
                            
                            if(!nameOfTab.equals("Home"))
                            {
                                if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.removeTabxpath(nameOfTab)))
                                {
                                    tryCounter++;
                                    System.out.println(tryCounter);
                                    return false;
                                }
                                
                                SeleniumDriverInstance.pause(1000);
                            }
                                                      
                        }
        
        return true;
    }
}