/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.SintelligentPageObject;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author cmkhontwana
 */
public class GridValidations extends BaseClass
{

    String error = "";
    int pages;
    int totalpages = 0;
    int currentpage = 0;
    
    ArrayList <String> webItemsListText = new ArrayList();
    List <WebElement> webItemsList;
    ArrayList <String> CollectionItemsListText = new ArrayList();
    
    
    public boolean SortAndValidateGrid(String columnName,String Listxpath)
    {
        try
        {
            if(!ClickColumn(columnName))
            {
                error = "Failed to click '"+columnName+"' column name";
                return false;
            }
            //Wait for the columnl list 
            if(!SeleniumDriverInstance.waitForElementByXpath(Listxpath))
            {
                error = "Failed to wait for '"+columnName+"' list ";
                return false;
            }
            if(!ExtractGridColumnList(Listxpath))
            {
                error = "Failed to extract '"+columnName+"' column data";
                return false;
            }
            CollectionItemsListText.clear();
            CollectionItemsListText = webItemsListText;
            if(!ClickColumn(columnName))
            {
                error = "Failed to click '"+columnName+"' column name";
                return false;
            }
             webItemsListText.clear();
            if(!ExtractGridColumnList(Listxpath))
            {
                error = "Failed to extract '"+columnName+"' column data";
                return false;
            }
            //Validate sorting 
                try
                {
                    //Sort and compare CollectionItemsListText with webItemsListText
                    Collections.sort(CollectionItemsListText);
                     for (int i = 0; i < CollectionItemsListText.size(); i++) 
                    {
                        if(!CollectionItemsListText.get(i).equals(webItemsListText.get(i)))
                        {
                            error = "Column : '"+columnName+"' - The comparison of the sorted items failed: '"+CollectionItemsListText.get(i)+"' does not equal '"+webItemsListText.get(i)+"'";
                            return false;
                        }
                    }

                }
                catch(Exception e)
                {
                    error ="Validate Sorting";
                    return false;
                }
            return true;
        }
        catch(Exception e)
        {
            error = "Failed to Sort And Validate by '"+columnName+"'";
            return false;
        }
    }
    
    public boolean ExtractListClickNextSortAndValidate(String columnName,String Listxpath)
    {
        try
        {
            if(!ClickColumn(columnName))
            {
                error = "Failed to click '"+columnName+"' column name";
                return false;
            }
            //Wait for the columnl list 
            if(!SeleniumDriverInstance.waitForElementByXpath(Listxpath))
            {
                error = "Failed to wait for '"+columnName+"' list ";
                return false;
            }
            if(!ExtractColumnListAndClickNext(Listxpath))
            {
                error = "Failed to extract '"+columnName+"' column data";
                return false;
            }
            CollectionItemsListText.clear();
            CollectionItemsListText = webItemsListText;
            if(!ClickColumn(columnName))
            {
                error = "Failed to click '"+columnName+"' column name";
                return false;
            }
             webItemsListText.clear();
            if(!ExtractColumnListAndClickNext(Listxpath))
            {
                error = "Failed to extract '"+columnName+"' column data";
                return false;
            }
            //Validate sorting 
                try
                {
                    //Sort and compare CollectionItemsListText with webItemsListText
                    Collections.sort(CollectionItemsListText);
                     for (int i = 0; i < CollectionItemsListText.size(); i++) 
                    {
                        if(!CollectionItemsListText.get(i).equals(webItemsListText.get(i)))
                        {
                            error = "Column : '"+columnName+"' - The comparison of the sorted items failed: '"+CollectionItemsListText.get(i)+"' does not equal '"+webItemsListText.get(i)+"'";
                            return false;
                        }
                    }

                }
                catch(Exception e)
                {
                    error ="Validate Sorting";
                    return false;
                }
            return true;
        }
        catch(Exception e)
        {
            error = "Failed to Sort And Validate by '"+columnName+"'";
            return false;
        }
    }
    //Click column name using ( DivWithTextXpath )
    public boolean ClickColumn(String columnName)
    {
        //click coloumnName to sort
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.DivWithTextXpath(columnName)))
        {
            error = "Could not wait for the '"+columnName+"' to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.DivWithTextXpath(columnName)))
        {
            error = "Could not wait for the '"+columnName+"' to be clickable";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.DivWithTextXpath(columnName)))
        {
            error = "Could not click the '"+columnName+"' to sort";
            return false;
        }
        return true;
    }
    public boolean ValidateGridPaging()
    {
        if(!GetPages("totalpages"))
         {
              error = "Failed to get totalpages";
              return false;
          }
          totalpages = pages;

          if(!GetPages("currentpage"))
          {
              error = "Failed to get currentpage";
              return false;
          }
          currentpage = pages;
          
          //Clicks the next button until it reach the total number of pages
          while(currentpage != totalpages)
          {
                if(!ClickPage("nextpage"))
                {
                    error = "Failed to click the nextpage";
                    return false;
                }
                if(!GetPages("currentpage"))
                {
                   error = "Failed to get currentpage";
                   return false;
                }
                currentpage = pages;
          }
          if(currentpage != totalpages)
          {
             error = "Failed the current page: "+currentpage+" is not equal to the total number of pages : "+totalpages+" after clicking the next button until it reach the total number of pages";
             return false;
          }
          
          //Clicks the previous page button until it reach the 1st page
          while(currentpage != 1)
          {
                if(!ClickPage("prevpage"))
                {
                    error = "Failed to click the prevpage";
                    return false;
                }
                if(!GetPages("currentpage"))
                {
                   error = "Failed to get currentpage";
                   return false;
                }
                currentpage = pages;
          }
          
          if(currentpage != 1)
          {
             error = "Failed the current page: "+currentpage+" is not equal to 1 after clicking the previous button until it reach the first page";
             return false;
          }
          
          //Click the last page
          if(!ClickPage("lastpage"))
          {
                error = "Failed to click the lastpage";
                return false;
          }
         if(!GetPages("currentpage"))
          {
               error = "Failed to get currentpage";
               return false;
          }
          currentpage = pages;
          //Validate that the last page is clicked
         if(currentpage != totalpages)
         {
               error = "Failed the current page: "+currentpage+" is not equal to the total number of pages : "+totalpages+" after clicking the last page button";
             return false;
         }
          
          //Click the fisrt page
          if(!ClickPage("firstpage"))
          {
                error = "Failed to click the firstpage";
                return false;
          }
         if(!GetPages("currentpage"))
          {
               error = "Failed to get currentpage";
               return false;
          }
          currentpage = pages;
          //Validate that the firstpage is clicked
         if(currentpage != 1)
         {
             error = "Failed the current page: "+currentpage+" is not equal to 1 after clicking the first page button";
             return false;
         }
        return true;
    }
    //Gets the page number
    public boolean GetPages(String page)
    {
       if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.TdWithId(page)))
        {
          error = "Failed to wait for the '"+page+"'";
          return false;
        }
        try
        {
            WebElement ele = SeleniumDriverInstance.Driver.findElement(By.xpath(SintelligentPageObject.TdWithId(page)));
            pages = Integer.parseInt(ele.getText().toString());
            
        }
        catch(Exception e)
        {
            error ="Failed to extract '"+page+"' text";
            return false;
        }
        return true; 
    }
    //Clicks the page
    public boolean ClickPage(String page)
    {
        //Wait for the page 
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.TdWithId(page)))
        {
            error = "Failed to wait for the "+page+" page button";
            return false;
        }
       //Wait for the page to be visible
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId(page)))
        {
            error = "Failed to wait for the "+page+" page button to be visible";
            return false;
        }
        //Click the apply button
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId(page)))
        {
            error = "Failed to click the "+page+" page button";
            return false;
        }
        return true;
    }
    //Extract the column list and click next button until the last page 
    public boolean ExtractColumnListAndClickNext(String Listxpath)
    { 
        try
        {
              //Click the fisrt page
              if(!ClickPage("firstpage"))
              {
                    error = "Failed to click the firstpage";
                    return false;
              }
             //Get page number
             if(!GetPages("currentpage"))
              {
                   error = "Failed to get currentpage";
                   return false;
              }
              currentpage = pages;
             if(currentpage != 1)
             {
                 error = "Failed the current page: "+currentpage+" is not equal to 1 after clicking the first page button";
                 return false;
             }
            //Get page number
             if(!GetPages("totalpages"))
              {
                  error = "Failed to get totalpages";
                  return false;
              }
              totalpages = pages;
             int count =0;
              //Clicks the next button until it reach the total number of pages
              while(currentpage <= totalpages)
              {
                    //Extract the data on the grid
                    webItemsList = SeleniumDriverInstance.Driver.findElements(By.xpath(Listxpath));
                    //Add to a string array
                    for (int i = 0; i < webItemsList.size(); i++) 
                    {
                      webItemsListText.add(webItemsList.get(i).getText().toString());
                    }
                    //Click next
                    if(!ClickPage("nextpage"))
                    {
                        error = "Failed to click the nextpage";
                        return false;
                    }
                    //Get the current page
                    if(!GetPages("currentpage"))
                    {
                       error = "Failed to get currentpage";
                       return false;
                    }
                    count++;
                    currentpage = pages;
                    if(count == totalpages)
                    {
                        return true;
                    }
              }
            return true;
        }
        catch(Exception e)
        {
            error = "Failed to extract  column list";
            return false;
        }
    }
    public boolean ExtractGridColumnList(String Listxpath)
    { 
        try
        {

            //Extract the data on the grid
            webItemsList = SeleniumDriverInstance.Driver.findElements(By.xpath(Listxpath));
             //Add to a string array
            for (int i = 0; i < webItemsList.size(); i++) 
            {
              webItemsListText.add(webItemsList.get(i).getText().toString());
            }

            return true;
        }
        catch(Exception e)
        {
            error = "Failed to extract  column list";
            return false;
        }
    }
}
