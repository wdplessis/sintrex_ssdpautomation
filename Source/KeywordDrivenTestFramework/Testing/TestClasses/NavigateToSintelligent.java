/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.SintelligentPageObject;

/**
 *
 * @author szeuch
 */
public class NavigateToSintelligent extends BaseClass 
{
    Narrator narrator;
    
    String error = "";

    public NavigateToSintelligent(TestEntity testData) 
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }
    
    public TestResult executeTest()
    {
        if(!NavigatetoHomePage())
        {
            return narrator.testFailed("Failed to navigate to the Sintelligent Homepage - " + error, false);
        }
        
        if(!LogIn())
        {
            return narrator.testFailed("Failed to enter the user credentials and log in - " + error, false);
        }
        
        if(!ValidateLogin())
        {
            return narrator.testFailed("Failed to validate the login was successful - " + error, false);
        }
        
        if (!testData.getData("InvalidUsername").equalsIgnoreCase("Parameter not found"))
        {
            return narrator.finalizeTest("Successfully navigated to the Sintelligent homepage and attempted to log in with invalid credentials", false, testData);
        }
        else
        {
            return narrator.finalizeTest("Successfully navigated to the Sintelligent homepage and logged in", false, testData);
        }
    }
    
    public boolean NavigatetoHomePage()
    {
        //Navigation to main Sintelligent URL
        if(!SeleniumDriverInstance.navigateTo(SintelligentPageObject.SintelligentURL()))
        {
            error = "Failed to navigate to the Sintelligent URL '"+SintelligentPageObject.SintelligentURL()+"'";
            return false;
        }
        
        testData.extractParameter("URL", SintelligentPageObject.SintelligentURL(), "");
        
        //Validates Username, Password and Login Button are present on the page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.UsernameTextBoxXpath())) 
        {
            error = "Failed to wait for the username text box";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.PasswordTextBoxXpath())) 
        {
            error = "Failed to wait for the password text box";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LoginButtonXpath())) 
        {
            error = "Failed to wait for the login button";
            return false;
        }
        
        narrator.stepPassedWithScreenshot("Successfully navigated to the Sintelligent login page", false);
        return true;
    }
    
    public boolean LogIn()
    {
        if (!testData.getData("InvalidUsername").equalsIgnoreCase("Parameter not found")) 
        {
            //Entering the invalid username
            if (!SeleniumDriverInstance.enterTextByXpathNew(SintelligentPageObject.UsernameTextBoxXpath(), testData.getData("InvalidUsername")))
            {
                error = "Failed to enter the invalid username '"+testData.getData("InvalidUsername")+"' into the username text box";
                return false;
            }

            //Invalid Password
            if (!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.PasswordTextBoxXpath(), testData.getData("InvalidPassword"))) 
            {
                error = "Failed to enter the invalid password '"+testData.getData("InvalidPassword")+"' into the password text box";
                return false;
            }
        }
        else
        {
            //Entering the valid username
            if (!SeleniumDriverInstance.enterTextByXpathNew(SintelligentPageObject.UsernameTextBoxXpath(), testData.getData("Username")))
            {
                error = "Failed to enter the username '"+testData.getData("Username")+"' into the username text box";
                return false;
            }

            //Valid Password
            if (!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.PasswordTextBoxXpath(), testData.getData("Password"))) 
            {
                error = "Failed to enter the password '"+testData.getData("Password")+"' into the password text box";
                return false;
            }
        }
        
        narrator.stepPassed("Successfully entered the login details", false);
        
        //Clicking the login button
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.LoginButtonXpath()))
        {
            error = "Failed to click the login button";
            return false;
        }
        
        if (!testData.getData("InvalidUsername").equalsIgnoreCase("Parameter not found")) 
        {
            if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.IncorrectLoginCredentialsXpath(),10))
            {
                if (SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainMenuButtonXpath())) 
                {
                    error = "The incorrect login details dialog was not displayed and the user was successfully logged in";
                    return false;
                }
            }
            else
            {
                narrator.stepPassedWithScreenshot("Successfully entered the invalid login details, clicked the login button and validated the incorrect login details dialog was present", false);
                
                SeleniumDriverInstance.pause(100);
                
                if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithTextXpath("OK"))) 
                {
                    error = "Failed to close the invalid login details dialog";
                    return false;
                }
                
                SeleniumDriverInstance.pause(100);
            }
        }
        else
        {
            narrator.stepPassed("Successfully entered the login details and clicked the login button", false);
        }
        
        return true;
    }
    
    public boolean ValidateLogin()
    {
        if (!testData.getData("InvalidUsername").equalsIgnoreCase("Parameter not found"))
        {
            
        }
        else
        {
            //Checks if the incorrect login details message is present
            if (SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.IncorrectLoginCredentialsXpath(), 3)) 
            {
                error = "Incorrect login details were specified";
                return false;
            }

            //Waits for the Left Main menu button
            if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainMenuButtonXpath())) 
            {
                error = "Failed to wait for the main menu side bar";
                return false;
            }

            narrator.stepPassed("Successfully validated the login process was successful", false);
        }
        
        return true;
    }
}
