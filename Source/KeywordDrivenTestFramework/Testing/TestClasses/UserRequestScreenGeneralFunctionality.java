/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.NavigateToPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.SintelligentPageObject;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.util.List;
import java.util.StringTokenizer;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
/**
 *
 * @author bkabona
 */
public class UserRequestScreenGeneralFunctionality extends BaseClass
{
    Narrator narrator;

    String error = "";
    String parentHandle ="";
    String report = "";
    String reportText = "";
    int pages;
    int totalpages = 0;
    int currentpage = 0;
    int menuDrillDownAmount;

    public UserRequestScreenGeneralFunctionality(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }

    public TestResult executeTest()
    {
        if(!ValidatePage())
        {
            return narrator.testFailed("Failed to validate the correct SINTelligent page was loaded - " + error, false);
        }

        //perform actions on a host to generate data for User Request screen
        if(!TestStartup())
        {
            return narrator.testFailed("Failed to perform test startup by performing host interrogation, ping, SNMP and creating a baseline to ensure there is relevant data in the User Request screen - " + error, false);
        }
        
        // Navigate to User Request Screen
        if(!NavigateToUserRequestScreen())
        {
            return narrator.testFailed("Failed to Navigate to User Request Screen - " + error, false);
        }
        
        // Test 'Refresh Time' dropdown list functionality
        if(!TestDropdownList("cboRefreshDropdown", "Refresh Time"))
        {
            return narrator.testFailed("Failed to test 'Refresh Time' dropdown list functionality - " + error, false);
        }

        // Test 'Users' dropdown list functionality
        if(!TestDropdownList("cboUsersDropdown", "Users"))
        {
            return narrator.testFailed("Failed to test 'Users' dropdown list functionality - " + error, false);
        }

        // Test 'filtering by time' functionality
        if(!TestFilterByTime())
        {
            return narrator.testFailed("Failed to test 'filtering by time' functionality - " + error, false);
        }

        if(!TestExpandButton())
        {
            return narrator.testFailed("Failed to test Report Type dropdown list - " + error, false);
        }
        
        // Test SNMP Option
        if(!TestReportTypeProperties("SNMP"))
        {
            return narrator.testFailed("Failed to test SNMP option from Report Type dropdown list and validate it appears in the 'Detail' window - " + error, false);
        }
        
        // Test Ping Option
        if(!TestReportTypeProperties("Ping"))
        {
            return narrator.testFailed("Failed to test Ping option from Report Type dropdown list and validate it appears in the 'Detail' window - " + error, false);
        }
        
        // Navigate to Reports page
        if(!NavigateToReports())
        {
            return narrator.testFailed("Failed to Navigate to Reports page - " + error, false);
        }

        // Generate reports
        if(!GenerateReports())
        {
            return narrator.testFailed("Failed to Generate Reports - " + error, false);
        }

        // Navigate to User Request Screen
        if(!NavigateToUserRequestScreen())
        {
            return narrator.testFailed("Failed to Navigate to User Request Screen - " + error, false);
        }

        // View Reports
        if(!ValidateLastGeneratedReport())
        {
            return narrator.testFailed("Failed to validate the added report was visible on the User Request screen - " + error, false);
        }
        
        // Test 'Report Type' dropdown list functionality
        if(!TestReportType())
        {
            return narrator.testFailed("Failed to test 'Report Type' dropdown list functionality - " + error, false);
        }
        
        // Validate pagination
        if(!ValidatePagination())
        {
            return narrator.testFailed("Failed to Validate pagination - " + error, false);
        }

        return narrator.finalizeTest("Successfully tested the User Request screen component's functionality", false, testData);
    }

    public boolean ValidatePage()
    {
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to default content";
            return false;
        }

        //Validate page is still logged in - left main menu button
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainMenuButtonXpath()))
        {
            error = "Failed to wait for the main menu side bar";
            return false;
        }

        narrator.stepPassed("Successfully validated the correct SINTelligent page was loaded", false);
        return true;
    }

    // Method to navigate to User Request Screen
    public boolean NavigateToUserRequestScreen()
    {
        //Switches the frames
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to default content";
            return false;
        }

        // Wait for User Request Icon visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility((SintelligentPageObject.ImgWithIdXpath("btnRequest"))))
        {
            error = "Failed to wait for User Request icon visibility";
            return false;
        }

        // Wait for User Request Icon clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath((SintelligentPageObject.ImgWithIdXpath("btnRequest"))))
        {
            error = "Failed to wait for User Request icon clickability";
            return false;
        }

        // Click on User Request Icon button
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRequest")))
        {
            error = "Failed to click User Request icon";
            return false;
        }
        
        // Wait for Refresh Icon visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility((SintelligentPageObject.ImgWithIdXpath("btnRefreshTab"))))
        {
            error = "Failed to wait for Refresh icon visibility";
            return false;
        }

        // Wait for Refresh Icon clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath((SintelligentPageObject.ImgWithIdXpath("btnRefreshTab"))))
        {
            error = "Failed to wait for Refresh icon clickability";
            return false;
        }

        // Click on the Refresh Icon button
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
        {
            error = "Failed to click Refresh icon";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //Switches back to the default window
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch back to the default content";
            return false;
        }

        // Wait for a frame visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.IframeContainsNameXpath("iframeuserRequests")))
        {
            error="Failed to wait for iframe";
            return false;
        }

        // switches to a new iframe
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.UserRequestsFrame()))
        {
            error = "Failed to switch to iframe '"+SintelligentPageObject.UserRequestsFrame()+"'";
            return false;
        }
         SeleniumDriverInstance.pause(5000);
        // Wait for User Request Screen visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Users")))
        {
            error = "Failed to wait for User Request Screen visibility";
            return false;
        }

        // Wait for User Request Screen clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdContainsTextXpath("Users")))
        {
            error = "Failed to wait for User Request Screen clickability";
            return false;
        }

       // narrator.stepPassed("Successfully validated that User Request Screen was opened successfully", false);
        return true;
    }
    
    //test start up - perform interrogation, ping, SNMP, create baseline on a host to generate data for User Request screen
    public boolean TestStartup()
    {
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Could not switch to the default content";
            return false;
        }
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            error = "Could not switch to the '"+SintelligentPageObject.ManagerRedirectFrame()+"' iframe";
            return false;
        }
                
        //switch context
        if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
        {
            error = "Could not switch to the 'Host Groups' context";
            return false;
        }
        
        //expand routers
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OpenHostGroupDropdown("Routers")))
        {
            error = "Could not wait for the 'Routers' container dropdown to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.OpenHostGroupDropdown("Routers")))
        {
            error = "Could not wait for the 'Routers' container dropdown to be clickable";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupDropdown("Routers")))
        {
            error = "Could not click the 'Routers' container dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath(testData.getData("Host")))) 
        {
            error = "Failed to tag host '"+testData.getData("Host")+"'";
            return false;
        }
        
        //perform startup - PING , SNMP , interrogation
        String test = performStartupAction("Ping", "OK");
        if(!test.isEmpty())
        {
            error = test;
            return false;
        }
        
        test = performStartupAction("SNMP", "OK");
        if(!test.isEmpty())
        {
            error = test;
            return false;
        }
        
        test = performStartupAction("Interrogate", testData.getData("HostName") + " Submitted successfully for interrogation");
        if(!test.isEmpty())
        {
            error = test;
            return false;
        }
        
        //create baseline
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Could not switch to the default content";
            return false;
        }
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
             error = "Could not switch to the '"+SintelligentPageObject.ManagerRedirectFrame()+"' iframe";
            return false;
        }
        
        SeleniumDriverInstance.pause(500);
        
        //hover over 'dropdown' menu
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
             error = "Could not wait for the menu '"+"Tasks"+"' to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
             error = "Could not wait for the menu '"+"Tasks"+"' to be clickable";
            return false;
        }
        if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Tasks")))
        {
             error = "Could not hover over the menu '"+"Tasks"+"'";
            return false;
        }
        
        //click action option from dropdown
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Create Baseline")))
        {
             error = "Could not wait for the action '"+"Create Baseline"+"' to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Create Baseline")))
        {
             error = "Could not wait for the action '"+"Create Baseline"+"' to be clickable";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("Create Baseline")))
        {
            error = "Could not click the action '"+"Create Baseline"+"' ";
            return false;
        }
        
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Could not switch to the default content";
            return false;
        }
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            error = "Could not switch to the '"+SintelligentPageObject.ManagerRedirectFrame()+"' iframe";
            return false;
        }
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            error = "Could not switch to the '"+SintelligentPageObject.ScreenFrame()+"' iframe";
            return false;
        }
        
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Generate")))
        {
            error = "Could not wait for the 'Generate' button to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdContainsTextXpath("Generate")))
        {
            error = "Could not wait for the 'Generate' button to be clickable";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Generate")))
        {
            error = "Could not click the 'Generate' button";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivWithTextXpath("You can track the progress of this process via the Request Screen."))) 
        {
            error = "Failed to validate the message 'You can track the progress of this process via the Request Screen.' appeared after starting the create baseline process";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("alertOK"))) 
        {
            error = "Failed to click the 'OK' button after Creating a Baseline";
            return false;
        }
        
        testData.extractParameter("Tasks performed during test startup: ", "PING, SNMP, Interrogation, Create Baseline", "");
        narrator.stepPassed("Succesfully performed test startup by performing tasks on the Config Console page to generate User Request data for host '"+testData.getData("Host")+"' ", true);
        return true;
    }

    // Method to navigate to Reports Screen
    public boolean NavigateToReports()
    {
        //Switches the frames
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to default content";
            return false;
        }

         // Opens the left main menu
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
        {
            error = "Failed to click the main menu bar";
            return false;
        }
        
        //close operational management
        // Wait for LayerOneMenuItem clickability
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath(testData.getData("LayerOneMenuItem"))))
        {
            error = "Failed to wait for LayerOneMenuItem visibility";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AContainsTextXpath(testData.getData("LayerOneMenuItem"))))
        {
            error = "Failed to wait for LayerOneMenuItem clickability";
            return false;
        }
        SeleniumDriverInstance.pause(500);
        // Click on the first layer menu item
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AContainsTextXpath(testData.getData("LayerOneMenuItem"))))
        {
            error = "Failed to click on '"+testData.getData("LayerOneMenuItem")+"'";
            return false;
        }
        SeleniumDriverInstance.pause(500);

        // Wait for LayerOneMenuItem clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AContainsTextXpath(testData.getData("LayerOneMenuItem"))))
        {
            error = "Failed to wait for LayerOneMenuItem clickability";
            return false;
        }

        // Click on the first layer menu item
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AContainsTextXpath(testData.getData("LayerOneMenuItem"))))
        {
            error = "Failed to click on '"+testData.getData("LayerOneMenuItem")+"'";
            return false;
        }

        // Click on the second layer menu item
        if (!SeleniumDriverInstance.clickElementByXpath(NavigateToPageObject.SecondLayerMenuItemContainsText(testData.getData("LayerOneMenuItem"),
                testData.getData("LayerTwoMenuItem"))))
        {
            error = "Failed to click on '"+testData.getData("LayerTwoMenuItem")+"'";
            return false;
        }

        // Click on the third layer menu item
        if (!SeleniumDriverInstance.clickElementByXpath(NavigateToPageObject.thirdLayerMenuItemContainsText(testData.getData("LayerOneMenuItem"),
                testData.getData("LayerTwoMenuItem"), testData.getData("LayerThreeMenuItem"))))
        {
            error = "Failed to click on '"+testData.getData("LayerThreeMenuItem")+"'";
            return false;
        }

        // Click on the fourth layer menu item
        if (!SeleniumDriverInstance.clickElementByXpath(NavigateToPageObject.fourthLayerMenuItemContainsText(testData.getData("LayerOneMenuItem"),
                testData.getData("LayerTwoMenuItem"), testData.getData("LayerThreeMenuItem"), testData.getData("LayerFourMenuItem"))))
        {
            error = "Failed to click on '"+testData.getData("LayerFourMenuItem")+"'";
            return false;
        }

        //Switches back to the default window
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch back to the default content";
            return false;
        }

        // Wait for a frame visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.IframeContainsNameXpath("iframereportMaintenance")))
        {
            error="Failed to wait for iframe";
            return false;
        }

        // switches to a new iframe
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ReportsIframe()))
        {
            error = "Failed to switch to iframe - '"+SintelligentPageObject.ReportsIframe()+"'";
            return false;
        }

        //narrator.stepPassed("'"+testData.getData("LayerFourMenuItem")+"' page loaded successfully", false);
        return true;

    }

    // Method to test dropdown List functionality
    public boolean TestDropdownList(String dropdownListID, String dropdownName)
    {
        String options = "";
        try 
        {
            
            List<WebElement>  dropdownItems = SeleniumDriverInstance.getElementsByXpath(SintelligentPageObject.SelectWithIdXpath(dropdownListID)+"/option");
            
            
            for(int i = 0; i < dropdownItems.size(); i++)
            {
                String dropDownItem = dropdownItems.get(i).getText().toString();

                // Wait for dropdown list visibility
                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath(dropdownListID)))
                {
                    error = "Failed to wait for '"+dropdownName+"' drowndown list visibility";
                    return false;
                }

                // Wait for drowndown list clickability
                if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath(dropdownListID)))
                {
                    error = "Failed to wait for '"+dropdownName+"' drowndown list clickability";
                    return false;
                }

                if(!SeleniumDriverInstance.selectFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath(dropdownListID), dropDownItem))
                {
                    error = "Failed to select '"+dropDownItem+"' from Users dropdown list";
                    return false;
                }

                //validate that the select dropdown list item
                WebElement selectedItem;
                try
                {
                    selectedItem = SeleniumDriverInstance.Driver.findElement(By.xpath(SintelligentPageObject.SelectWithIdXpath(dropdownListID)));
                }
                catch (Exception e)
                {
                    error = "Failed to retrieve select dropdown list value";
                    System.out.println("[ERROR] - " + e.getMessage());
                    return false;
                }

                String selectedItemValue = selectedItem.getText().toString();
                options = options + dropDownItem + " - ";
                    
                if(!selectedItemValue.contains(dropDownItem))
                {
                    error = "Dropdown list value does not match - expected: '"+dropDownItem+"', actual: '"+selectedItemValue+"'";
                    return false;
                }

            }
        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            error = "Failed to retrieve dropdown list items";
            return false;
        }
        
        if (dropdownName.equals("Users"))
        {
            if(!SeleniumDriverInstance.selectFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath(dropdownListID), "dvt dvt"))
            {
                error = "Failed to select 'dvt' from Users dropdown list";
                return false;
            }
        }

        testData.extractParameter("Options selected for '"+dropdownName+"':", options, "");
        narrator.stepPassed("Successfully validated the options '"+dropdownName+"' dropdown on the 'User Request' screen are displayed after being selected", false);
        return true;
    }

    // Method filter by time
    public boolean TestFilterByTime()
    {
        
        String searchTime = testData.getData("Time");

        // Wait for Filter drowndown list visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown")))
        {
            error = "Failed to wait for Filter drowndown list visibility";
            return false;
        }

        // Wait for Filter drowndown list clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown")))
        {
            error = "Failed to wait for Filter drowndown list clickability";
            return false;
        }

        // Select 'Complete Time' from Filter dropdown list
        if(!SeleniumDriverInstance.selectByIndexFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown"), 1))
        {
            error = "Failed to select 'Complete Time' from Filter dropdown list";
            return false;
        }

        SeleniumDriverInstance.pause(200);
        
        //narrator.stepPassed("Select 'Complete Time' from Filter drowndown list and search for entries that contains '"+searchTime+"'", false);

            WebElement filterDropdown;
            try
            {
                filterDropdown = SeleniumDriverInstance.Driver.findElement(By.xpath(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown")));
            }
            catch (Exception e)
            {
                error = "Failed to retrieve select dropdown list value";
                System.out.println("[ERROR] - " + e.getMessage());
                return false;
            }

            String filterDropdownValue = filterDropdown.getText().toString();

            if(!filterDropdownValue.contains("Complete Time"))
            {
                error = "Dropdown list value does not match - expected: 'Complete Time', actual: '"+filterDropdownValue+"'";
                return false;
            }

            // Wait for Filter textfield visibility
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.InputWithIdXpath("txtfilterValue")))
            {
                error = "Failed to wait for Filter textfield visibility";
                return false;
            }

            // Wait for Filter textfield clickability
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.InputWithIdXpath("txtfilterValue")))
            {
                error = "Failed to wait for Filter textfield clickability";
                return false;
            }

            // Enter text in Filter textfield
            if(!SeleniumDriverInstance.enterTextByXpath(SintelligentPageObject.InputWithIdXpath("txtfilterValue"), searchTime))
            {
                error = "Failed to Enter text in Filter textfield";
                return false;
            }

            // Click on search button
            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("Filter")))
            {
                error = "Failed to Click on search button";
                return false;
            }

            SeleniumDriverInstance.pause(200);
            int numCompletedItems =0;
            
            // Validate status
            try {
                List<WebElement>  statuses = SeleniumDriverInstance.getElementsByXpath(SintelligentPageObject.userRequestTableCellByClass("grid_normalGrid_c2"));
                List<WebElement>  times = SeleniumDriverInstance.getElementsByXpath(SintelligentPageObject.userRequestTableCellByClass("grid_normalGrid_c3"));
                

                if(statuses.isEmpty())
                {
                    narrator.stepPassed("No entries found when filtering 'Complete Time' with '"+searchTime+"'", false);
                }
                else{

                    for(int i = 0; i < statuses.size(); i++)
                    {
                        String status = statuses.get(i).getText().toString();

                        if(!status.contains("Completed")){
                            error = "Status value does not match - expected: 'Completed', actual: '"+status+"'";
                        }
                    }
                    for(int i = 0; i < times.size(); i++)
                    {
                        String time = times.get(i).getText().toString();

                        if(!time.contains(searchTime)){
                            error = "Time value does not match - expected: '"+searchTime+"', actual: '"+time+"'";
                        }

                    }
                    numCompletedItems = statuses.size();
                }

            } catch (Exception e) {
                error = "Failed to store the status values";
            }

        narrator.stepPassed("Successfully validated 'Complete Time' option from 'Filter' dropdown list displays only completed items containing searched time '"+searchTime+"' - '"+numCompletedItems+"' items found", false);
        return true;

    }

    // Method to test 'Report Type' dropdown list
    public boolean TestReportType()
    {
        // Wait for Filter drowndown list visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown")))
        {
            error = "Failed to wait for Filter drowndown list visibility";
            return false;
        }

        // Wait for Filter drowndown list clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown")))
        {
            error = "Failed to wait for Filter drowndown list clickability";
            return false;
        }

        // Wait for Report Type drowndown list visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
            error = "Failed to wait for Report Type drowndown list visibility";
            return false;
        }

        // Wait for Report Type drowndown list clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
            error = "Failed to wait for Report Type drowndown list clickability";
            return false;
        }

          if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.SelectWithIdXpath("cboUsersDropdown")))
        {
            error = "Failed to select 'dvt dvt' option within Users dropdown list";
            return false;
        }
              if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboUsersDropdown")))
        {
            error = "Failed to select 'dvt dvt' option within Users dropdown list";
            return false;
        }
        // Select a User
        if(!SeleniumDriverInstance.selectFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboUsersDropdown"), testData.getData("User")))
        {
            error = "Failed to select 'dvt dvt' option within Users dropdown list";
            return false;
        }

       /*  test = validateFilterOptionWithColumnHeader("Report");
        if(!test.isEmpty())
        {
            error = test;
            return false;
        }
        
        test = validateFilterOptionWithColumnHeader("Cisco Command");
        if(!test.isEmpty())
        {
            error = test;
            return false;
        }*/
        
        String test = validateFilterOptionWithColumnHeader("Work List");
        if(!test.isEmpty())
        {
            error = test;
            return false;
        }
        
        test = validateFilterOptionWithColumnHeader("SNMP");
        if(!test.isEmpty())
        {
            error = test;
            return false;
        }
        
        test = validateFilterOptionWithColumnHeader("Ping");
        if(!test.isEmpty())
        {
            error = test;
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.SelectWithIdXpath("requestType"))){
            error = "";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
            error = "";
            return false;
        }
        
        //Nelson did this
        if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIDByoptionTextXpath("requestType", "SSH"),1))
        {
            test = validateFilterOptionWithColumnHeader("SSH");
            if (!test.isEmpty())
            {
                error = test;
                return false;
            }
        }

        if(SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIDByoptionTextXpath("requestType", "Telnet"),1))
        {
            test = validateFilterOptionWithColumnHeader("Telnet");
            if(!test.isEmpty())
            {
                error = test;
                return false;
            }
        }
        
        narrator.stepPassed("Successfully validated that the 'Filter' options have correct corresponding data columns for all the Request Type options on the User Request screen", true);
        return true;
    }

    public boolean TestExpandButton()
    {
        
        // Select 'Baseline' option within Report Type dropdown list
        if(!SeleniumDriverInstance.selectFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("requestType"), "Baseline"))
        {
            error = "Failed to select 'Baseline' from Report Type dropdown list";
            return false;
        }

        // Wait for small arrow visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("imgAdvancedBTN_Filter1")))
        {
            error = "Failed to wait for small arrow visibility";
            return false;
        }

        // Wait for small arrow clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("imgAdvancedBTN_Filter1")))
        {
            error = "Failed to wait for small arrow clickability";
            return false;
        }

        // Click on small arrow
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("imgAdvancedBTN_Filter1")))
        {
            error = "Failed to click on small arrow";
            return false;
        }
        
        
        
        // Wait for done reports value visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("tdDone")))
        {
            error = "Failed to wait for done reports value visibility";
            return false;
        }

        // Wait for done reports value clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("tdDone")))
        {
            error = "Failed to wait for done reports value clickability";
            return false;
        }
        
        
        //On fireFox we need a pause please do not remove
        if(ApplicationConfig.SelectedBrowser() ==Enums.BrowserType.FireFox)
        {
           SeleniumDriverInstance.pause(1000); 
        }
        
            WebElement doneElement;
            try
            {
                doneElement = SeleniumDriverInstance.Driver.findElement(By.xpath(SintelligentPageObject.TdWithId("tdDone")));
            }
            catch (Exception e)
            {
                error = "Failed to retrieve doneElement value";
                System.out.println("[ERROR] - " + e.getMessage());
                return false;
            }
            
            int numberOfCompletedReports = 0;
            int doneValue = Integer.parseInt(doneElement.getText().toString());
            
            try {

                List<WebElement>  statuses = SeleniumDriverInstance.getElementsByXpath(SintelligentPageObject.userRequestTableCellByClassAndByText("grid_normalGrid_c2", "Completed"));

                if(!statuses.isEmpty())
                {
                    numberOfCompletedReports = statuses.size();
                }

                narrator.stepPassed("Successfully validated the expand status arrow button contains the information on the User Request screen: Completed found ["+numberOfCompletedReports+"]", false);
                testData.extractParameter("Number of 'Done' items validted after clicking the status expand arrow:", ""+doneValue, "");
                
            } catch (Exception e) {
                error = "Failed to store the status values";
            }
        
        // Click on small arrow
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("imgAdvancedBTN_Filter1")))
        {
            error = "Failed to click on small arrow";
            return false;
        }    
        

        return true;
    }

    // Method to test some Report Type Proprieties
    public boolean TestReportTypeProperties(String property)
    {
        
        // Wait for report type drowndown list visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
            error = "Failed to wait for report type drowndown list visibility";
            return false;
        }

        // Wait for report type drowndown list clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
            error = "Failed to wait for report type drowndown list clickability";
            return false;
        }

        //On fireFox dropdown does not work
        String browserType = ApplicationConfig.SelectedBrowser().toString();
        if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
        {
           // Select 'SNMP' option within Report Type dropdown list
            SeleniumDriverInstance.pause(60000);
            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpathNew(SintelligentPageObject.SelectWithIdXpath("requestType"), property))
            {
                error = "Failed to select '"+property+"' from Report Type dropdown list";
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("requestType")))
            {
                error = "Failed to select '"+property+"' from Report Type dropdown list";
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("requestType")))
            {
                error = "Failed to select '"+property+"' from Report Type dropdown list";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementByXpath_Actions(SintelligentPageObject.SelectWithIdXpath("requestType")))
            {
                error = "Failed to click '"+property+"' on the Report Type Dropdown";
                return false;
            }
            SeleniumDriverInstance.pause(500);
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OptionWithTextXpath(property)))
            {
                error = "Failed to select '"+property+"' from Report Type dropdown list";
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.OptionWithTextXpath(property)))
            {
                error = "Failed to select '"+property+"' from Report Type dropdown list";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementByXpath_Actions(SintelligentPageObject.OptionWithTextXpath(property)))
            {
                error = "Failed to click '"+property+"' on the Report Type Dropdown";
                return false;
            }
            
            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpathNew(SintelligentPageObject.SelectWithIdXpath("requestType"), property))
            {
                error = "Failed to select '"+property+"' from Report Type dropdown list";
                return false;
            }
            
        }
         SeleniumDriverInstance.pause(10000);
        // Select 'SNMP' option within Report Type dropdown list
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
             SeleniumDriverInstance.pause(60000);
           if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("requestType"),10))
            {
                error = "Failed to wait for '"+property+"' from Report Type dropdown list to be visible";
                return false;
            }
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
            error = "Failed to wait for'"+property+"' from Report Type dropdown list to be clikable";
            return false;
        }
        if(!SeleniumDriverInstance.selectFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("requestType"), property))
        {
            if(!SeleniumDriverInstance.selectFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("requestType"), property))
            {
                SeleniumDriverInstance.pause(60000);
                error = "Failed to select '"+property+"' from Report Type dropdown list";
                return false;
            }
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.userRequestTableCellByClass("grid_normalGrid_c1")))
        {
            error = "Failed to wait for visibility to Click on the first host";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.userRequestTableCellByClass("grid_normalGrid_c1")))
        {
            error = "Failed to wait for clickability to Click on the first host";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.userRequestTableCellByClass("grid_normalGrid_c1")))
        {
            error = "Failed to Click on the first host";
            return false;
        }
        
        //-change refresh time to 60 seconds to ensure DOM does not change when referencing elements-//
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown")))
        {
            error = "Could not wait for the Refresh Time dropdown to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown")))
        {
            error = "Could not wait for the Refresh Time dropdown to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown"), "60 seconds"))
        {
            error = "Could not wait for the Refresh Time dropdown to be visible";
            return false;
        }
        
        String item = "";
        try 
        {

            List<WebElement>  items = SeleniumDriverInstance.getElementsByXpath(SintelligentPageObject.hostPropertyDescription());

            if(!items.isEmpty())
            {
//                narrator.stepPassed("No entries found for '"+property+"'", false);
         
                for(int i = 0; i < items.size(); i++)
                {
                    item = items.get(i).getText().toString();

                    if(!item.contains(property))
                    {
                        error = "Property does not match - expected: '"+property+"', actual: '"+item+"'";
                        return false;
                    }
                }
            }
        } 
        catch (Exception e) 
        {
            System.out.println("[ERROR] - " + e.getMessage());
            error = "Failed to store the report type property";
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown")))
        {
            error = "Could not wait for the Refresh Time dropdown to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown")))
        {
            error = "Could not wait for the Refresh Time dropdown to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown"), "5 seconds"))
        {
            error = "Could not wait for the Refresh Time dropdown to be visible";
            return false;
        }
        //-return refresh time to 5 seconds-//
        
        narrator.stepPassed("Successfully opened 'Detail' window for '"+property+"' and validated the selected item '"+item+"' was displayed", false);
        return true;
    }

    // Method to generate reports
    public boolean GenerateReports()
    {

        // Wait for a frame visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.reportTableCellContainsTexAndClass("grid_grid_c2", "Host List Report")))
        {
            error="Failed to wait for the first report";
            return false;
        }

        // Right click on the first report
        if(!SeleniumDriverInstance.rightClickElementByXpath(SintelligentPageObject.reportTableCellContainsTexAndClass("grid_grid_c2", "Host List Report")))
        {
            error = "Failed to right Click on the first report";
            return false;
        }

        // Click on generate button
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Generate")))
        {
            error = "Failed to click on generate button";
            return false;
        }

        // Switch frames
        if (!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ModalIframeId1Frame()))
        {
            error = "Failed to switch to '"+SintelligentPageObject.ModalIframeId1Frame()+"' iframe";
            return false;
        }

        // Wait for Format drowndown list visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("cboFormat")))
        {
            error = "Failed to wait for Format drowndown list visibility";
            return false;
        }

        // Wait for Format drowndown list clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboFormat")))
        {
            error = "Failed to wait for Format drowndown list clickability";
            return false;
        }

        // Select 'Excel 2007 and Later (XLSX)' option within Users dropdown list
        if(!SeleniumDriverInstance.
                selectByIndexFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboFormat"), 3))
        {
            error = "Failed to select 'Excel 2007 and Later (XLSX)' option within Users dropdown list";
            return false;
        }

        // Click on generate button
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.generateButtonWithId("applyButton")))
        {
            error = "Failed to Click on generate button";
            return false;
        }

        // switches to a new iframe
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ReportsIframe()))
        {
            error = "Could not switch to a new iframe";
            return false;
        }

        //Switches back to the default window
        if (!SeleniumDriverInstance.switchToFrameByPartialId(SintelligentPageObject.ModalIframeId2Frame()))
        {
            error = "Failed to switch back to the default content";
            return false;
        }

        SeleniumDriverInstance.pause(15000);
      
        for (int i = 0; i < 25; i++) {
            if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("Filter"))) {
                error = "Failed to Click 'Filter' button";
                return false;
            }

        }
   
        
        //wait until report is visible
        boolean reportIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.reportViewerTableCellByClass("grid_gridReportViewer_c0"),3);
        int stopCounter = 0;
        while(!reportIsVisible && stopCounter != 60)
        {
            SeleniumDriverInstance.pause(1000);
            //click refresh button
            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("Filter")))
            {
                error = "Could not wait for the 'refresh button' to be visible";
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("Filter")))
            {
                error = "Could not wait for the 'refresh button' to be clickable";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("Filter")))
            {
                error = "Could not click the 'refresh button'";
                return false;
            }
            
            reportIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.reportViewerTableCellByClass("grid_gridReportViewer_c0"));
            stopCounter++;
        }
        
        //order reports by latest
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivContainsTextXpath("Status Time")))
        {
            error = "Could not wait for the 'Status Time' header to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.DivContainsTextXpath("Status Time")))
        {
            error = "Could not wait for the 'Status Time' header to be clickable";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.DivContainsTextXpath("Status Time")))
        {
            error = "Could not click the 'Status Time' header to sort oldest reports";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivContainsTextXpath("Status Time")))
        {
            error = "Could not wait for the 'Status Time' header to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.DivContainsTextXpath("Status Time")))
        {
            error = "Could not wait for the 'Status Time' header to be clickable";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.DivContainsTextXpath("Status Time")))
        {
            error = "Could not click the 'Status Time' header to sort latest reports";
            return false;
        }

        // Validate reports generation
        try {

            List<WebElement>  reports = SeleniumDriverInstance.getElementsByXpath(SintelligentPageObject.reportViewerTableCellByClass("grid_gridReportViewer_c0"));

            if(reports.isEmpty())
            {
                error = ("No report generated");
                return false;
            }
            else{


                for(int i = 0; i < reports.size(); i++)
                {
                    report = reports.get(0).getText().toString();
                }

                StringTokenizer tokenizer = new StringTokenizer(report, ".");

                reportText = tokenizer.nextElement().toString();

            }

        } catch (Exception e) {
            error = "Failed to store generated reports";
        }
            
        //delete the added report
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.reportViewerTableCellByClass("grid_gridReportViewer_c0")))
        {
            error = "Could not wait for the generated report to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.reportViewerTableCellByClass("grid_gridReportViewer_c0")))
        {
            error = "Could not wait for the added report to be clickable";
            return false;
        }
        if(!SeleniumDriverInstance.rightClickElementByXpath(SintelligentPageObject.reportViewerTableCellByClass("grid_gridReportViewer_c0")))
        {
            error = "Could not right click the added report";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Delete")))
        {
            error = "Could not wait for the delete button to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdContainsTextXpath("Delete")))
        {
            error = "Could not wait for the delete button to be clickable";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdContainsTextXpath("Delete")))
        {
            error = "Could not click the delete button";
            return false;
        }

        narrator.stepPassed("Successfully generated report '"+reportText+"' on the 'Host List Report' page", false);
        return true;
    }

    // Method to validate last generated report
    public boolean ValidateLastGeneratedReport()
    {
        //switch to the user requests iframe
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Could not switch to the default content";
            return false;
        }
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.UserRequestsFrame()))
        {
            error = "Could not switch to the User Requests iframe";
            return false;
        }
        
        // Clear Filter dropdown list
        if(!SeleniumDriverInstance.clearTextByXpath(SintelligentPageObject.InputWithIdXpath("txtfilterValue")))
        {
            error = "Could not clear the filter dropdown list";
            return false;
        }
        
        // Wait for 'Request Type' drowndown list visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
            error = "Failed to wait for 'Request Type' drowndown list visibility";
            return false;
        }

        // Wait for 'Request Type' drowndown list clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
            error = "Failed to wait for 'Request Type' drowndown list clickability";
            return false;
        }
        
        // Select a User
        if(!SeleniumDriverInstance.selectFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboUsersDropdown"), "dvt dvt"))
        {
            error = "Failed to select 'dvt dvt' option within Users dropdown list";
            return false;
        }

        // Select 'Report' option within Report Type dropdown list
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
            error = "Could not wait for the Report Type dropdown to be visible";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
            error = "Could not wait for the Report Type dropdown to be clickable";
            return false;
        }
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("requestType"),"Report"))
        {
            error = "Failed to select 'Report' from Report Type dropdown list";
            return false;
        }

        // Wait for the last generated report to be visible
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.reportDescription("HostListReport")))
        {
            error = "Failed to wait for the last generated report 'HostListReport' to be visible";
            return false;
        }

        narrator.stepPassed("Successfully validated that generated report '"+reportText+"' has been displayed on the User Request screen", false);
        return true;
    }
    
    // Method to validate pagination
    public boolean ValidatePagination()
    {
        //On fireFox we need a pause please do not remove
        if(ApplicationConfig.SelectedBrowser() ==Enums.BrowserType.FireFox)
        {
           SeleniumDriverInstance.pause(1000); 
        }
        
        // Wait for Filter drowndown list visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("pageNumber")))
        {
            error = "Failed to wait for Filter drowndown list visibility";
            return false;
        }

        // Wait for Filter drowndown list clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("pageNumber")))
        {
            error = "Failed to wait for Filter drowndown list clickability";
            return false;
        }
       String paging = pagingMethod();
            
            if (!paging.equals(""))
            {
                error = paging;
                return false;
            }

        narrator.stepPassed("Successfully validated the paging functionality of the 'Report' page", false);
        return true;
    }
    
    public boolean GetPages(String page)
    {
       
        // Wait for Filter drowndown list visibility
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId(page)))
        {

            error = "Failed to wait for the '"+page+"'";
            return false;
        }

        // Wait for Filter drowndown list clickability
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId(page)))
        {
            error = "Failed to wait for the '"+page+"'";
            return false;
        }

        try
        {
            WebElement ele = SeleniumDriverInstance.Driver.findElement(By.xpath(SintelligentPageObject.TdWithId(page)));
            pages = Integer.parseInt(ele.getText().toString());
            
        }
        catch(Exception e)
        {
            error ="Failed to extract '"+page+"' text";
            return false;
        }
        return true; 
    }
    
    public boolean ClickPage(String page, String type)
    {
        //Wait for the page 
        if(!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.paginationButtonWithOnClick(page)))
        {
            error = "Failed to wait for the "+type+" button";
            return false;
        }
        //Click the apply button
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.paginationButtonWithOnClick(page)))
        {
            error = "Failed to click the "+type+" button";
            return false;
        }
        return true;
    }
    
    
    // custom methods //
    
    public String performStartupAction(String action,String validationText)
    {
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return error = "Could not switch to the default content";
        }
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            return error = "Could not switch to the '"+SintelligentPageObject.ManagerRedirectFrame()+"' iframe";
        }
        
        //click 'action'
         if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath(action)))
        {
            return error = "Could not wait for the '"+action+"' button to be visible";
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AContainsTextXpath(action)))
        {
            return error = "Could not wait for the '"+action+"' button to be clickable";
        }
        if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AContainsTextXpath(action)))
        {
            return error = "Could not click the '"+action+"' button";
        }
        
        SeleniumDriverInstance.pause(3000);
        String loaderWait = SeleniumDriverInstance.WaitForLoaderIconNotVisible();
        if(!loaderWait.isEmpty())
        {
            return loaderWait;
        }
        
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return error = "Could not switch to the default content";
        }
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            return error = "Could not switch to the '"+SintelligentPageObject.ManagerRedirectFrame()+"' iframe";
        }
        if(!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ScreenFrame()))
        {
            return error = "Could not switch to the '"+SintelligentPageObject.ScreenFrame()+"' iframe";
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath(validationText)))
        {
            return error = "Could not validate the action '"+action+"' was performed";
        }
        
        //switch iframes
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return error = "Could not switch to the default content";
        }
        if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
        {
            return error = "Could not switch to the '"+SintelligentPageObject.ManagerRedirectFrame()+"' iframe";
        }
        
        return "";
    }
    
    public String validateFilterOptionWithColumnHeader(String requestType)
    {
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
            return error = "Failed to wait for '"+requestType+"' to be visible";
        }
        
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("requestType")))
        {
            return error = "Failed to wait for '"+requestType+"' to be clickable ";
        }
        // Select 'requestType' option within Report Type dropdown list
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("requestType"), requestType))
        {
            return error = "Failed to select '"+requestType+"' from Report Type dropdown list";
        }
        
        //wait for elements to be visible
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown")+"/option"))
        {
            return error = "Could not wait for the 'Filter' options to be visible";
        }
        if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown")+"/option"))
        {
            return error = "Could not wait for the 'Filter' options to be clickable";
        }
        SeleniumDriverInstance.pause(500);
        
        
        String itemsList = "";
        int size = 0;
        try 
        {
            List<WebElement>  dropdownItems = SeleniumDriverInstance.getElementsByXpath(SintelligentPageObject.SelectWithIdXpath("cboFilterDropdown")+"/option");
            String tempCompare = "";

            //validate the items in the dropdown match the column items
            for(int i = 0; i < dropdownItems.size(); i++)
            {
                tempCompare = dropdownItems.get(i).getText().toString();

                if(!tempCompare.isEmpty())
                {
                    //validate that there are present columns for every item in the dropdown list
                    if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.DivContainsTextXpath(tempCompare)))
                    {
                        return error = "Could not validate that the 'Filter' dropdown option '"+tempCompare+"' has a corresponding data column";
                    }
                    size++;
                    itemsList = itemsList + tempCompare + " - ";
                }

            }
        }
        catch (Exception e)
        {
            error = "Failed to retrieve dropdown list items";
        }
        
        testData.extractParameter(""+requestType+ " filter options validated: ","[Found: "+size+"] " + itemsList, "");
        //narrator.stepPassed("Successfully validated the 'Filter' options [Found: "+size+"] have corresponding data columns for the Request Type option: '"+requestType+"'", true);
        return "";
    }
    
    public String pagingMethod()
    {
    
        //Next
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.PagingTdWithOnclick("onBtnNextPageClick()")))
        {
            return "Failed to wait for the next page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.PagingTdWithOnclick("onBtnNextPageClick()")))
        {
            return "Failed to wait for the next page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.PagingTdWithOnclick("onBtnNextPageClick()")))
        {
            return "Failed to click the next page button";
        }
           
                //Previous
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.PagingTdWithOnclick("onBtnPreviousPageClick()")))
        {
            return "Failed to wait for the previous page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.PagingTdWithOnclick("onBtnPreviousPageClick()")))
        {
            return "Failed to wait for the previous page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.PagingTdWithOnclick("onBtnPreviousPageClick()")))
        {
            return "Failed to click the previous page button";
        }
        
        //Last Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.PagingTdWithOnclick("onBtnLastPageClick()")))
        {
            return "Failed to wait for the last page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.PagingTdWithOnclick("onBtnLastPageClick()")))
        {
            return "Failed to wait for the last page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.PagingTdWithOnclick("onBtnLastPageClick()")))
        {
            return "Failed to click the last page button";
        }
                        
        //First Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.PagingTdWithOnclick("onBtnFirstPageClick()")))
        {
            return "Failed to wait for the first page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.PagingTdWithOnclick("onBtnFirstPageClick()")))
        {
            return "Failed to wait for the first page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.PagingTdWithOnclick("onBtnFirstPageClick()")))
        {
            return "Failed to click the first page button";
        }
        return "";
    }
}
