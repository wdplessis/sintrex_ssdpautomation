/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.SintelligentPageObject;
/**
 *
 * @author szeuch
 */
public class NavigateToPage extends BaseClass 
{
    Narrator narrator;
    
    String error = "";
    int menuDrillDownAmount;

    public NavigateToPage(TestEntity testData) 
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }
    
    public TestResult executeTest()
    {
        if(!ValidatePage())
        {
            return narrator.testFailed("Failed to validate the correct SINTelligent page was loaded - " + error, false);
        }
        
        if(!NavigateToPage())
        {
            return narrator.testFailed("Failed to navigate to the " + testData.getData("Menu"+menuDrillDownAmount) + " page - " + error, false);
        }
        
        return narrator.finalizeTest("Successfully navigated to the SINTelligent " + testData.getData("Menu"+menuDrillDownAmount) + " page ", false, testData);
    }
    
    public boolean ValidatePage()
    {
        if (!SeleniumDriverInstance.switchToDefaultContent()) 
        {
            error = "Failed to switch to default content";
            return false;
        }
        
        //Validate page is still logged in - left main menu button
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.MainMenuButtonXpath())) 
        {
            error = "Failed to wait for the main menu side bar";
            return false;
        }
        
        narrator.stepPassed("Successfully validate the correct SINTelligent page was loaded", false);
        return true;
    }
    
     public boolean NavigateToPage()
    {
        SeleniumDriverInstance.pause(2000);
        //Opens the left main menu
        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath())) 
        {
            error = "Failed to click the main menu bar";
            return false;
        }
        
        SeleniumDriverInstance.pause(100);
        
        //How many menu items it must drill down
        menuDrillDownAmount = Integer.parseInt(testData.getData("MenuDrillDownAmount"));
        for (int i = 1; i < menuDrillDownAmount + 1; i++) 
        {
            SeleniumDriverInstance.pause(100);

            //Waits for menu item to be visible and clickable
//            if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AWithTextXpath(testData.getData("Menu"+i)))) 
//            {
//                error = "Failed to wait for the menu item '"+testData.getData("Menu"+i)+"' to be visible";
//                return false;
//            }
//            
//            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AWithTextXpath(testData.getData("Menu"+i))))
//            {
//                error = "Failed to wait for the menu item '"+testData.getData("Menu"+i)+"' to be clickable";
//                return false;
//            }
            
//            SeleniumDriverInstance.pause(100);
            
            //Expands the menu item
            if (!SeleniumDriverInstance.expandNavigationMenuItemByXpath(SintelligentPageObject.AWithTextXpath(testData.getData("Menu"+i))))
            {
                error = "Failed to click the menu item '"+testData.getData("Menu"+i)+"'";
                return false;
            }
            
            SeleniumDriverInstance.pause(100);
        }
        
        testData.extractParameter("Page", testData.getData("Menu"+menuDrillDownAmount), "");
        
        SeleniumDriverInstance.pause(100);
        
        //Switches back to the default window
        if (!SeleniumDriverInstance.switchToDefaultContent()) 
        {
            error = "Failed to switch back to the default content";
            return false;
        }
        
        narrator.stepPassed("Successfully validated the SINTelligent page " + testData.getData("Menu"+menuDrillDownAmount) + " loaded successfully ", false);
        return true;
    }
}
