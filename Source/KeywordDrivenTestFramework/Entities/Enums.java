package KeywordDrivenTestFramework.Entities;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

/**
 * Created with IntelliJ IDEA. User: fnell Date: 2013/04/26 Time: 3:20 PM To
 * change this template use File | Settings | File Templates.
 */
public class Enums {

    static String reportDirectory = ApplicationConfig.ReportFileDirectory();
    public enum BrowserType {

        IE, FireFox, Chrome, Safari
    }

    public enum ResultStatus {

        PASS, FAIL, WARNING, UNCERTAIN
    }

    public enum RelativePosition {

        Above, Below, Right, Left
    }

    public enum Environment 
    {    
        // Here you will add your own Boxes to run the Scripts on. Scroll down once done.
        AutomationTesting1_131("http://10.0.4.131/csp/umdb/UI.Login.cls","10.0.4.131"),
        AutomationTesting2_132("http://10.0.4.132/csp/umdb/UI.Login.cls","10.0.4.132"),
        AutomationTesting3_133("http://10.0.4.133/csp/umdb/UI.Login.cls","10.0.4.133"),
        SSDPAutomation1_63("http://10.15.1.63/csp/umdb/UI.Login.cls","10.15.1.63"),
        SSDPAutomation2_71("http://10.15.1.71/csp/umdb/UI.Login.cls","10.15.1.71"),
        OpenAFile(ApplicationConfig.ReportFileDirectory() , "C:\\sintrex_keyworddriventestautomation\\KeywordDrivenTestReports\\S I N-100- Config  Console -  Multi Link Tool Part2\\extentReport.html");
        
        // For each system (website1, database1, website2 etc.) within the defined environment (Dev, QA, Prod etc.)
        // you will have to declare the appropriate string to store its properties (URL or connection string etc.).
        public final String PageUrl;
        public final String Ip;

        // public final String ForgotPasswordURL;
        // This constructor defines and instantiates the parameters declared above. Parameter order is specified here and will 
        // define the order in which the enum types' properties are specified. 
        Environment(String pageUrl,String ip) {
            this.PageUrl = pageUrl;
            this.Ip = ip;
        }
    }

    public static Environment resolveTestEnvironment(String environment) 
    {
        switch (environment.toUpperCase()) 
        {
            // Also add it here before running.
            case "SINTELLIGENT":
                return Environment.AutomationTesting1_131;
            case "SINTELLIGENT-EXECUTION":
                return Environment.AutomationTesting2_132;
            case "SINTELLIGENT-REGRESSION":
                return Environment.AutomationTesting3_133;
            case "SINTELLIGENT-RELEASE":
                return Environment.SSDPAutomation1_63;    
            case "OpenAFile":
                return Environment.OpenAFile;
            default:
                return null;
        }
    }
}
