/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.RetrievedTestValues;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.SintelligentPageObject;
import com.google.common.collect.Ordering;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.monte.media.Format;
import org.monte.media.FormatKeys;
import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.FormatKeys.KeyFrameIntervalKey;
import static org.monte.media.FormatKeys.MIME_AVI;
import static org.monte.media.FormatKeys.MediaTypeKey;
import static org.monte.media.FormatKeys.MimeTypeKey;
import static org.monte.media.VideoFormatKeys.CompressorNameKey;
import static org.monte.media.VideoFormatKeys.DepthKey;
import static org.monte.media.VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE;
import static org.monte.media.VideoFormatKeys.QualityKey;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;




/**
 *
 * @author fnell
 * @editir jjoubert
 */
// Contains logic for handling accessor methods and driver calls.
public class SeleniumDriverUtility extends BaseClass
{

    /**
     *
     */
    public WebDriver Driver;
    private Enums.BrowserType browserType;
    File fileIEDriver;
    File fileChromeDriver;
    File fileFireFoxDriver;
    private Boolean _isDriverRunning;
    public RetrievedTestValues retrievedTestValues;
    public String DriverExceptionDetail = "";
    TestEntity testData;
    private Object document;
    private String mainWindowsHandle;
    private int count = 0;
    private ScreenRecorder screenRecorder;

    public SeleniumDriverUtility(Enums.BrowserType selectedBrowser)
    {
        retrievedTestValues = new RetrievedTestValues();

        _isDriverRunning = false;
        browserType = selectedBrowser;

        fileIEDriver = new File("IEDriverServer.exe");
        System.setProperty("webdriver.ie.driver", fileIEDriver.getAbsolutePath());

        fileChromeDriver = new File("chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", fileChromeDriver.getAbsolutePath());
        
//        fileFireFoxDriver = new File("geckodriver.exe");
//        System.setProperty("webdriver.gecko.driver", fileFireFoxDriver.getAbsolutePath());
        
        
    }

    public boolean isDriverRunning()
    {
        return _isDriverRunning;
    }

    public void startDriver()
    {
        try
        {
            System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");

            switch (browserType)
            {
                case IE:
                    DesiredCapabilities caps = DesiredCapabilities.internetExplorer(); caps.setCapability("ignoreZoomSetting", true);
                    caps.setCapability("nativeEvents",false);
                    caps.setCapability("requireWindowFocus", true);
                  String[] dialog =  new String[]{ System.getProperty("user.dir") +"\\IEDriverServer.exe","Save to...","Save", System.getProperty("user.home") + "\\Downloads\\" }; // path to exe, dialog title, save/cancel/run, path to save file.
                  Process pp1 = Runtime.getRuntime().exec(dialog);
//                  pp1.destroy();
                    Driver = new InternetExplorerDriver(caps);
                    Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                    _isDriverRunning = true;
                    break;
                case FireFox:
                    Driver = new FirefoxDriver(FirefoxDriverProfile());
                    _isDriverRunning = true;
                    break;
                case Chrome:
                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("disable-infobars");
                    //options.addArguments("user-data-dir=C:\\Users\\cstemmet\\AppData\\Local\\Google\\Chrome\\User Data");
                    Driver = new ChromeDriver(options);
                    _isDriverRunning = true;
                    break;
                case Safari: ;
                    break;
            }
            retrievedTestValues = new RetrievedTestValues();
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            Driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
            Driver.manage().timeouts().setScriptTimeout(1, TimeUnit.SECONDS);
            Driver.manage().window().maximize();
        }
        catch (Exception e)
        {
            Narrator.logError("Error starting the driver - " + e.getMessage());
        }
    }

    public static FirefoxProfile FirefoxDriverProfile() throws Exception
    {
        try
        {
            File firePath = new File("C:\\firepath.xpi");
            File firebug = new File("C:\\firebug.xpi");

            FirefoxProfile profile = new FirefoxProfile();
            String downloadPath = System.getProperty("user.home") + "/Downloads/";
            profile.setPreference("browser.download.folderList", 2);
            profile.setPreference("browser.download.manager.showWhenStarting", false);
            profile.setPreference("browser.download.dir", downloadPath);
            profile.setPreference("browser.helperApps.neverAsk.openFile",
                    "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml,application/zip,application/octet-stream");
            profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
                    "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml,application/zip,application/octet-stream");
            profile.setPreference("browser.helperApps.alwaysAsk.force", false);
            profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
            profile.setPreference("browser.download.manager.focusWhenStarting", true);
            profile.setPreference("browser.download.manager.useWindow", false);
            profile.setPreference("browser.download.manager.showAlertOnComplete", false);
            profile.setPreference("browser.download.manager.closeWhenDone", false);
            profile.setPreference("browser.download.manager.showAllDownloads", false);

            if (firePath.isFile())
            {
                profile.addExtension(firePath);
            }

            if (firebug.isFile())
            {
                profile.addExtension(firebug);
            }

            return profile;
        }
        catch (Exception e)
        {
            Narrator.logError("Error  setting the Firefox profiles - " + e.getMessage());
            return null;
        }
    }

    public boolean clickElementById(String elementId)
    {
        try
        {

            Narrator.logDebug("Attempting to click element by ID - " + elementId);
            waitForElementById(elementId);
            //Pause is needed in some cases, do not remove
            SeleniumDriverInstance.pause(100);
            Driver.findElement(By.id(elementId)).click();
            Narrator.logDebug("Element clicked successfully...proceeding");
            return true;
        }
        catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean tryClickElementsByCSSSelector(String elementCSSSelector)
    {
        try
        {
            Narrator.logDebug("Finding elements by CSS Selector - " + elementCSSSelector);
            waitForElementByCSSSelector(elementCSSSelector);
            //Thread.sleep(1000);

            List<WebElement> detectedElements = Driver.findElements(By.cssSelector(elementCSSSelector));

            Narrator.logDebug("Found - " + detectedElements.size() + " matching elements");

            for (WebElement element : detectedElements)
            {
                if (element == null || !element.isDisplayed() || !element.isEnabled())
                {
                    continue;
                }

                Narrator.logDebug("Element is valid, attempting to click...");

                element.click();

                Narrator.logDebug("Element clicked sucessfully, exiting search.");

                break;

            }

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" clicking elements by CSS Selector - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean alertHandler()
    {
        try
        {
            Narrator.logDebug("Attempting to click OK in alert pop-up");
            // Get a handle to the open alert, prompt or confirmation
            Alert alert = Driver.switchTo().alert();
            // Get the text of the alert or prompt
            alert.getText();
            // And acknowledge the alert (equivalent to clicking "OK")
            alert.accept();
            Narrator.logDebug("Ok Clicked successfully...proceeding");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" clicking OK in alert pop-up - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean truncateData(String url)
    {

        APICallUtility api = new APICallUtility();
        try
        {
            //NO NOT EDIT THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //NO NOT EDIT THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //NO NOT EDIT THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //NO NOT EDIT THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            api.sendPost(url,
                    "{\n"
                    + "\"type\":\"SELECT\",\n"
                    + "\"statement\":\"CALL TMP.Packaging_PackageCleanSQLProc()\"\n"
                    + "}");
        }
        catch (Exception ex)
        {
            System.out.println("[ERROR] - " + ex.getMessage());
            return false;
        }

        return true;
    }

    public boolean alertHandlerFirefox()
    {
        try
        {
            Narrator.logDebug("Attempting to click OK in alert pop-up");
            // Get a handle to the open alert, prompt or confirmation
            Alert alert = Driver.switchTo().alert();
            // Get the text of the alert or prompt
            alert.getText();
            // And acknowledge the alert (equivalent to clicking "OK")
            alert.accept();
//            alert.sendKeys(testCaseId);
            Narrator.logDebug("Ok Clicked successfully...proceeding");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" clicking OK in alert pop-up - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean FilterDropdownScrollDown(String contextItem, String validationText)
    {
        if (!SeleniumDriverInstance.clickElementByXpath("//div[@id='dropdownlistArrowcboTreeFilter']//div"))
        {
            return false;
        }

        for (int i = 0; i < 18; i++)
        {
            SeleniumDriverInstance.pause(100);
            if (!SeleniumDriverInstance.clickElementByXpath("//div[@id='jqxScrollBtnDownverticalScrollBarinnerListBoxcboTreeFilter']//div"))
            {
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementByXpath("//span[text()='" + contextItem + "']"))
        {
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpathVisibility("//a[text()='"+validationText+"']"))
//        {
//            return false;
//        }
        return true;
    }

    public boolean clickElementByCSSSelector(String elementCSSSelector)
    {
        try
        {

            Narrator.logDebug("Attempting to click element by CSS Selector - " + elementCSSSelector);
            //Thread.sleep(1000);
            waitForElementByCSSSelector(elementCSSSelector);
            Driver.findElement(By.cssSelector(elementCSSSelector)).click();
            Narrator.logDebug("Element clicked successfully...proceeding");

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" clicking element by CSS Selector - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public String getPageText()
    {
        return Driver.getPageSource();
    }

    public boolean assertPageText(String textToValidate)
    {
        if (Driver.getPageSource().contains(textToValidate))
        {
            return true;
        }
        return false;
    }

    public boolean switchToFrameById(String elementId)
    {
        try
        {
            try
            {
                if (!this.waitForElementById(elementId))
                {
                    return false;
                }
                Driver.switchTo().frame(elementId);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to frame by Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToFrameByClass(String elementClass, WebElement elementWebElement)
    {
        try
        {
            try
            {
                if (!this.waitForElementByClassName(elementClass))
                {
                    return false;
                }
                Driver.switchTo().frame(elementWebElement);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to frame by Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToFrameByPartialId(String elementId)
    {
        int waitCount = 0;
        try
        {
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    List<WebElement> frameMain = SeleniumDriverInstance.Driver.findElements(By.xpath("//iframe[contains(@id, '" + elementId + "')]"));
                    SeleniumDriverInstance.Driver.switchTo().frame(frameMain.get(0));
                    return true;
                }
                catch (Exception e)
                {
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to frame by Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToFramesetByPartialName(String elementName)
    {
        int waitCount = 0;
        try
        {
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    List<WebElement> frameMain = SeleniumDriverInstance.Driver.findElements(By.xpath("//frameset[contains(@name, '" + elementName + "')]"));
                    SeleniumDriverInstance.Driver.switchTo().frame(frameMain.get(0));
                    return true;
                }
                catch (Exception e)
                {
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to frameset by name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToFrameByPartialName(String elementName)
    {
        int waitCount = 0;
        try
        {
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    List<WebElement> frameMain = SeleniumDriverInstance.Driver.findElements(By.xpath("//frame[contains(@name, '" + elementName + "')]"));
                    SeleniumDriverInstance.Driver.switchTo().frame(frameMain.get(0));
                    return true;
                }
                catch (Exception e)
                {
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to frame by name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToFrameByName(String elementName)
    {
        int waitCount = 0;
        try
        {
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    List<WebElement> frameMain = SeleniumDriverInstance.Driver.findElements(By.xpath("//iframe[contains(@name, '" + elementName + "')]"));
                    SeleniumDriverInstance.Driver.switchTo().frame(frameMain.get(0));
                    return true;
                }
                catch (Exception e)
                {
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to frame by name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToInnerSintelligentFrame(String mainFrame, String frameText)
    {
        try
        {
            try
            {
                SeleniumDriverInstance.Driver.switchTo().defaultContent();
                if (!this.waitForElementByXpath("//iframe[contains(@name, '" + mainFrame + "')]"))
                {
                    return false;
                }
                else
                {
                    List<WebElement> frameMain = SeleniumDriverInstance.Driver.findElements(By.xpath("//iframe[contains(@name, '" + mainFrame + "')]"));
                    SeleniumDriverInstance.Driver.switchTo().frame(frameMain.get(0));
                    List<WebElement> frame = SeleniumDriverInstance.Driver.findElements(By.xpath("//frame[@name='" + frameText + "']"));
                    SeleniumDriverInstance.Driver.switchTo().frame(frame.get(0));
                }
            }
            catch (Exception e)
            {
                System.err.println("[ERROR] - " + e.getMessage());
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to inner sintelligent frame  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToIFrameByPartialName(String elementName)
    {
        int waitCount = 0;
        try
        {
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    List<WebElement> frameMain = SeleniumDriverInstance.Driver.findElements(By.xpath("//iframe[contains(@name, '" + elementName + "')]"));
                    SeleniumDriverInstance.Driver.switchTo().frame(frameMain.get(0));
                    return true;
                }
                catch (Exception e)
                {
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to frame by name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean expandNavigationMenuItemByXpath(String elementXpath)
    {
        try
        {
            List<WebElement> menuItem = SeleniumDriverInstance.Driver.findElements(By.xpath(elementXpath));
            if (menuItem.size() > 1)
            {
                for (int i = 0; i < menuItem.size(); i++)
                {
                    try
                    {
                        if (!menuItem.get(i).getAttribute("class").contains("test"))
                        {
                          if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
                            {                                                     
                                Actions builder = new Actions(Driver);
                                builder.moveToElement(menuItem.get(i)).build().perform();
                                builder.click(menuItem.get(i)).build().perform();
                                SeleniumDriverInstance.pause(100);
                                return true;
                            }
                            this.waitForElementByXpath(elementXpath);
                            menuItem.get(i).click();
                            SeleniumDriverInstance.pause(100);
                            return true;
                        }
                    }
                    catch (Exception e)
                    {
                        Narrator.logError("Failed to expand the menu item with xpath '" + elementXpath + "'" + e.getMessage());
                        this.DriverExceptionDetail = e.getMessage();
                        return false;
                    }
                }
            }
            else
            {
                this.waitForElementByXpath(elementXpath);
                this.waitForElementToBeClickableByXpath(elementXpath);
                if (!menuItem.get(0).getAttribute("class").contains("test"))
                {
                     if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
                    {                                                     
                        Actions builder = new Actions(Driver);
                        builder.moveToElement(menuItem.get(0)).build().perform();
                        builder.click(menuItem.get(0)).build().perform();
                        SeleniumDriverInstance.pause(100);
                        return true;
                    }
                     
                     if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
                     {
                        waitForElementByXpath(elementXpath);
                        waitForElementToBeClickableByXpath(elementXpath);
                        WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
//                        WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));                    

            //           JavascriptExecutor js = (JavascriptExecutor) Driver;
            //            js.executeScript("var evt = document.createEvent('MouseEvents');" + "evt.initMouseEvent('click',true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0,null);" + "arguments[0].dispatchEvent(evt);", elementToClick);
            
                    if (menuItem.get(0).getAttribute("href")!= null)
                    {
                        JavascriptExecutor executor = (JavascriptExecutor) Driver;
                        executor.executeScript("arguments[0].click();", menuItem.get(0));
                        return true;  
                    }
                    else
                    {
                         List <WebElement> we = this.Driver.findElements(By.xpath(elementXpath));     

                        
                        int ok_size=Driver.findElements(By.xpath(elementXpath)).size();

                        for (int i = 0; i < ok_size; i++) 
                        {
                            int x = we.get(i).getLocation().getX();
                            
                            if (x!=0) 
                            {
                                we.get(i).click();

                                return true;
                            }
                        }
                    }
                        return true;
                     }
                    this.waitForElementByXpath(elementXpath);
                    menuItem.get(0).click();
                    SeleniumDriverInstance.pause(100);
                    return true;
                }
            }
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to expand the menu item with xpath '" + elementXpath + "'" + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return true;
    }

    public boolean switchToFrameByPartialId(String elementId, Integer timeout)
    {
        int waitCount = 0;
        try
        {
            while (waitCount < timeout)
            {
                try
                {
                    List<WebElement> frameMain = SeleniumDriverInstance.Driver.findElements(By.xpath("//iframe[contains(@id, '" + elementId + "')]"));
                    SeleniumDriverInstance.Driver.switchTo().frame(frameMain.get(0));
                    return true;
                }
                catch (Exception e)
                {
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to frame by Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToLastDuplicateFrameById(String elementId)
    {
        int waitCount = 0;
        try
        {
            this.switchToDefaultContent();
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    List<WebElement> iframes = Driver.findElements(By.id(elementId));

                    Driver.switchTo().frame((WebElement) iframes.toArray()[iframes.size() - 1]);
                    return true;
                }
                catch (Exception e)
                {
                    //Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to last duplicate frame by Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToDefaultContent()
    {
        try
        {
            Driver.switchTo().defaultContent();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to default content  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToInnerSintelligentFrame(String mainFrame)
    {
        try
        {
            try
            {
                SeleniumDriverInstance.Driver.switchTo().defaultContent();
                if (!this.waitForElementByXpath("//iframe[contains(@name, '" + mainFrame + "')]"))
                {
                    return false;
                }
                else
                {
                    List<WebElement> frameMain = SeleniumDriverInstance.Driver.findElements(By.xpath("//iframe[contains(@name, '" + mainFrame + "')]"));
                    SeleniumDriverInstance.Driver.switchTo().frame(frameMain.get(0));
                    List<WebElement> frame = SeleniumDriverInstance.Driver.findElements(By.xpath("//frame[@name='workSpace']"));
                    SeleniumDriverInstance.Driver.switchTo().frame(frame.get(0));
                }
            }
            catch (Exception e)
            {
                System.err.println("[ERROR] - " + e.getMessage());
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to inner sintelligent frame  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToInnerSintelligentDataFrame()
    {
        try
        {
            SeleniumDriverInstance.Driver.switchTo().frame("screen");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to inner sintelligent data frame  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
    
    public boolean setUserRequstRefreshTime()
    {
        
        WebElement elementToChange = SeleniumDriverInstance.Driver.findElement(By.xpath("//option[text()='60 seconds']"));
        JavascriptExecutor js = (JavascriptExecutor)this.Driver;
        js.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",elementToChange, "value", "60000000");
        return true;
    }

    public boolean SwitchContext(String contextItem, String validationText)
    {
         String loader = this.WaitForLoaderIconNotVisible("//div[contains(@class,'jqx-datatable-load') and contains(@style,'visibility: visible; display: block;')]");
         if (!loader.equals("")) 
         {
             System.out.println("Failed to wait for icon to be invisible");
            return false;
         }

            if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
            {
                //switch iframes
            if(!SeleniumDriverInstance.switchToDefaultContent())
            {
                System.out.println("Failed to switch to the default context");
                return false;
            }

            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
            {
                System.out.println("Failed to wait for the refresh button to be available");
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
            {
                System.out.println("Failed to to wait for the refresh button to be clickable");
                return false;
            }
            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
            {
                System.out.println("Failed to click the refresh button");
                return false;
            }
            SeleniumDriverInstance.pause(3000);
            if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
            {
                System.out.println("Failed to switch to the inner sintelligent frame");
                return false;
            }
            
            if (!this.waitForElementByXpathVisibility(".//div[@id='dropdownlistArrowcboContext']/div"))
            {
                return false;
            }

            if (!this.waitForElementToBeClickableByXpath(".//div[@id='dropdownlistArrowcboContext']/div"))
            {
                return false;
            }
             WebElement elementToChange = SeleniumDriverInstance.Driver.findElement(By.xpath("//td [@class='contextDivTd']"));
            JavascriptExecutor js = (JavascriptExecutor)this.Driver;
            js.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",elementToChange, "class", "Luyanda");
            
            SeleniumDriverInstance.pause(5000);
            if (!this.clickElementByXpath(".//div[@id='dropdownlistArrowcboContext']/div"))
            {
                return false;
            }
        }
        else
        {
            if (!this.waitForElementByXpathVisibility("//td[text()='Context']//div[contains(@class, 'jqx-icon-arrow-down')]"))
            {
                return false;
            }

            if (!this.waitForElementToBeClickableByXpath("//td[text()='Context']//div[contains(@class, 'jqx-icon-arrow-down')]"))
            {
                return false;
            }

            if (!SeleniumDriverInstance.clickElementByXpath("//div[@id='dropdownlistArrowcboContext']/div"))
            {
                return false;
            }
        }

        this.pause(100);
         if (!this.waitForElementByXpath("//span[text()='" + contextItem + "']"))
        {
            System.out.println("Failed wait for "+contextItem+" expath");
            return false;
        }
        if (!this.waitForElementByXpathVisibility("//span[text()='" + contextItem + "']"))
        {
            System.out.println("Failed wait for "+contextItem+" Visibility");
            return false;
        }
        if (!this.waitForElementToBeClickableByXpath("//span[text()='" + contextItem + "']"))
        {
            System.out.println("Failed wait for "+contextItem+" to be clickable");
            return false;
        }
        if (!this.clickElementByXpathActions("//span[text()='" + contextItem + "']"))
        {
            System.out.println("Failed to click on "+contextItem);
            return false;
        }
           loader = this.WaitForLoaderIconNotVisible("//div[contains(@class,'jqx-datatable-load') and contains(@style,'visibility: visible; display: block;')]");
         if (!loader.equals("")) 
         {
             System.out.println("Failed to wait for icon to be invisible");
            
         }
        return true;
    }

    public String closeAllVisibleSintelligentTabs()
    {
        if (SeleniumDriverInstance.switchToDefaultContent())
        {
            return "Failed to switch to the default content when attempting to close the visible tabs";
        }

        //extract number of open tabs
        List<WebElement> visibleTabs;
        try
        {
            visibleTabs = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.AllVisibleMainTabCloseButtons()));
        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return "Failed to retrieve the open tab items";
        }

        //start at position 2 - skip 'Home'
        for (int i = 2; i < visibleTabs.size() + 1; i++)
        {
            if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpecificVisibleMainTabCloseButton("" + i)))
            {
                return "Failed to wait for the visible tab close button at position '" + i + "' to be visible";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpecificVisibleMainTabCloseButton("" + i)))
            {
                return "Failed to wait for the visible tab close button at position '" + i + "' to be clickable";
            }
            if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpecificVisibleMainTabCloseButton("" + i)))
            {
                return "Failed to click the visible tab close button at position '" + i + "'";
            }
        }

        return "";
    }

    public String WaitForLoaderIconNotVisible()
    {
        try
        {
            int killCounter = 0;
            boolean isPresent = false;
            SeleniumDriverInstance.pause(100);
            isPresent = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.smallLoaderIcon(), 1);
            SeleniumDriverInstance.pause(100);
            if (isPresent)
            {
                SeleniumDriverInstance.pause(100);
                isPresent = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.smallLoaderIcon(), 1);
                while (isPresent)
                {
                    SeleniumDriverInstance.pause(500);
                    isPresent = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.smallLoaderIcon(), 1);
                    SeleniumDriverInstance.pause(500);
                    if (killCounter == 60)
                    {
                        return "Could not wait for the page to load - loader icon did not complete";
                    }

                    //wait to change to false
                    isPresent = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.smallLoaderIcon(), 1);
                    killCounter++;
                }
            }
            else
            {
                SeleniumDriverInstance.pause(100);
            }
        }
        catch (Exception e)
        {
            return "Failed to wait for the loader icon to disappear";
        }

        return "";
    }

    public String WaitForLoaderIconNotVisible(String iconXpath)
    {
        int killCounter = 0;
        boolean isPresent = false;
        isPresent = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(iconXpath, 2);
        if (isPresent)
        {
            SeleniumDriverInstance.pause(100);
            isPresent = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(iconXpath, 1);
            while (isPresent)
            {
                SeleniumDriverInstance.pause(500);
                isPresent = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(iconXpath, 1);
                SeleniumDriverInstance.pause(500);
                if (killCounter == 60)
                {
                    return "Could not wait for the page to load - loader icon did not complete";
                }

                //wait to change to false
                isPresent = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(iconXpath, 1);
                killCounter++;
            }
        }
        else
        {
            SeleniumDriverInstance.pause(100);
        }

        return "";
    }

    public String ExpandAllConfigConsoleVisibleItems()
    {

        List<WebElement> expands = null;

        try
        {
            expands = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.ConfigConsoleCollapseArrow()));
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return "Could not find the 'collapse' web elements after navigating to the page";
        }
        int size = expands.size();
        while (expands.size() != 0)
        {

            try
            {

                expands = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.ConfigConsoleCollapseArrow()));
                SeleniumDriverInstance.pause(100);

                expands.get(expands.size() - 1).click();

                //wait for the 'Loading' to dissappear
                boolean isLoading = false;
                isLoading = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Loading..."), 1);
                while (isLoading)
                {
                    isLoading = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Loading..."), 1);
                    this.pause(100);
                }

                SeleniumDriverInstance.pause(100);

                expands.clear();
                expands = new ArrayList<WebElement>();

                SeleniumDriverInstance.pause(500);

                expands = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.ConfigConsoleCollapseArrow()));
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
                return "Could not find the 'collapse' web elements after '" + (size - (expands.size())) + "' iterations";
            }
        }

        return "";
    }

    public String CustomQuickExpandAllConfigConsoleVisibleItems()
    {

        List<WebElement> expands = null;

        try
        {
            expands = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.ConfigConsoleCollapseArrow()));
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return "Could not find the 'collapse' web elements after navigating to the page";
        }
        int size = expands.size();
        while (expands.size() != 0)
        {

            try
            {

                expands = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.ConfigConsoleCollapseArrow()));
                SeleniumDriverInstance.pause(100);
                Actions builder = new Actions(Driver);
                builder.moveToElement(expands.get(expands.size() - 1)).build().perform();
                builder.click(expands.get(expands.size() - 1)).build().perform();
                

                //wait for the 'Loading' to dissappear
                boolean isLoading = false;
                isLoading = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.SpanContainsTextXpath("Loading..."), 10);
                while (isLoading)
                {
                    isLoading = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.SpanContainsTextXpath("Loading..."), 10);
                    this.pause(100);
                }

                SeleniumDriverInstance.pause(100);

                expands.clear();
                expands = new ArrayList<WebElement>();

                SeleniumDriverInstance.pause(100);

                expands = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.ConfigConsoleCollapseArrow()));
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
                return "Could not find the 'collapse' web elements after '" + (size - (expands.size())) + "' iterations";
            }
        }

        return "";
    }

    public String CollapseAllConfigConsoleVisibleItems()
    {

        List<WebElement> collapse = null;

        try
        {
            collapse = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.ConfigConsoleExpandArrow()));
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return "Could not find the 'collapse' web elements after navigating to the page";
        }
        int size = collapse.size();
        while (collapse.size() != 0)
        {

            try
            {
                SeleniumDriverInstance.pause(100);
                collapse = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.ConfigConsoleExpandArrow()));

                collapse.get(collapse.size() - 1).click();

                //wait for the 'Loading' to dissappear
                boolean isLoading = false;
                isLoading = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Loading..."), 1);
                while (isLoading)
                {
                    isLoading = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Loading..."), 1);
                    this.pause(100);
                }

                SeleniumDriverInstance.pause(100);
                collapse.clear();
                SeleniumDriverInstance.pause(100);

                collapse = SeleniumDriverInstance.Driver.findElements(By.xpath(SintelligentPageObject.ConfigConsoleExpandArrow()));
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
                return "Could not find the 'expand' web elements after '" + (size - (collapse.size())) + "' iterations";
            }
        }

        return "";
    }

    public boolean hoverOverElementByXpathNew(String elementXpath)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);

            Actions hoverTo = new Actions(Driver);
            hoverTo.moveToElement(Driver.findElement(By.xpath(elementXpath)));
            hoverTo.perform();

            Narrator.logInfo("[Info]Text retrieved successfully from element - " + elementXpath);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("[Error] Failed to retrieve text from element Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean SwitchContextScrollUp(String contextItem, String validationText)
    {
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
             if(!SeleniumDriverInstance.switchToDefaultContent())
            {
                System.out.println("Failed to switch to the default context");
                return false;
            }

            if(!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnRefreshTab")))
            {
                System.out.println("Failed to click the refresh button");
                return false;
            }
            SeleniumDriverInstance.pause(3000);
            if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
            {
                System.out.println("Failed to switch to the inner sintelligent frame");
                return false;
            }
                    SeleniumDriverInstance.pause(5000);
             WebElement elementToChange = SeleniumDriverInstance.Driver.findElement(By.xpath("//td [@class='contextDivTd']"));
          JavascriptExecutor js = (JavascriptExecutor)this.Driver;
         js.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",elementToChange, "class", "Luyanda");
            
         SeleniumDriverInstance.pause(2500);
        }

        if (!SeleniumDriverInstance.clickElementByXpath("//td[text()='Context']//div[contains(@class, 'jqx-icon-arrow-down')]"))
        {
            return false;
        }

        for (int i = 0; i < 5; i++)
        {
            SeleniumDriverInstance.pause(250);
            if (!SeleniumDriverInstance.clickElementByXpath("//div[@id='innerListBoxcboContext']//div[contains(@class, 'jqx-icon-arrow-up')]"))
            {
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementByXpath("//span[text()='" + contextItem + "']"))
        {
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpathVisibility("//a[text()='"+validationText+"']"))
//        {
//            return false;
//        }
        return true;
    }

    public boolean SwitchContextScrollDown(String contextItem, String validationText)
    {
        if (!SeleniumDriverInstance.clickElementByXpath("//td[text()='Context']//div[contains(@class, 'jqx-icon-arrow-down')]"))
        {
            return false;
        }

        for (int i = 0; i < 5; i++)
        {
            SeleniumDriverInstance.pause(500);
            if (!SeleniumDriverInstance.clickElementByXpath("//div[@id='innerListBoxcboContext']//div[contains(@class, 'jqx-icon-arrow-down')]"))
            {
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementByXpath("//span[text()='" + contextItem + "']"))
        {
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpathVisibility("//a[text()='"+validationText+"']"))
//        {
//            return false;
//        }
        return true;
    }

    public String ConfigConsoleScrollToElement(String Xpath)
    {
        int stopCounter = 0;

        boolean hostIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibility(Xpath, 1);

        while (!hostIsVisible && stopCounter != 20)
        {
            if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not click the ConfigConsoleHostsDownScrollArrow";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not click the ConfigConsoleHostsDownScrollArrow";
            }

            stopCounter++;
            hostIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibility(Xpath, 1);

            //to ensure properly visible
            if (hostIsVisible)
            {
                if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
            }
        }
        return "";
    }

    public String ConfigConsoleScrollToElementFaster(String Xpath)
    {
        int stopCounter = 0;

        boolean hostIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibility(Xpath, 1);

        while (!hostIsVisible && stopCounter != 100)
        {
            if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not click the ConfigConsoleHostsDownScrollArrow";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not click the ConfigConsoleHostsDownScrollArrow";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not click the ConfigConsoleHostsDownScrollArrow";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not click the ConfigConsoleHostsDownScrollArrow";
            }

            stopCounter++;
            hostIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibility(Xpath, 1);

            //to ensure properly visible
            if (hostIsVisible)
            {
                if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
            }
        }
        return "";
    }

    public String ConfigConsoleScrollDownToElement(String Xpath)
    {
        int stopCounter = 0;

        boolean hostIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(Xpath, 1);

        while (!hostIsVisible && stopCounter != 20)
        {
            if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not click the ConfigConsoleHostsDownScrollArrow";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
            {
                return "Could not click the ConfigConsoleHostsDownScrollArrow";
            }

            stopCounter++;
            hostIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(Xpath, 1);

            //to ensure properly visible
            if (hostIsVisible)
            {
                if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be clickable";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(SintelligentPageObject.ConfigConsoleHostsDownScrollArrow()))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
            }
        }
        return "";
    }

    public String ConfigConsoleScrollToElementNewFast(String Xpath, String ArrowXpath)
    {
        int stopCounter = 0;

        boolean hostIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(Xpath, 1);

        while (!hostIsVisible)
        {
            if (!SeleniumDriverInstance.waitForElementByXpathVisibility(ArrowXpath))
            {
               if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.DivWithId("dropdownlistArrowcboTreeFilter")))
                {
                        return "Failed to wait for filter drop down arrow and could not wait for the directional arrow button to be visible";
                }

                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.DivWithId("dropdownlistArrowcboTreeFilter")))
                {
                        return "Failed to wait filter drop down arrow to be clickable and could not wait for the directional arrow button to be visible";
                }

                if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.DivWithId("dropdownlistArrowcboTreeFilter")))
                {
                        return "Failed to click on filter drop down arrow and could not wait for the directional arrow button to be visible";
                }
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
            {
                return "Could not wait for the directional arrow button to be clickable";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
            {
                return "Could not click the directional arrow button";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
            {
                return "Could not wait for the directional arrow button to be clickable";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
            {
                return "Could not click the directional arrow button";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
            {
                return "Could not click the directional arrow button";
            }

            stopCounter++;
            hostIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(Xpath, 1);

            //to ensure properly visible
            if (hostIsVisible)
            {
                if (!SeleniumDriverInstance.waitForElementByXpathVisibility(ArrowXpath))
                {
                    if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.DivWithId("dropdownlistArrowcboTreeFilter")))
                    {
                            return "Failed to wait for filter drop down arrow and could not wait for the directional arrow button to be visible";
                    }

                    if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.DivWithId("dropdownlistArrowcboTreeFilter")))
                    {
                            return "Failed to wait filter drop down arrow to be clickable and could not wait for the directional arrow button to be visible";
                    }

                    if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.DivWithId("dropdownlistArrowcboTreeFilter")))
                    {
                            return "Failed to click on filter drop down arrow and could not wait for the directional arrow button to be visible";
                    }
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
                {
                    return "Could not wait for the directional arrow button to be clickable";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
                {
                    return "Could not click the directional arrow button";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
                {
                    return "Could not wait for the directional arrow button to be clickable";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
                {
                    return "Could not click the directional arrow button";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
                {
                    return "Could not wait for the directional arrow button to be clickable";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
                {
                    return "Could not click the directional arrow button";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
                {
                    return "Could not click the directional arrow button";
                }
            }
        }

        if (stopCounter == 100)
        {
            return "Could not scroll to the element";
        }

        return "";
    }

    public String ConfigConsoleScrollToElementNew(String Xpath, String ArrowXpath)
    {
        int stopCounter = 0;

        boolean hostIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibility(Xpath, 1);

        while (!hostIsVisible && stopCounter != 50)
        {
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
            {
                return "Could not click the ConfigConsoleHostsDownScrollArrow";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
            {
                return "Could not click the ConfigConsoleHostsDownScrollArrow";
            }

            stopCounter++;
            hostIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibility(Xpath, 1);

            //to ensure properly visible
            if (hostIsVisible)
            {
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }
            }
        }
        return "";
    }

    public boolean moveToElementById(String elementId)
    {
        try
        {
            Actions moveTo = new Actions(Driver);
            moveTo.moveToElement(Driver.findElement(By.id(elementId)));
            moveTo.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" moving to element - " + elementId + " - " + e.getStackTrace());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean moveToElementByXpath(String elementXpath)
    {
        try
        {
            Actions moveTo = new Actions(Driver);
            moveTo.moveToElement(Driver.findElement(By.xpath(elementXpath)));
            moveTo.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" moving to element - " + elementXpath + " - " + e.getStackTrace());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToDefaultContentWhenElementNoLongerVisible(String previousFrameId)
    {
        try
        {
            waitForElementNoLongerPresentById(previousFrameId);
            Driver.switchTo().defaultContent();
            Narrator.logDebug("Successfully switched to default content, current frame handle = " + Driver.getWindowHandle() + ", previous frameId - " + previousFrameId);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to default content when element is no longer visible - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByValueFromDropDownListUsingId(String elementId, String valueToSelect)
    {
        try
        {
            waitForElementById(elementId);
            Select dropDownList = new Select(Driver.findElement(By.id(elementId)));
            dropDownList.selectByValue(valueToSelect);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting from dropdownlist by value using Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByValueFromDropDownListUsingXpath(String elementXpath, String valueToSelect)
    {
        try
        {
             if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
            {
                
                            waitForElementByXpath(elementXpath);
            waitForElementToBeClickableByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).build().perform();
            builder.click(elementToClick).build().perform();
                        List<WebElement> options = elementToClick.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getAttribute("value");
                if (optTxt.equals(valueToSelect))
                {
            builder.moveToElement(option).build().perform();
            builder.click(option).build().perform();
                    //Actions action = new Actions(SeleniumDriverInstance.Driver);
                    //action.sendKeys(Keys.ENTER).build().perform();
                    //option.click();

                    Narrator.logError("[Info] Selected partial text '" + valueToSelect + "' from element '" + elementXpath + "'");
                    return true;
                }
            }
            
            }
             else
             {                       
            waitForElementByXpath(elementXpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
            dropDownList.selectByValue(valueToSelect);
             }
            return true;
             
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting from dropdownlist by value using Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean acceptAlertDialog()
    {
        int waitCount = 0;
        try
        {
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    Driver.switchTo().alert().accept();
                    return true;
                }
                catch (Exception e)
                {
                    //Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError(" accepting alert dialog - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public String retrieveTextById(String elementId)
    {
        String retrievedText = "";
        try
        {
            waitForElementById(elementId);
            WebElement elementToRead = Driver.findElement(By.id(elementId));
            retrievedText = elementToRead.getText();
            return retrievedText;
        }
        catch (Exception e)
        {
            Narrator.logError(" reading text from element - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public String retrieveTextByClassName(String elementClassName)
    {
        String retrievedText = "";
        try
        {
            this.waitForElementByClassName(elementClassName);
            WebElement elementToRead = Driver.findElement(By.className(elementClassName));
            retrievedText = elementToRead.getText();
            return retrievedText;
        }
        catch (Exception e)
        {
            Narrator.logError(" reading text from element - " + elementClassName + " error - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public String retrieveTextByName(String elementName)
    {
        String retrievedText = "";
        try
        {
            this.waitForElementByName(elementName);
            WebElement elementToRead = Driver.findElement(By.name(elementName));
            retrievedText = elementToRead.getAttribute("value");
            return retrievedText;
        }
        catch (Exception e)
        {
            Narrator.logError(" reading text from element - " + elementName + " error - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public String retrieveTextByLinkText(String elementLinkText)
    {
        String retrievedText = "";
        try
        {
            this.waitForElementByLinkText(elementLinkText);
            WebElement elementToRead = Driver.findElement(By.linkText(elementLinkText));
            retrievedText = elementToRead.getText();
            return retrievedText;
        }
        catch (Exception e)
        {
            Narrator.logError(" reading text from element - " + elementLinkText + " error - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public String retrieveTextByCSS(String elementCSS)
    {
        String retrievedText = "";

        try
        {
            this.waitForElementByCSSSelector(elementCSS);
            WebElement elementToRead = Driver.findElement(By.cssSelector(elementCSS));
            retrievedText = elementToRead.getText();
            return retrievedText;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to read text from element CSS '" + elementCSS + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public Boolean ClickEmbeddedElementByTagNameUsingContainerId(String containerId, String tagName, String validationText)//
    {
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement DivList = SeleniumDriverInstance.Driver.findElement(By.id(containerId));

            List<WebElement> subElements = DivList.findElements(By.tagName(tagName));

            for (WebElement subElement : subElements)
            {
                //SeleniumDriverInstance.pause(500);
                Narrator.logDebug("Detected entry " + subElement.getText() + "        Clickable");

                if (subElement.getText().toUpperCase().trim().equals(validationText.toUpperCase().trim()))
                {
                    subElement.click();
                    Narrator.logDebug("Validation match  " + validationText + " = " + subElement.getText());
                    Narrator.logDebug("Validation match  " + validationText + "       FOUND!!!");
                    Narrator.logDebug("Validation match  " + subElement.getText().toUpperCase() + "   <---FOUND!!!");
                    return true;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickEmbeddedListElementByTagNameUsingContainerId(String containerId, String tagName, String validationText)
    {
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement DivList = SeleniumDriverInstance.Driver.findElement(By.id(containerId));

            List<WebElement> subElements = DivList.findElements(By.tagName(tagName));

            for (WebElement subElement : subElements)
            {
                this.pause(2000);
                Narrator.logDebug("Detected entry " + subElement.getText() + "        Clickable");

                if (subElement.getText().toUpperCase().trim().equals(validationText.toUpperCase().trim()))
                {
                    subElement.click();
                    Narrator.logDebug("Validation match  " + validationText + "       FOUND!!!");
                    Narrator.logDebug("Validation match  " + subElement.getText().toUpperCase() + "   <---FOUND!!!");
                    return true;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickListElementByXpath(String elementXpath, String validationText)
    {
        try
        {
            SeleniumDriverInstance.waitForElementByXpath(elementXpath);

            WebElement elementToEnter = SeleniumDriverInstance.Driver.findElement(By.xpath(elementXpath));
            elementToEnter.findElement(By.xpath(elementXpath)).sendKeys(validationText);
            elementToEnter.sendKeys(Keys.TAB);
            Narrator.logDebug("ClickListElementByXpath complete");
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickEmbeddedElementByIDUsingContainerId(String containerId, String ElementID)
    {
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement DivList = Driver.findElement(By.id(containerId));
            WebElement subElement = DivList.findElement(By.id(ElementID));

            Narrator.logDebug("Detected entry " + subElement.getText());

            subElement.click();

            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickEmbeddedListElementByIDUsingContainerId(String containerId, String ElementID, String ChildTagName, String validationText)
    {
        String ChildElementText = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement DivList = Driver.findElement(By.id(containerId));

            WebElement subElement = DivList.findElement(By.id(ElementID));

            Narrator.logDebug("Detected entry " + subElement.getText());

            subElement.click();

            List<WebElement> ChildElements = subElement.findElements(By.tagName(ChildTagName));

            for (WebElement ChildElement : ChildElements)
            {
                ChildElementText = ChildElement.getText();
                if (ChildElementText == null)
                {
                    continue;
                }

                Narrator.logDebug("Detected Child Element: " + ChildElementText);

                if (ChildElementText.equals(validationText))
                {
                    Narrator.logDebug("About to click child element: " + ChildElementText);
                    ChildElement.click();
                    Narrator.logDebug("Clicked child element: " + ChildElementText);
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean SelectFromEmbeddedTableElementByIDUsingContainerId(String containerId, String ParentTag, String ChildTag, String validationText, String ListContainerId, String ItemTag, String ItemText)
    {
        String ChildElementText = "";
        String ListItemText = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);
            WebElement Div = Driver.findElement(By.id(containerId));
            List<WebElement> ParentElements = Div.findElements(By.tagName(ParentTag));

            for (WebElement ParentElement : ParentElements)
            {
                List<WebElement> ChildElements = ParentElement.findElements(By.tagName(ChildTag));

                for (WebElement ChildElement : ChildElements)
                {
                    ChildElementText = ChildElement.findElement(By.tagName(ChildTag)).getText();

                    if (ChildElementText == null)
                    {
                        continue;
                    }

                    Narrator.logDebug("Detected child element text :" + ChildElementText);

                    if (ChildElementText.equals(validationText))
                    {
                        Narrator.logDebug("About to click: " + ChildElementText);
                        ParentElement.click();
                        Narrator.logDebug("Clicked: " + ChildElementText);
                        WebElement ListContainer = Driver.findElement(By.id(ListContainerId));
                        Narrator.logDebug("Found Container Element: " + ListContainer.getText());
                        List<WebElement> ListItems = ListContainer.findElements(By.tagName(ItemTag));
                        Narrator.logDebug("Listing List Items");
                        SeleniumDriverInstance.pause(500);
                        for (WebElement ListItem : ListItems)
                        {
                            ListItemText = ListItem.getText();
                            Narrator.logDebug("Found ListElement: " + ListItemText);

                            if (ListItemText.equals(ItemText))
                            {
                                ListItem.click();
                                Narrator.logDebug("Clicking Item :" + ListItem.getText());
                                return true;
                            }
                        }
                    }
                }
            }
            Narrator.logError(" failed to find element : " + ItemText);
            return false;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickEmbeddedButtonByTagNameUsingContainerTagName(String containerId, String TableXpath, String ParentTagName, String ChildtagName, String LinkTag, String validationText)
    {
        String ChildElementText = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement Div = Driver.findElement(By.id(containerId));
            Narrator.logDebug("Found element DIV: " + Div.getText());
            WebElement Table = Div.findElement(By.xpath(TableXpath));
            Narrator.logDebug("Found element Table: " + Div.getText());
            List<WebElement> ParentElements = Table.findElements(By.tagName(ParentTagName));
            Narrator.logDebug("Listing parent elements");
            for (WebElement ParentElement : ParentElements)
            {
                Narrator.logDebug("Found element: " + ParentElement.getText());
                List<WebElement> ChildElements = ParentElement.findElements(By.tagName(ParentTagName));
                Narrator.logDebug("Listing ChildElements");
                for (WebElement ChildElement : ChildElements)
                {
                    Narrator.logDebug("Found Child Element: " + ChildElement.getText());
                    ChildElementText = ChildElement.getText();
                    Narrator.logDebug("Need to compare elements: " + ChildElement.getText() + " And " + validationText);
                    if (ChildElementText.toUpperCase().trim().equals(validationText.toUpperCase()))
                    {
                        Narrator.logDebug("Found Element: " + ChildElement.getText());
                        WebElement LinkElement = ParentElement.findElement(By.tagName(LinkTag));
                        Narrator.logDebug("Found sub Element: " + LinkElement.getText());
                        LinkElement.click();

                        return true;
                    }
                }

            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickEmbeddedElementByTagNameUsingContainerTagName(String DivID, String containerTagName, String tagName, String validationText)
    {
        String subElementText = "";
        try
        {
            WebElement Div = Driver.findElement(By.id(DivID));
            List<WebElement> ParentElements = Div.findElements(By.tagName(containerTagName));

            for (WebElement ParentElement : ParentElements)
            {

                subElementText = ParentElement.findElement(By.tagName(tagName)).getText();

                if (subElementText == null)
                {
                    continue;
                }

                Narrator.logDebug("Detected child element text :" + subElementText);

                if (subElementText.equals(validationText))
                {
                    ParentElement.click();
                    Narrator.logDebug("Validation match  " + validationText + " FOUND!!!");
                    return true;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean validateTextForEmbeddedElementsByTagNameUsingContainerId(String containerId, String tagName, String validationText)
    {
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement DivList = SeleniumDriverInstance.Driver.findElement(By.id(containerId));

            List<WebElement> subElements = DivList.findElements(By.tagName(tagName));

            for (WebElement subElement : subElements)
            {
                Narrator.logDebug("Detected entry " + subElement.getText());

                if (subElement.getText().equals(validationText))
                {
                    Narrator.logDebug("Validation match  " + validationText + "FOUND!!!");
                    return true;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickEmbeddedElementWithinSubElementByTagNameUsingContainerId(String containerId, String Xpath, String SubTagName, String EmbeddedTagName, String FindName, String validationText)
    {
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement div = Driver.findElement(By.id(containerId));
            Narrator.logDebug("Found div");
            WebElement table = div.findElement(By.xpath(Xpath));
            Narrator.logDebug("Found table");
            List<WebElement> rows = table.findElements(By.tagName(SubTagName));
            Narrator.logDebug("Found rows");
            int rowCount = 1;
            for (WebElement row : rows)
            {
                Narrator.logDebug("Row - " + rowCount);
                List<WebElement> cells = row.findElements(By.tagName(EmbeddedTagName));

                if (cells == null || cells.size() < 1)
                {
                    continue;
                }
                String firstcell = cells.get(0).getText();
                Narrator.logDebug(firstcell);
                Narrator.logDebug("Checking if " + firstcell + " = " + FindName);

                if (firstcell.toUpperCase().trim().equals(FindName.toUpperCase()))
                {
                    Narrator.logDebug(firstcell + "=" + FindName);
                    WebElement Button = row.findElement(By.linkText(validationText));
                    Button.click();
                    Narrator.logDebug("Button Clicked successfully - " + FindName + " = " + validationText);
                    return true;
                }

                rowCount++;
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickEmbeddedItemWithinTableByAttributeUsingContainerID(String containerId, String Xpath, String SubTagName, String EmbeddedTagName, String FindName, String ButtonTag, String Attribute, String AttributeName)
    {
        String ButtonAttribute = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement div = Driver.findElement(By.id(containerId));
            Narrator.logDebug("Found div");
            WebElement table = div.findElement(By.xpath(Xpath));
            Narrator.logDebug("Found table");
            List<WebElement> rows = table.findElements(By.tagName(SubTagName));
            Narrator.logDebug("Found rows");
            int rowCount = 1;
            for (WebElement row : rows)
            {
                Narrator.logDebug("Row - " + rowCount);
                List<WebElement> cells = row.findElements(By.tagName(EmbeddedTagName));

                if (cells == null || cells.size() < 1)
                {
                    continue;
                }
                String firstcell = cells.get(0).getText();
                Narrator.logDebug(firstcell);
                Narrator.logDebug("Checking if " + firstcell + " = " + FindName);

                if (firstcell.toUpperCase().trim().equals(FindName.toUpperCase()))
                {
                    Narrator.logDebug(firstcell + "=" + FindName);
                    List<WebElement> Buttons = row.findElements(By.tagName(ButtonTag));
                    for (WebElement Button : Buttons)
                    {
                        ButtonAttribute = Button.getAttribute(Attribute).toString();
                        if (ButtonAttribute.toUpperCase().trim().equals(AttributeName.toUpperCase()))
                        {
                            Narrator.logDebug("About to click " + ButtonAttribute + " button.");
                            Button.click();
                            return true;
                        }
                    }
                    return true;
                }
                rowCount++;
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickEmbeddedFormButttonUsingContainerXpath(String ContainerXpath, String TagName, String ValidationText)
    {
        String ButtonText = "";
        try
        {
            SeleniumDriverInstance.waitForElementByXpath(ContainerXpath);

            WebElement div = Driver.findElement(By.xpath(ContainerXpath));
            Narrator.logDebug("Found Div");
            List<WebElement> Buttons = div.findElements(By.tagName(TagName));
            Narrator.logDebug("listing Buttons");
            for (WebElement Button : Buttons)
            {
                Narrator.logDebug(Button.getText());
                ButtonText = Button.getText();
                Narrator.logDebug(ButtonText);
                if (ButtonText.toUpperCase().trim().equals(ValidationText))
                {
                    Narrator.logDebug("About to click button: " + Button.getText());
                    Button.click();
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickEmbeddedFilterItemWithinTableUsingContainerID(String containerId, String Xpath, String SubTagName, String EmbeddedTagName, int CellNumber, String FindName, String ButtonTag)
    {
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement div = Driver.findElement(By.id(containerId));
            Narrator.logDebug("Found div");
            WebElement table = div.findElement(By.xpath(Xpath));
            Narrator.logDebug("Found table");
            WebElement row = table.findElement(By.tagName(SubTagName));
            Narrator.logDebug("Found rows");
            List<WebElement> cells = row.findElements(By.tagName(EmbeddedTagName));

            for (WebElement cell : cells)
            {
                String Neededcell = cells.get(CellNumber).getText();
                System.out.println(Neededcell);
                Narrator.logDebug("Checking if " + Neededcell + " = " + FindName);

                if (Neededcell.toUpperCase().trim().equals(FindName.toUpperCase()))
                {
                    Narrator.logDebug(Neededcell + "=" + FindName);
                    WebElement Button = cell.findElement(By.xpath(ButtonTag));
                    Narrator.logDebug("Button found, about to click");
                    Button.click();

                    return true;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickElementWithinTableByTagNameUsingContainerId(String containerId, String Xpath, String SubTagName, String EmbeddedTagName, int CellNumber)
    {
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement div = Driver.findElement(By.id(containerId));
            Narrator.logDebug("Found div");
            WebElement table = div.findElement(By.xpath(Xpath));
            Narrator.logDebug("Found table");
            List<WebElement> rows = table.findElements(By.tagName(SubTagName));
            Narrator.logDebug("Found rows");
            int rowCount = 1;
            for (WebElement row : rows)
            {
                Narrator.logDebug("Row - " + rowCount);
                List<WebElement> cells = row.findElements(By.tagName(EmbeddedTagName));

                if (cells == null || cells.size() < 1)
                {
                    continue;
                }
                WebElement firstcell = cells.get(CellNumber);
                Narrator.logDebug("Found Cells:" + cells.size());

                firstcell.click();
                Narrator.logDebug("Cell Clicked successfully");
                return true;

            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean SelectValueFromEmbeddedElementByNameAndTagNameUsingContainerId(String containerId, String tagName, String validationText, String valueToSelect)
    {
        try
        {
            this.waitForElementById(containerId);

            WebElement DivList = this.Driver.findElement(By.id(containerId));

            List<WebElement> subElements = DivList.findElements(By.tagName(tagName));

            for (WebElement subElement : subElements)
            {
                Narrator.logDebug("Detected entry " + subElement.getText());

                if (subElement.getText().equals(validationText))
                {
                    Select dropDownList = new Select(DivList.findElement(By.name(validationText)));
                    dropDownList.selectByVisibleText(valueToSelect);
                    Narrator.logDebug("Validation match  " + validationText + "FOUND!!!");
                    return true;
                }
            }
            Narrator.logError(" failed to find element : " + validationText);
            return false;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickEmbeddedParentElementByTagNameAndChildElementValidationTextUsingContainerXpath(String containerXpath, String tagName, String validationTagName, String validationText)
    {
        try
        {
            this.waitForElementByXpath(containerXpath);

            WebElement Container = this.Driver.findElement(By.xpath(containerXpath));
            Narrator.logDebug("Detected entry " + Container.getText());
            List<WebElement> subElements = Container.findElements(By.tagName(tagName));

            for (WebElement subElement : subElements)
            {
                Narrator.logDebug("Detected entry " + subElement.getText());
                WebElement childElement = subElement.findElement(By.tagName(validationTagName));

                if (childElement == null)
                {
                    continue;
                }

                Narrator.logDebug("Detected entry " + childElement.getText());

                if (childElement.getText().equals(validationText))
                {
                    subElement.click();
                    Narrator.logDebug("Validation match  " + validationText + "FOUND!!!");
                    return true;
                }
            }
            return false;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickEmbeddedParentElementByTagNameAndChildElementValidationTextUsingContainerClassName(String containerClassName, String tagName, String validationTagName, String validationText)
    {
        try
        {
            this.waitForElementByClassName(containerClassName);

            List<WebElement> Containers = this.Driver.findElements(By.className(containerClassName));

            Narrator.logDebug("Detected " + Containers.size() + "container entries ");

            for (WebElement Container : Containers)
            {

                List<WebElement> subElements = Container.findElements(By.tagName(tagName));

                Narrator.logDebug("Detected " + subElements.size() + " subelements");

                for (WebElement subElement : subElements)
                {
                    Narrator.logDebug("Detected sub element entry: " + subElement.getText());

                    WebElement childElement = subElement.findElement(By.tagName(validationTagName));

                    if (childElement == null || !subElement.isDisplayed())
                    {
                        Narrator.logDebug("Detected entry is null or not visible");

                        continue;

                    }

                    String elementText = childElement.getText();

                    Narrator.logDebug("Detected child element entry:  " + elementText);

                    if (elementText.toUpperCase().trim().equals(validationText.toUpperCase().trim()))
                    {
                        Narrator.logDebug("Validation match  " + validationText + "FOUND!!! Clicking element.");
                        subElement.click();
                        return true;
                    }
                    else
                    {
                        Narrator.logDebug("Validation failed entry text did not match expected value - " + elementText + " <> " + validationText);
                    }
                }
            }
            Narrator.logError(" failed to find element : " + validationText);
            return false;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean SelectValueFromEmbeddedElementByNameUsingContainerId(String containerId, String validText, String valueToSelect)
    {
        try
        {
            this.waitForElementById(containerId);

            WebElement Div = this.Driver.findElement(By.id(containerId));

            //WebElement subElement = Div.findElement(By.id(tagID));
            Select dropDownList = new Select(Div.findElement(By.name(validText)));

            dropDownList.selectByVisibleText(valueToSelect);

            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" selecting from dropdownlist by text using Id - " + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
    }

    public Boolean SelectValueFromEmbeddedElementByTagNameUsingContainerId(String containerId, String TagName, String valueToSelect)
    {
        try
        {
            this.waitForElementById(containerId);

            WebElement Div = this.Driver.findElement(By.id(containerId));

            //WebElement subElement = Div.findElement(By.id(tagID));
            Select dropDownList = new Select(Div.findElement(By.tagName(TagName)));
            Narrator.logDebug("[Found:] " + dropDownList.toString());
            dropDownList.selectByVisibleText(valueToSelect);

            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" selecting from dropdownlist by text using Id - " + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
    }

    public Boolean SelectValueFromEmbeddedElementByIDUsingContainerId(String containerId, String elementID, String valueToSelect)
    {
        try
        {
            this.waitForElementById(containerId);

            WebElement Div = this.Driver.findElement(By.id(containerId));

            //WebElement subElement = Div.findElement(By.id(tagID));
            Select dropDownList = new Select(Div.findElement(By.id(elementID)));

            dropDownList.selectByVisibleText(valueToSelect);

            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" selecting from dropdownlist by text using Id - " + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
    }

    public Boolean ClickEmbeddedTableElementByNameUsingContainerXpath(String containerXpath, String elementTagName, String childSubElementTagName, int valueToSelect)
    {
        try
        {
            WebElement table = SeleniumDriverInstance.Driver.findElement(By.xpath(containerXpath));
            List<WebElement> rows = table.findElements(By.tagName(elementTagName));
            for (WebElement row : rows)
            {
                List<WebElement> cells = row.findElements(By.tagName(childSubElementTagName));

                if (cells == null || cells.size() < 1)
                {
                    continue;
                }
                WebElement cellToClick = cells.get(valueToSelect);
                cellToClick.click();
                SeleniumDriverInstance.pause(2000);

            }

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError("Could not find second cell" + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
    }

    public boolean selectOrEnterTextByIdUsingParentChildTagName(String parentElementId, String childTagName, String innerChildTagName, String innerSiblingText, String babyElementName, String textToEnter)
    {
        try
        {
            WebElement parentElement = Driver.findElement(By.id(parentElementId));
            List<WebElement> children = parentElement.findElements(By.tagName(childTagName));
            for (WebElement child : children)
            {
                List<WebElement> innerChildren = child.findElements(By.tagName(innerChildTagName));
                for (WebElement innerChild : innerChildren)
                {
                    if (innerChild.getText().equals(innerSiblingText))
                    {
                        WebElement babyElement = child.findElement(By.name(babyElementName));
                        if (babyElement.getTagName().equalsIgnoreCase("input"))
                        {
                            this.enterTextByName(babyElementName, textToEnter);
                            return true;
                        }
                        else if (babyElement.getTagName().equalsIgnoreCase("select"))
                        {
                            this.selectByTextFromDropDownListUsingName(babyElementName, textToEnter);
                            return true;
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            Narrator.logError("Could not find elements" + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
        return false;
    }

    public Boolean ClickEmbeddedTableElementByNameUsingContainerXpath(String containerXpath, String elementTagName, String childSubElementTagName, String valueToSelect)
    {
        try
        {
            WebElement table = SeleniumDriverInstance.Driver.findElement(By.xpath(containerXpath));
            List<WebElement> rows = table.findElements(By.tagName(elementTagName));

            for (WebElement row : rows)
            {
                List<WebElement> cells = row.findElements(By.tagName(childSubElementTagName));

                if (cells == null || cells.size() < 1)
                {
                    continue;
                }
//                WebElement cellToClick = cells.get(valueToSelect);

                WebElement Button = Driver.findElement(By.xpath(valueToSelect));
                Button.click();
                break;

            }

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError("Could not find second cell" + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
    }

    public Boolean ClickDeleteButtonEmbeddedTableElementByNameUsingContainerXpath(String containerXpath, String elementTagName, String childSubElementTagName, String childTagElementName, String ButtonToClick)
    {
        try
        {
            WebElement table = SeleniumDriverInstance.Driver.findElement(By.xpath(containerXpath));
            List<WebElement> rows = table.findElements(By.tagName(elementTagName));
            for (WebElement row : rows)
            {
                List<WebElement> cells = row.findElements(By.tagName(childSubElementTagName));

                if (cells == null || cells.size() < 1)
                {
                    continue;
                }
                List<WebElement> TagName = row.findElements(By.tagName(childTagElementName));

                if (TagName == null || TagName.size() < 1)
                {
                    continue;
                }
//                WebElement cellToClick = cells.get(valueToSelect);
                WebElement Button = Driver.findElement(By.xpath(ButtonToClick));
                Button.click();
                SeleniumDriverInstance.pause(2000);

            }

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError("Could not find second cell" + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
    }

    public Boolean ClickDeleteButtonEmbeddedTableElementByNameUsingContainerXpathAndFirstCellName(String containerXpath, String elementTagName, String childSubElementTagName, String ValidationText, int valueToSelect)
    {
        String FirstCellText = "";
        String CellText = "";
        try
        {
            WebElement table = SeleniumDriverInstance.Driver.findElement(By.xpath(containerXpath));
            List<WebElement> rows = table.findElements(By.tagName(elementTagName));
            System.out.println("Printing out rows :");
            for (WebElement row : rows)
            {
                System.out.println(row.getText());
                System.out.println("Printing out cells");
                List<WebElement> cells = row.findElements(By.tagName(childSubElementTagName));

                if (cells == null || cells.size() < 1)
                {
                    continue;
                }

                System.out.println(FirstCellText);
                for (WebElement cell : cells)
                {
                    FirstCellText = "FirstCellText = " + cell.getText();

                    CellText = cell.getText();
                    System.out.println("");
                    if (CellText.toUpperCase().trim().equals(ValidationText))
                    {
                        WebElement cellToClick = cells.get(valueToSelect);
                        cellToClick.click();
                        SeleniumDriverInstance.pause(2000);
                        return true;
                    }
                }
            }

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError("Could not find second cell" + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
    }

    public Boolean SetTextInEmbeddedElementByIdUsingContainerId(String containerId, String tagID, String textToEnter)
    {
        try
        {
            this.waitForElementById(containerId);

            WebElement Div = this.Driver.findElement(By.id(containerId));

            WebElement subElement = Div.findElement(By.id(tagID));
            //subElement.clear();

            Actions typeText = new Actions(Driver);
            typeText.moveToElement(subElement);
            typeText.click(subElement);
            typeText.sendKeys(subElement, textToEnter);
            typeText.click(subElement);
            typeText.perform();

            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to enter text : " + ex.getMessage());
            return false;
        }
    }

    public Boolean EnterTextInEmbeddedElementByIdUsingContainerId(String containerId, String tagID, String textToEnter)
    {
        try
        {
            this.waitForElementById(containerId);

            WebElement Div = this.Driver.findElement(By.id(containerId));

            WebElement subElement = Div.findElement(By.id(tagID));
            subElement.clear();

            Actions typeText = new Actions(Driver);
            typeText.moveToElement(subElement);
            typeText.click(subElement);
            typeText.sendKeys(subElement, textToEnter);
            typeText.click(subElement);
            typeText.perform();

            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to enter text : " + ex.getMessage());
            return false;
        }
    }

    public Boolean SelectValueFromEmbeddedElementByIdUsingContainerId(String containerId, String tagID, String valueToSelect)
    {
        try
        {
            this.waitForElementById(containerId);

            WebElement Div = this.Driver.findElement(By.id(containerId));

            //WebElement subElement = Div.findElement(By.id(tagID));
            Select dropDownList = new Select(Div.findElement(By.id(tagID)));

            dropDownList.selectByVisibleText(valueToSelect);

            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" selecting from dropdownlist by text using Id - " + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
    }

    public Boolean SelectDropdownListValue(String divXpath, String tagName1, String valueToSelect)
    {
        Narrator.logDebug("valueToSelect: " + valueToSelect);
        try
        {
            this.waitForElementByXpath(divXpath);
            WebElement div = this.Driver.findElement(By.xpath(divXpath));
            WebElement aTag = div.findElement(By.xpath(tagName1));
            System.out.println("a tag created");
            aTag.click();

            if (aTag.getText().toUpperCase().trim().equals(valueToSelect.toUpperCase().trim()))
            {
                System.out.println("match found");
                aTag.isSelected();
                aTag.sendKeys(Keys.TAB);
                Narrator.logDebug("item selected: " + aTag.getText());
            }
            else
            {
                Narrator.logDebug("match  not found: " + aTag.getText());
                WebElement selectOption = aTag.findElement(By.xpath("//*[contains(text(),'" + valueToSelect + "')]"));
                selectOption.click();
                aTag.sendKeys(Keys.TAB);
            }
            System.out.println("Selection made");
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" selecting from dropdownlist by text using Id - " + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
    }

    public String retrieveTextByXpath(String elementXpath)
    {
        String retrievedText = "";
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToRead = Driver.findElement(By.xpath(elementXpath));
            retrievedText = elementToRead.getText();
            Narrator.logDebug("Text retrieved successfully from element - " + elementXpath);
            return retrievedText;

        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to retrieve text from element Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public boolean selectByIndexFromDropDownListUsingId(String elementId, Integer indexToSelect)
    {
        try
        {
            waitForElementById(elementId);
            Select dropDownList = new Select(Driver.findElement(By.id(elementId)));
            dropDownList.selectByIndex(indexToSelect);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to select option from dropdownlist by index using Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByIndexFromDropDownListUsingXpath(String elementXpath, Integer indexToSelect)
    {
        try
        {
            waitForElementByXpath(elementXpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
            dropDownList.selectByIndex(indexToSelect);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to select option from dropdownlist by index using xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByIndexFromDropDownListUsingName(String elementName, String elementName2, Integer indexToSelect)
    {
        try
        {
            if (waitForElementByNameNoExceptions(elementName, 3))
            {
                Select dropDownList = new Select(Driver.findElement(By.name(elementName)));
                dropDownList.selectByIndex(indexToSelect);
                return true;
            }
            if (waitForElementByNameNoExceptions(elementName2, 3))
            {
                Select dropDownList = new Select(Driver.findElement(By.name(elementName2)));
                dropDownList.selectByIndex(indexToSelect);
                return true;
            }
            //waitForElementByName(elementName);
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to select option from dropdownlist by index using Name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return false;
    }

    public boolean selectByTextFromDropDownListUsingId(String elementId, String valueToSelect)
    {
        try
        {
          if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
            {
                
            waitForElementById(elementId);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.id(elementId)));
            WebElement elementToClick = Driver.findElement(By.id(elementId));
            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).build().perform();
            builder.click(elementToClick).build().perform();
                        List<WebElement> options = elementToClick.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.contains(valueToSelect))
                {
            builder.moveToElement(option).build().perform();
            builder.click(option).build().perform();
                    //Actions action = new Actions(SeleniumDriverInstance.Driver);
                    //action.sendKeys(Keys.ENTER).build().perform();
                    //option.click();

                    Narrator.logError("[Info] Selected partial text '" + valueToSelect + "' from element '" + elementId + "'");
                    return true;
                }
            }
                 
            }
            else
            waitForElementById(elementId);
            Select dropDownList = new Select(Driver.findElement(By.id(elementId)));
            dropDownList.selectByVisibleText(valueToSelect);
            return true;
           
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting from dropdownlist by text using Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean acceptSslErrorMsg()
    {
        try
        {
            this.pause(4000);
            Driver.navigate().to("javascript:document.getElementById('overridelink').click()");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" accepting SSL Certificate message - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public String retrieveTextByValueXpath(String xpath)
    {
        try
        {
            this.waitForElementByXpath(xpath);
            WebElement element = SeleniumDriverInstance.Driver.findElement(By.xpath(xpath));
            return element.getAttribute("value").toString();
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to retrieve value by xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return "";
        }
    }

    public String retrieveAttributeByXpath(String xpath, String attr)
    {
        try
        {
            this.waitForElementByXpath(xpath);
            WebElement element = SeleniumDriverInstance.Driver.findElement(By.xpath(xpath));
            return element.getAttribute(attr).toString();
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to retrieve attribute by xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return "";
        }
    }

    public boolean selectByValueFromDropDownListUsingName(String elementName, String valueToSelect)
    {
        try
        {
            this.waitForElementByName(elementName);
            Select dropDownList = new Select(Driver.findElement(By.name(elementName)));
            dropDownList.selectByValue(valueToSelect);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting from dropdownlist by value using Name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void pressKeyOnElementById(String elementId, Keys keyToPress)
    {
        try
        {
            this.waitForElementById(elementId);
            WebElement elementToAccess = Driver.findElement(By.id(elementId));
            elementToAccess.sendKeys(keyToPress);

        }
        catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            Narrator.logError(" Failed to send keypress to element - " + elementId);
        }
    }

    public boolean SwitchTabs(int tabAmount)
    {
        this.pause(5000);
        try
        {
            Robot robot = null;
            try
            {
                robot = new Robot();
            }
            catch (AWTException ex)
            {

            }

            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_L);
            robot.keyRelease(KeyEvent.VK_L);
            robot.keyRelease(KeyEvent.VK_CONTROL);

            for (int i = 0; i < tabAmount; i++)
            {
                robot.keyPress(KeyEvent.VK_TAB);
                robot.keyRelease(KeyEvent.VK_TAB);
                SeleniumDriverInstance.pause(100);
            }

            SeleniumDriverInstance.pause(250);

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to switch tabs using tab stroke - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return true;
    }

    public boolean selectByTextFromDropDownListUsingName(String elementName, String valueToSelect)
    {
        try
        {
            this.waitForElementByName(elementName);
            Select dropDownList = new Select(Driver.findElement(By.name(elementName)));
            dropDownList.selectByVisibleText(valueToSelect);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Selecting from dropdownlist by text using name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByTextFromDropDownListUsingNameAndClick(String elementName, String valueToSelect)
    {
        try
        {
            this.waitForElementByName(elementName);
            Select ddl = new Select(Driver.findElement(By.name(elementName)));
            ddl.deselectAll();
            WebElement dropDownList = Driver.findElement(By.name(elementName));
            List<WebElement> options = dropDownList.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                if (option.getText().equals(valueToSelect))
                {
                    option.click();
                    return true;
                }
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" selecting from dropdownlist by text using name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return false;
    }

    public Integer getSelectedDropdownOptionIndexById(String elementId)
    {
        try
        {
            waitForElementById(elementId);
            Select dropDownList = new Select(Driver.findElement(By.id(elementId)));
            String selectedValue = dropDownList.getFirstSelectedOption().getText();

            List<WebElement> list = dropDownList.getOptions();

            for (int i = 0; i < list.size(); i++)
            {
                if (list.get(i).getText().equals(selectedValue))
                {
                    return i;
                }
            }

            return -1;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to retrieve selected dropdown option index using Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return -1;
        }
    }

    public Integer getSelectedDropdownOptionIndexByName(String elementName, String elementName2)
    {
        try
        {

            if (waitForElementByNameNoExceptions(elementName, 3))
            {
                Select dropDownList = new Select(Driver.findElement(By.name(elementName)));
                String selectedValue = dropDownList.getFirstSelectedOption().getText();

                List<WebElement> list = dropDownList.getOptions();

                for (int i = 0; i < list.size(); i++)
                {
                    if (list.get(i).getText().equals(selectedValue))
                    {
                        return i;
                    }
                }

                return -1;
            }
            if (waitForElementByNameNoExceptions(elementName2, 3))
            {
                Select dropDownList = new Select(Driver.findElement(By.name(elementName2)));
                String selectedValue = dropDownList.getFirstSelectedOption().getText();

                List<WebElement> list = dropDownList.getOptions();

                for (int i = 0; i < list.size(); i++)
                {
                    if (list.get(i).getText().equals(selectedValue))
                    {
                        return i;
                    }
                }

                return -1;
            }

            //waitForElementByName(elementName);
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to retrieve selected dropdown option index using name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return -1;
        }
        return -1;
    }

    public Integer getSelectedDropdownOptionIndexByXpath(String elementXpath)
    {
        try
        {

            if (waitForElementByXpath(elementXpath, 3))
            {
                Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
                String selectedValue = dropDownList.getFirstSelectedOption().getText();

                List<WebElement> list = dropDownList.getOptions();

                for (int i = 0; i < list.size(); i++)
                {
                    if (list.get(i).getText().equals(selectedValue))
                    {
                        return i;
                    }
                }

                return -1;
            }

            //waitForElementByXpath(elementXpath);
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to retrieve selected dropdown option index using name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return -1;
        }
        return -1;
    }

    /* public boolean waitForPageToLoad()
    {
        boolean isEnabled = false;
        try{
            int counter = 0;

            WebDriverWait wait = new WebDriverWait(Driver, 4);

            while (!isEnabled && counter < ApplicationConfig.WaitTimeout())
            {
                JavascriptExecutor js = (JavascriptExecutor)wait;

                if(wait.until(js.executeScript("return document.readyState").equals("complete")))
                {
                    isEnabled = true;
                    break;
                }else
                    counter++;
            }

        }catch(Exception e)
        {
            Narrator.errorLog(" waiting for page to be enabled - " + e.getMessage());

        }

        return isEnabled;
    }*/
    public void waitUntilElementEnabledByID(String elementID)
    {
        try
        {
            int counter = 0;
            boolean isEnabled = false;
            WebDriverWait wait = new WebDriverWait(Driver, 1);

            while (!isEnabled && counter < ApplicationConfig.WaitTimeout())
            {
                if (wait.until(ExpectedConditions.elementToBeClickable(By.id(elementID))) != null)
                {
                    isEnabled = true;
                    break;
                }
                else
                {
                    counter++;
                }

            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be enabled - " + e.getMessage());
        }

    }

    public boolean checkBoxSelectionById(String elementId, boolean mustBeSelected)
    {
        try
        {
            this.waitForElementById(elementId);
            this.waitUntilElementEnabledByID(elementId);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.id(elementId)));
            WebElement checkBox = Driver.findElement(By.id(elementId));
            if (checkBox.isSelected() != mustBeSelected)
            {
                checkBox.click();
            }
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting checkbox byId - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean checkBoxSelectionByName(String elementName, boolean mustBeSelected)
    {
        try
        {
            this.waitForElementById(elementName);
            this.waitUntilElementEnabledByID(elementName);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.id(elementName)));
            WebElement checkBox = Driver.findElement(By.name(elementName));
            if (checkBox.isSelected() != mustBeSelected)
            {
                checkBox.click();
            }
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting checkbox by name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean checkBoxSelectionByXpath(String elementXpath, boolean mustBeSelected)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            //this.waitUntilElementEnabledByID(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement checkBox = Driver.findElement(By.xpath(elementXpath));
            if (checkBox.isSelected() != mustBeSelected)
            {
               if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
                {
                    Actions builder = new Actions(Driver);
                    builder.moveToElement(checkBox).build().perform();
                    builder.click(checkBox).build().perform();
                }
               else
               {   
                    checkBox.click();
               }
            }
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting checkbox by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean checkStateOfCheckBoxXpath(String elementXpath, boolean mustBeSelected)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            //this.waitUntilElementEnabledByID(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement checkBox = Driver.findElement(By.xpath(elementXpath));
            if (checkBox.isSelected() != mustBeSelected)
            {
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting checkbox by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean uncheckCheckBoxSelectionById(String elementId, boolean mustBeSelected)
    {
        try
        {
            this.waitForElementById(elementId);
            this.waitUntilElementEnabledByID(elementId);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.id(elementId)));
            WebElement checkBox = Driver.findElement(By.id(elementId));
            if (checkBox.isSelected() == mustBeSelected)
            {
                checkBox.click();
            }
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting checkbox byId - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean validateElementTextValueByClassName(String elementClassName, String elementText)
    {
        try
        {
            if (waitForElementByClassName(elementClassName))
            {
                WebElement elementToValidate = Driver.findElement(By.className(elementClassName));
                String textDetected = elementToValidate.getText();
                if (textDetected.contains(elementText))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" validating element text value by class name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean validateElementTextValueById(String elementId, String elementText)
    {
        try
        {
            if (waitForElementById(elementId))
            {
                WebElement elementToValidate = Driver.findElement(By.id(elementId));
                String textDetected = elementToValidate.getText();
                if (textDetected.contains(elementText))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" validating element text value by ID - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean validateElementTextValueByXpath(String elementXpath, String elementText)
    {
        try
        {
            if (waitForElementByXpath(elementXpath))
            {
                WebElement elementToValidate = Driver.findElement(By.xpath(elementXpath));
                String textDetected = elementToValidate.getText();
                if (textDetected.contains(elementText))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to validate element text value by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean validateElementValueAttributeByXpath(String elementXpath, String elementText)
    {
        try
        {
            if (waitForElementByXpath(elementXpath))
            {
                WebElement elementToValidate = Driver.findElement(By.xpath(elementXpath));
                String textDetected = elementToValidate.getAttribute("value");
                if (textDetected.contains(elementText))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to validate element text value by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
    
    public void WaitUntilDropDownListPopulatedById(String elementId)
    {

        try
        {
            this.waitForElementById(elementId);
            int waitCount = 0;
            List<WebElement> optionsList;
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    Select dropDownList = new Select(Driver.findElement(By.id(elementId)));
                    optionsList = dropDownList.getOptions();
                    if (optionsList.size() > 0)
                    {
                        break;
                    }
                }
                catch (Exception e)
                {

                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for dropdownlist to be populated by ID - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
    }

    public void WaitUntilDropDownListPopulatedByName(String elementName)
    {

        try
        {
            this.waitForElementById(elementName);
            int waitCount = 0;
            List<WebElement> optionsList;
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    Select dropDownList = new Select(Driver.findElement(By.name(elementName)));
                    optionsList = dropDownList.getOptions();
                    if (optionsList.size() > 0)
                    {
                        break;
                    }
                }
                catch (Exception e)
                {

                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for dropdownlist to be populated by Name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
    }

    public boolean hoverOverElementAndClickSubElementbyIdAndLinkText(String elementId, String LinkText)
    {
        try
        {
            this.waitForElementById(elementId);

            Actions actions = new Actions(Driver);
            WebElement menuHoverLink = Driver.findElement(By.id(elementId));
            actions.moveToElement(menuHoverLink);

            WebElement subLink = menuHoverLink.findElement(By.linkText(LinkText));
            actions.moveToElement(subLink);
            actions.doubleClick();
            actions.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to hover over element and click sub element by ID and link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean hoverOverElementAndClickSubElementbyIdAndXpath(String elementId, String XPath)
    {
        try
        {
            this.waitForElementById(elementId);

            Actions actions = new Actions(Driver);
            WebElement menuHoverLink = Driver.findElement(By.id(elementId));
            actions.moveToElement(menuHoverLink);

            WebElement subLink = menuHoverLink.findElement(By.xpath(XPath));
            actions.moveToElement(subLink);
            actions.doubleClick();
            actions.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to hover over element and click sub element by ID and Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean hoverOverElementAndClickSubElementbyXpathAndXpath(String elementXPath, String XPath)
    {
        try
        {
            this.waitForElementByXpath(elementXPath);

            Actions actions = new Actions(Driver);
            WebElement menuHoverLink = Driver.findElement(By.xpath(elementXPath));
            actions.moveToElement(menuHoverLink);

            WebElement subLink = menuHoverLink.findElement(By.xpath(XPath));
            actions.moveToElement(subLink);
            actions.click();
            actions.perform();
            Narrator.logDebug("Successfully hovered over '" + elementXPath + "' and clicked '" + XPath + "'...proceeding");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to hover over element and click sub element by Xpath and Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean hoverOverElementPauseAndClickSubElementbyXpathAndXpath(String elementXPath, String XPath)
    {
        try
        {
            this.waitForElementByXpath(elementXPath);

            Actions actions = new Actions(Driver);
            WebElement menuHoverLink = Driver.findElement(By.xpath(elementXPath));
            actions.moveToElement(menuHoverLink);
            pause(1000);
            this.waitForElementByXpath(XPath);
            WebElement subLink = menuHoverLink.findElement(By.xpath(XPath));
            actions.moveToElement(subLink);
            actions.click();
            actions.perform();
            Narrator.logDebug("Successfully hovered over '" + elementXPath + "' and clicked '" + XPath + "'...proceeding");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to hover over element and click sub element by Xpath and Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean hoverOverElementActionsByXpath(String elementXPath)
    {
        try
        {
            this.waitForElementByXpath(elementXPath);

            Actions actions = new Actions(Driver);
            WebElement menuHoverLink = Driver.findElement(By.xpath(elementXPath));
            Actions builder = new Actions(SeleniumDriverInstance.Driver);
            builder.moveToElement(menuHoverLink).perform();

            Narrator.logDebug("Successfully hovered over '" + elementXPath + "'...proceeding");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to hover over element and click sub element by Xpath and Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean hoverOverAndClickElementUsingJavascript(String elementXpath, String elementToClickXpath)
    {
        try
        {
            WebElement elementToClick = Driver.findElement(By.xpath(elementToClickXpath));
            JavascriptExecutor js = (JavascriptExecutor) Driver;
            js.executeScript("onmouseover=menus['0'].exec('0',2)");
            js = (JavascriptExecutor) Driver;
            js.executeScript("arguments[0].click();", elementToClick);
            Narrator.logDebug("Successfully found and clicked element '" + elementToClickXpath + "' using javascript...proceeding");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to hover over element and click sub element by Xpath and Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean hoverOverElementUsingJavascriptAndClickElementByLinkText(String elementLinkText)
    {
        try
        {
            WebElement elementToClick = Driver.findElement(By.linkText(elementLinkText));
            JavascriptExecutor js = (JavascriptExecutor) Driver;
            js.executeScript("onmouseover=menus['0'].exec('0',2)");
            js = (JavascriptExecutor) Driver;
            js.executeScript("arguments[0].click();", elementToClick);
            Narrator.logDebug("Successfully found and clicked element '" + elementLinkText + "' using javascript...proceeding");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to hover over element and click sub element by Xpath and Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean hoverOverElementAndClickByXpath(String XPath)
    {
        try
        {
            //this.waitForElementByXpath(XPath);

            Actions actions = new Actions(Driver);
            WebElement closeDiv = Driver.findElement(By.xpath(XPath));
            actions.moveToElement(closeDiv);
            actions.click();
            actions.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to hover over element and click sub element by ID and Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean hoverOverElementByXpath(String XPath, String element2, String element3)
    {
        try
        {
            this.waitForElementByXpath(XPath);
            Actions actions = new Actions(Driver);
            WebElement closeDiv = Driver.findElement(By.xpath(XPath));
            actions.click(closeDiv);
            actions.perform();
            this.waitForElementByXpath(element2);
            WebElement SecondElement = closeDiv.findElement(By.xpath(element2));
            actions.moveToElement(SecondElement);
            actions.perform();
            this.waitForElementByXpath(element3);
            WebElement thirdElement = SecondElement.findElement(By.xpath(element3));
            actions.click(thirdElement);
            actions.perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to hover over element and click sub element by ID and Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean hoverOverElementAndClickSubElementbyIdAndId(String elementId, String subElementId)
    {
        try
        {
            this.waitForElementById(elementId);

            Actions actions = new Actions(Driver);
            WebElement menuHoverLink = Driver.findElement(By.id(elementId));
            actions.moveToElement(menuHoverLink);

            WebElement subLink = menuHoverLink.findElement(By.id(subElementId));
            actions.moveToElement(subLink);
            actions.click();
            actions.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to hover over element and click sub element by ID and subElementID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickNestedElementUsingIds(String parentElementId, String childElementId)
    {
        try
        {
            this.waitForElementById(parentElementId);
            WebElement parentElement = Driver.findElement(By.id(parentElementId));
            WebElement childElement = parentElement.findElement(By.id(childElementId));
            childElement.click();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" clicking nested elements usind IDs  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementByXpath(String elementXpath)
    {
        try
        {
            if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
            {
                return clickElementByXpathActions(elementXpath);  
            }
            
            if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
            {
               if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
        
                try
                {
                Actions action = new Actions(SeleniumDriverInstance.Driver);
                action.release();
                }
                catch(Exception ex)
                {

                }
        
                try 
                {
                    WebElement elementToClick1 = Driver.findElement(By.xpath(elementXpath));            
                    WebElement elementToClick2 = Driver.findElement(By.xpath(elementXpath+"//.."));
                    if((elementToClick1.getAttribute("class")!= null && elementToClick1.getAttribute("class").contains("requestTypeHeaders")) || (elementToClick2.getAttribute("class")!= null && elementToClick2.getAttribute("class").contains("requestTypeHeaders")))
                    {
                        return clickElementByXpathFireFox(elementXpath);
                    }
                    else if (elementToClick1.getAttribute("href")!= null ||(elementToClick2.getAttribute("onmouseout")!= null && elementToClick2.getAttribute("onmouseout").contains("ImageGreyMain();")))
                    {
                        return clickElementByXpathFireFox(elementXpath);  
                    }
                    else
                    {
                         List <WebElement> we = this.Driver.findElements(By.xpath(elementXpath));     

                        
                        int ok_size=Driver.findElements(By.xpath(elementXpath)).size();

                        int j=0;
                        for (int i = 0; i < ok_size; i++) 
                        {
                            int x = we.get(i).getLocation().getX();
                            
                            if (x!=0) 
                            {
                                we.get(i).click();
                                return true;
                            }
                            j++;
                        }
                        if (j == ok_size) 
                        {
                            return false;
                        }
                    }

                }
                catch (Exception e) 
                {
                    try
                    {
                        WebElement elementToClick2 = Driver.findElement(By.xpath(elementXpath+"//.."));

                        if (elementToClick2.getAttribute("onmouseout").contains("ImageGreyMain();"))
                        {
                            return clickElementByXpathFireFox(elementXpath);  
                        }
                    }
                    catch (Exception es) 
                    {
                        
                        
                        
                        List <WebElement> we = this.Driver.findElements(By.xpath(elementXpath));     

                        
                        int ok_size=Driver.findElements(By.xpath(elementXpath)).size();
                        int j=0;
                        for (int i = 0; i < ok_size; i++) 
                        {
                            int x = we.get(i).getLocation().getX();
                            
                            if (x!=0) 
                            {
                                we.get(i).click();
                                return true;
                            }
                            j++;
                        }
                        if (j == ok_size) 
                        {
                            return false;
                        }

                    }

                }
            }
            else
            {
                Narrator.logDebug(" Clicking element by Xpath : " + elementXpath);
                waitForElementByXpath(elementXpath);
                waitForElementToBeClickableByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
                WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
                elementToClick.click();
            }
            return true;
            
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        
    }

    public boolean clickElementByXpathJavascript(String elementXpath)
    {
        try
        {
            Narrator.logDebug(" Clicking element by Xpath : " + elementXpath);
            waitForElementByXpath(elementXpath);
            waitForElementToBeClickableByXpath(elementXpath);
            WebElement element = Driver.findElement(By.xpath(elementXpath));
            JavascriptExecutor executor = (JavascriptExecutor) Driver;
            executor.executeScript("arguments[0].click();", element);

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementByXpathActions(String elementXpath)
    {
        try
        {
           if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
           {
        
                try
                {
                Actions action = new Actions(SeleniumDriverInstance.Driver);
                action.release().build().perform();
                }
                catch(Exception ex)
                {

                }
            }
            Narrator.logDebug(" Clicking element by Xpath : " + elementXpath);
            waitForElementByXpath(elementXpath);
            waitForElementToBeClickableByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).build().perform();
            builder.click(elementToClick).build().perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementByXpath_Actions(String elementXpath)
    {
        try
        {
            Narrator.logDebug(" Clicking element by Xpath : " + elementXpath);
            waitForElementByXpath(elementXpath);
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).build().perform();
            builder.click(elementToClick).build().perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean ReadCSVContents(String filePath, List<String> listName)
    {
        File file = new File(filePath);

        BufferedReader counterReader;
        BufferedReader returnInfo;
        
        try
        {
            counterReader = new BufferedReader(new FileReader(filePath));
            //reads first line in csv file - metadata
            counterReader.readLine();

            returnInfo = new BufferedReader(new FileReader(filePath));
            //reads first line in csv file - metadata
            returnInfo.readLine();

            String currentLine;

            while (counterReader.readLine() != null)
            {
                currentLine = returnInfo.readLine();
                listName.add(currentLine);
            }

            counterReader.close();
            returnInfo.close();

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - Could not read file -" + e.getMessage());
            return false;
        }

        return true;
    }

    public ArrayList<String> readCsvToArrayList(String filePath)
    {
        File file = new File(filePath);
        ArrayList<String> csvData = new ArrayList<String>();
        BufferedReader counterReader;
        BufferedReader returnInfo;

        try
        {
            counterReader = new BufferedReader(new FileReader(filePath));
            //reads first line in csv file - metadata
            counterReader.readLine();

            returnInfo = new BufferedReader(new FileReader(filePath));
            //reads first line in csv file - metadata
            returnInfo.readLine();

            String currentLine;

            while (counterReader.readLine() != null)
            {
                currentLine = counterReader.readLine();
                csvData.add(currentLine);
            }

            counterReader.close();
            returnInfo.close();

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - Could not read file -" + e.getMessage());
            return null;
        }

        return csvData;
    }

    public String[][] readExcelFile(String filePath, String fileName, String sheetName)
    {
        String[][] excelData = null;
        try
        {
            //Create a object of File class to open xlsx file
            File file = new File(filePath + "\\" + fileName);
            //Create an object of FileInputStream class to read excel file
            FileInputStream inputStream = new FileInputStream(file);
            Workbook sintrexWorkbook = null;
            //Find the file extension by spliting file name in substring and getting only extension name
            String fileExtensionName = fileName.substring(fileName.indexOf("."));
            //Check condition if the file is xlsx file
            if (fileExtensionName.equals(".xlsx"))
            {
                //If it is xlsx file then create object of XSSFWorkbook class
                sintrexWorkbook = new XSSFWorkbook(inputStream);

            } //Check condition if the file is xls file
            else if (fileExtensionName.equals(".xls"))
            {
                //If it is xls file then create object of XSSFWorkbook class
                sintrexWorkbook = new HSSFWorkbook(inputStream);

            }

            //Read sheet inside the workbook by its name
            Sheet sintrexSheet = sintrexWorkbook.getSheet(sheetName);

            //Find number of rows in excel file
            int rowCount = sintrexSheet.getLastRowNum() - sintrexSheet.getFirstRowNum();

            int numRows = sintrexSheet.getLastRowNum() + 1;
            int numCols = sintrexSheet.getRow(0).getLastCellNum();
            excelData = new String[numRows][numCols];
            //Create a loop over all the rows of excel file to read it
            for (int i = 0; i < rowCount + 1; i++)
            {
                Row row = sintrexSheet.getRow(i);
                for (int j = 0; j < row.getLastCellNum(); j++)
                {
                    DataFormatter fmt = new DataFormatter();
                    String valueAsSeenInExcel = fmt.formatCellValue(row.getCell(j));
                    excelData[i][j] = valueAsSeenInExcel;
                }
            }
            return excelData;
        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - Failed to read " + fileName + " file -" + e.getMessage());
            return null;
        }
    }

    public String[][] readXLSMFile(String filePath, String fileName, String sheetName)
    {
        String[][] excelData = null;
        try
        {
//            FileInputStream file = new FileInputStream(new File(filename));
////            System.out.println("found file");
//            XSSFWorkbook workbook = new XSSFWorkbook(file);
//            System.out.println("in workbook");
//            XSSFSheet sheet = workbook.getSheet(Sheet);
//            System.out.println("got sheet");

            //Create a object of File class to open xlsx file
            File file = new File(filePath + "\\\\" + fileName);
            //Create an object of FileInputStream class to read excel file
            FileInputStream inputStream = new FileInputStream(file);
            XSSFWorkbook sintrexWorkbook = null;

            //If it is xlsx file then create object of XSSFWorkbook class
            sintrexWorkbook = new XSSFWorkbook(inputStream);

            //Read sheet inside the workbook by its name
            XSSFSheet sintrexSheet = sintrexWorkbook.getSheet(sheetName);

            //Find number of rows in excel file
            int rowCount = sintrexSheet.getLastRowNum() - sintrexSheet.getFirstRowNum();

            int numRows = sintrexSheet.getLastRowNum() + 1;
            int numCols = sintrexSheet.getRow(0).getLastCellNum();
            excelData = new String[numRows][numCols];
            //Create a loop over all the rows of excel file to read it
            for (int i = 0; i < numRows; i++)
            {
                //get the row
                XSSFRow row = sintrexSheet.getRow(i);

                if (row != null)
                {
                    for (int j = 0; j < numCols; j++)
                    {
                        //this gets the cell and sets it as blank if it's empty.
                        XSSFCell cell = row.getCell(j, Row.CREATE_NULL_AS_BLANK);
                        String value = String.valueOf(cell);

                        excelData[i][j] = value;
                    }
                }
            }
            return excelData;
        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - Failed to read " + fileName + " file -" + e.getMessage());
            return null;
        }
    }

    public String[][] readXLSMFileWithStartRowIndex(String filePath, String fileName, String sheetName, int startRowIndex)
    {
        String[][] excelData = null;
        try
        {
//            FileInputStream file = new FileInputStream(new File(filename));
////            System.out.println("found file");
//            XSSFWorkbook workbook = new XSSFWorkbook(file);
//            System.out.println("in workbook");
//            XSSFSheet sheet = workbook.getSheet(Sheet);
//            System.out.println("got sheet");

            //Create a object of File class to open xlsx file
            File file = new File(filePath + "\\\\" + fileName);
            //Create an object of FileInputStream class to read excel file
            FileInputStream inputStream = new FileInputStream(file);
            XSSFWorkbook sintrexWorkbook = null;

            //If it is xlsx file then create object of XSSFWorkbook class
            sintrexWorkbook = new XSSFWorkbook(inputStream);

            //Read sheet inside the workbook by its name
            XSSFSheet sintrexSheet = sintrexWorkbook.getSheet(sheetName);

            //Find number of rows in excel file
            int rowCount = sintrexSheet.getLastRowNum() - sintrexSheet.getFirstRowNum();

            int numRows = sintrexSheet.getLastRowNum() + 1;
            int numCols = sintrexSheet.getRow(startRowIndex).getLastCellNum();
            excelData = new String[numRows][numCols];
            //Create a loop over all the rows of excel file to read it
            for (int i = 0; i < numRows; i++)
            {
                //get the row
                XSSFRow row = sintrexSheet.getRow(i);

                if (row != null)
                {
                    for (int j = 0; j < numCols; j++)
                    {
                        //this gets the cell and sets it as blank if it's empty.
                        XSSFCell cell = row.getCell(j, Row.CREATE_NULL_AS_BLANK);
                        String value = String.valueOf(cell);

                        excelData[i][j] = value;
                    }
                }
            }
            return excelData;
        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - Failed to read " + fileName + " file -" + e.getMessage());
            return null;
        }
    }

    public boolean WriteNewLineToCSV(String theCSVAbsolutePath, String lineToAdd)
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            //read csv contents
            ArrayList<String> currentCSVContentsLines = new ArrayList();

            File file = new File(theCSVAbsolutePath);

            BufferedReader counterReader;
            BufferedReader returnInfo;

            counterReader = new BufferedReader(new FileReader(theCSVAbsolutePath));
            //reads first line in csv file - metadata

            returnInfo = new BufferedReader(new FileReader(theCSVAbsolutePath));
            //reads first line in csv file - metadata

            String currentLine;

            while (counterReader.readLine() != null)
            {
                currentLine = returnInfo.readLine();
                currentCSVContentsLines.add(currentLine);
            }

            counterReader.close();
            returnInfo.close();

            //append the current lines
            for (int i = 0; i < currentCSVContentsLines.size(); i++)
            {
                sb.append(currentCSVContentsLines.get(i).toString() + "\n");
            }

            //declare new pw
            PrintWriter pw = new PrintWriter(new File(theCSVAbsolutePath));

            //append new lines
            sb.append(lineToAdd + '\n');

            //pw.write
            pw.write(sb.toString());

            //pw close
            pw.close();
        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return false;
        }

        return true;
    }

    public String interactWithFile(String fileAbsolutePath)
    {
        try
        {
            File file = new File(fileAbsolutePath);

            file.exists();
            file.canExecute();
            file.canRead();
            file.canWrite();
            file.setReadOnly();
            file.setWritable(true);
            file.renameTo(file);

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return "Could not interact with the .csv file with path: " + fileAbsolutePath;
        }

        return "";
    }

    public String setFileReadable(String fileAbsolutePath)
    {
        try
        {
            File file = new File(fileAbsolutePath);
            file.setReadable(true);

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return "Could not interact with the .csv file with path: " + fileAbsolutePath;
        }

        return "";
    }

    public boolean moveToAndClickElementByNestedSiblingTagNameAndText(String parentTagName, String childTagName, String siblingTagName, String siblingImg)
    {
        try
        {
            //this.waitForElementById(elementId);
            List<WebElement> parents = Driver.findElements(By.tagName(parentTagName));

            for (WebElement parent : parents)
            {
                try
                {
                    WebElement child = parent.findElement(By.tagName(childTagName));

                    WebElement sibling = child.findElement(By.tagName(siblingTagName));

                    List<WebElement> imageList = sibling.findElements(By.tagName(siblingImg));
                    for (WebElement image : imageList)
                    {
                        if (image.getAttribute("src").equals("//js//pixel.gif"))
                        {
                            Actions builder = new Actions(Driver);
                            builder.moveToElement(image).perform();
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return false;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to move to element and click by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean moveToAndClickElementById(String elementId)
    {
        try
        {
            //this.waitForElementById(elementId);
            WebElement elementToClick = Driver.findElement(By.id(elementId));

            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).click(elementToClick).perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to move to element and click by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean moveToAndClickElementByXpath(String elementXpath)
    {
        try
        {
            //this.waitForElementById(elementId);
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));

            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).click(elementToClick).perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to move to element and click by Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean moveToAndClickElementByLinkText(String elementLinkText)
    {
        try
        {
            //this.waitForElementById(elementId);
            WebElement elementToClick = Driver.findElement(By.linkText(elementLinkText));

            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick);
            builder.click(elementToClick);
            builder.perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to move to element and click by link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementByLinkText(String elementLinkText)
    {
        try
        {
            waitForElementByLinkText(elementLinkText);
            WebElement elementToClick = Driver.findElement(By.linkText(elementLinkText));
            elementToClick.click();

            Narrator.logDebug("Element successfully clicked by Link Text '" + elementLinkText + "'...proceeding");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to click element by link text '" + elementLinkText + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterAllRequiredFields(List<String> valueList, String requiredFieldClassName)
    {
        String type = "";
        int loopCount = 0;
        try
        {
            List<WebElement> requiredElements = Driver.findElements(By.className(requiredFieldClassName));
            while (loopCount < valueList.size())
            {
                WebElement element = requiredElements.get(loopCount);
                type = element.getTagName();
                switch (type)
                {
                    case "input":
                    {
                        Actions typeText = new Actions(Driver);
                        typeText.moveToElement(element);
                        typeText.click(element);
                        typeText.sendKeys(element, valueList.get(loopCount));
                        typeText.perform();
                        loopCount++;
                    }
                    break;
                    case "select":
                    {
                        Select dropDownList = new Select(element);
                        dropDownList.selectByVisibleText(valueList.get(loopCount));
                        loopCount++;
                    }
                    break;
                }

            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError("Failed to enter in all required data '" + "' - " + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
    }

    public boolean validatePage(String elementClassName, String textToVerify)
    {
        try
        {
            WebElement span = Driver.findElement(By.className(elementClassName));
            if (span.getText().equals(textToVerify))
            {
                return true;
            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" locating element by class name  - " + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
        return false;
    }

//    //delete and purge all hosts
//    public String deleteAndPurgeAllHosts()
//    {
//        //expand all
//        String expands = SeleniumDriverInstance.CustomQuickExpandAllConfigConsoleVisibleItems();
//        if(!expands.isEmpty())
//        {
//            return expands;
//        }
//
//        List <WebElement> allConfigItems;
//
//        try
//        {
//            allConfigItems = SeleniumDriverInstance.Driver.findElements(By.xpath(AddHostInitialRouterDiscoveryPageObject.AllConfigItems()));
//            String hostsDeletedStr = "";
//            ArrayList <String> toBeDeleted = new ArrayList<>();
//
//            for (int i = 0; i < allConfigItems.size(); i++)
//            {
//                //switch iframes
//                if(!SeleniumDriverInstance.switchToDefaultContent())
//                {
//                    return "Could not switch to the default content";
//                }
//                if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
//                {
//                    return "Could not switch to the '"+SintelligentPageObject.ManagerRedirectFrame()+"' iframe";
//                }
//
//                //click on current item
//                allConfigItems.get(i).click();
//
//
//
//                //identify host by looking for 'interrogate' option after clicking
//                if(SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecondNew(SintelligentPageObject.AContainsTextXpath("Interrogate"), 1))
//                {
//                    hostsDeletedStr = hostsDeletedStr + allConfigItems.get(allConfigItems.size()-(i+1)).getText().toString() + " | ";
//                    toBeDeleted.add(allConfigItems.get(allConfigItems.size()-(i+1)).getText().toString());
//                }
//            }
//
//            //delete all the hosts that were added to the list
//            for (int i = 0; i < toBeDeleted.size(); i++)
//            {
//                //click '"+toBeDeleted.get(i)+"'
//                if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.OpenHostGroupCheckBox(toBeDeleted.get(i))))
//                {
//                    return "Could not wait for the '"+toBeDeleted.get(i)+"' button to be visible to delete host '"+toBeDeleted.get(i)+"' ";
//                }
//                if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.OpenHostGroupCheckBox(toBeDeleted.get(i))))
//                {
//                    return "Could not wait for the '"+toBeDeleted.get(i)+"' button to be clickable to delete host '"+toBeDeleted.get(i)+"' ";
//                }
//                if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.OpenHostGroupCheckBox(toBeDeleted.get(i))))
//                {
//                    return "Could not click the '"+toBeDeleted.get(i)+"' button to delete host '"+toBeDeleted.get(i)+"' ";
//                }
//            }
//            //delete from maintain drop
//            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Maintain")))
//            {
//                return "Could not wait for the maintain option to be visible";
//            }
//            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Maintain")))
//            {
//                return "Could not wait for the maintain option to be clickable";
//            }
//            if(!SeleniumDriverInstance.hoverOverElementByXpathNew(SintelligentPageObject.LiContainsTextXpath("Maintain")))
//            {
//                return "Could not hover over the maintain option to delete hosts ";
//            }
//
//            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.LiContainsTextXpath("Delete Host")))
//            {
//                return "Could not wait for the Delete Host option to be visible";
//            }
//            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.LiContainsTextXpath("Delete Host")))
//            {
//                return "Could not wait for the Delete Host option to be clickable";
//            }
//            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.LiContainsTextXpath("Delete Host")))
//            {
//                return "Could not click over the Delete Host option to delete hosts ";
//            }
//
//            for (int i = 0; i < toBeDeleted.size(); i++)
//            {
//                //validate was deleted
//                boolean isPresent = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.SpanContainsTextXpath(toBeDeleted.get(i)), 1);
//                int stopCounter = 0;
//                while(isPresent && (stopCounter != 3))
//                {
//                    SeleniumDriverInstance.pause(500);
//                    stopCounter++;
//                    isPresent = SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.SpanContainsTextXpath(toBeDeleted.get(i)), 1);
//                }
//
//                if(!hostsDeletedStr.isEmpty())
//                {
//                    testData.extractParameter("Hosts deleted for test startup: ", hostsDeletedStr, "");
//                }
//            }
//
//        }
//        catch (Exception e)
//        {
//            System.out.println("[ERROR] - " + e.getMessage());
//            return "Could not retrieve the Config Console table items";
//        }
//
//        //delete the 'Discover' container if visible
//        if(SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.SpanContainsTextXpath("Discover"), 2))
//        {
//            //delete the container
//            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SpanContainsTextXpath("Discover")))
//            {
//                return "Could not wait for the 'Discover' container to be clickable";
//            }
//            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.SpanContainsTextXpath("Discover")))
//            {
//                return "Could not click the 'Discover' container";
//            }
//
//            if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AContainsTextXpath("Delete")))
//            {
//                return "Could not wait for the Delete button to be visible";
//            }
//            if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AContainsTextXpath("Delete")))
//            {
//                return "Could not wait for the Delete button to be clickable";
//            }
//            if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AContainsTextXpath("Delete")))
//            {
//                return "Could not click the Delete button";
//            }
//        }
//
//        //purge all deleted hosts
//        try
//        {
//            //purge all
//            //switch context to 'deleted hosts'
//            if(!SeleniumDriverInstance.SwitchContext("Deleted Hosts", ""))
//            {
//                return "Could not switch to 'Deleted Hosts' context";
//            }
//
//            //expand all
//            String tester = SeleniumDriverInstance.CustomQuickExpandAllConfigConsoleVisibleItems();
//            if(!tester.isEmpty())
//            {
//                return tester;
//            }
//
//            List <WebElement> purgeItems = SeleniumDriverInstance.Driver.findElements(By.xpath(AddHostInitialRouterDiscoveryPageObject.PurgeItems()));
//            int deleteCounter = 1;
//
//            while(purgeItems.size() != 1)
//            {
//                //switch iframes
//                if(!SeleniumDriverInstance.switchToDefaultContent())
//                {
//                    return "Could not switch to the default content";
//                }
//                if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
//                {
//                    return "Could not switch to the '"+SintelligentPageObject.ManagerRedirectFrame()+"' iframe";
//                }
//                SeleniumDriverInstance.pause(500);
//
//                purgeItems.get(purgeItems.size()-1).click();
//                String checkStr = purgeItems.get(purgeItems.size()-1).getText().toString();
//
//                if(SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.AContainsTextXpath("Purge Host"), 5))
//                {
//                    if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AContainsTextXpath("Purge Host")))
//                    {
//                        return "Could not wait for clickability to click on 'Purge Host' after ["+deleteCounter+"] iterations";
//                    }
//                    if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AContainsTextXpath("Purge Host")))
//                    {
//                       return "Could not click on 'Purge Host' after ["+deleteCounter+"] iterations";
//                    }
//
//                    //confirm purge
//                    if(!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ButtonWithTextXpath("Ok")))
//                    {
//                        return "Could not wait for visibility to click on 'Ok' to confirm delete";
//                    }
//                    if(!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ButtonWithTextXpath("Ok")))
//                    {
//                        return "Could not wait for clickability to click on 'Ok' to confirm delete";
//                    }
//                    if(!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ButtonWithTextXpath("Ok")))
//                    {
//                        return "Could not click on 'Ok' to confirm delete";
//                    }
//
//                    //validate that the item was succesfully purged
//                    if(SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecond(SintelligentPageObject.SpanContainsTextXpath(checkStr), 1))
//                    {
//                        return "Could not validate that the host '"+checkStr+"' was purged";
//                    }
//                }
//
//                //switch iframes
//                if(!SeleniumDriverInstance.switchToDefaultContent())
//                {
//                    return "Could not switch to the default content";
//                }
//                if(!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ManagerRedirectFrame()))
//                {
//                    return "Could not switch to the '"+SintelligentPageObject.ManagerRedirectFrame()+"' iframe";
//                }
//
//                purgeItems = SeleniumDriverInstance.Driver.findElements(By.xpath(AddHostInitialRouterDiscoveryPageObject.PurgeItems()));
//                SeleniumDriverInstance.pause(500);
//            }
//
//            if(!SeleniumDriverInstance.SwitchContext("Host Groups", ""))
//            {
//                return "Could not switch back to the 'Host Groups' context";
//            }
//
//        }
//        catch (Exception e)
//        {
//            System.out.println("[ERROR] - " + e.getMessage());
//            return "Could not find the hosts";
//        }
//
//        return "";
//    }
    public boolean doubleClickElementbyLinkText(String elementLinkText)
    {
        try
        {
            waitForElementByLinkText(elementLinkText);
            WebElement elementToClick = Driver.findElement(By.linkText(elementLinkText));
            elementToClick.click();
            elementToClick.click();

            Narrator.logDebug("Element successfully double-clicked by Link Text...proceeding");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" double-clicking element by link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean doubleClickElementbyPartialLinkText(String elementLinkText)
    {
        try
        {
            //waitForElementByLinkText(elementLinkText);
            Actions act = new Actions(Driver);
            WebElement elementToClick = Driver.findElement(By.partialLinkText(elementLinkText));

            act.doubleClick(elementToClick).perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" double clicking element by link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean doubleClickElementbyXpath(String xpath)
    {
        try
        {
           if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE)
            {
                return doubleClickElementbyActionXpath(xpath);
            }
           else
           {
            waitForElementByXpath(xpath);
            waitForElementToBeClickableByXpath(xpath);
            WebElement elementToClick = Driver.findElement(By.xpath(xpath));

            elementToClick.click();
            elementToClick.click();

            return true;
           }
        }
        catch (Exception e)
        {
            Narrator.logError(" double clicking element by xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean doubleClickElementbyActionXpath(String xpath)
    {
        try
        {
            waitForElementByXpath(xpath);
            waitForElementToBeClickableByXpath(xpath);
            Actions action = new Actions(Driver);
            WebElement elementToClick = Driver.findElement(By.xpath(xpath));
            action.doubleClick(elementToClick).perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" double clicking element by actions xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean tripleClickElementbyXpath(String xpath)
    {
        try
        {
            //waitForElementByLinkText(elementLinkText);
//            Actions act = new Actions(Driver);
            WebElement elementToClick = Driver.findElement(By.xpath(xpath));

            elementToClick.click();
            elementToClick.click();
            elementToClick.click();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" double clicking element by link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyPartialLinkText(String elementPartialLinkText)
    {
        try
        {
            waitForElementByPartialLinkText(elementPartialLinkText);
            WebElement elementToClick = Driver.findElement(By.partialLinkText(elementPartialLinkText));
            elementToClick.click();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" clicking element by partial link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyName(String elementName)
    {
        try
        {
            this.waitForElementByName(elementName);
            WebElement elementToClick = Driver.findElement(By.name(elementName));
            elementToClick.click();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" clicking element by name  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyClassName(String elementClassName)
    {
        try
        {
            this.waitForElementByClassName(elementClassName);
            WebElement elementToClick = Driver.findElement(By.className(elementClassName));
            elementToClick.click();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" clicking element by class name  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementUsingParentChildTagName(String containerXpath, String phoneNumber)
    {
        List<String> values = new ArrayList<>();

        try
        {

            WebElement container = Driver.findElement(By.xpath(containerXpath));

            List<WebElement> divs = container.findElements(By.tagName("div"));
            int i = 0;
            WebElement elementToClick = null;
            for (WebElement div : divs)
            {
                try
                {
                    if (div.getAttribute("dest").contains(phoneNumber))
                    {
                        elementToClick = divs.get(i);
                    }
                }
                catch (Exception ex)
                {

                }
                i++;
            }

            elementToClick.click();
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError("Could not find elements" + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }

    }

    public String getTextOfLastElementInList(String containerXpath)
    {
        List<String> values = new ArrayList<>();
        try
        {

            WebElement container = Driver.findElement(By.xpath(containerXpath));
            List<WebElement> divs = container.findElements(By.className("mod-multiChat-smsfloat1 mod-multiChat-smsItemCon i-width100p"));
            int i = 0;
            WebElement elementOfInterest = null;
            for (WebElement div : divs)
            {
                try
                {
                    if (div.getTagName().equals("div"))
                    {
                        elementOfInterest = divs.get(i);
                    }
                }
                catch (Exception ex)
                {

                }
                i++;
            }

            Narrator.logDebug("A: " + elementOfInterest.getText());
            return elementOfInterest.getText();
        }
        catch (Exception ex)
        {

            Narrator.logError("Could not find elements" + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return "";
        }

    }

    public boolean elementContainsTextById(String elementId)
    {
        try
        {
            String retrievedText = "";
            this.waitForElementById(elementId);
            WebElement elementToEvaluate = Driver.findElement(By.id(elementId));
            retrievedText = elementToEvaluate.getText();
            return !retrievedText.equals("");
        }
        catch (Exception e)
        {
            Narrator.logError(" checking if element contains text - " + elementId + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void clearTextById(String elementId)
    {
        try
        {
            this.waitForElementById(elementId);
            WebElement elementToTypeIn = Driver.findElement(By.id(elementId));
            elementToTypeIn.clear();
        }
        catch (Exception e)
        {
            Narrator.logError(" clearing text - " + elementId + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
    }

    public boolean clearTextByXpath(String elementXpath)
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys();
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();
            this.pause(50);
            elementToTypeIn.clear();
            this.pause(50);
            elementToTypeIn.clear();
        }
        catch (Exception e)
        {
            Narrator.logError(" clearing text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return true;
    }

    public boolean clearTextBy_Xpath(String elementXpath)
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys();
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();
            this.pause(50);
            elementToTypeIn.clear();
            this.pause(50);
            elementToTypeIn.clear();
            elementToTypeIn.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END));
        }
        catch (Exception e)
        {
            Narrator.logError(" clearing text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return true;
    }

    public void KeyPress(String keyToPress)
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys(keyToPress);
            action.perform();

        }
        catch (Exception e)
        {
            Narrator.logError(" Pressing Key - " + keyToPress + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
    }

    public boolean enterTextById(String elementId, String textToEnter)
    {
        try
        {
            this.waitForElementById(elementId);
            WebElement elementToTypeIn = Driver.findElement(By.id(elementId));
            elementToTypeIn.clear();
            Actions typeText = new Actions(Driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementId + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterTextByXpath(String elementXpath, String textToEnter)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
            this.pause(50);
            elementToTypeIn.clear();
            pause(200);
            Actions typeText = new Actions(Driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();
            
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterTextByXpathNew(String elementXpath, String textToEnter)
    {
        try
        {
           if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE)
            {
                return enterTextByXpath(elementXpath,textToEnter);
            }
           else
           {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();
            elementToTypeIn.click();
            elementToTypeIn.sendKeys(textToEnter);
            elementToTypeIn.click();

            Narrator.logInfo("[Info] Entered text '" + textToEnter + "' into element '" + elementXpath + "'");

            return true;
           }
        }
        catch (Exception e)
        {
            System.err.println("[Error] Failed to enter text by xpath  - " + elementXpath + " Error : " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clearAndEnterTextByXpath(String elementXpath, String textToEnter)
    {
        try
        {
//            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
//            elementToTypeIn.clear();
            Actions typeText = new Actions(Driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.keyDown(elementToTypeIn, Keys.BACK_SPACE);
            pause(2000);
            typeText.keyUp(elementToTypeIn, Keys.BACK_SPACE);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void scrollDownByOnePage(String elementId)
    {
        try
        {
            this.waitForElementById(elementId);
            WebElement scrollElement = Driver.findElement(By.id(elementId));
            scrollElement.sendKeys(Keys.PAGE_DOWN);

            Narrator.logDebug("Sucessfully scrolled down...");
        }
        catch (Exception e)
        {
            Narrator.logError(" scrolling page - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
    }

    public boolean enterTextByName(String elementName, String textToEnter)
    {
        try
        {
            this.waitForElementByName(elementName);
            WebElement elementToTypeIn = Driver.findElement(By.name(elementName));
            elementToTypeIn.clear();
            elementToTypeIn.sendKeys(textToEnter);
            elementToTypeIn.click();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text by name  - " + elementName + "  : " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterTextByClassName(String elementClassName, String textToEnter)
    {
        try
        {
            this.waitForElementByClassName(elementClassName);
            WebElement elementToTypeIn = Driver.findElement(By.className(elementClassName));
            elementToTypeIn.clear();
            elementToTypeIn.sendKeys(textToEnter);

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text by class name  - " + elementClassName + "  : " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    // Requires a fully qualified URL - eg: "http://www.myURL.com"
    public boolean navigateTo(String pageUrl)
    {
        try
        {
            Driver.navigate().to(pageUrl);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" navigating to URL - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean waitForElementNoLongerPresentById(String elementId)
    {
        boolean elementFound = true;
        try
        {
            int waitCount = 0;
            while (elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))) == null)
                    {
                        elementFound = false;
                        break;
                    }
                }
                catch (Exception e)
                {
                    this.DriverExceptionDetail = e.getMessage();
                    elementFound = false;
                    break;
                }

                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be no longer present by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementById(String elementId)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))) != null)
                    {

                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByIdVisibility(String elementId)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))) != null)
                    {
                        if (wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id(elementId))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by ID visibility  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementById(String elementId, Integer timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))) != null)
                    {

                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByCSSSelector(String elementCSSSelector)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(elementCSSSelector))) != null)
                    {

                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by CSS Selector  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByLinkText(String elementLinkText)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {

                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(elementLinkText))) != null)
                    {

                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by link text : '" + elementLinkText + "'  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByPartialLinkText(String elementPartialLinkText)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    Driver.findElement(By.partialLinkText(elementPartialLinkText));
                    elementFound = true;
                    break;
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by partial link text : '" + elementPartialLinkText + "'  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByClassName(String elementClassName)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.className(elementClassName))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by class name : '" + elementClassName + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByClassName(String elementClassName, Integer timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.className(elementClassName))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by class name : '" + elementClassName + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByName(String elementName)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.name(elementName))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by name  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementNoLongerPresentByClass(String elementClass, Integer timeout)
    {
        boolean elementFound = true;
        try
        {
            int waitCount = 0;
            while (elementFound && waitCount < timeout)
            {
                try
                {
                    Driver.findElement(By.className(elementClass));
                    elementFound = true;
                }
                catch (Exception e)
                {
                    this.DriverExceptionDetail = e.getMessage();
                    elementFound = false;
                    break;
                }
                Thread.sleep(500);
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be no longer present by Class  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        if (elementFound)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean CompareWebItemsBeforeAndAfterSort(String xpathForElementsFromColumnNumber, String coloumnName, String returnError)
    {
        try
        {
            List<WebElement> webItemsList;
            ArrayList<String> tempList = new ArrayList();

            webItemsList = SeleniumDriverInstance.Driver.findElements(By.xpath(xpathForElementsFromColumnNumber));
            for (int i = 0; i < webItemsList.size(); i++)
            {
                //System.out.println("^"+webItemsList.get(i).getText().toString());
                tempList.add(webItemsList.get(i).getText().toString());
            }

            String temp1 = tempList.get(0);
            String temp2 = webItemsList.get(0).getText().toString();

            //if the items are all blank
            if (temp1.contentEquals(" ") && temp2.contentEquals(" "))
            {
                //click coloumnName to sort
                if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.DivWithTextXpath(coloumnName)))
                {
                    returnError = "Could not wait for the '" + coloumnName + "' to be visible";
                    return false;
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.DivWithTextXpath(coloumnName)))
                {
                    returnError = "Could not wait for the '" + coloumnName + "' to be clickable";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.DivWithTextXpath(coloumnName)))
                {
                    returnError = "Could not click the '" + coloumnName + "' to sort";
                    return false;
                }
                return true;
            }

            int stopCounter = 0;

            while (temp1.contentEquals(temp2))
            {

                //click coloumnName to sort
                if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.DivWithTextXpath(coloumnName)))
                {
                    returnError = "Could not wait for the '" + coloumnName + "' to be visible";
                    return false;
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.DivWithTextXpath(coloumnName)))
                {
                    returnError = "Could not wait for the '" + coloumnName + "' to be clickable";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.DivWithTextXpath(coloumnName)))
                {
                    returnError = "Could not click the '" + coloumnName + "' to sort";
                    return false;
                }

                //if sorting the list returns it to its original state without any change, the items are all equal
                if (stopCounter == 3)
                {
                    return true;
                }

                webItemsList = SeleniumDriverInstance.Driver.findElements(By.xpath(xpathForElementsFromColumnNumber));
                temp2 = webItemsList.get(0).getText();
                stopCounter++;
            }

            //no dynamic elements to wait for
            SeleniumDriverInstance.pause(500);

            List<WebElement> newWebItemsList = SeleniumDriverInstance.Driver.findElements(By.xpath(xpathForElementsFromColumnNumber));

            int counter1 = 0;
            int counter2 = newWebItemsList.size();

            while (counter2 > 0)
            {
                temp1 = newWebItemsList.get(counter1).getText().toString();
                temp2 = tempList.get(counter2 - 1);

                //returns true if the two values are not the same after sorting
                if (!temp1.contentEquals(temp2))
                {
                    returnError = "The comparison of the sorted items failed: '" + temp1 + "' does not equal '" + temp2 + "'";
                    return false;
                }
                counter1++;
                counter2--;
            }
        }
        catch (Exception e)
        {
            returnError = "[ERROR] - " + e.getMessage() + " - Could not retrieve the sorted webItems from the coloumn:'" + coloumnName + "' ";
            return false;
        }

        return true;
    }

    public boolean SintelligentModalFrameColumnResizeRight(String columnName, int resizeRightAmount)
    {
       if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
        {
            try
            {
            Actions action = new Actions(SeleniumDriverInstance.Driver);
            action.release().build().perform();
            }
            catch(Exception ex)
            {
                
            }
        }
        try
        {
            List <WebElement> columnDivider = SeleniumDriverInstance.Driver.findElements(By.xpath("//div[contains(text(), '" + columnName + "')]//..//div[contains(@id,'Shift')]"));
            Actions action = new Actions(SeleniumDriverInstance.Driver);
               
            int ok_size=columnDivider.size();
            int max=0;
            for (int i = 0; i < ok_size; i++) 
            {
                int x = columnDivider.get(i).getLocation().getX();
                 
                if (x!=0) 
                {
                    action.moveToElement(columnDivider.get(i)).build().perform();
                    action.clickAndHold(columnDivider.get(i)).build().perform();

                    action.moveByOffset((resizeRightAmount), 0).build().perform();

                    action.release().build().perform();
                    return true;
                }
                max++;
            }
            if (max == ok_size) 
            {
                return false;
            }
 

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean Move(String xpath, int resizeRightAmount)
    {
        try
        {
            WebElement columnDivider = SeleniumDriverInstance.Driver.findElement(By.xpath(xpath));
            Actions action = new Actions(SeleniumDriverInstance.Driver);

            action.clickAndHold(columnDivider).build().perform();

            action.moveByOffset((resizeRightAmount), 0).build().perform();

            action.release().build().perform();

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean SintelligentModalFrameColumnResizeLeft(String columnName, int resizeLeftAmount)
    {
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
        {
            try
            {
            Actions action = new Actions(SeleniumDriverInstance.Driver);
            action.release().build().perform();
            }
            catch(Exception ex)
            {
                
            }
        }
        try
        {
            List <WebElement> columnDivider = SeleniumDriverInstance.Driver.findElements(By.xpath("//div[contains(text(), '" + columnName + "')]//..//div[contains(@id,'Shift')]"));
            Actions action = new Actions(SeleniumDriverInstance.Driver);

            int ok_size=columnDivider.size();
            int max=0;
            for (int i = 0; i < ok_size; i++) 
            {
                int x = columnDivider.get(i).getLocation().getX();
                
                if (x!=0) 
                {
                    action.moveToElement(columnDivider.get(i)).build().perform();
                    action.clickAndHold(columnDivider.get(i)).build().perform();

                    action.moveByOffset((-1 * resizeLeftAmount), 0).build().perform();

                    action.release().build().perform();
                    return true;
                }
                max++;
            }
            
            if (max == ok_size) 
            {
                return false;
            }



        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean SintelligentColumnResizeLeft(String columnName, int resizeLeftAmount)
    {
        try
        {
            waitForElementByXpath("//div[contains(text(), '" + columnName + "')]//..//div[2]");
            waitForElementByXpathVisibility("//div[contains(text(), '" + columnName + "')]//..//div[2]");
            waitForElementToBeClickableByXpath("//div[contains(text(), '" + columnName + "')]//..//div[2]");
            WebElement columnDivider = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[contains(text(), '" + columnName + "')]//..//div[2]"));
            Actions action = new Actions(SeleniumDriverInstance.Driver);

            action.clickAndHold(columnDivider).build().perform();

            action.moveByOffset((-1 * resizeLeftAmount), 0).build().perform();

            action.release().build().perform();

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean SintelligentColumnResizeRight(String columnName, int resizeRightAmount)
    {
        try
        {
            waitForElementByXpath("//div[contains(text(), '" + columnName + "')]//..//div[2]");
            waitForElementByXpathVisibility("//div[contains(text(), '" + columnName + "')]//..//div[2]");
            waitForElementToBeClickableByXpath("//div[contains(text(), '" + columnName + "')]//..//div[2]");
            WebElement columnDivider = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[contains(text(), '" + columnName + "')]//..//div[2]"));
            Actions action = new Actions(SeleniumDriverInstance.Driver);

            action.clickAndHold(columnDivider).build().perform();

            action.moveByOffset((resizeRightAmount), 0).build().perform();

            action.release().build().perform();

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean selectByTextFromDropDownListUsingXpathNew(String elementXpath, String elementName)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement dropdown = Driver.findElement(By.xpath(elementXpath));
            if (ApplicationConfig.SelectedBrowser() != Enums.BrowserType.FireFox) {
               dropdown.click(); 
            }
            

            SeleniumDriverInstance.pause(2000);
            List<WebElement> options = dropdown.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.contains(elementName))
                {
                        option.click();

                        Narrator.logError("[Info] Selected xpath text '" + elementName + "' from element '" + elementXpath + "'");

                        return true;
                    }
            }
        }
        catch (Exception e)
        {
            System.err.println("[Error] Failed to select from dropdownlist by text using name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return false;
    }

    public boolean selectByTextFromDropDownList_UsingXpathNew(String elementXpath, String elementName)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement dropdown = Driver.findElement(By.xpath(elementXpath));
//            dropdown.click();

            //SeleniumDriverInstance.pause(2000);
            List<WebElement> options = dropdown.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.contains(elementName))
                {
                    option.click();

                    Narrator.logError("[Info] Selected xpath text '" + elementName + "' from element '" + elementXpath + "'");

                    return true;
                }
            }
        }
        catch (Exception e)
        {
            System.err.println("[Error] Failed to select from dropdownlist by text using name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return false;
    }

    public boolean selectByTextFromDropDownListViaTextboxUsingXpath(String elementXpath, String elementName, String textBox)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement dropdown = Driver.findElement(By.xpath(elementXpath));
            dropdown.click();

            //SeleniumDriverInstance.pause(2000);
//            List<WebElement> options = dropdown.findElements(By.tagName("option"));
//
//            for (WebElement option : options)
//            option.click();
//            {
//                WebElement textBoxValue = SeleniumDriverInstance.Driver.findElement(By.xpath(textBox));
//                String optTxt = textBoxValue.getText();
//
//                if (optTxt.contains(elementName))
//                {
//
//                    Narrator.logError("[Info] Selected xpath text '" + elementName + "' from element '" + elementXpath + "'");
//
//                    return true;
//                }
//            }
        }
        catch (Exception e)
        {
            System.err.println("[Error] Failed to select from dropdownlist by text using name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return false;
    }

    public boolean selectByTextFromDropDownListUsingXpathSintelligent(String elementXpath, String elementName)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement dropdown = Driver.findElement(By.xpath(elementXpath));

            //SeleniumDriverInstance.pause(2000);
            List<WebElement> options = dropdown.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.contains(elementName))
                {
                    option.click();

                    Narrator.logError("[Info] Selected xpath text '" + elementName + "' from element '" + elementXpath + "'");

                    return true;
                }
            }
        }
        catch (Exception e)
        {
            System.err.println("[Error] Failed to select from dropdownlist by text using name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return false;
    }

    public boolean specialWaitForElementByName(String elementName, String type)
    {
        boolean elementFound = false;
        String elementType = "";
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.name(elementName))) != null)
                    {
                        WebElement element = Driver.findElement(By.name(elementName));
                        try
                        {
                            String tagName = element.getTagName();
                            if (tagName.equals("input"))
                            {
                                elementType = "textbox";
                            }
                            else if (tagName.equals("select"))
                            {
                                elementType = "dropdownList";
                            }
                        }
                        catch (Exception ex)
                        {
//                            Narrator.errorLog(" Failed to wait for element by name  - " + ex.getMessage());
                            this.DriverExceptionDetail = ex.getMessage();
                            elementFound = false;
                        }

                        if (type.equals(elementType))
                        {
                            elementFound = true;
                        }
                        else
                        {
                            elementFound = false;
                        }
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by name  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByName(String elementName, int waitTimeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < waitTimeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.name(elementName))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by name  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByNameNoExceptions(String elementName, int waitTimeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < waitTimeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.name(elementName))) != null)
                    {
                        if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(elementName))) != null)
                        {
                            elementFound = true;
                        }
                        else
                        {
                            elementFound = false;
                            Narrator.logError(" Element found but is not visible.");
                        }

                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to find element by name '" + elementName + "'");
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByLinkTextNoExceptions(String elementText, int waitTimeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < waitTimeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(elementText))) != null)
                    {
                        if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(elementText))) != null)
                        {
                            elementFound = true;
                        }
                        else
                        {
                            elementFound = false;
                            Narrator.logError(" Element found but is not visible.");
                        }

                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to find element by link text '" + elementText + "'");
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpath(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementToBeClickableByXpath(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be clickable by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementToBeClickableByXpath(String elementXpath, Integer timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be clickable by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementToBeClickableById(String elementXpath, Integer timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.elementToBeClickable(By.id(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be clickable by id '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpathVisibility(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpathVisibility(String elementXpath, Integer timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpathVisibilityMillisecond(String elementXpath, Integer hundredsOfMilliseconds)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < hundredsOfMilliseconds)
            {
                try
                {
                    WebDriverWait waitMSecs = new WebDriverWait(Driver, 0, 100);
                    if (waitMSecs.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (waitMSecs.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpathVisibilityMillisecondNew(String elementXpath, Integer milliseconds)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < milliseconds)
            {
                try
                {
                    WebDriverWait waitMSecs = new WebDriverWait(Driver, 0, 1);
                    if (waitMSecs.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (waitMSecs.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpath(String elementXpath, Integer timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public void pause(int milisecondsToWait)
    {
        try
        {
            Thread.sleep(milisecondsToWait);
        }
        catch (Exception e)
        {

        }
    }

    public void shutDown()
    {
        retrievedTestValues = null;
        try
        {

            Driver.quit();

        }
        catch (Exception e)
        {
            Narrator.logError(" shutting down driver - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        _isDriverRunning = false;
    }

    public void takeScreenShot(String screenShotDescription, boolean isError)
    {
        String imageFilePathString = "";

        try
        {
            StringBuilder imageFilePathBuilder = new StringBuilder();
            // add date time folder and test case id folder
            imageFilePathBuilder.append(this.reportDirectory + "\\" + testCaseId + "\\");

            if (isError)
            {
                imageFilePathBuilder.append("FAILED_");
            }
            else
            {
                imageFilePathBuilder.append("PASSED_");
            }

            imageFilePathBuilder.append(testCaseId + "_");

            imageFilePathBuilder.append(screenShotDescription + ".png");

            //imageFilePathBuilder.append(this.generateDateTimeString() + ".png");
            imageFilePathString = imageFilePathBuilder.toString();

            setScreenshotPath(imageFilePathString);

            File screenShot = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenShot, new File(imageFilePathString.toString()));
        }
        catch (Exception e)
        {
            Narrator.logError(" could not take screenshot - " + imageFilePathString.toString() + " - " + e.getMessage());
        }
    }

    public String getSelectedDropdownOptionTextByXpath(String elementXpath)
    {
        try
        {
            if (waitForElementByXpath(elementXpath))
            {
                Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
                String selectedValue = dropDownList.getFirstSelectedOption().getText();

                return selectedValue;

            }
        }
        catch (Exception e)
        {
            System.err.println("[Error] Failed to retrieve selected dropdown option index using name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return "";
        }

        return "";
    }

    public boolean selectByTextFromDropDownListAndClickEnterUsingXpath(String elementXpath, String valueToSelect)
    {
        try
        {
          if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
            {
                
                            waitForElementByXpath(elementXpath);
            waitForElementToBeClickableByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).build().perform();
            builder.click(elementToClick).build().perform();
                        List<WebElement> options = elementToClick.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.contains(valueToSelect))
                {
            builder.moveToElement(option).build().perform();
            builder.click(option).build().perform();
                    //Actions action = new Actions(SeleniumDriverInstance.Driver);
                    //action.sendKeys(Keys.ENTER).build().perform();
                    //option.click();

                    Narrator.logError("[Info] Selected partial text '" + valueToSelect + "' from element '" + elementXpath + "'");
                    return true;
                }
            }
                 
            }
            else{
            waitForElementByXpath(elementXpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));

            dropDownList.selectByVisibleText(valueToSelect);
//            dropDownList.selectByValue(valueToSelect);

            Actions action = new Actions(SeleniumDriverInstance.Driver);
            action.sendKeys(Keys.ENTER).build().perform();
          }
            return true;
         
          
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting from dropdownlist by text using xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByTextFromDropDownListUsingXpath(String elementXpath, String valueToSelect)
    {
        try
        {
            if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
            {
                
            waitForElementByXpath(elementXpath);
            waitForElementToBeClickableByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).build().perform();
            builder.click(elementToClick).build().perform();
                        List<WebElement> options = elementToClick.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.equals(valueToSelect))
                {
            builder.moveToElement(option).build().perform();
            builder.click(option).build().perform();
                    //Actions action = new Actions(SeleniumDriverInstance.Driver);
                    //action.sendKeys(Keys.ENTER).build().perform();
                    //option.click();

                    Narrator.logError("[Info] Selected partial text '" + valueToSelect + "' from element '" + elementXpath + "'");
                    return true;
                }
            }
                
            }
            else{
            waitForElementByXpath(elementXpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
            
            dropDownList.selectByVisibleText(valueToSelect);
//            dropDownList.selectByValue(valueToSelect);

//                Actions action = new Actions(SeleniumDriverInstance.Driver);
//                action.sendKeys(Keys.ENTER).build().perform();
            }
            return true;
            
        }
        catch (Exception e)
        {
            Narrator.logError("selecting from dropdownlist by text using xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByPartialTextFromDropDownListUsingXpath(String elementXpath, String partialValueToSelect)
    {
        try
        {
          if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
            {
                
                            waitForElementByXpath(elementXpath);
            waitForElementToBeClickableByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).build().perform();
            builder.click(elementToClick).build().perform();
                        List<WebElement> options = elementToClick.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.contains(partialValueToSelect))
                {
            builder.moveToElement(option).build().perform();
            builder.click(option).build().perform();
                    //Actions action = new Actions(SeleniumDriverInstance.Driver);
                    //action.sendKeys(Keys.ENTER).build().perform();
                    //option.click();

                    Narrator.logError("[Info] Selected partial text '" + partialValueToSelect + "' from element '" + elementXpath + "'");
                    return true;
                }
            }
                
            }
            else{
            this.waitForElementByXpath(elementXpath);
            WebElement dropdown = Driver.findElement(By.xpath(elementXpath));
            dropdown.click();

            List<WebElement> options = dropdown.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.contains(partialValueToSelect))
                {

                    Actions action = new Actions(SeleniumDriverInstance.Driver);
                    action.sendKeys(Keys.ENTER).build().perform();
                    option.click();

                    Narrator.logError("[Info] Selected partial text '" + partialValueToSelect + "' from element '" + elementXpath + "'");
                    return true;
                }
            }
          }
        }
        catch (Exception e)
        {
            System.err.println("[Error] Failed to select from dropdownlist by text using xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return false;
    }

    public boolean selectByPartialTextFromListboxUsingXpath(String elementXpath, String partialValueToSelect)
    {
        try
        {
          if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
            {
                
                            waitForElementByXpath(elementXpath);
            waitForElementToBeClickableByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).build().perform();
            builder.click(elementToClick).build().perform();
                        List<WebElement> options = elementToClick.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.contains(partialValueToSelect))
                {
            builder.moveToElement(option).build().perform();
            builder.click(option).build().perform();
                    //Actions action = new Actions(SeleniumDriverInstance.Driver);
                    //action.sendKeys(Keys.ENTER).build().perform();
                    //option.click();

                    Narrator.logError("[Info] Selected partial text '" + partialValueToSelect + "' from element '" + elementXpath + "'");
                    return true;
                }
            }
                
            }
            else{ 
            this.waitForElementByXpath(elementXpath);
            WebElement dropdown = Driver.findElement(By.xpath(elementXpath));
            //dropdown.click();

            List<WebElement> options = dropdown.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.contains(partialValueToSelect))
                {

                    Actions action = new Actions(SeleniumDriverInstance.Driver);
                    // action.sendKeys(Keys.ENTER).build().perform();
                    option.click();

                    Narrator.logError("[Info] Selected partial text '" + partialValueToSelect + "' from element '" + elementXpath + "'");
                    return true;
                }
            }
         }
        }
        catch (Exception e)
        {
            System.err.println("[Error] Failed to select from dropdownlist by text using xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return false;
    }

    public boolean selectByPartialTextFromDropDownListUsingXpathNew(String elementXpath, String partialValueToSelect)
    {
        try
        {
                                   
            if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
            {
                
                            waitForElementByXpath(elementXpath);
            waitForElementToBeClickableByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).build().perform();
            builder.click(elementToClick).build().perform();
                        List<WebElement> options = elementToClick.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.contains(partialValueToSelect))
                {
            builder.moveToElement(option).build().perform();
            builder.click(option).build().perform();
                    //Actions action = new Actions(SeleniumDriverInstance.Driver);
                    //action.sendKeys(Keys.ENTER).build().perform();
                    //option.click();

                    Narrator.logError("[Info] Selected partial text '" + partialValueToSelect + "' from element '" + elementXpath + "'");
                    return true;
                }
            }
                
            }
            else{
            this.waitForElementByXpath(elementXpath);
            WebElement dropdown = Driver.findElement(By.xpath(elementXpath));
            if (ApplicationConfig.SelectedBrowser() != Enums.BrowserType.FireFox) 
            {
              dropdown.click();  
            }
            
            List<WebElement> options = dropdown.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.contains(partialValueToSelect))
                {

                    Actions action = new Actions(SeleniumDriverInstance.Driver);
                    //action.sendKeys(Keys.ENTER).build().perform();
                    option.click();

                    Narrator.logError("[Info] Selected partial text '" + partialValueToSelect + "' from element '" + elementXpath + "'");
                    return true;
                }
            }
          }
        }
        catch (Exception e)
        {
            System.err.println("[Error] Failed to select from dropdownlist by text using xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return false;
    }

    public boolean rightClickElementByXpathFireFox(String elementXpath)
    {
        try 
        {
            WebElement element = SeleniumDriverInstance.Driver.findElement(By.xpath(elementXpath));
            Actions action = new Actions(Driver).contextClick(element);
            action.build().perform();
            return true;
        } 
        catch (Exception e) 
        {
            Narrator.logError(" right clicking element by xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        
    }
    
    public boolean rightClickElementByXpath(String elementXpath)
    {
        this.waitForElementByXpath(elementXpath);
        try
        {
            WebElement elem = SeleniumDriverInstance.Driver.findElement(By.xpath(elementXpath));

            Actions actions = new Actions(SeleniumDriverInstance.Driver);
            actions.moveToElement(elem);
            actions.contextClick(elem).build().perform();//Right click
//            actions.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" right clicking element by xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean dragAndDropElementByXpath(String elementSourceXpath, String elementTargetXpath)
    {
        this.waitForElementByXpath(elementSourceXpath);
        this.waitForElementByXpath(elementTargetXpath);
        try
        {
            WebElement elem = SeleniumDriverInstance.Driver.findElement(By.xpath(elementSourceXpath));
            WebElement elemTarget = SeleniumDriverInstance.Driver.findElement(By.xpath(elementTargetXpath));

            (new Actions(Driver)).dragAndDrop(elem, elemTarget).perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Drag and drop element by xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean rightClickMainSubnetsSintelligentMenu(String elementToRightClickXpath, String menuChoice)
    {
        //right click element
        if (!this.waitForElementByXpathVisibility(elementToRightClickXpath))
        {
            return false;
        }
        if (!this.waitForElementToBeClickableByXpath(elementToRightClickXpath))
        {
            return false;
        }
        if (!this.rightClickElementByXpath(elementToRightClickXpath))
        {
            return false;
        }

        //no dynamic elements to wait for
        SeleniumDriverInstance.pause(300);

        //select choice
        if (!this.waitForElementByXpathVisibility("//div[@id='jqContextMenu']//span[contains(text(), '" + menuChoice + "')]"))
        {
            return false;
        }
        if (!this.waitForElementToBeClickableByXpath("//div[@id='jqContextMenu']//span[contains(text(), '" + menuChoice + "')]"))
        {
            return false;
        }
        if (!this.clickElementByXpath("//div[@id='jqContextMenu']//span[contains(text(), '" + menuChoice + "')]"))
        {
            return false;
        }

        this.pause(100);

        return true;
    }

    public String generateDateTimeString()
    {
        Date dateNow = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");

        return dateFormat.format(dateNow).toString();
    }

    public String generateDateTimeString(String format)
    {
        Date dateNow = new Date();

        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        return dateFormat.format(dateNow).toString();
    }

    public String generateNextFirstDayOfTheMonth(String format)
    {
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            Date dateNow = new Date();
            Calendar cal = new GregorianCalendar();

            cal.setTime(dateNow);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            if (day == 1)
            {
                return dateFormat.format(cal.getTime()).toString();
            }

            cal.add(Calendar.MONTH, 1);
            cal.set(Calendar.DAY_OF_MONTH, 1);

            Narrator.logDebug(" Successfully generated next first day of the month - '" + dateFormat.format(cal.getTime()).toString() + "'");
            return dateFormat.format(cal.getTime()).toString();
        }
        catch (Exception e)
        {
            Narrator.logError(" Unable to generate the next first day of the month - " + e.getMessage());
            return "";
        }

    }

    public String generateFirstDayOfTheCurrentMonth(String format)
    {
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            Date dateNow = new Date();
            Calendar cal = new GregorianCalendar();

            cal.setTime(dateNow);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            if (day == 1)
            {
                return dateFormat.format(cal.getTime()).toString();
            }

            cal.set(Calendar.DAY_OF_MONTH, 1);

            Narrator.logDebug(" Successfully generated first day of the current month - '" + dateFormat.format(cal.getTime()).toString() + "'");
            return dateFormat.format(cal.getTime()).toString();
        }
        catch (Exception e)
        {
            Narrator.logError(" Unable to generate the first day of the current month - " + e.getMessage());
            return "";
        }
    }

    public void CloseChromeInstances() throws IOException
    {
        if (browserType.equals(browserType.Chrome))
        {
            String TASKLIST = "tasklist";
            String KILL = "taskkill /IM ";
            String line;
            String serviceName = "chrome.exe";
            Process p = Runtime.getRuntime().exec(TASKLIST);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            Narrator.logDebug("Closing Chrome tasks...");
            while ((line = reader.readLine()) != null)
            {

                if (line.contains(serviceName))
                {
                    Runtime.getRuntime().exec(KILL + serviceName);
                }
            }
            Driver.close();
        }
    }

    public boolean compareValues(String ElementId, String valueToCompare)
    {
        try
        {
            WebElement Element = Driver.findElement(By.id(ElementId));
            String valueFromElement = Element.getAttribute("value");

            if (valueFromElement.toUpperCase().trim().equals(valueToCompare.toUpperCase().trim()))
            {
                Narrator.logDebug("Specified values found: " + valueFromElement + " AND " + valueToCompare);
                return true;
            }
            else
            {
                Narrator.logError("Specified values do not match : " + valueFromElement + " AND " + valueToCompare);
                return false;
            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" Failed to find specified element : " + ex.getMessage());
            return false;
        }
    }

    public boolean compareValuesWithSubString(String ElementId, int StartValue, int EndValue, String valueToCompare)
    {
        try
        {

            WebElement Element = Driver.findElement(By.id(ElementId));
            String valueFromElement = Element.getAttribute("value").substring(StartValue, EndValue);

            if (valueFromElement.toUpperCase().trim().equals(valueToCompare.toUpperCase().trim()))
            {
                Narrator.logDebug("Specified values found: " + valueFromElement + " AND " + valueToCompare);
                return true;
            }
            else
            {
                Narrator.logError("Specified values do not match : " + valueFromElement + " AND " + valueToCompare);
                return false;
            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" Failed to find specified element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean CheckForPrescenceOfAnElementUsingId(String containerId, String ElementID)
    {
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement DivList = Driver.findElement(By.id(containerId));
            List<WebElement> Elements = DivList.findElements(By.id(ElementID));
            if (Elements.size() != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean CheckForPrescenceOfAnElementUsingSingleId(String ElementID)
    {
        try
        {
            SeleniumDriverInstance.waitForElementById(ElementID);

            WebElement DivList = Driver.findElement(By.id(ElementID));
            List<WebElement> Elements = DivList.findElements(By.id(ElementID));
            if (Elements.size() != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean CheckForPrescenceOfAnElementUsingClassName(String containerXpath, String ElementClassName)
    {
        try
        {
            SeleniumDriverInstance.waitForElementByXpath(containerXpath);

            WebElement DivList = Driver.findElement(By.xpath(containerXpath));
            List<WebElement> Elements = DivList.findElements(By.className(ElementClassName));
            if (Elements.size() != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean CheckForPrescenceOfAnElementUsingName(String ElementName)
    {
        try
        {
            SeleniumDriverInstance.waitForElementByName(ElementName);

            WebElement DivList = Driver.findElement(By.name(ElementName));
            List<WebElement> Elements = DivList.findElements(By.className(ElementName));
            if (Elements.size() != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean CheckForPrescenceOfAnElementUsingLinkText(String containerId, String ElementLinkText)
    {
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement DivList = Driver.findElement(By.id(containerId));
            List<WebElement> Elements = DivList.findElements(By.linkText(ElementLinkText));
            if (Elements.size() != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean CheckForPrescenceOfAnElementUsingLinkTextSingle(String ElementLinkText)
    {
        try
        {
            SeleniumDriverInstance.waitForElementByLinkText(ElementLinkText);

            WebElement DivList = Driver.findElement(By.linkText(ElementLinkText));
            return DivList != null;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean CheckForPrescenceOfAnElementUsingPartialLinkTextSingle(String ElementLinkText)
    {
        try
        {
            SeleniumDriverInstance.waitForElementByPartialLinkText(ElementLinkText);

            WebElement DivList = Driver.findElement(By.partialLinkText(ElementLinkText));
            return DivList != null;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean CheckForPrescenceOfAnElementUsingXpath(String containerId, String Xpath)
    {
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement DivList = Driver.findElement(By.id(containerId));
            List<WebElement> Elements = DivList.findElements(By.xpath(Xpath));
            if (Elements.size() != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public Boolean ClickEmbeddedTableButtonUsingFirstCellText(String containerID, String tableXpath, String subTag, String EmbeddedTag, String validationText, String ButtonXpath)
    {
        String firstCellText = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerID);

            WebElement div = Driver.findElement(By.id(containerID));
            Narrator.logDebug("Found div");
            WebElement table = div.findElement(By.xpath(tableXpath));
            Narrator.logDebug("Found table");
            List<WebElement> rows = table.findElements(By.tagName(subTag));
            int rowCount = 1;
            for (WebElement row : rows)
            {
                Narrator.logDebug("Row - " + rowCount);
                List<WebElement> cells = row.findElements(By.tagName(EmbeddedTag));

                if (cells == null || cells.size() < 1)
                {
                    continue;
                }
                WebElement firstCell = cells.get(0);
                Narrator.logDebug("Found cells: " + cells.size());
                firstCellText = firstCell.getText();

                if (firstCellText.toUpperCase().trim().equals(validationText.toUpperCase().trim()))
                {
                    Narrator.logDebug(firstCellText + " = " + validationText);
                    WebElement button = row.findElement(By.xpath(ButtonXpath));
                    Narrator.logDebug("Button found, about to click!!!!!");
                    button.click();

                    return true;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean nestedHoverOverElementAndClickByXpath(String ParentElementXpath, String SubElementXpath, String ChildElementXpath)
    {
        try
        {

            Actions actions = new Actions(Driver);
            WebElement Div = Driver.findElement(By.xpath(ParentElementXpath));
            if (!waitForElementByXpathVisibility(ParentElementXpath))
            {
                return false;
            }
            if (!waitForElementToBeClickableByXpath(ParentElementXpath))
            {
                return false;
            }
            actions.moveToElement(Div);

            SeleniumDriverInstance.pause(1000);
            actions.perform();
            WebElement SubElement = Driver.findElement(By.xpath(SubElementXpath));
            if (!waitForElementByXpathVisibility(SubElementXpath))
            {
                return false;
            }
            if (!waitForElementToBeClickableByXpath(SubElementXpath))
            {
                return false;
            }

            actions.moveToElement(SubElement);
            SeleniumDriverInstance.pause(1000);
            actions.perform();

            WebElement ChildElement = Driver.findElement(By.xpath(ChildElementXpath));
            if (!waitForElementByXpathVisibility(ChildElementXpath))
            {
                return false;
            }
            if (!waitForElementToBeClickableByXpath(ChildElementXpath))
            {
                return false;
            }

            actions.moveToElement(ChildElement);
            SeleniumDriverInstance.pause(1000);
            actions.click();
            actions.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to hover over element and click sub element by ID and Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean nestedHoverOverJobsElementAndClickByXpath(String ParentElementXpath, String SubElementXpath, String ChildElementXpath)
    {
        try
        {
            if (!this.waitForElementByXpath(ParentElementXpath))
            {
                return false;
            }
//            SeleniumDriverInstance.pause(5000);
            Actions actions = new Actions(Driver);
            WebElement Div = Driver.findElement(By.xpath(ParentElementXpath));
            actions.moveToElement(Div);
            actions.perform();
            if (!this.waitForElementByXpath(SubElementXpath))
            {
                return false;
            }
            //SeleniumDriverInstance.pause(5000);
            WebElement SubElement = Driver.findElement(By.xpath(SubElementXpath));
            actions.moveToElement(SubElement);
            actions.perform();
            if (!this.waitForElementByXpath(ChildElementXpath))
            {
                return false;
            }
            //SeleniumDriverInstance.pause(1000);
            WebElement ChildElement = Driver.findElement(By.xpath(ChildElementXpath));
            actions.moveToElement(ChildElement);
            SeleniumDriverInstance.pause(1000);
            actions.click();
            actions.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to hover over element and click sub element by ID and Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean ClickEmbeddedTableButtonUsingCellTextAndButtonText(String containerID, String tableXpath, String subTag, String EmbeddedTag, String validationText, String ButtonTag, String ButtonValText)
    {
        String ButtonText = "";
        String firstCellText = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerID);

            WebElement div = Driver.findElement(By.id(containerID));
            Narrator.logDebug("Found div");
            WebElement table = div.findElement(By.xpath(tableXpath));
            Narrator.logDebug("Found table");
            List<WebElement> rows = table.findElements(By.tagName(subTag));
            int rowCount = 1;
            for (WebElement row : rows)
            {
                Narrator.logDebug("Row - " + rowCount);
                List<WebElement> cells = row.findElements(By.tagName(EmbeddedTag));

                if (cells == null || cells.size() < 1)
                {
                    continue;
                }
                WebElement firstCell = cells.get(0);
                Narrator.logDebug("Found cells: " + cells.size());
                firstCellText = firstCell.getText();
                Narrator.logDebug("First cell: " + firstCellText + " while the Validation text: " + validationText);

                if (firstCellText.toUpperCase().trim().equals(validationText.toUpperCase().trim()))
                {
                    Narrator.logDebug("firstCell = " + firstCellText + "   &   Validation Text = " + validationText);
                    Narrator.logDebug(firstCellText + " = " + validationText);
                    List<WebElement> buttons = row.findElements(By.tagName(ButtonTag));
                    Narrator.logDebug("Buttons found, about to click!!!!!");
                    for (WebElement button : buttons)
                    {
                        ButtonText = button.getText();
                        Narrator.logDebug("Found : " + ButtonText + " button.");
                        if (ButtonText.toUpperCase().trim().equals(ButtonValText.toUpperCase()))
                        {
                            Narrator.logDebug("Clicking button:         Button Text: " + ButtonText + "  <===>  " + ButtonValText);
                            button.click();
                            Narrator.logDebug("Clicking button");
                            return true;
                        }
                    }
                    return true;
                }
                rowCount++;
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean ClickListItemUsingDivId(String containerID, String parentTagName, String childTagName, String EmbeddedParentTag, String validationText)
    {
        String ChildElementText = "";

        try
        {
            SeleniumDriverInstance.waitForElementById(containerID);

            WebElement div = Driver.findElement(By.id(containerID));
            Narrator.logDebug(" Found Div" + containerID);
            List<WebElement> ParentElements = div.findElements(By.tagName(parentTagName));
            Narrator.logDebug(" Found parent element: " + parentTagName);

            int rowCount = 1;
            for (WebElement ParentElement : ParentElements)
            {
                Narrator.logDebug("Row - " + rowCount);
                List<WebElement> ChildElements = ParentElement.findElements(By.tagName(childTagName));
                Narrator.logDebug("Making a list of child elements");

                int itemCount = 1;
                for (WebElement ChildElement : ChildElements)
                {
                    Narrator.logDebug("[INFORMATION] Found these Child elements: " + ChildElement.getText());
                    List<WebElement> EmbeddedParentElements = ChildElement.findElements(By.tagName(EmbeddedParentTag));

                    for (WebElement EmbeddedParentElement : EmbeddedParentElements)
                    {
                        ChildElementText = EmbeddedParentElement.getText();
                        Narrator.logDebug(ChildElementText);
                        Narrator.logDebug("These are embedded tags: " + EmbeddedParentElement.getText());

                        if (ChildElementText.toUpperCase().trim().equals(validationText.toUpperCase().trim()))
                        {
                            Narrator.logDebug("About to click child element button");
                            ChildElement.click();
                            Narrator.logDebug("Child element button click");
                            return true;
                        }
                    }
                    itemCount++;
                }
                rowCount++;
                return true;
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean ClickListItemUsingDivId2(String divID, String aTagName, String spanTag, String validationText)
    {
        String ChildElementText = "";

        try
        {
            SeleniumDriverInstance.waitForElementById(divID);

            WebElement div = Driver.findElement(By.id(divID));
            Narrator.logDebug(" Found Div" + divID);
            List<WebElement> ParentElements = div.findElements(By.tagName(aTagName));
            Narrator.logDebug(" Found parent element: " + aTagName);

            int itemCount = 1;
            for (WebElement ParentElement : ParentElements)
            {
                Narrator.logDebug("[INFORMATION] Found these Child elements: " + ParentElement.getText());
                List<WebElement> ChildElements = ParentElement.findElements(By.tagName(spanTag));

                for (WebElement ChildElement : ChildElements)
                {
                    ChildElementText = ChildElement.getText();
                    Narrator.logDebug(ChildElementText);
                    Narrator.logDebug("These are embedded tags: " + ChildElement.getText());

                    if (ChildElementText.toUpperCase().trim().equals(validationText.toUpperCase().trim()))
                    {
                        Narrator.logDebug("About to click child element button");
                        ChildElement.click();
                        Narrator.logDebug("Child element button click");
                        return true;
                    }
                }
                itemCount++;
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean ClickSubMenuItemUsingContainerID(String containerID, String parentTagName, String childTagName, String SubTagName, String validationText)
    {
        String SubElementText = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerID);

            WebElement div = Driver.findElement(By.id(containerID));
            Narrator.logDebug(" Found Div" + containerID);
            List<WebElement> ParentElements = div.findElements(By.tagName(parentTagName));
            Narrator.logDebug(" Found parent element: " + parentTagName);

            int rowCount = 1;
            for (WebElement ParentElement : ParentElements)
            {
                Narrator.logDebug("Row - " + rowCount);
                List<WebElement> ChildElements = ParentElement.findElements(By.tagName(childTagName));
                Narrator.logDebug("Making a list of child elements");

                int itemCount = 1;
                for (WebElement ChildElement : ChildElements)
                {
                    Narrator.logDebug("[INFORMATION] Found these Child elements: " + ChildElement.getText());
                    WebElement subElement = ChildElement.findElement(By.tagName(SubTagName));
                    SubElementText = subElement.getText();
                    Narrator.logDebug(SubElementText);
                    Narrator.logDebug("These are embedded tags: " + subElement.getText());

                    if (SubElementText.toUpperCase().trim().equals(validationText.toUpperCase().trim()))
                    {
                        Narrator.logDebug("About to click child element button");
                        subElement.click();
                        Narrator.logDebug("Child element button click");
                        return true;
                    }
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean ClickItemUsingContainerIdAndLists(String containerID, String Elementxpath, String subChildTag, String ValidationText)
    {
        String elementText = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerID);

            WebElement div = Driver.findElement(By.id(containerID));
            Narrator.logDebug(" Found div: " + containerID);
            WebElement element = div.findElement(By.xpath(Elementxpath));
            Narrator.logDebug(" Found primary element: " + element.getText());
            WebElement subChild = element.findElement(By.tagName(subChildTag));
            Narrator.logDebug("SUBCHILD ELEMENT: " + subChild.getText());

            elementText = subChild.getText();
            Narrator.logDebug("Looking for: " + elementText);

            if (elementText.toUpperCase().trim().equals(ValidationText.toUpperCase().trim()))
            {
                Narrator.logDebug("About to click element");
                //SeleniumDriverInstance.highlightElement(Driver, subChild);
                Actions action = new Actions(Driver);
                action.moveToElement(subChild);
                action.perform();
                this.pause(2000);
                action.doubleClick(subChild);
                action.perform();

//                subChild.click();
                return true;
            }

            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage() + "   element: " + elementText);
            return false;
        }
    }

    public boolean ClickItemUsingContainerIdAndList(String containerID, String Elementxpath, String subChildTag, String ValidationText)
    {
        String elementText = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerID);

            WebElement div = Driver.findElement(By.id(containerID));
            Narrator.logDebug(" Found div: " + containerID);
            WebElement element = div.findElement(By.xpath(Elementxpath));
            Narrator.logDebug(" Found primary element: " + element.getText());
            List<WebElement> subChilds = element.findElements(By.tagName(subChildTag));

            for (WebElement subChild : subChilds)
            {
                Narrator.logDebug("SUBCHILD ELEMENT: " + subChild.getText());
                elementText = subChild.getText();
                Narrator.logDebug("Looking for: " + elementText);

                if (elementText.toUpperCase().trim().equals(ValidationText.toUpperCase().trim()))
                {
                    Narrator.logDebug("About to click element");
                    //SeleniumDriverInstance.highlightElement(Driver, subChild);
//                    Actions action = new Actions(Driver);
//                    action.moveToElement(subChild);
//                    action.perform();
                    this.pause(2000);
                    subChild.click();
//                    action.doubleClick(subChild);
//                    action.perform();
                    return true;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage() + "   element: " + elementText);
            return false;
        }
    }

    public boolean ClickLinkElementViaTagUsingContainerID(String containerID, String ElementTag, String ValidationText)
    {
        String elementText = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerID);

            WebElement div = Driver.findElement(By.id(containerID));
            Narrator.logDebug("Found div");
            List<WebElement> LinkElements = div.findElements(By.tagName(ElementTag));

            for (WebElement LinkElement : LinkElements)
            {
                elementText = LinkElement.getText().toUpperCase().trim();
                Narrator.logDebug("Looking for: " + elementText);

                if (elementText.equals(ValidationText.toUpperCase().trim()))
                {
                    Narrator.logDebug("About to click Link Element");

                    LinkElement.click();
                    this.pause(1000);
                }

                return true;
            }

            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage() + "   element: " + elementText);
            return false;
        }
    }

    public boolean ElementHighlighter(String containerID, String Elementxpath, String subChildTag, String ValidationText)
    {
        String elementText = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerID);

            WebElement div = Driver.findElement(By.id(containerID));
            Narrator.logDebug(" Found div: " + containerID);
            WebElement element = div.findElement(By.xpath(Elementxpath));
            Narrator.logDebug(" Found primary element: " + element.getText());
            List<WebElement> subChilds = element.findElements(By.tagName(subChildTag));

            for (WebElement subChild : subChilds)
            {
                Narrator.logDebug("SUBCHILD ELEMENT: " + subChild.getText());

                elementText = subChild.getText();
                Narrator.logDebug("Looking for: " + elementText);

                if (elementText.toUpperCase().trim().equals(ValidationText.toUpperCase().trim()))
                {
                    Narrator.logDebug("About to highlight element");
                    SeleniumDriverInstance.highlightElement(Driver, subChild);
                    return true;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage() + "   element: " + elementText);
            return false;
        }
    }

    public void highlightElement(WebDriver Driver, WebElement element)
    {
        for (int i = 0; i < 1; i++)
        {
            JavascriptExecutor js = (JavascriptExecutor) Driver;
            js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "color: blue; border: 2px solid blue;");
        }
    }

    public boolean ClickItemUsingContainerIdAndListItems(String containerID, String Elementxpath, String subChildTag, String ValidationText)
    {
        String elementText = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerID);

            WebElement div = Driver.findElement(By.id(containerID));
            Narrator.logDebug(" Found div: " + containerID);
            WebElement element = div.findElement(By.xpath(Elementxpath));
            Narrator.logDebug(" Found primary element: " + element.getText());
            List<WebElement> subChilds = element.findElements(By.tagName(subChildTag));

            for (WebElement subChild : subChilds)
            {
                Narrator.logDebug("SUBCHILD ELEMENT: " + subChild.getText());

                elementText = subChild.getText();
                Narrator.logDebug("Looking for: " + elementText);

                if (elementText.toUpperCase().trim().equals(ValidationText.toUpperCase().trim()))
                {
                    Narrator.logDebug("About to click element");
                    //SeleniumDriverInstance.highlightElement(Driver, subChild);
                    Actions action = new Actions(Driver);
                    action.moveToElement(subChild);
                    action.perform();
                    this.pause(2000);
                    action.doubleClick(subChild);
                    action.perform();
                    return true;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage() + "   element: " + elementText);
            return false;
        }
    }

    public boolean ClickElementUsingContainerIDandElementTag(String ContainerID, String ElementTag)
    {
        try
        {
            SeleniumDriverInstance.waitForElementById(ContainerID);

            WebElement div = Driver.findElement(By.id(ContainerID));
            Narrator.logDebug(" Found div @:" + ContainerID);
            WebElement element = div.findElement(By.tagName(ElementTag));
            Narrator.logDebug(" found element @:" + ElementTag);

            Narrator.logDebug("About to click the element");
            element.click();

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean uploadFileUsingElementIdAndFileLocation(String browseButtonId, String fileLocation)
    {
        try
        {
            WebElement fileUpload = Driver.findElement(By.id(browseButtonId));
            fileUpload.sendKeys(fileLocation);
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public boolean selectRadioButtonUsingElementXpathAndBoolean(String radioButtonXpath, boolean mustBeSelected)
    {
        try
        {
            WebElement radioButton = Driver.findElement(By.xpath(radioButtonXpath));

            if (mustBeSelected == true)
            {
                radioButton.click();
            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public boolean selectRadioButtonUsingElementIdAndBoolean(String radioButtonId, boolean mustBeSelected)
    {
        try
        {
            WebElement radioButton = Driver.findElement(By.id(radioButtonId));

            if (mustBeSelected == true)
            {
                radioButton.click();
            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public Boolean SelectFromEmbeddedTableElementByXpathUsingContainerXpath(String containerId, String ParentTag, String ChildTag, String validationText, String ListContainerId, String ItemTag, String ItemText)
    {
        String ChildElementText = "";
        String ListItemText = "";
        try
        {
            SeleniumDriverInstance.waitForElementById(containerId);
            WebElement Div = Driver.findElement(By.id(containerId));
            List<WebElement> ParentElements = Div.findElements(By.tagName(ParentTag));

            for (WebElement ParentElement : ParentElements)
            {
                List<WebElement> ChildElements = ParentElement.findElements(By.tagName(ChildTag));

                for (WebElement ChildElement : ChildElements)
                {
                    ChildElementText = ChildElement.findElement(By.tagName(ChildTag)).getText();

                    if (ChildElementText == null)
                    {
                        continue;
                    }

                    Narrator.logDebug("Detected child element text :" + ChildElementText);

                    if (ChildElementText.equals(validationText))
                    {
                        Narrator.logDebug("About to click: " + ChildElementText);
                        ParentElement.click();
                        Narrator.logDebug("Clicked: " + ChildElementText);
                        WebElement ListContainer = Driver.findElement(By.id(ListContainerId));
                        Narrator.logDebug("Found Container Element: " + ListContainer.getText());
                        List<WebElement> ListItems = ListContainer.findElements(By.tagName(ItemTag));
                        Narrator.logDebug("Listing List Items");
                        SeleniumDriverInstance.pause(500);
                        for (WebElement ListItem : ListItems)
                        {
                            ListItemText = ListItem.getText();
                            Narrator.logDebug("Found ListElement: " + ListItemText);

                            if (ListItemText.equals(ItemText))
                            {
                                ListItem.click();
                                Narrator.logDebug("Clicking Item :" + ListItem.getText());
                                return true;
                            }
                        }
                    }
                }
            }
            Narrator.logError(" failed to find element : " + ItemText);
            return false;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public void pressKey(Keys keyToPress)
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys(keyToPress);
            action.perform();
        }
        catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            Narrator.logError(" Failed to send keypress to element - " + keyToPress);

        }
    }

    public void releaseKey(Keys keyToRelease)
    {
        try
        {
            Actions action = new Actions(Driver);
            action.keyUp(keyToRelease);
            action.perform();
        }
        catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            Narrator.logError(" Failed to send keypress to element - " + keyToRelease);

        }
    }

    public void copyKeys()
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys(Keys.CONTROL, "c");
            action.perform();
        }
        catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            Narrator.logError(" Failed to send keypress to element - Contoll + C");

        }
    }

    public void pressKey(Keys keyToPress, int NumberOfPresses)
    {
        try
        {
            for (int i = 0; i <= NumberOfPresses; i++)
            {
                Actions action = new Actions(Driver);
                action.sendKeys(keyToPress);

                action.perform();
            }
        }
        catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            Narrator.logError(" Failed to send keypress to element - " + keyToPress);

        }
    }

    public Boolean ClickElementOnDropdownListByXpathAndText(String FormXpath, String paragraphXpath, String dropdown, String valueToSelect)
    {
        String firstCellText = "";
        try
        {
            SeleniumDriverInstance.waitForElementByXpath(FormXpath);

            WebElement formxpath = Driver.findElement(By.xpath(FormXpath));
            Narrator.logDebug("Found div");
            WebElement Paragraph = formxpath.findElement(By.xpath(paragraphXpath));
            Narrator.logDebug("Found table");
            WebElement dropdownItem = formxpath.findElement(By.xpath(dropdown));
            dropdownItem.click();
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public Boolean SelectElementOnDropdownListByXpathAndText(String FormXpath, String paragraphXpath, String dropdown, String Div, String ULtagName, String liTagName, String valueToSelect)
    {
        String firstCellText = "";
        try
        {
            SeleniumDriverInstance.waitForElementByXpath(FormXpath);

            WebElement formxpath = Driver.findElement(By.xpath(FormXpath));
            Narrator.logDebug("Found div");
            WebElement Paragraph = formxpath.findElement(By.xpath(paragraphXpath));
            Narrator.logDebug("Found Paragraph");
            WebElement dropdownItem = formxpath.findElement(By.xpath(dropdown));
            dropdownItem.click();

            //selecting on the dropdown list
            WebElement DropdownDiv = Driver.findElement(By.xpath(Div));
            Narrator.logDebug("Found div");
            WebElement ListItemsUL = DropdownDiv.findElement(By.tagName(ULtagName));
            List<WebElement> ListItemsLI = ListItemsUL.findElements(By.tagName(liTagName));
            Narrator.logDebug("Listing List Items");
            SeleniumDriverInstance.pause(500);
            for (WebElement ListItem : ListItemsLI)
            {
                String ListItemText = ListItem.getText().toUpperCase();
                Narrator.logDebug("Found ListElement: " + ListItemText);

                if (ListItemText.equals(valueToSelect))
                {
                    ListItem.click();
                    //Narrator.debugLog("Clicking Item :" + ListItem.getText());
                    return true;
                }

            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public Boolean SelectButtonOnByFormXpath(String FormXpath, String paragraphXpath, String buttonToClick)
    {
        String firstCellText = "";
        try
        {
            SeleniumDriverInstance.waitForElementByXpath(FormXpath);

            WebElement formxpath = Driver.findElement(By.xpath(FormXpath));
            Narrator.logDebug("Found div");
            WebElement ButtonToClick = formxpath.findElement(By.xpath(buttonToClick));
            ButtonToClick.click();

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public boolean selectFromDropDownListUsingXpath(String elementXpath, String valueToSelect)
    {
        try
        {
            waitForElementByXpath(elementXpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
            WebElement formxpath = Driver.findElement(By.xpath(elementXpath));
            formxpath.click();
            dropDownList.selectByVisibleText(valueToSelect);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting from dropdownlist by text using Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public String checkElementTypeById(String elementId)
    {
        WebElement element = Driver.findElement(By.id(elementId));
        try
        {
            WebElement child = null;
            child = element.findElement(By.tagName("option"));
            if (child == null)
            {
                return "textbox";
            }
            else
            {
                return "dropDownList";
            }

        }
        catch (Exception ex)
        {
            Narrator.logError(" selecting from dropdownlist by text using Id - " + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return ex.getMessage();
        }
    }

    public boolean ClickEmbeddedRowUsingCellTextAndButtonText(String Div, String tableXpath, String Tbody, String tableRow, String TableD, String ValueToSelect)
    {
        String ButtonText = "";
        String firstCellText = "";
        try
        {
            SeleniumDriverInstance.waitForElementByXpath(Div);
            //div
            WebElement div = Driver.findElement(By.xpath(Div));
            Narrator.logDebug("Found div");
            //table
            WebElement table = div.findElement(By.xpath(tableXpath));
            Narrator.logDebug("Found table");

            WebElement tBody = div.findElement(By.tagName(Tbody));
            Narrator.logDebug("Found table body");

            List<WebElement> rows = tBody.findElements(By.tagName(tableRow));
            int rowCount = 1;
            for (WebElement row : rows)
            {

////                Narrator.debugLog("Row - " + rowCount);
                List<WebElement> cells = row.findElements(By.tagName(TableD));
                for (WebElement insidecells : cells)
                {

                    String cellvalue = insidecells.getText().toUpperCase();
                    String selectValue = ValueToSelect.substring(0, 4);
                    if (cellvalue.contains(ValueToSelect.toUpperCase()))
                    {
                        Narrator.logDebug("selecting:<===>  " + ValueToSelect);
                        insidecells.click();
                        return true;
                    }
                }
            }

            rowCount++;
            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean enterTextonEmbeddedTextBoxByXpath(String elementXpath, String textToEnter)
    {
        try
        {
            //div 1
            WebElement Div1 = Driver.findElement(By.xpath("//*[@id=\"contacts\"]/div[1]"));

            //div 2
            WebElement div2 = Div1.findElement(By.xpath("//*[@id=\"contacts\"]/div[1]/div"));

            //div 3
            WebElement elementToTypeIn = div2.findElement(By.xpath(elementXpath));
            this.waitForElementByXpath(elementXpath);

//            WebElement elementToTypeIn = div3.findElement(By.xpath(elementXpath));
//            List <WebElement> cells = div3.findElements(By.xpath("//*[@id=\"contacts\"]/div[1]/div/div"));
//            for(WebElement insidecells : cells)
//            {
//                Narrator.debugLog("Clicking button"+ insidecells);
//            }
            if (!elementToTypeIn.getText().isEmpty())
            {
                elementToTypeIn.clear();
            }
            Actions typeText = new Actions(Driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterText(String textToEnter)
    {
        try
        {
//            JavascriptExecutor js = (JavascriptExecutor)Driver;
//
//            this.waitForElementByXpath(elementXpath);
//            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
//
//
//            elementToTypeIn.clear();

//            typeText.moveToElement(elementToTypeIn);
//            js.executeScript("arguments[0].style.visibility='visible';", elementXpath);
//            typeText.click(elementToTypeIn);
            Actions typeText = new Actions(Driver);
            typeText.sendKeys(textToEnter);
            typeText.perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text - " + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public Boolean selectdropdownContainerUsingXpath(String Div1Xpath, String Div2Xpath, String TableXpath, String Div3Xpath, String Div4Xpath, String ListBoxId, String Tag, String ValueToselect, String DropdownID)
    {
        try
        {
//            SeleniumDriverInstance.waitForElementById(containerId);

            WebElement div1 = Driver.findElement(By.xpath(Div1Xpath));
            Narrator.logDebug("Found div");
            WebElement div2 = div1.findElement(By.xpath(Div2Xpath));

            Narrator.logDebug("Found table");
            WebElement Div3 = div2.findElement(By.xpath(TableXpath));
            Div3.click();
            SeleniumDriverInstance.pause(2000);
            WebElement SelectDiv1 = div2.findElement(By.xpath(Div3Xpath));
            WebElement SelectDiv2 = SelectDiv1.findElement(By.xpath(Div4Xpath));

//
            WebElement cells = SelectDiv2.findElement(By.id(ListBoxId));
            List<WebElement> value = cells.findElements(By.tagName(Tag));

            for (WebElement row : value)
            {
                String selectvalue = row.getText();
                if (selectvalue.toUpperCase().equals(ValueToselect.toUpperCase()))
                {
                    row.click();
                }
            }

            Select dropDownList = new Select(Div3.findElement(By.id(DropdownID)));
            dropDownList.selectByVisibleText(ValueToselect);

            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element : " + ex.getMessage());
            return false;
        }
    }

    public boolean ClickEmbeddedRowUsingCellTextAndButtonText(String Div, String table, String tbody, String tr, String Cell, String SelectionDiv, String Ultag, String LItag, String Organization)
    {

        try
        {

            SeleniumDriverInstance.waitForElementByXpath(Div);
            WebElement tableelement = Driver.findElement(By.xpath(Div));
            Narrator.logDebug("Found table");

            WebElement mytable = tableelement.findElement(By.tagName(table));
            Narrator.logDebug("Found table body");

            WebElement tableBody = mytable.findElement(By.tagName(tbody));
            WebElement tableRow = tableBody.findElement(By.tagName(tr));
            WebElement tableCell = tableRow.findElement(By.xpath(Cell));
            tableCell.click();
            SeleniumDriverInstance.pause(2000);

            WebElement ListDiv2 = Driver.findElement(By.xpath(SelectionDiv));
            WebElement tableBodyULTagName = ListDiv2.findElement(By.tagName(Ultag));
            List<WebElement> rows = tableBodyULTagName.findElements(By.tagName(LItag));
            for (WebElement row : rows)
            {

                String selectvalue = row.getText();
                if (selectvalue.toUpperCase().equals(Organization.toUpperCase()))
                {
                    row.click();
                    break;
                }
            }

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean DoubleClickEmbeddedElement(String Div, String table, String tbody, String tr, String Cell)
    {

        try
        {

            SeleniumDriverInstance.waitForElementByXpath(Div);
            WebElement tableelement = Driver.findElement(By.xpath(Div));
            Narrator.logDebug("Found table");

            WebElement mytable = tableelement.findElement(By.tagName(table));
            Narrator.logDebug("Found table body");

            WebElement tableBody = mytable.findElement(By.tagName(tbody));
            WebElement tableRow = tableBody.findElement(By.tagName(tr));
            WebElement tableCell = tableRow.findElement(By.xpath(Cell));
            tableCell.click();
            tableCell.click();

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean ClickElementAndSelectFromADropdownList(String ElementToSelectXpath, String SelectionDiv, String Ultag, String LItag, String ValueToSelect)
    {

        try
        {

            SeleniumDriverInstance.waitForElementByXpath(ElementToSelectXpath);
            WebElement tableelement = Driver.findElement(By.xpath(ElementToSelectXpath));
            tableelement.click();

            WebElement ListDiv2 = tableelement.findElement(By.xpath(SelectionDiv));
            WebElement tableBodyULTagName = ListDiv2.findElement(By.tagName(Ultag));
            List<WebElement> rows = tableBodyULTagName.findElements(By.tagName(LItag));
            for (WebElement row : rows)
            {

                String selectvalue = row.getText();
                if (selectvalue.toUpperCase().equals(ValueToSelect))
                {
                    row.click();
                    break;
                }
            }

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean ClickEmbeddedRowUsingCellButtonXpath(String parent, String child1, String child2, String Child3, String cellXpath)
    {
        try
        {
            WebElement Div = Driver.findElement(By.xpath(parent));
            WebElement tableName = Div.findElement(By.tagName(child1));
            WebElement LowerGradetableBody = tableName.findElement(By.tagName(child2));
            WebElement tablerow = LowerGradetableBody.findElement(By.tagName(Child3));

            WebElement tableElement = tablerow.findElement(By.xpath(cellXpath));
            tableElement.click();

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to click cell by xpath: " + ex.getMessage());
            return false;
        }
    }

    public boolean enterTextByCss(String elementId, String textToEnter)
    {

        try
        {
            Narrator.logDebug("Attempting to enable element - " + elementId);
            JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
            js.executeScript("document.getElementById('" + elementId + "').removeAttribute('disabled')");
            Narrator.logDebug("Element enabled successfully - " + elementId);
            WebElement select = Driver.findElement(By.id(elementId));
            select.sendKeys(textToEnter);

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to enable specifed element - " + e.getMessage());
            return false;
        }

    }

    public boolean uploadFileUsingElementIdAndFileLocations(String browseButtonId, String fileLocation)
    {
        try
        {
            WebElement select = Driver.findElement(By.name(browseButtonId));
            pause(200);

            select.sendKeys(fileLocation);
            pause(200);

        }
        catch (Exception ex)
        {

            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public boolean nestedHoverOverEmbededElementAndClickByXpath(String ParentElementXpath, String SubElementXpath, String ChildElementXpath)
    {
        try
        {

            WebElement ListDiv2 = Driver.findElement(By.xpath(ParentElementXpath));
            WebElement tableBodyULTagName = ListDiv2.findElement(By.tagName(SubElementXpath));

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }

    }

    public boolean selectUsingCellTextAndButtonText(String DivXpath, String ParenttagName, String ChildTagName, String ValueToSelect)
    {
        String ButtonText = "";
        String firstCellText = "";

        try
        {
            WebElement ListDiv2 = Driver.findElement(By.xpath(DivXpath));
            WebElement tableBodyULTagName = ListDiv2.findElement(By.tagName(ParenttagName));
            List<WebElement> rows = tableBodyULTagName.findElements(By.tagName(ChildTagName));
            for (WebElement row : rows)
            {

                String selectvalue = row.getText();
                if (selectvalue.toUpperCase().contains(ValueToSelect.toUpperCase()))
                {
                    row.click();
                    break;
                }

            }

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean selectTextUsingASubtringedValue(String DivXpath, String ParenttagName, String ChildTagName, String selectValue)
    {
        String ButtonText = "";
        String firstCellText = "";

        try
        {
            WebElement ListDiv2 = Driver.findElement(By.xpath(DivXpath));
            WebElement tableBodyULTagName = ListDiv2.findElement(By.tagName(ParenttagName));
            List<WebElement> rows = tableBodyULTagName.findElements(By.tagName(ChildTagName));
            for (WebElement row : rows)
            {

                String cellvalue = row.getText().toUpperCase();
                String selectionValue = selectValue.substring(0, 5);
                if (cellvalue.toUpperCase().contains(selectValue.toUpperCase()))
                {
                    Narrator.logDebug("selecting:<===>  " + selectValue);
                    row.click();
                    break;

                }

            }

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    //Container Class Name = jspPane
    public List<String> getElementsValueUsingParentChildTagName(String containerClassName)
    {
        List<String> values = new ArrayList<>();
        try
        {
            WebElement container = Driver.findElement(By.xpath(containerClassName));
            List<WebElement> divs = container.findElements(By.tagName("div"));
            for (WebElement div : divs)
            {
                try
                {
                    values.add(div.getAttribute("dest"));
                }
                catch (Exception ex)
                {

                }
            }

        }
        catch (Exception ex)
        {
            Narrator.logError("Could not find elements" + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return values;
        }
        return values;
    }

    public boolean selectUsingCellTextAndButtonText(String DivXpath, String ParenttagName, String ChildTagName, String TagName, String ValueToSelect)
    {
        String ButtonText = "";
        String firstCellText = "";

        try
        {
            WebElement ListDiv2 = Driver.findElement(By.xpath(DivXpath));
            WebElement tableBodynavTagName = ListDiv2.findElement(By.tagName(ParenttagName));
            WebElement tableBodyULTagName = tableBodynavTagName.findElement(By.tagName(ChildTagName));
            List<WebElement> rows = tableBodyULTagName.findElements(By.tagName(TagName));
            for (WebElement row : rows)
            {

                String selectvalue = row.getText();
                if (selectvalue.toUpperCase().equals(ValueToSelect.toUpperCase()))
                {
                    row.click();
                    break;
                }

            }

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean selectUsingCellTextAndButtonbyxpath(String DivXpath, String ParenttagName, String ChildTagName, String TagName, String InnerTagName, String ButtonClass, String buttonClass, String ValueToSelect, String Button)
    {
        String ButtonText = "";
        String firstCellText = "";

        try
        {
            WebElement ListDiv2 = Driver.findElement(By.xpath(DivXpath));
            WebElement tableTagName = ListDiv2.findElement(By.tagName(ParenttagName));
            WebElement tableBodyTagName = tableTagName.findElement(By.tagName(ChildTagName));
            WebElement tableRowTagName = tableBodyTagName.findElement(By.tagName(TagName));
//            WebElement tabledataTagName = tableRowTagName.findElement(By.xpath(InnerTagName));//("//*[@id=\"job-grid\"]/div[4]/table/tbody/tr[1]/td[12]"));
            List<WebElement> rows = tableRowTagName.findElements(By.tagName(ButtonClass));

            for (WebElement row : rows)
            {

                String tabledata = row.getAttribute(buttonClass);
                if (tabledata.toUpperCase().equals(ValueToSelect.toUpperCase()))
                {
                    Actions moveTo = new Actions(Driver);
                    moveTo.moveToElement(Driver.findElement(By.xpath(Button)));
                    pause(2000);
                    moveTo.click();
                    moveTo.perform();
//                     WebElement Button = row.findElement(By.xpath(ValueToSelect));
//
//                     Button.click();
                    break;
                }
            }

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public boolean selectUsingTextAndButtonbyxpath(String DivXpath, String ParenttagName, String ChildTagName, String TagName, String InnerTagName, String ButtonClass, String ValidationText, String ValueToSelect, String Button)
    {
        String ButtonText = "";
        String firstCellText = "";

        try
        {
            WebElement ListDiv2 = Driver.findElement(By.xpath(DivXpath));
            WebElement tableTagName = ListDiv2.findElement(By.tagName(ParenttagName));
            WebElement tableBodyTagName = tableTagName.findElement(By.tagName(ChildTagName));
//            WebElement tableRowTagName = tableBodyTagName.findElement(By.tagName(TagName));
            List<WebElement> rows = tableBodyTagName.findElements(By.tagName(TagName));

            for (WebElement row : rows)
            {
                boolean found = false;
                List<WebElement> InnerRows = row.findElements(By.tagName(ButtonClass));
                for (WebElement Myrow : InnerRows)
                {
                    String tabledata = Myrow.getText();

                    if (tabledata.toUpperCase().contains("RemTestGradeSystem".toUpperCase()))
                    {
                        found = true;
                        Actions moveTo = new Actions(Driver);
                        moveTo.moveToElement(row.findElement(By.linkText("Delete")));
                        moveTo.click();
                        moveTo.perform();
                        break;
                    }
                }
                if (found)
                {
                    break;
                }
            }

            return true;

        }
        catch (Exception ex)
        {
            Narrator.logError(" failed to find element: " + ex.getMessage());
            return false;
        }
    }

    public String setDateFormat(String inputFormat, String inputDate, String outputFormat)
    {
        try
        {
            //Converts string to date
            String expectedPattern = inputFormat;
            SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
            String userInput = inputDate;
            Date date = formatter.parse(userInput);

            //Sets the date format
            SimpleDateFormat dateFormat = new SimpleDateFormat(outputFormat);

            return dateFormat.format(date).toString();
        }
        catch (Exception ex)
        {
            Narrator.logError(" Failed to convert date '" + inputDate + "': " + ex.getMessage());
            return inputDate;
//            return false;
        }
    }

    public String findTableRowXpathByXpath(String tableXpath, String searchText)
    {
        try
        {
            int rowCount = Driver.findElements(By.xpath(tableXpath + "tr")).size();

            for (int i = 2; i < rowCount; i++)
            {
                String currentRowXpath = tableXpath + "tr[" + i + "]/";
                String currentRowText = retrieveTextByXpath(currentRowXpath + "td[1]/a/span");

                if (currentRowText.contains(searchText))
                {
                    Narrator.logDebug(" Successfully found the search text : '" + searchText + "' at Xpath : '" + currentRowXpath + "'");
                    return currentRowXpath;
                }
            }

            Narrator.logError(" Failed to find the search text : '" + searchText + "'");
            return "";
        }
        catch (Exception ex)
        {
            Narrator.logError(" Failed to find the search text : '" + searchText + "' - " + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return "";
        }
    }

    public String SplitAndReturnStringPart(String splitText, String splitBy, int returnPartPosition)
    {
        try
        {
            String[] splitParts = splitText.split(splitBy);
            return splitParts[returnPartPosition].trim();
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to split the text : '" + splitText + "' by '" + splitBy + "' - " + e.getMessage());
            return "";
        }
    }

    public Map<String, String> retrieveValues(String superParent, String parentElementCss, String childElementCss, String innerChildLabelCss, String innerChildValueCss, int position)
    {
        Map<String, String> values = new HashMap<>();
        try
        {
            int count = 1;
            WebElement span = Driver.findElement(By.id(superParent));
            WebElement parentElement = span.findElement(By.id(parentElementCss));
            List<WebElement> children = parentElement.findElements(By.className(childElementCss));
            if (children.isEmpty())
            {
                Narrator.logDebug("No sub-elements found - null value was returned");
            }
            else
            {
                Narrator.logDebug("Number of elements found: " + children.size());
                for (WebElement child : children)
                {
                    if (checkIfElementHasChildren(child))
                    {
                        try
                        {
                            List<WebElement> innerChildrenLabels = child.findElements(By.className(innerChildLabelCss));
                            List<WebElement> innerChildrenValues = child.findElements(By.className(innerChildValueCss));
                            values.put(innerChildrenLabels.get(position).getText(), innerChildrenValues.get(position).getText());
                            values.put(innerChildrenLabels.get(position + 1).getText(), innerChildrenValues.get(position + 1).getText());

                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Narrator.logError(" Failed to find element by Id: " + parentElementCss + "' - " + ex.getMessage());
            return null;
        }
        return values;
    }

    public boolean checkIfElementHasChildren(WebElement element)
    {
        List<WebElement> subElements = element.findElements(By.xpath(".//*"));
        return (subElements.size() > 1);
    }

    public boolean checkIfGroupIsExpanded(String elementName)
    {
        try
        {
            WebElement element = Driver.findElement(By.name(elementName));
            if (element.getAttribute("src").contains("/th/test/img/mnu/tv/minus.gif"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        catch (Exception ex)
        {
            Narrator.logError(" Failed to find element by name: " + elementName + "' - " + ex.getMessage());
            return false;
        }
    }

    public String checkIfTableIsPresent(String tableXpath, String tableHeaderClassName, String tableHeader)
    {
        String returnString = "Table Not Found";
        try
        {
            WebElement table = Driver.findElement(By.id(tableXpath));
            if (table == null)
            {
                Narrator.logDebug("Table not found with xpath: " + tableXpath);
            }
            else
            {
                try
                {
                    List<WebElement> subElements = table.findElements(By.className(tableHeaderClassName));
                    if (subElements == null)
                    {
                        Narrator.logDebug("Table Data not found with class name of: " + tableHeaderClassName);
                    }
                    else
                    {
                        for (WebElement subElement : subElements)
                        {
                            if (subElement.getText().equals(tableHeader))
                            {
                                returnString = "Table Found";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Narrator.logError(" Failed to find element with the header of: " + tableHeader + " by xpath: " + tableXpath + "' - " + ex.getMessage());
                    return returnString;
                }
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to find element by xpath: " + tableXpath + "' - " + e.getMessage());
            return returnString;
        }
        return returnString;
    }

    public boolean clickElementUsingJavascript(String jsAction)
    {
        JavascriptExecutor js = (JavascriptExecutor) Driver;
        js.executeScript("onclick=\"requestMain(event);\"");
        return true;
    }

    public boolean getCheckboxState(String elementName)
    {
        boolean defaultReturn = false;
        try
        {
            WebElement checkbox = Driver.findElement(By.name(elementName));
            defaultReturn = checkbox.isSelected();
        }
        catch (Exception ex)
        {
            Narrator.logError(" Failed to find element by name: " + elementName + "' - " + ex.getMessage());
            return defaultReturn;
        }
        return defaultReturn;
    }

    public boolean getCheckboxStateByXpath(String elementXpath)
    {
        boolean defaultReturn = false;
        try
        {
            WebElement checkbox = Driver.findElement(By.xpath(elementXpath));
            defaultReturn = checkbox.isSelected();
        }
        catch (Exception ex)
        {
            Narrator.logError(" Failed to find element by xpath: " + elementXpath + "' - " + ex.getMessage());
            return defaultReturn;
        }
        return defaultReturn;
    }

    public boolean hoverOverElementUsingJavascriptAndClickElementByXpath(String elementXpath)
    {
        try
        {
            JavascriptExecutor js = (JavascriptExecutor) Driver;
            js.executeScript("onmouseover=menus['0'].exec('0',2)");
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            js = (JavascriptExecutor) Driver;
            js.executeScript("arguments[0].click();", elementToClick);
            Narrator.logDebug("Successfully found and clicked element '" + elementXpath + "' using javascript...proceeding");
            return true;
        }
        catch (Exception ex)
        {
            Narrator.logError(" Failed to find element by xpath: [" + elementXpath + "] and clicking element using Javascript --> " + ex.getMessage());
            return false;
        }
    }

    public List<String> getAllOptionsFromDropDownListUsingName(String elementName)
    {
        List<String> values = new ArrayList<>();
        try
        {
            waitForElementByName(elementName);
            Select element = new Select(Driver.findElement(By.tagName(elementName)));
            List<WebElement> options = element.getOptions();
            for (WebElement option : options)
            {
                values.add(option.getText());
            }
            return values;
        }
        catch (Exception ex)
        {
            Narrator.logError(" Failed to find element by name: [" + elementName + "] and retreiving text from all the options --> " + ex.getMessage());
            return values;
        }
    }

    public boolean selectOrEnterTextInElementUsinName(String elementName, String value)
    {
        try
        {
            WebElement element = Driver.findElement(By.name(elementName));
            if (element.getTagName().equalsIgnoreCase("input"))
            {
                this.enterTextByName(elementName, value);
                return true;
            }
            else if (element.getTagName().equalsIgnoreCase("select"))
            {
                this.selectByTextFromDropDownListUsingName(elementName, value);
                return true;
            }
        }
        catch (Exception ex)
        {
            Narrator.logError("Could not find elements" + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
        return false;
    }

    public boolean switchToTabOrWindow()
    {
        try
        {
            String winHandleBefore = SeleniumDriverInstance.Driver.getWindowHandle();
            for (String winHandle : SeleniumDriverInstance.Driver.getWindowHandles())
            {
                SeleniumDriverInstance.Driver.switchTo().window(winHandle);
            }
        }
        catch (Exception ex)
        {
            Narrator.logError("Could not switch to new tab" + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
        return true;
    }

    public boolean swithToWindow(WebDriver driver, String title)
    {
        mainWindowsHandle = driver.getWindowHandle();
        Set<String> handles = driver.getWindowHandles(); // Gets all the available windows
        for (String handle : handles)
        {
            driver.switchTo().window(handle); // switching back to each window in loop
            if (driver.getTitle().equals(title)) // Compare title and if title matches stop loop and return true
            {
                return true; // We switched to window, so stop the loop and come out of funcation with positive response
            }
        }
        driver.switchTo().window(mainWindowsHandle); // Switch back to original window handle
        return false; // Return false as failed to find window with given title.
    }

    public boolean isElementVisibleByXpath(String Xpath, int len)
    {
        try
        {
            WebDriverWait wait = new WebDriverWait(Driver, len);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath)));

        }
        catch (TimeoutException e)
        {
            Narrator.logError("Timed out after 10 seconds waiting for visibility of element located by By.xpath: " + Xpath);
            return false;
        }

        return true;
    }

    public Boolean hoverOverElementByXpath(String elementXpath)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);

            WebElement elementToRead = Driver.findElement(By.xpath(elementXpath));
            Point coordinates = elementToRead.getLocation();
            Robot robot = new Robot();
            robot.mouseMove(coordinates.getX(), coordinates.getY() + 65); //Number 65 should vary
            Thread.sleep(3000);

            Narrator.logInfo("[Info]Text retrieved successfully from element - " + elementXpath);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("[Error] Failed to retrieve text from element Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean scrollToElement(String elementXpath)
    {
        try
        {
            WebElement element = Driver.findElement(By.xpath(elementXpath));
            ((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", element);

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error scrolling to element - " + elementXpath + " - " + e.getStackTrace());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

    }

    public String ConfigConsoleScrollToElement(String elementToXpath, String ArrowXpath, String ScrollButtonXpath)
    {
        int stopCounter = 0;

        boolean hostIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibility(elementToXpath, 1);

        while (!hostIsVisible && stopCounter != 50)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ScrollButtonXpath))
            {
                return "Could not wait for the ConfigConsoleHostsDownScroll button";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ScrollButtonXpath))
            {
                return "Could not wait for the ConfigConsoleHostsDownScroll button to be visible";
            }
            if (!SeleniumDriverInstance.clickElementByXpathActions(ScrollButtonXpath))
            {
                return "Could not click the ConfigConsoleHostsDownScroll button";
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(ArrowXpath))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
            {
                return "Could not click the ConfigConsoleHostsDownScrollArrow";
            }

            //jqxScrollThumbverticalScrollBarobjectContainer_grid
            if (!SeleniumDriverInstance.waitForElementByXpath(ScrollButtonXpath))
            {
                return "Could not wait for the ConfigConsoleHostsDownScroll button";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ScrollButtonXpath))
            {
                return "Could not wait for the ConfigConsoleHostsDownScroll button to be visible";
            }
            if (!SeleniumDriverInstance.clickElementByXpathActions(ScrollButtonXpath))
            {
                return "Could not click the ConfigConsoleHostsDownScroll button";
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(ArrowXpath))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
            {
                return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
            {
                return "Could not click the ConfigConsoleHostsDownScrollArrow";
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(ScrollButtonXpath))
            {
                return "Could not wait for the ConfigConsoleHostsDownScroll button";
            }
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ScrollButtonXpath))
            {
                return "Could not wait for the ConfigConsoleHostsDownScroll button to be visible";
            }
            if (!SeleniumDriverInstance.clickElementByXpathActions(ScrollButtonXpath))
            {
                return "Could not click the ConfigConsoleHostsDownScroll button";
            }
            stopCounter++;
            hostIsVisible = SeleniumDriverInstance.waitForElementByXpathVisibility(elementToXpath, 1);

            //to ensure properly visible
            if (hostIsVisible)
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(ScrollButtonXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScroll button";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ScrollButtonXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScroll button to be visible";
                }
                if (!SeleniumDriverInstance.clickElementByXpathActions(ScrollButtonXpath))
                {
                    return "Could not click the ConfigConsoleHostsDownScroll button";
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(ArrowXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
                }

                if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(ScrollButtonXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScroll button";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ScrollButtonXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScroll button to be visible";
                }
                if (!SeleniumDriverInstance.clickElementByXpathActions(ScrollButtonXpath))
                {
                    return "Could not click the ConfigConsoleHostsDownScroll button";
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(ArrowXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(ScrollButtonXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScroll button button";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ScrollButtonXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScroll button to be visible";
                }
                if (!SeleniumDriverInstance.clickElementByXpathActions(ScrollButtonXpath))
                {
                    return "Could not click the ConfigConsoleHostsDownScroll button";
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(ArrowXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ArrowXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScrollArrow to be visible";
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(ArrowXpath))
                {
                    return "Could not click the ConfigConsoleHostsDownScrollArrow";
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(ScrollButtonXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScroll button";
                }
                if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(ScrollButtonXpath))
                {
                    return "Could not wait for the ConfigConsoleHostsDownScroll button to be visible";
                }
                if (!SeleniumDriverInstance.clickElementByXpathActions(ScrollButtonXpath))
                {
                    return "Could not click the ConfigConsoleHostsDownScroll button";
                }
            }
        }
        return "";
    }

    /**
     * Combines data collected from old CSV file with new data that need to be
     * added to the old csv file to create a new CSV file
     *
     * @param oldCSV Requires old csv data
     * @param newDataToAdd Requires the new data to be added to the list
     * @return Returns the combined 2D array for the new csv
     */
    public String[][] editCSVByAddingRows(String[][] oldCSV, String[][] newDataToAdd)
    {
        if (oldCSV == null || newDataToAdd == null)
        {
            return null;
        }
        String[][] newCsvData = new String[oldCSV.length + newDataToAdd.length][];
        int counter = 0;

        for (int i = 0; i < newCsvData.length; i++)
        {
            if (i >= oldCSV.length)
            {
                if (newCsvData[i] == null)
                {
                    newCsvData[i] = newDataToAdd[counter].clone();
                    counter++;
                }
            }
            else if (i < oldCSV.length)
            {
                newCsvData[i] = oldCSV[i].clone();
            }
            else
            {
                return null;
            }
        }

        return newCsvData;
    }

    public String WaitForFileDownloadCompletion(String fileExtension, int timeOutSet)
    {
        String parentHandle = SeleniumDriverInstance.Driver.getWindowHandle(); // get the current window handle
        String downloadedCSVName = "";
        int timeOut = 0;

        while ((downloadedCSVName == "" || !downloadedCSVName.endsWith(fileExtension)) && (timeOut != timeOutSet))
        {
            try
            {
                downloadedCSVName = SeleniumDriverInstance.GetTheFileName("", fileExtension);
                this.pause(1000);
                timeOut++;
            }
            catch (Exception e)
            {
                downloadedCSVName = "";
                this.pause(1000);
                timeOut++;
            }
        }

        //close popup if chrome
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.Chrome)
        {
            //switch window
            while (parentHandle.equalsIgnoreCase(SeleniumDriverInstance.Driver.getWindowHandle()))
            {
                for (String winHandle : SeleniumDriverInstance.Driver.getWindowHandles())
                {
                    SeleniumDriverInstance.Driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
                }
            }

            SeleniumDriverInstance.Driver.close();//close pop up window

            SeleniumDriverInstance.Driver.switchTo().window(parentHandle); // switch back to the original window
        }

        return downloadedCSVName;
    }

    public String GetTheFileName(String SourceFileDirectory, String fileContainsName)
    {
        //get the file name from the c drive
        String dirName = new File(System.getProperty("user.home") + "/Downloads/").toString();
        //"C:/Users/" +
        // String dirName = new File(SourceFileDirectory).toString();
        // File dir = new File(SourceFileDirectory);
        File dir = new File(dirName);
        String FileName = "";
        try
        {
            String fileName = "";
            if (dir.isDirectory())
            {
                File[] children = dir.listFiles();
                if (null != children)
                {
                    for (int i = 0; i < children.length; i++)
                    {
                        fileName = children[i].getName();
                        if (fileName.contains(fileContainsName))
                        {
                            FileName = fileName;
                            break;
                            //FileUtils.copyFile(new File(fileName) , new File(destinationFile));
                        }
                    }
                }
            }

            return FileName;
        }
        catch (Exception ex)
        {
            System.err.println("[Error] Fail to get the file name - " + ex.getMessage());
            return null;
        }
    }

    public String Get_TheFileName(String SourceFileDirectory, String fileContainsName)
    {
        //get the file name from the c drive
        String dirName = new File(SourceFileDirectory).toString();
        File dir = new File(dirName);
        String FileName = "";
        try
        {
            String fileName = "";
            if (dir.isDirectory())
            {
                File[] children = dir.listFiles();
                if (null != children)
                {
                    for (int i = 0; i < children.length; i++)
                    {
                        fileName = children[i].getName();
                        if (fileName.contains(fileContainsName))
                        {
                            FileName = fileName;
                            break;
                            //FileUtils.copyFile(new File(fileName) , new File(destinationFile));
                        }
                    }
                }
            }

            return FileName;
        }
        catch (Exception ex)
        {
            System.err.println("[Error] Fail to get the file name - " + ex.getMessage());
            return null;
        }
    }

    // Method to enforce expension of a menu
    public boolean expendContainer(String link)
    {

        WebElement deletedHostExpand;

        try
        {
            deletedHostExpand = SeleniumDriverInstance.Driver.findElement(By.xpath("//span[text()='" + link + "']/..//span[1][contains(@class, 'jqx-tree')]"));
        }
        catch (Exception e)
        {
            //etc.
            return false;
        }

        String state = deletedHostExpand.getAttribute("class").toString();

        if (state.contains("collapse"))
        {
            deletedHostExpand.click();
        }

        return true;
    }

    public String GetTheFileName(String fileContainsName)
    {
        //get the file name from the c drive
        String dirName = new File(System.getProperty("user.home") + "/Downloads/").toString();
        // String dirName = new File(SourceFileDirectory).toString();
        // File dir = new File(SourceFileDirectory);
        File dir = new File(dirName);
        String FileName = "";
        try
        {
            String fileName = "";
            if (dir.isDirectory())
            {
                File[] children = dir.listFiles();
                if (null != children)
                {
                    for (int i = 0; i < children.length; i++)
                    {
                        fileName = children[i].getName();
                        if (fileName.contains(fileContainsName))
                        {
                            FileName = fileName;
                            break;
                            //FileUtils.copyFile(new File(fileName) , new File(destinationFile));
                        }
                    }
                }
            }

            return FileName;
        }
        catch (Exception ex)
        {
            System.err.println("[Error] Fail to get the file name - " + ex.getMessage());
            return null;
        }
    }

    public String pagingMethodWithImg()
    {

        //Next
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithSrc("resources/images/next.png")))
        {
            return "Failed to wait for the next page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithSrc("resources/images/next.png")))
        {
            return "Failed to wait for the next page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithSrc("resources/images/next.png")))
        {
            return "Failed to click the next page button";
        }

        //Previous
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithSrc("resources/images/previous.png")))
        {
            return "Failed to wait for the previous page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithSrc("resources/images/previous.png")))
        {
            return "Failed to wait for the previous page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithSrc("resources/images/previous.png")))
        {
            return "Failed to click the previous page button";
        }

        //Last Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithSrc("resources/images/last.png")))
        {
            return "Failed to wait for the last page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithSrc("resources/images/last.png")))
        {
            return "Failed to wait for the last page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithSrc("resources/images/last.png")))
        {
            return "Failed to click the last page button";
        }

        //First Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithSrc("resources/images/first.png")))
        {
            return "Failed to wait for the first page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithSrc("resources/images/first.png")))
        {
            return "Failed to wait for the first page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithSrc("resources/images/first.png")))
        {
            return "Failed to click the first page button";
        }

        return "";
    }

    public String pagingMethodWithImgWait()
    {

        //Next
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithSrc("resources/images/next.png")))
        {
            return "Failed to wait for the next page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithSrc("resources/images/next.png")))
        {
            return "Failed to wait for the next page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithSrc("resources/images/next.png")))
        {
            return "Failed to click the next page button";
        }

        String prevWait = WaitForLoaderIconNotVisible();
        if (!prevWait.equals(""))
        {
            return prevWait;
        }
        //Previous
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithSrc("resources/images/previous.png")))
        {
            return "Failed to wait for the previous page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithSrc("resources/images/previous.png")))
        {
            return "Failed to wait for the previous page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithSrc("resources/images/previous.png")))
        {
            return "Failed to click the previous page button";
        }

        String lastWait = WaitForLoaderIconNotVisible();
        if (!lastWait.equals(""))
        {
            return lastWait;
        }

        //Last Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithSrc("resources/images/last.png")))
        {
            return "Failed to wait for the last page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithSrc("resources/images/last.png")))
        {
            return "Failed to wait for the last page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithSrc("resources/images/last.png")))
        {
            return "Failed to click the last page button";
        }

        String firstWait = WaitForLoaderIconNotVisible();
        if (!firstWait.equals(""))
        {
            return firstWait;
        }

        //First Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithSrc("resources/images/first.png")))
        {
            return "Failed to wait for the first page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithSrc("resources/images/first.png")))
        {
            return "Failed to wait for the first page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithSrc("resources/images/first.png")))
        {
            return "Failed to click the first page button";
        }

        String finalWait = WaitForLoaderIconNotVisible();
        if (!finalWait.equals(""))
        {
            return finalWait;
        }

        return "";
    }

    public boolean UnzipCsvFile(String file, String outputDir)
    {
        try
        {
            ZipFile zipFile = new ZipFile(file);
            //This method unzip the csv file from the download folder and extract it to the current test case test report folder
            try
            {
                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                while (entries.hasMoreElements())
                {
                    ZipEntry entry = entries.nextElement();
                    File entryDestination = new File(outputDir, entry.getName());
                    if (entry.isDirectory())
                    {
                        entryDestination.mkdirs();
                    }
                    else
                    {
                        entryDestination.getParentFile().mkdirs();
                        InputStream in = zipFile.getInputStream(entry);
                        OutputStream out = new FileOutputStream(entryDestination);
                        IOUtils.copy(in, out);
                        IOUtils.closeQuietly(in);
                        out.close();
                    }
                }
            }
            finally
            {
                zipFile.close();
                return true;
            }
        }
        catch (Exception ex)
        {
            System.err.println("[Error] Fail to unzip the file - " + ex.getMessage());
            return false;
        }
    }

    public boolean uploadCSV(String xpathOfUploadButton, String fileLocation, String returnError)
    {
        try
        {
            SeleniumDriverInstance.Driver.findElement(By.xpath(xpathOfUploadButton)).sendKeys(fileLocation);
        }
        catch (Exception e)
        {
            returnError = "Could not upload the CSV file:" + fileLocation;
            return false;
        }

        return true;
    }

    public String uploadCSVNew(String xpathOfUploadButton, String fileLocation)
    {
        try
        {
            SeleniumDriverInstance.Driver.findElement(By.xpath(xpathOfUploadButton)).sendKeys(fileLocation);
        }
        catch (Exception e)
        {
            return "Could not upload the CSV file: " + fileLocation;
        }

        return "";
    }

    public String waitForThreeDotsMethod(String loadingIconXpath, String elementBehindXpath)
    {
        //waits for three dots
        if (SeleniumDriverInstance.waitForElementByXpathVisibility(loadingIconXpath, 2))
        {
            //waits for element you want to inter act with
            if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementBehindXpath, 180))
            {
                return "Failed to wait for " + elementBehindXpath + " to be clickable - page still loading";
            }
        }
        else if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementBehindXpath, 3))
        {
            return "Failed to wait for " + elementBehindXpath + " to be clickable";
        }
        return "";
    }

    public String filteringMethod(String dropdownItem, String search, String columnIndex, String dropdownXpath, String textBoxXpath, String searchButtonXpath, String columnXpath)
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(dropdownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }
        
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
        {
            SeleniumDriverInstance.pause(1500);
        }

        if (!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(dropdownXpath, dropdownItem))
        {
            return "Failed to select Value '" + dropdownItem + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(textBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
        {
            SeleniumDriverInstance.pause(2000);
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(textBoxXpath, search))
        {
            return "Failed to enter '" + search + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.stableClickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }
        
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(columnXpath))
        {
            return "Failed to wait for the '" + columnIndex + "' column";
        }
        
        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                SeleniumDriverInstance.pause(1000);
                if (!values.get(i).getText().toUpperCase().contains(search.toUpperCase()))
                {
                    return "Failed to validate that filtering '" + dropdownItem + "' with data '" + search + "' brought back the required results - Expected: " + search + " - Actual: " + values.get(i).getText();
                }
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values";
        }

        return "";
    }

    public String filteringMethodWithWait(String dropdownItem, String search, String columnIndex, String dropdownXpath, String textBoxXpath, String searchButtonXpath, String columnXpath)
    {
        String loadScreen = "";

        if (!SeleniumDriverInstance.waitForElementByXpath(dropdownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(dropdownXpath, dropdownItem))
        {
            return "Failed to select Value '" + dropdownItem + "' from the Filter dropdown";
        }

        loadScreen = SeleniumDriverInstance.WaitForLoaderIconNotVisible();
        if (!loadScreen.equals(""))
        {

            return loadScreen;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(textBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(textBoxXpath, search))
        {
            return "Failed to enter '" + search + "' into the filter search text box";
        }

        loadScreen = SeleniumDriverInstance.WaitForLoaderIconNotVisible();
        if (!loadScreen.equals(""))
        {

            return loadScreen;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        loadScreen = SeleniumDriverInstance.WaitForLoaderIconNotVisible();
        if (!loadScreen.equals(""))
        {

            return loadScreen;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(columnXpath))
        {
            return "Failed to wait for the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                if (!values.get(i).getText().contains(search))
                {
                    return "Failed to validate that filtering '" + dropdownItem + "' with data '" + search + "' brought back the required results - Expected: " + search + " - Actual: " + values.get(i).getText();
                }
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values";
        }

        return "";
    }

    public String changeUserRequestRefreshTime(String time)
    {
        //change user request settings to update
        //open the user request submenu
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgContainsSrc(SintelligentPageObject.UserRequestSubmenuSettingsButtonSrc())))
        {
            return "Could not wait for visibility to click the user request submenu settings button";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgContainsSrc(SintelligentPageObject.UserRequestSubmenuSettingsButtonSrc())))
        {
            return "Could not wait for clickability to click the user request submenu settings button";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgContainsSrc(SintelligentPageObject.UserRequestSubmenuSettingsButtonSrc())))
        {
            return "Could not click the user request submenu settings button";
        }

        //change refresh time
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown")))
        {
            return "Could not wait for visibility change the 'Refresh time' drop down on the User Request settings menu";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown")))
        {
            return "Could not wait for clickability to change the 'Refresh time' drop down on the User Request settings menu";
        }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboRefreshDropdown"), time))
        {
            return "Could not could not change the 'Refresh time' drop down on the User Request settings menu to '" + time + "'";
        }

        //for visual purpose
        SeleniumDriverInstance.pause(500);

        //close menu
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgContainsSrc(SintelligentPageObject.UserRequestSubmenuSettingsCloseButtonSrc())))
        {
            return "Could not wait for visibility to click the submenu close button";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgContainsSrc(SintelligentPageObject.UserRequestSubmenuSettingsCloseButtonSrc())))
        {
            return "Could not wait for clickability to click the submenu close button";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgContainsSrc(SintelligentPageObject.UserRequestSubmenuSettingsCloseButtonSrc())))
        {
            return "Could not click the submenu close button";
        }

        return "";
    }

    public String filteringMethodFrameSwitching(String dropdownItem, String search, String columnIndex, String dropdownXpath, String textBoxXpath, String searchButtonXpath, String columnXpath, boolean frameSwitching,
            String frameOne, String innerFrameOne, String frameTwo, String innerFrameTwo)
    {
        if (frameSwitching)
        {
            if (frameSwitching)
            {
                if (frameOne.length() > 0)
                {
                    if (!this.switchToInnerSintelligentFrame(frameOne))
                    {
                        return "Failed to switch to the sintelligent frame one";
                    }
                }

                if (innerFrameOne.length() > 0)
                {
                    if (!this.switchToFrameById(innerFrameOne))
                    {
                        return "Failed to switch to the inner sintelligent frame one";
                    }
                }
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(dropdownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(dropdownXpath, dropdownItem))
        {
            return "Failed to select Value '" + dropdownItem + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(textBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(textBoxXpath, search))
        {
            return "Failed to enter '" + search + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        if (frameSwitching)
        {
            if (frameTwo.length() > 0)
            {
                if (!this.switchToInnerSintelligentFrame(frameTwo))
                {
                    return "Failed to switch to the sintelligent frame two";
                }
            }

            if (innerFrameTwo.length() > 0)
            {
                if (!this.switchToFrameById(innerFrameTwo))
                {
                    return "Failed to switch to the inner sintelligent frame two";
                }
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(columnXpath))
        {
            return "Failed to wait for the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                if (!values.get(i).getText().contains(search))
                {
                    return "Failed to validate that filtering '" + dropdownItem + "' with data '" + search + "' brought back the required results - Expected: " + search + " - Actual: " + values.get(i).getText();
                }
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values";
        }

        return "";
    }

    public String filteringMethodFrameSwitchingWithWait(String dropdownItem, String search, String columnIndex, String dropdownXpath, String textBoxXpath, String searchButtonXpath, String columnXpath, boolean frameSwitching,
            String frameOne, String innerFrameOne, String frameTwo, String innerFrameTwo)
    {
        if (frameSwitching)
        {
            if (frameSwitching)
            {
                if (frameOne.length() > 0)
                {
                    if (!this.switchToInnerSintelligentFrame(frameOne))
                    {
                        return "Failed to switch to the sintelligent frame one";
                    }
                }

                if (innerFrameOne.length() > 0)
                {
                    if (!this.switchToFrameById(innerFrameOne))
                    {
                        return "Failed to switch to the inner sintelligent frame one";
                    }
                }
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(dropdownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(dropdownXpath, dropdownItem))
        {
            return "Failed to select Value '" + dropdownItem + "' from the Filter dropdown";
        }

        String loadingWait = WaitForLoaderIconNotVisible();
        if (!loadingWait.equals(""))
        {
            return loadingWait;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(textBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(textBoxXpath, search))
        {
            return "Failed to enter '" + search + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        if (frameSwitching)
        {
            if (frameTwo.length() > 0)
            {
                if (!this.switchToInnerSintelligentFrame(frameTwo))
                {
                    return "Failed to switch to the sintelligent frame two";
                }
            }

            if (innerFrameTwo.length() > 0)
            {
                if (!this.switchToFrameById(innerFrameTwo))
                {
                    return "Failed to switch to the inner sintelligent frame two";
                }
            }
        }
        loadingWait = WaitForLoaderIconNotVisible();
        if (!loadingWait.equals(""))
        {
            return loadingWait;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(columnXpath))
        {
            return "Failed to wait for the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                if (!values.get(i).getText().contains(search))
                {
                    return "Failed to validate that filtering '" + dropdownItem + "' with data '" + search + "' brought back the required results - Expected: " + search + " - Actual: " + values.get(i).getText();
                }
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values";
        }

        return "";
    }

    /**
     * Used to filter grids
     *
     * @param dropdownItem requires option to select from filter drop down
     * @param search requires Text to filter with
     * @param columnIndex Index of the column the
     * @param dropdownXpath requires Xpath of the drop down
     * @param textBoxXpath requires TextBox xpath
     * @param searchButtonXpath requires search Button Xpath
     * @param columnXpath requires column Xpath
     * @param frameSwitching requires boolean stating if frame switching is
     * required
     * @param frameNameOne requires Main frame 1
     * @param innerFrameNameOne requires inner frame of main frame 1
     * @param frameNameTwo requires second main frame
     * @param innerFrameNameTwo requires inner frame of second main frame
     * @return an error message in failed case else empty String
     */
    public String filteringMethodFrameSwitchingByFrameNames(String dropdownItem, String search, String columnIndex, String dropdownXpath, String textBoxXpath, String searchButtonXpath, String columnXpath, boolean frameSwitching,
            String frameNameOne, String innerFrameNameOne, String frameNameTwo, String innerFrameNameTwo)
    {
        if (frameSwitching)
        {
            if (frameSwitching)
            {
                if (!this.switchToInnerSintelligentFrame(frameNameOne, innerFrameNameOne))
                {
                    return "Failed to switch to the sintelligent iframe :'" + frameNameOne + "' and frame :'" + innerFrameNameOne + "'";
                }
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(dropdownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(dropdownXpath, dropdownItem))
        {
            return "Failed to select Value '" + dropdownItem + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(textBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(textBoxXpath, search))
        {
            return "Failed to enter '" + search + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        if (frameSwitching)
        {

            if (!this.switchToInnerSintelligentFrame(frameNameTwo, innerFrameNameTwo))
            {
                return "Failed to switch to the sintelligent iframe :'" + frameNameTwo + "' and frame :'" + innerFrameNameTwo + "'";
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(columnXpath))
        {
            return "Failed to wait for the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            if (values.isEmpty())
            {
                return "Failed to extract the '" + dropdownItem + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                if (!values.get(i).getText().toLowerCase().contains(search.toLowerCase()))
                {
                    return "Failed to validate that filtering '" + dropdownItem + "' with data '" + search + "' brought back the required results - Expected: " + search + " - Actual: " + values.get(i).getText();
                }
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values";
        }

        return "";
    }

    
    /**
     * Used to filter grids
     *
     * @param dropdownItem requires option to select from filter drop down
     * @param search requires Text to filter with
     * @param columnIndex Index of the column the
     * @param dropdownXpath requires Xpath of the drop down
     * @param textBoxXpath requires TextBox xpath
     * @param searchButtonXpath requires search Button Xpath
     * @param columnXpath requires column Xpath
     * @param frameSwitching requires boolean stating if frame switching is
     * required
     * @param frameNameOne requires Main frame 1
     * @param innerFrameNameOne requires inner frame of main frame 1
     * @param frameNameTwo requires second main frame
     * @param innerFrameNameTwo requires inner frame of second main frame
     * @return an error message in failed case else empty String
     */
    public String filteringMethodFrameSwitchingByFrameNamesWaitForLoader(String dropdownItem, String search, String columnIndex, String dropdownXpath, String textBoxXpath, String searchButtonXpath, String columnXpath, boolean frameSwitching,
            String frameNameOne, String innerFrameNameOne, String frameNameTwo, String innerFrameNameTwo)
    {
        if (frameSwitching)
        {
            if (frameSwitching)
            {
                if (!this.switchToInnerSintelligentFrame(frameNameOne, innerFrameNameOne))
                {
                    return "Failed to switch to the sintelligent iframe :'" + frameNameOne + "' and frame :'" + innerFrameNameOne + "'";
                }
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(dropdownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(dropdownXpath, dropdownItem))
        {
            return "Failed to select Value '" + dropdownItem + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(textBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(textBoxXpath, search))
        {
            return "Failed to enter '" + search + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        String test = SeleniumDriverInstance.WaitForLoaderIconNotVisible();
        if (!test.isEmpty())
        {
            return "Failed to wait for the Loader to disappear";
        }
        
        if (frameSwitching)
        {

            if (!this.switchToInnerSintelligentFrame(frameNameTwo, innerFrameNameTwo))
            {
                return "Failed to switch to the sintelligent iframe :'" + frameNameTwo + "' and frame :'" + innerFrameNameTwo + "'";
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(columnXpath))
        {
            return "Failed to wait for the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            if (values.isEmpty())
            {
                return "Failed to extract the '" + dropdownItem + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                if (!values.get(i).getText().toLowerCase().contains(search.toLowerCase()))
                {
                    return "Failed to validate that filtering '" + dropdownItem + "' with data '" + search + "' brought back the required results - Expected: " + search + " - Actual: " + values.get(i).getText();
                }
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values";
        }

        return "";
    }
    
    public String filteringTextMethodWithDropDownFrameSwitchingByFrameNames(String dropdownItem, String search, String srcForvalidation, String columnIndex, String dropdownXpath, String dropdownXpath2, String searchButtonXpath, String columnXpath, boolean frameSwitching,
            String frameNameOne, String innerFrameNameOne, String frameNameTwo, String innerFrameNameTwo)
    {
        if (frameSwitching)
        {
            if (frameSwitching)
            {
                if (!this.switchToInnerSintelligentFrame(frameNameOne, innerFrameNameOne))
                {
                    return "Failed to switch to the sintelligent iframe :'" + frameNameOne + "' and frame :'" + innerFrameNameOne + "'";
                }
            }
        }
       
        if (!SeleniumDriverInstance.waitForElementByXpath(dropdownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(dropdownXpath, dropdownItem))
        {
            return "Failed to select Value '" + dropdownItem + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(dropdownXpath2))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(dropdownXpath2, search))
        {
            return "Failed to enter '" + search + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }
        
        String loadingWait = WaitForLoaderIconNotVisible();
        if (!loadingWait.equals(""))
        {
            return loadingWait;
        }
        
        /*if (frameSwitching)
        {

            if (!this.switchToInnerSintelligentFrame(frameNameTwo, innerFrameNameTwo))
            {
                return "Failed to switch to the sintelligent iframe :'" + frameNameTwo + "' and frame :'" + innerFrameNameTwo + "'";
            }
        }*/
        if (frameSwitching)
        {
            if (frameNameTwo.length() > 0)
            {
                if (!this.switchToInnerSintelligentFrame(frameNameTwo))
                {
                    return "Failed to switch to the sintelligent frame two";
                }
            }

            if (innerFrameNameTwo.length() > 0)
            {
                if (!this.switchToFrameById(innerFrameNameTwo))
                {
                    return "Failed to switch to the inner sintelligent frame two";
                }
            }
        }
        
        loadingWait = WaitForLoaderIconNotVisible();
        if (!loadingWait.equals(""))
        {
            return loadingWait;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(columnXpath))
        {
            return "Failed to wait for the '" + columnIndex + "' column";
        }
        /*
        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {

                WebDriverWait wait = new WebDriverWait(Driver, 1);
                if (wait.until(ExpectedConditions.visibilityOf(values.get(i))) != null)
                {
                    if (!values.get(i).getAttribute("src").toString().toLowerCase().contains(srcForvalidation.toLowerCase()))
                    {
                        return "Failed to validate that filtering '" + dropdownItem + "' with data '" + search + "' brought back the required results - Expected: " + search + " - Actual: " + values.get(i).getText();
                    }
                }
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values";
        }*/
        
        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                if (!values.get(i).getText().toLowerCase().contains(srcForvalidation.toLowerCase()))
                {
                    return "Failed to validate that filtering '" + dropdownItem + "' with data '" + srcForvalidation + "' brought back the required results - Expected: " + search + " - Actual: " + values.get(i).getText();
                }
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values";
        }

        return "";
    }
    
    public String filteringMethodWithDropDownFrameSwitchingByFrameNames(String dropdownItem, String search, String srcForvalidation, String columnIndex, String dropdownXpath, String dropdownXpath2, String searchButtonXpath, String columnXpath, boolean frameSwitching,
            String frameNameOne, String innerFrameNameOne, String frameNameTwo, String innerFrameNameTwo)
    {
        if (frameSwitching)
        {
            if (frameSwitching)
            {
                if (!this.switchToInnerSintelligentFrame(frameNameOne, innerFrameNameOne))
                {
                    return "Failed to switch to the sintelligent iframe :'" + frameNameOne + "' and frame :'" + innerFrameNameOne + "'";
                }
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(dropdownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(dropdownXpath, dropdownItem))
        {
            return "Failed to select Value '" + dropdownItem + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(dropdownXpath2))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(dropdownXpath2, search))
        {
            return "Failed to enter '" + search + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        if (frameSwitching)
        {

            if (!this.switchToInnerSintelligentFrame(frameNameTwo, innerFrameNameTwo))
            {
                return "Failed to switch to the sintelligent iframe :'" + frameNameTwo + "' and frame :'" + innerFrameNameTwo + "'";
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(columnXpath))
        {
            return "Failed to wait for the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {

                WebDriverWait wait = new WebDriverWait(Driver, 1);
                if (wait.until(ExpectedConditions.visibilityOf(values.get(i))) != null)
                {
                    if (!values.get(i).getAttribute("src").toString().toLowerCase().contains(srcForvalidation.toLowerCase()))
                    {
                        return "Failed to validate that filtering '" + dropdownItem + "' with data '" + search + "' brought back the required results - Expected: " + search + " - Actual: " + values.get(i).getText();
                    }
                }
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values";
        }

        return "";
    }

    public String filteringMethodFrameSwitchingByFrameNameAndFrameIdWithWaits(String dropdownItem, String search, String columnIndex, String dropdownXpath, String textBoxXpath, String searchButtonXpath, String columnXpath, boolean frameSwitching,
            String frameNameOne, String innerFrameNameOne, String frameNameTwo, String innerFrameNameTwo)
    {
        if (frameSwitching)
        {
            if (frameSwitching)
            {
                if (!this.switchToInnerSintelligentFrame(frameNameOne, innerFrameNameOne))
                {
                    return "Failed to switch to the sintelligent iframe :'" + frameNameOne + "' and frame :'" + innerFrameNameOne + "'";
                }
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(dropdownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(dropdownXpath, dropdownItem))
        {
            return "Failed to select Value '" + dropdownItem + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(textBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(textBoxXpath, search))
        {
            return "Failed to enter '" + search + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        String loadingWait = WaitForLoaderIconNotVisible();
        if (!loadingWait.equals(""))
        {
            return loadingWait;
        }

        if (frameSwitching)
        {
            if (frameNameTwo.length() > 0)
            {
                if (!this.switchToInnerSintelligentFrame(frameNameTwo))
                {
                    return "Failed to switch to the sintelligent frame two";
                }
            }

            if (innerFrameNameTwo.length() > 0)
            {
                if (!this.switchToFrameById(innerFrameNameTwo))
                {
                    return "Failed to switch to the inner sintelligent frame two";
                }
            }
        }

        loadingWait = WaitForLoaderIconNotVisible();
        if (!loadingWait.equals(""))
        {
            return loadingWait;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(columnXpath))
        {
            return "Failed to wait for the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                if (!values.get(i).getText().toLowerCase().contains(search.toLowerCase()))
                {
                    return "Failed to validate that filtering '" + dropdownItem + "' with data '" + search + "' brought back the required results - Expected: " + search + " - Actual: " + values.get(i).getText();
                }
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values";
        }

        return "";
    }

    public String sortingMethodFrameSwitching(String filterDropDownXpath, String filterDropDown, String filterSearchBoxXpath, String filterSearchText, String searchButtonXpath,
            String columnXpath, String columnIndex, String columHeaderXpath, String type, boolean frameSwitching,
            String frameOne, String innerFrameOne, String frameTwo, String innerFrameTwo)
    {
        if (frameSwitching)
        {
            if (frameOne.length() > 0)
            {
                if (!this.switchToInnerSintelligentFrame(frameOne))
                {
                    return "Failed to switch to the sintelligent frame one";
                }
            }

            if (innerFrameOne.length() > 0)
            {
                if (!this.switchToFrameById(innerFrameOne))
                {
                    return "Failed to switch to the inner sintelligent frame one";
                }
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterDropDownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(filterDropDownXpath, filterDropDown))
        {
            return "Failed to select Value '" + filterDropDown + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterSearchBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(filterSearchBoxXpath, filterSearchText))
        {
            return "Failed to enter '" + filterSearchText + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        if (frameSwitching)
        {
            if (frameTwo.length() > 0)
            {
                if (!this.switchToInnerSintelligentFrame(frameTwo))
                {
                    return "Failed to switch to the sintelligent frame two";
                }
            }

            if (innerFrameTwo.length() > 0)
            {
                if (!this.switchToFrameById(innerFrameTwo))
                {
                    return "Failed to switch to the inner sintelligent frame two";
                }
            }
        }

        if (!SeleniumDriverInstance.stableClickElementByXpath(columHeaderXpath))
        {
            return "Failed to sort the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            ArrayList list = new ArrayList();
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                list.add(values.get(i).getText().toString().toLowerCase());
            }

            if (list.size() != values.size())
            {
                return "Failed to change the front end list to a alphabetical list - front end list size did not match Expected: " + values.size() + " - Actual: " + list.size();
            }

            boolean sorted = false;

            if (type.equalsIgnoreCase("asc"))
            {
                sorted = Ordering.natural().isOrdered(list);
            }
            else if (type.equalsIgnoreCase("desc"))
            {
                sorted = Ordering.natural().reverse().isOrdered(list);
            }

            if (!sorted)
            {
                return "Failed to validate the list for row index " + columnIndex + " was in reverse-alphabetical order";
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values and validate the columns where in " + type + " order";
        }

        return "";
    }

    public String sortingMethodFrameSwitching(String columnXpath, String columnIndex, String columHeaderXpath, String elementOfvalidation, String type)
    {

        if (!SeleniumDriverInstance.stableClickElementByXpath(columHeaderXpath))
        {
            return "Failed to sort the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            ArrayList list = new ArrayList();
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            if (elementOfvalidation.isEmpty())
            {
                for (int i = 0; i < values.size(); i++)
                {
                    list.add(values.get(i).getText().toString().toLowerCase());
                }
            }
            else
            {
                for (int i = 0; i < values.size(); i++)
                {
                    if (values.get(i).getAttribute("src").toString().toLowerCase().contains(elementOfvalidation.toLowerCase()))
                    {
                        list.add("0");
                    }
                    else
                    {
                        list.add("1");
                    }
                }
            }

            if (list.size() != values.size())
            {
                return "Failed to change the front end list to a alphabetical list - front end list size did not match Expected: " + values.size() + " - Actual: " + list.size();
            }

            boolean sorted = false;

            if (type.equalsIgnoreCase("asc"))
            {
                sorted = Ordering.natural().isOrdered(list);
            }
            else if (type.equalsIgnoreCase("desc"))
            {
                sorted = Ordering.natural().reverse().isOrdered(list);
            }

            if (!sorted)
            {
                return "Failed to validate the list for row index " + columnIndex + " was in reverse-alphabetical order";
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values and validate the columns where in " + type + " order";
        }

        return "";
    }

    public String CSVValidationSwitchingByFrameName(int frontEndColumn, int CSVColumn, String[][] CSVData, String columnXpath, boolean frameSwitch, String frameNameOne, String innerFrameNameOne)
    {
        if (frameSwitch)
        {
            if (!this.switchToInnerSintelligentFrame(frameNameOne, innerFrameNameOne))
            {
                return "Failed to switch to the sintelligent iframe :'" + frameNameOne + "' and frame :'" + innerFrameNameOne + "'";
            }
        }

        ArrayList CSVTempColumn = new ArrayList();
        try
        {
            for (int i = 1; i < CSVData.length; i++)
            {
                String temp = CSVData[i][CSVColumn];
                CSVTempColumn.add(temp);
            }
        }
        catch (Exception e)
        {
            return "";
        }

        List<WebElement> frontEndColumnItems;

        try
        {
            frontEndColumnItems = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
        }
        catch (Exception e)
        {
            return "Failed to extract the front end column " + frontEndColumn + "";
        }

        if (frontEndColumnItems.isEmpty())
        {
            return "Failed to extract the front end column";
        }

        ArrayList frontEndTempColumn = new ArrayList();

        for (int i = 0; i < frontEndColumnItems.size(); i++)
        {
            frontEndTempColumn.add(frontEndColumnItems.get(i).getText().toString());
        }

        if (frontEndTempColumn.size() != CSVTempColumn.size())
        {
            return "The size of items in column " + frontEndColumn + " on the front end does not equal the amount found in the CSV";
        }

        for (int i = 0; i < frontEndTempColumn.size(); i++)
        {
            String tempCSV = CSVTempColumn.get(i).toString().trim();
            String tempFrontEnd = frontEndTempColumn.get(i).toString().trim();
            if (!tempCSV.equalsIgnoreCase(tempFrontEnd))
            {
                return "Failed to validate the CSV contained the same data as the front end Column: " + frontEndColumn + " Front End: " + frontEndTempColumn.toString() + " - CSV: " + CSVTempColumn.toString();
            }
        }

        return "";
    }

    public String CSVValidationSwitchingByFrameName(int frontEndColumn, int CSVColumn, String[][] CSVData, String columnXpath, String elementValidation, String elementExcepted, boolean frameSwitch, String frameNameOne, String innerFrameNameOne)
    {
        if (frameSwitch)
        {
            if (!this.switchToInnerSintelligentFrame(frameNameOne, innerFrameNameOne))
            {
                return "Failed to switch to the sintelligent iframe :'" + frameNameOne + "' and frame :'" + innerFrameNameOne + "'";
            }
        }

        ArrayList CSVTempColumn = new ArrayList();
        try
        {
            for (int i = 1; i < CSVData.length; i++)
            {
                String temp = CSVData[i][CSVColumn];
                CSVTempColumn.add(temp);
            }
        }
        catch (Exception e)
        {
            return "";
        }

        List<WebElement> frontEndColumnItems;

        try
        {
            frontEndColumnItems = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
        }
        catch (Exception e)
        {
            return "Failed to extract the front end column " + frontEndColumn + "";
        }

        if (frontEndColumnItems.isEmpty())
        {
            return "Failed to extract the front end column";
        }

        ArrayList frontEndTempColumn = new ArrayList();

        if (elementValidation.isEmpty())
        {
            for (int i = 0; i < frontEndColumnItems.size(); i++)
            {
                frontEndTempColumn.add(frontEndColumnItems.get(i).getText().toString());
            }
        }
        else
        {
            for (int i = 0; i < frontEndColumnItems.size(); i++)
            {

                if (frontEndColumnItems.get(i).getAttribute("src").toString().toLowerCase().contains(elementValidation.toLowerCase()))
                {
                    frontEndTempColumn.add(elementExcepted);
                }

            }

        }

        if (frontEndTempColumn.size() != CSVTempColumn.size())
        {
            return "The size of items in column " + frontEndColumn + " on the front end does not equal the amount found in the CSV";
        }

        for (int i = 0; i < frontEndTempColumn.size(); i++)
        {
            String tempCSV = CSVTempColumn.get(i).toString().trim();
            String tempFrontEnd = frontEndTempColumn.get(i).toString().trim();
            if (!tempCSV.equalsIgnoreCase(tempFrontEnd))
            {
                return "Failed to validate the CSV contained the same data as the front end Column: " + frontEndColumn + " Front End: " + frontEndTempColumn.toString() + " - CSV: " + CSVTempColumn.toString();
            }
        }

        return "";
    }

    public String[][] CSVReader(String fileLocation, final String partialFileName)
    {
        String[][] CSVData = null;

        boolean saved = false;
        int count = 0;
        String downloadedFileName = "";
        String downloadLocation = "";
        String csvFileName = "";

        while (count < 5 && saved == false)
        {
            downloadLocation = new File(fileLocation).toString();
            File dir = new File(downloadLocation);
            try
            {
                String fileName = "";
                if (dir.isDirectory())
                {
                    File[] children = dir.listFiles();
                    if (null != children)
                    {
                        for (int i = 0; i < children.length; i++)
                        {
                            fileName = children[i].getName();
                            if (fileName.contains(partialFileName))
                            {
                                downloadedFileName = fileName;
                                saved = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            // list the files using a anonymous FileFilter
            File[] files = dir.listFiles(new FileFilter()
            {

                @Override
                public boolean accept(File file)
                {
                    return file.getName().contains(partialFileName.toString());
                }
            });

            csvFileName = files[files.length - 1].toString();

            try
            {
                String temp = partialFileName + "\\" + csvFileName;
                CSVReader csvReader = new CSVReader(new FileReader(new File(csvFileName)));
                List<String[]> list = csvReader.readAll();
                CSVData = new String[list.size()][];
                CSVData = list.toArray(CSVData);
            }
            catch (Exception e)
            {
                return null;
//            return "Could not read and store the CSV data";
            }

            if (CSVData.equals(null))
            {
                return null;
//            return "Failed to read the CSV contents - no data was found in the CSV";
            }

        }
        return CSVData;
    }

    public String[][] CSVReader(boolean extract, String downloadButtonXpath, final String partialFileName, String outputDirectory, String downloadFilePath, final String zipFileName)
    {

        String[][] CSVData = null;

        if (downloadButtonXpath != "")
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(downloadButtonXpath))
            {
                return null;
            }

            if (!SeleniumDriverInstance.clickElementByXpath(downloadButtonXpath))
            {
                return null;
            }
            
                  if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE )
                {
                          try{
                              
                              if(downloadButtonXpath.contains("Download") || downloadButtonXpath.contains("apply"))
                              {
                                 clickAndSaveFileIE(downloadButtonXpath);
                              }
                              else
                              {
                                clickAndSaveFileIE1(downloadButtonXpath);
                              }
                          }
                          catch(Exception e)
                          {

                          }  
                }
        }

        SeleniumDriverInstance.pause(5000);
        boolean saved = false;
        int count = 0;
        String downloadedFileName = "";
        String downloadLocation = "";
        String csvFileName = "";

        while (count < 5 && saved == false)
        {
            downloadLocation = new File(System.getProperty("user.home") + "/Downloads/").toString();
            File dir = new File(downloadLocation);
            try
            {
                String fileName = "";
                if (dir.isDirectory())
                {
                    File[] children = dir.listFiles();
                    if (null != children)
                    {
                        for (int i = 0; i < children.length; i++)
                        {
                            fileName = children[i].getName();
                            if (fileName.contains(partialFileName))
                            {
                                downloadedFileName = fileName;
                                saved = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            if (!saved)
            {
                SeleniumDriverInstance.pause(2000);
                count++;
            }
        }

        String parentHandle = SeleniumDriverInstance.Driver.getWindowHandle();

        if (extract)
        {
            try
            {
                ZipFile zipFile = new ZipFile(downloadLocation + "\\" + downloadedFileName);
                try
                {
                    Enumeration<? extends ZipEntry> entries = zipFile.entries();
                    while (entries.hasMoreElements())
                    {
                        ZipEntry entry = entries.nextElement();
                        File entryDestination = new File(outputDirectory, zipFileName);
                        if (entry.isDirectory())
                        {
                            entryDestination.mkdirs();
                        }
                        else
                        {
                            entryDestination.getParentFile().mkdirs();
                            InputStream in = zipFile.getInputStream(entry);
                            OutputStream out = new FileOutputStream(entryDestination);
                            IOUtils.copy(in, out);
                            IOUtils.closeQuietly(in);
                            out.close();
                        }
                    }
                }
                finally
                {
                    zipFile.close();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        else
        {
            try
            {
                if (!CopyFileFromSourceToDestination(downloadLocation + "\\" + downloadedFileName, outputDirectory + "\\" + zipFileName))
                {
                    return null;
                }
                //FileUtils.copyFile(new File(downloadLocation + "\\" + downloadedFileName) , new File(outputDirectory));
            }
            catch (Exception e)
            {
                return null;
            }
        }

        try
        {
            String browser = ApplicationConfig.SelectedBrowser().toString();

            if (!browser.equalsIgnoreCase("FireFox") && !browser.equalsIgnoreCase("IE") )
            {
                while (parentHandle.equalsIgnoreCase(SeleniumDriverInstance.Driver.getWindowHandle()))
                {
                    for (String winHandle : SeleniumDriverInstance.Driver.getWindowHandles())
                    {
                        SeleniumDriverInstance.Driver.switchTo().window(winHandle);
                    }
                }
                SeleniumDriverInstance.Driver.close();
                SeleniumDriverInstance.Driver.switchTo().window(parentHandle);
            }
        }
        catch (Exception e)
        {
            return null;
//            return "Failed to switch back to the original window";
        }

        try
        {
            File fileToDelete = new File(downloadFilePath + downloadedFileName);
            if (!SeleniumDriverInstance.removeDirectory(fileToDelete))
            {
                return null;
//                return "Failed to delete the downloaded CSV after the zip file was extracted";
            }
        }
        catch (Exception e)
        {
            return null;
//            return "Failed to delete '"+downloadedFileName+"' file";
        }

//        String Str = new String(downloadedFileName);
//        csvFileName = Str.replace(".zip", ".csv");
        File[] files;

        try
        {
            File dir = new File(outputDirectory);
            files = dir.listFiles(new FileFilter()
            {

                @Override
                public boolean accept(File file)
                {
                    return file.getName().contains(zipFileName);
                }
            });

        }
        catch (Exception e)
        {
            return null;
        }

        // list the files using a anonymous FileFilter
        try
        {
            csvFileName = files[files.length - 1].toString();
        }
        catch (Exception e)
        {
            return null;
        }

        try
        {
            String temp = outputDirectory + "\\" + zipFileName;
            CSVReader csvReader = new CSVReader(new FileReader(new File(temp)));
            List<String[]> list = csvReader.readAll();
            CSVData = new String[list.size()][];
            CSVData = list.toArray(CSVData);
        }
        catch (Exception e)
        {
            return null;
//            return "Could not read and store the CSV data";
        }

        if (CSVData.equals(null))
        {
            return null;
//            return "Failed to read the CSV contents - no data was found in the CSV";
        }

        return CSVData;
    }
    
    public String[][] CSVReader(boolean extract, String downloadButtonXpath, final String partialFileName, String outputDirectory, String downloadFilePath)
    {

        String[][] CSVData = null;

        if (downloadButtonXpath != "")
        {
            if (!SeleniumDriverInstance.cleanDownloadsDirectory())
            {
                return null;
//                return "Failed to delete the downloaded CSV after the zip file was extracted";
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(downloadButtonXpath))
            {
                return null;
            }

            if (!SeleniumDriverInstance.stableClickElementByXpath(downloadButtonXpath))
            {
                return null;
            }
            
                  if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE )
                {
                          try{
                              
                              if(downloadButtonXpath.contains("Download") || downloadButtonXpath.contains("apply")|| downloadButtonXpath.contains("Select"))
                              {
                                 clickAndSaveFileIE(downloadButtonXpath);
                              }
                              else
                              {
                                clickAndSaveFileIE1(downloadButtonXpath);
                              }
                          }
                          catch(Exception e)
                          {

                          }  
                }
            
        }
        else if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE)
        {
              try
              {
                   clickAndSaveFileIE(downloadButtonXpath);
              }
              catch(Exception e)
              {

              }  
        }
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
           SeleniumDriverInstance.pause(5000); 
        }
        SeleniumDriverInstance.pause(5000);
        boolean saved = false;
        int count = 0;
        String downloadedFileName = "";
        String downloadLocation = "";
        String csvFileName = "";

        while (count < 5 && saved == false)
        {
            downloadLocation = new File(System.getProperty("user.home") + "/Downloads/").toString();
            File dir = new File(downloadLocation);
            try
            {
                String fileName = "";
                if (dir.isDirectory())
                {
                    File[] children = dir.listFiles();
                    if (null != children)
                    {
                        for (int i = 0; i < children.length; i++)
                        {
                            fileName = children[i].getName();
                            if (fileName.contains(partialFileName))
                            {
                                downloadedFileName = fileName;
                                saved = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            if (!saved)
            {
                SeleniumDriverInstance.pause(2000);
                count++;
            }
        }

        String parentHandle = SeleniumDriverInstance.Driver.getWindowHandle();

        if (extract)
        {
            try
            {
                ZipFile zipFile = new ZipFile(downloadLocation + "\\" + downloadedFileName);
                try
                {
                    Enumeration<? extends ZipEntry> entries = zipFile.entries();
                    while (entries.hasMoreElements())
                    {
                        ZipEntry entry = entries.nextElement();
                        File entryDestination = new File(outputDirectory, entry.getName());
                        if (entry.isDirectory())
                        {
                            entryDestination.mkdirs();
                        }
                        else
                        {
                            entryDestination.getParentFile().mkdirs();
                            InputStream in = zipFile.getInputStream(entry);
                            OutputStream out = new FileOutputStream(entryDestination);
                            IOUtils.copy(in, out);
                            IOUtils.closeQuietly(in);
                            out.close();
                        }
                    }
                }
                finally
                {
                    zipFile.close();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        else
        {
            try
            {
                if (!CopyFileFromSourceToDestination(downloadLocation + "\\" + downloadedFileName, outputDirectory + "\\" + downloadedFileName))
                {
                    return null;
                }
                //FileUtils.copyFile(new File(downloadLocation + "\\" + downloadedFileName) , new File(outputDirectory));
            }
            catch (Exception e)
            {
                return null;
            }
        }

        try
        {
            String browser = ApplicationConfig.SelectedBrowser().toString();

            if (!browser.equalsIgnoreCase("FireFox") && (ApplicationConfig.SelectedBrowser() != Enums.BrowserType.IE) )
            {
                while (parentHandle.equalsIgnoreCase(SeleniumDriverInstance.Driver.getWindowHandle()))
                {
                    for (String winHandle : SeleniumDriverInstance.Driver.getWindowHandles())
                    {
                        SeleniumDriverInstance.Driver.switchTo().window(winHandle);
                    }
                }
                SeleniumDriverInstance.Driver.close();
                SeleniumDriverInstance.Driver.switchTo().window(parentHandle);
            }
        }
        catch (Exception e)
        {
            return null;
//            return "Failed to switch back to the original window";
        }

        try
        {
            File fileToDelete = new File(downloadFilePath + downloadedFileName);
            if (!SeleniumDriverInstance.removeDirectory(fileToDelete))
            {
                return null;
//                return "Failed to delete the downloaded CSV after the zip file was extracted";
            }
        }
        catch (Exception e)
        {
            return null;
//            return "Failed to delete '"+downloadedFileName+"' file";
        }

//        String Str = new String(downloadedFileName);
//        csvFileName = Str.replace(".zip", ".csv");
        File[] files;

        try
        {
            File dir = new File(outputDirectory);
            files = dir.listFiles(new FileFilter()
            {

                @Override
                public boolean accept(File file)
                {
                    return file.getName().contains(partialFileName);
                }
            });

        }
        catch (Exception e)
        {
            return null;
        }

        // list the files using a anonymous FileFilter
        try
        {
            csvFileName = files[files.length - 1].toString();
        }
        catch (Exception e)
        {
            return null;
        }

        try
        {
            String temp = outputDirectory + "\\" + csvFileName;
            CSVReader csvReader = new CSVReader(new FileReader(new File(csvFileName)));
            List<String[]> list = csvReader.readAll();
            CSVData = new String[list.size()][];
            CSVData = list.toArray(CSVData);
        }
        catch (Exception e)
        {
            return null;
//            return "Could not read and store the CSV data";
        }

        if (CSVData.equals(null))
        {
            return null;
//            return "Failed to read the CSV contents - no data was found in the CSV";
        }

        return CSVData;
    }

    public String[][] CSVReaderNoPopUp(boolean extract, String downloadButtonXpath, final String partialFileName, String outputDirectory, String downloadFilePath)
    {

        String[][] CSVData = null;

        if (downloadButtonXpath != "")
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(downloadButtonXpath))
            {
                return null;
            }

            if (!SeleniumDriverInstance.clickElementByXpath(downloadButtonXpath))
            {
                return null;
            }
        }

        SeleniumDriverInstance.pause(5000);
        boolean saved = false;
        int count = 0;
        String downloadedFileName = "";
        String downloadLocation = "";
        String csvFileName = "";

        while (count < 5 && saved == false)
        {
            downloadLocation = new File(System.getProperty("user.home") + "/Downloads/").toString();
            File dir = new File(downloadLocation);
            try
            {
                String fileName = "";
                if (dir.isDirectory())
                {
                    File[] children = dir.listFiles();
                    if (null != children)
                    {
                        for (int i = 0; i < children.length; i++)
                        {
                            fileName = children[i].getName();
                            if (fileName.contains(partialFileName))
                            {
                                downloadedFileName = fileName;
                                saved = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            if (!saved)
            {
                SeleniumDriverInstance.pause(2000);
                count++;
            }
        }

        String parentHandle = SeleniumDriverInstance.Driver.getWindowHandle();

        if (extract)
        {
            try
            {
                ZipFile zipFile = new ZipFile(downloadLocation + "\\" + downloadedFileName);
                try
                {
                    Enumeration<? extends ZipEntry> entries = zipFile.entries();
                    while (entries.hasMoreElements())
                    {
                        ZipEntry entry = entries.nextElement();
                        File entryDestination = new File(outputDirectory, entry.getName());
                        if (entry.isDirectory())
                        {
                            entryDestination.mkdirs();
                        }
                        else
                        {
                            entryDestination.getParentFile().mkdirs();
                            InputStream in = zipFile.getInputStream(entry);
                            OutputStream out = new FileOutputStream(entryDestination);
                            IOUtils.copy(in, out);
                            IOUtils.closeQuietly(in);
                            out.close();
                        }
                    }
                }
                finally
                {
                    zipFile.close();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        else
        {
            try
            {
                if (!CopyFileFromSourceToDestination(downloadLocation + "\\" + downloadedFileName, outputDirectory + "\\" + downloadedFileName))
                {
                    return null;
                }
                //FileUtils.copyFile(new File(downloadLocation + "\\" + downloadedFileName) , new File(outputDirectory));
            }
            catch (Exception e)
            {
                return null;
            }
        }

        try
        {
            File fileToDelete = new File(downloadFilePath + downloadedFileName);
            if (!SeleniumDriverInstance.removeDirectory(fileToDelete))
            {
                return null;
//                return "Failed to delete the downloaded CSV after the zip file was extracted";
            }
        }
        catch (Exception e)
        {
            return null;
//            return "Failed to delete '"+downloadedFileName+"' file";
        }

//        String Str = new String(downloadedFileName);
//        csvFileName = Str.replace(".zip", ".csv");
        File[] files;

        try
        {
            File dir = new File(outputDirectory);
            files = dir.listFiles(new FileFilter()
            {

                @Override
                public boolean accept(File file)
                {
                    return file.getName().contains(partialFileName);
                }
            });

        }
        catch (Exception e)
        {
            return null;
        }

        // list the files using a anonymous FileFilter
        try
        {
            csvFileName = files[files.length - 1].toString();
        }
        catch (Exception e)
        {
            return null;
        }

        try
        {
            String temp = outputDirectory + "\\" + csvFileName;
            CSVReader csvReader = new CSVReader(new FileReader(new File(csvFileName)));
            List<String[]> list = csvReader.readAll();
            CSVData = new String[list.size()][];
            CSVData = list.toArray(CSVData);
        }
        catch (Exception e)
        {
            return null;
//            return "Could not read and store the CSV data";
        }

        if (CSVData.equals(null))
        {
            return null;
//            return "Failed to read the CSV contents - no data was found in the CSV";
        }

        return CSVData;
    }

    public String[][] CSVReader_PerformanceGraph(boolean extract, String downloadButtonXpath, final String partialFileName, String outputDirectory, String downloadFilePath)
    {

        String[][] CSVData = null;

        if (downloadButtonXpath != "")
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(downloadButtonXpath))
            {
                return null;
            }

            if (!SeleniumDriverInstance.clickElementByXpath(downloadButtonXpath))
            {
                return null;
            }
        }

        SeleniumDriverInstance.pause(5000);

        boolean saved = false;
        int count = 0;
        String downloadedFileName = "";
        String downloadLocation = "";
        String csvFileName = "";

        while (count < 5 && saved == false)
        {
            downloadLocation = new File(System.getProperty("user.home") + "/Downloads/").toString();
            File dir = new File(downloadLocation);
            try
            {
                String fileName = "";
                if (dir.isDirectory())
                {
                    File[] children = dir.listFiles();
                    if (null != children)
                    {
                        for (int i = 0; i < children.length; i++)
                        {
                            fileName = children[i].getName();
                            if (fileName.contains(partialFileName))
                            {
                                downloadedFileName = fileName;
                                saved = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            if (!saved)
            {
                SeleniumDriverInstance.pause(2000);
                count++;
            }
        }

        String parentHandle = SeleniumDriverInstance.Driver.getWindowHandle();

        if (extract)
        {
            SeleniumDriverInstance.pause(2000);

            try
            {
                ZipFile zipFile = new ZipFile(downloadLocation + "\\" + downloadedFileName);
                try
                {
                    Enumeration<? extends ZipEntry> entries = zipFile.entries();
                    while (entries.hasMoreElements())
                    {
                        ZipEntry entry = entries.nextElement();
                        File entryDestination = new File(outputDirectory, entry.getName());
                        if (entry.isDirectory())
                        {
                            entryDestination.mkdirs();
                        }
                        else
                        {
                            entryDestination.getParentFile().mkdirs();
                            InputStream in = zipFile.getInputStream(entry);
                            OutputStream out = new FileOutputStream(entryDestination);
                            IOUtils.copy(in, out);
                            IOUtils.closeQuietly(in);
                            out.close();
                        }
                    }
                }
                finally
                {
                    zipFile.close();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        else
        {
            try
            {
                if (!CopyFileFromSourceToDestination(downloadLocation + "\\" + downloadedFileName, outputDirectory + "\\" + downloadedFileName))
                {
                    return null;
                }
                //FileUtils.copyFile(new File(downloadLocation + "\\" + downloadedFileName) , new File(outputDirectory));
            }
            catch (Exception e)
            {
                return null;
            }
        }

        try
        {
            String browser = ApplicationConfig.SelectedBrowser().toString();

            if (!browser.equalsIgnoreCase("FireFox") && !downloadedFileName.contains("90"))
            {
                while (parentHandle.equalsIgnoreCase(SeleniumDriverInstance.Driver.getWindowHandle()))
                {
                    for (String winHandle : SeleniumDriverInstance.Driver.getWindowHandles())
                    {
                        SeleniumDriverInstance.Driver.switchTo().window(winHandle);
                    }
                }
                SeleniumDriverInstance.Driver.close();
                SeleniumDriverInstance.Driver.switchTo().window(parentHandle);
            }
        }
        catch (Exception e)
        {
            return null;
//            return "Failed to switch back to the original window";
        }

        try
        {
            File fileToDelete = new File(downloadFilePath + downloadedFileName);
            if (!SeleniumDriverInstance.removeDirectory(fileToDelete))
            {
                return null;
//                return "Failed to delete the downloaded CSV after the zip file was extracted";
            }
        }
        catch (Exception e)
        {
            return null;
//            return "Failed to delete '"+downloadedFileName+"' file";
        }

//        String Str = new String(downloadedFileName);
//        csvFileName = Str.replace(".zip", ".csv");
        File[] files;

        try
        {
            File dir = new File(outputDirectory);
            files = dir.listFiles(new FileFilter()
            {

                @Override
                public boolean accept(File file)
                {
                    return file.getName().contains(partialFileName);
                }
            });

        }
        catch (Exception e)
        {
            return null;
        }

        // list the files using a anonymous FileFilter
        try
        {
            csvFileName = files[files.length - 1].toString();
        }
        catch (Exception e)
        {
            return null;
        }

        try
        {
            String temp = outputDirectory + "\\" + csvFileName;
            CSVReader csvReader = new CSVReader(new FileReader(new File(csvFileName)));
            List<String[]> list = csvReader.readAll();
            CSVData = new String[list.size()][];
            CSVData = list.toArray(CSVData);
        }
        catch (Exception e)
        {
            return null;
//            return "Could not read and store the CSV data";
        }

        if (CSVData.equals(null))
        {
            return null;
//            return "Failed to read the CSV contents - no data was found in the CSV";
        }

        return CSVData;
    }

    public boolean DownloadFileToReportFolder(boolean extract, String downloadButtonXpath, final String partialFileName, String outputDirectory, String downloadFilePath)
    {
        if (downloadButtonXpath != "")
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(downloadButtonXpath))
            {
                return false;
            }

            if (!SeleniumDriverInstance.clickElementByXpath(downloadButtonXpath))
            {
                return false;
            }
        }

        SeleniumDriverInstance.pause(10000);
           if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
            {
              try{

                  if(downloadButtonXpath.contains("Download")|| downloadButtonXpath.contains("apply"))
                  {
                     clickAndSaveFileIE(downloadButtonXpath);
                  }
                  else
                  {
                    clickAndSaveFileIE1(downloadButtonXpath);
                  }
              }
              catch(Exception e)
              {

              }  
            }

           else if(ApplicationConfig.SelectedBrowser() == Enums.BrowserType.Chrome)
           {
                String parentHandle = SeleniumDriverInstance.Driver.getWindowHandle();
                try
                {
                    String browser = ApplicationConfig.SelectedBrowser().toString();

                    if (!browser.equalsIgnoreCase("FireFox"))
                    {
                        while (parentHandle.equalsIgnoreCase(SeleniumDriverInstance.Driver.getWindowHandle()))
                        {
                            for (String winHandle : SeleniumDriverInstance.Driver.getWindowHandles())
                            {
                                SeleniumDriverInstance.Driver.switchTo().window(winHandle);
                            }
                        }
                        SeleniumDriverInstance.Driver.close();
                        SeleniumDriverInstance.Driver.switchTo().window(parentHandle);
                    }
                }
                catch (Exception e)
                {
                    return false;
                }
           }
        boolean saved = false;
        int count = 0;
        String downloadedFileName = "";
        String downloadLocation = "";

        while (count < 5 && saved == false)
        {
            downloadLocation = new File(System.getProperty("user.home") + "/Downloads/").toString();
            File dir = new File(downloadLocation);
            try
            {
                String fileName = "";
                if (dir.isDirectory())
                {
                    File[] children = dir.listFiles();
                    if (null != children)
                    {
                        for (int i = 0; i < children.length; i++)
                        {
                            fileName = children[i].getName();
                            if (fileName.contains(partialFileName))
                            {
                                downloadedFileName = fileName;
                                saved = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            if (!saved)
            {
                SeleniumDriverInstance.pause(2000);
                count++;
            }
        }

        if (extract)
        {
            try
            {
                ZipFile zipFile = new ZipFile(downloadLocation + "\\" + downloadedFileName);
                try
                {
                    Enumeration<? extends ZipEntry> entries = zipFile.entries();
                    while (entries.hasMoreElements())
                    {
                        ZipEntry entry = entries.nextElement();
                        File entryDestination = new File(outputDirectory, entry.getName());
                        if (entry.isDirectory())
                        {
                            entryDestination.mkdirs();
                        }
                        else
                        {
                            entryDestination.getParentFile().mkdirs();
                            InputStream in = zipFile.getInputStream(entry);
                            OutputStream out = new FileOutputStream(entryDestination);
                            IOUtils.copy(in, out);
                            IOUtils.closeQuietly(in);
                            out.close();
                        }
                    }
                }
                finally
                {
                    zipFile.close();
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        else
        {
            try
            {
                if (!CopyFileFromSourceToDestination(downloadLocation + "\\" + downloadedFileName, outputDirectory + "\\" + downloadedFileName))
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        try
        {
            File fileToDelete = new File(downloadFilePath + downloadedFileName);
            if (!SeleniumDriverInstance.removeDirectory(fileToDelete))
            {
                return false;
            }
        }
        catch (Exception e)
        {
            return false;
        }

        return true;
    }

    public String DownloadFileToReportFolder2(boolean extract, String downloadButtonXpath, final String partialFileName, String outputDirectory, String downloadFilePath)
    {
        if (downloadButtonXpath != "")
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(downloadButtonXpath))
            {
                return "Failed to wait for the download link";
            }

            if (!SeleniumDriverInstance.clickElementByXpath(downloadButtonXpath))
            {
                return "Failed to click on the download link";
            }
        }
                    SeleniumDriverInstance.pause(10000);
           if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
            {
              try{

                  if(downloadButtonXpath.contains("Download")|| downloadButtonXpath.contains("apply") || downloadButtonXpath.contains("ok"))
                  {
                     clickAndSaveFileIE(downloadButtonXpath);
                  }
                  else
                  {
                    clickAndSaveFileIE1(downloadButtonXpath);
                  }
              }
              catch(Exception e)
              {

              }  
            }
           else
           {
                SeleniumDriverInstance.pause(6000);
                String parentHandle = SeleniumDriverInstance.Driver.getWindowHandle();
                try
                {
                    String browser = ApplicationConfig.SelectedBrowser().toString();

                    if (!browser.equalsIgnoreCase("FireFox"))
                    {
                        while (parentHandle.equalsIgnoreCase(SeleniumDriverInstance.Driver.getWindowHandle()))
                        {
                            for (String winHandle : SeleniumDriverInstance.Driver.getWindowHandles())
                            {
                                SeleniumDriverInstance.Driver.switchTo().window(winHandle);
                            }
                        }
                        SeleniumDriverInstance.Driver.close();
                        SeleniumDriverInstance.Driver.switchTo().window(parentHandle);
                    }
                }
                catch (Exception e)
                {
                    return "Failed to switch back to the parent window";
                }
           }
        boolean saved = false;
        int count = 0;
        String downloadedFileName = "";
        String downloadLocation = "";
        String csvFileName = "";

        while (count < 5 && saved == false)
        {
            downloadLocation = new File(System.getProperty("user.home") + "/Downloads/").toString();
            File dir = new File(downloadLocation);
            try
            {
                String fileName = "";
                if (dir.isDirectory())
                {
                    File[] children = dir.listFiles();
                    if (null != children)
                    {
                        for (int i = 0; i < children.length; i++)
                        {
                            fileName = children[i].getName();
                            if (fileName.contains(partialFileName))
                            {
                                downloadedFileName = fileName;
                                saved = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return "";
            }
            if (!saved)
            {
                SeleniumDriverInstance.pause(2000);
                count++;
            }
        }

        if (extract)
        {

            SeleniumDriverInstance.pause(2000);
            try
            {
                ZipFile zipFile = new ZipFile(downloadLocation + "\\" + downloadedFileName);
                try
                {
                    Enumeration<? extends ZipEntry> entries = zipFile.entries();
                    while (entries.hasMoreElements())
                    {
                        ZipEntry entry = entries.nextElement();
                        File entryDestination = new File(outputDirectory, entry.getName());
                        if (entry.isDirectory())
                        {
                            entryDestination.mkdirs();
                        }
                        else
                        {
                            entryDestination.getParentFile().mkdirs();
                            InputStream in = zipFile.getInputStream(entry);
                            OutputStream out = new FileOutputStream(entryDestination);
                            IOUtils.copy(in, out);
                            IOUtils.closeQuietly(in);
                            out.close();
                        }
                    }
                }
                finally
                {
                    zipFile.close();
                }
            }
            catch (Exception ex)
            {
                return "Failed to unzip the file" + ex;
            }
        }
        else
        {
            try
            {
                if (!CopyFileFromSourceToDestination(downloadLocation + "\\" + downloadedFileName, outputDirectory + "\\" + downloadedFileName))
                {
                    return "Failed to copy the file to the report directory";
                }
            }
            catch (Exception e)
            {
                return "Failed to copy the file to the report directory";
            }
        }

        try
        {
            File fileToDelete = new File(downloadFilePath + downloadedFileName);
            if (!SeleniumDriverInstance.removeDirectory(fileToDelete))
            {
                return "Failed to delete the zip file";
            }
        }
        catch (Exception e)
        {
            return "Failed to delete the zip file";
        }

        return "";
    }

    public String[][] CSVToArrayReader(String csvFilePath)
    {
        String[][] CSVData = null;

        try
        {
            CSVReader csvReader = new CSVReader(new FileReader(new File(csvFilePath)));
            List<String[]> list = csvReader.readAll();
            CSVData = new String[list.size()][];
            CSVData = list.toArray(CSVData);
        }
        catch (Exception e)
        {
            return null;
//            return "Could not read and store the CSV data";
        }

        if (CSVData.equals(null))
        {
            return null;
//            return "Failed to read the CSV contents - no data was found in the CSV";
        }

        return CSVData;
    }

    public boolean CSVWriter(String[][] CSVData, String outputDirectory, String outputFileName)
    {
        try
        {
            String[] toWrite = null;
            for (int i = 0; i < CSVData.length; i++)
            {
                String temp = "";
                for (int j = 0; j < CSVData[i].length; j++)
                {
                    temp += CSVData[i][j];

                    if (j + 1 != CSVData[i].length)
                    {
                        temp += ",";
                    }
                }
                toWrite = temp.split(",");
                String output = outputDirectory + "\\" + outputFileName;
                CSVWriter writer = new CSVWriter(new FileWriter(output, true));
                writer.writeNext(toWrite);
                writer.close();
            }
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }

    /**
     * Write 2D array into a CSV file and save it into a specific directory
     *
     * @param CSVData Requires 2D Array containing data for CSV
     * @param outputDirectory Requires output directory path
     * @param outputFileName Requires the new CSV filename with the '.csv'
     * extension format
     * @return returns whether file has been successfully created or not
     */
    public boolean CSVWriterNoQuote(String[][] CSVData, String outputDirectory, String outputFileName)
    {
        try
        {
            String[] toWrite;
            for (String[] CSVData1 : CSVData)
            {
                String temp = "";
                for (int j = 0; j < CSVData1.length; j++)
                {
                    temp += CSVData1[j];
                    if (j + 1 != CSVData1.length)
                    {
                        temp += ",";
                    }
                }
                toWrite = temp.split(",");
                String output = outputDirectory + "\\" + outputFileName;
                try (CSVWriter writer = new CSVWriter(new FileWriter(output, true), ',', CSVWriter.NO_QUOTE_CHARACTER))
                {
                    writer.writeNext(toWrite);
                }
            }
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }

    public String CSVValidation(int frontEndColumn, int CSVColumn, String[][] CSVData, String columnXpath, boolean frameSwitch, String frameOne, String frameTwo)
    {
        if (frameSwitch)
        {
            if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(frameOne))
            {
                return "Failed to switch to the inner sintelligent frame '" + frameOne + "'";
            }

            if (!SeleniumDriverInstance.switchToFrameById(frameTwo))
            {
                return "Failed to switch to the inner sintelligent frame '" + frameTwo + "'";
            }
        }

        ArrayList CSVTempColumn = new ArrayList();
        try
        {
            for (int i = 1; i < CSVData.length; i++)
            {
                String temp = CSVData[i][CSVColumn];
                CSVTempColumn.add(temp);
            }
        }
        catch (Exception e)
        {
            return "Failed to read the CSV column '" + CSVColumn + "'s data";
        }

        List<WebElement> frontEndColumnItems;

        try
        {
            frontEndColumnItems = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
        }
        catch (Exception e)
        {
            return "Failed to extract the front end column " + frontEndColumn + "";
        }

        if (frontEndColumnItems.isEmpty())
        {
            return "Failed to extract the front end column";
        }

        ArrayList frontEndTempColumn = new ArrayList();

        for (int i = 0; i < frontEndColumnItems.size(); i++)
        {
            frontEndTempColumn.add(frontEndColumnItems.get(i).getText().toString());
        }

        if (frontEndTempColumn.size() != CSVTempColumn.size())
        {
            return "The size of items in column " + frontEndColumn + " on the front end does not equal the amount found in the CSV, front end size - '" + frontEndTempColumn.size() + "', CSV size - '" + CSVTempColumn.size() + "'";
        }

        for (int i = 0; i < frontEndTempColumn.size(); i++)
        {
            String tempCSV = CSVTempColumn.get(i).toString().trim();
            String tempFrontEnd = frontEndTempColumn.get(i).toString().trim();
            if (!tempCSV.equalsIgnoreCase(tempFrontEnd))
            {
                return "Failed to validate the CSV contained the same data as the front end Column: " + frontEndColumn + " Front End: " + frontEndTempColumn.toString() + " - CSV: " + CSVTempColumn.toString();
            }
        }

        return "";
    }

    public String FullExportCSVValidation(int frontEndColumn, int CSVColumn, String[][] CSVData, String columnXpath, boolean frameSwitch, String frameOne, String frameTwo)
    {
        if (frameSwitch)
        {
            if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(frameOne))
            {
                return "Failed to switch to the inner sintelligent frame '" + frameOne + "'";
            }

            if (!SeleniumDriverInstance.switchToFrameById(frameTwo))
            {
                return "Failed to switch to the inner sintelligent frame '" + frameTwo + "'";
            }
        }

        ArrayList CSVTempColumn = new ArrayList();
        try
        {
            for (int i = 1; i < CSVData.length; i++)
            {
                String temp = CSVData[i][CSVColumn];
                CSVTempColumn.add(temp);
            }
        }
        catch (Exception e)
        {
            return "Failed to read the CSV column '" + CSVColumn + "'s data";
        }

        List<WebElement> frontEndColumnItems;

        try
        {
            frontEndColumnItems = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
        }
        catch (Exception e)
        {
            return "Failed to extract the front end column " + frontEndColumn + "";
        }

        if (frontEndColumnItems.isEmpty())
        {
            return "Failed to extract the front end column";
        }

        ArrayList frontEndTempColumn = new ArrayList();

        for (int i = 0; i < frontEndColumnItems.size(); i++)
        {
            frontEndTempColumn.add(frontEndColumnItems.get(i).getText().toString());
        }

        if (frontEndTempColumn.size() != CSVTempColumn.size())
        {
            return "The size of items in column " + frontEndColumn + " on the front end does not equal the amount found in the CSV, front end size - '" + frontEndTempColumn.size() + "', CSV size - '" + CSVTempColumn.size() + "'";
        }

        for (int i = 0; i < frontEndTempColumn.size(); i++)
        {
            boolean found = false;
            String tempCSV = CSVTempColumn.get(i).toString().trim();

            for (int j = 0; j < frontEndTempColumn.size(); j++)
            {
                String tempFrontEnd = frontEndTempColumn.get(j).toString().trim();

                if (tempCSV.trim().equalsIgnoreCase(tempFrontEnd.trim()))
                {
                    found = true;
                }
            }

            if (!found)
            {
                return "Failed to validate the CSV contained the same data as the front end Column: " + frontEndColumn + " CSV Data : " + tempCSV + " was not found on - front end Data: " + frontEndTempColumn.toString();
            }

            found = false;
        }

        return "";
    }

    public String pagingMethod()
    {
        //Previous
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("prevpage")))
        {
            return "Failed to wait for the previous page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("prevpage")))
        {
            return "Failed to wait for the previous page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("prevpage")))
        {
            return "Failed to click the previous page button";
        }

        //Next
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("nextpage")))
        {
            return "Failed to wait for the next page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("nextpage")))
        {
            return "Failed to wait for the next page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("nextpage")))
        {
            return "Failed to click the next page button";
        }

        //Last Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("lastpage")))
        {
            return "Failed to wait for the last page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("lastpage")))
        {
            return "Failed to wait for the last page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("lastpage")))
        {
            return "Failed to click the last page button";
        }

        //First Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("firstpage")))
        {
            return "Failed to wait for the first page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("firstpage")))
        {
            return "Failed to wait for the first page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("firstpage")))
        {
            return "Failed to click the first page button";
        }
        return "";
    }

    public String pagingMethod(String filterDropDownXpath, String filterDropDown, String filterSearchBoxXpath, String filterSearchText, String searchButtonXpath,
            String prevPageXpath, String nextPageXpath, String lastPageXpath, String firstPageXpath, boolean frameSwitching,
            String frameNameOne, String innerFrameNameOne, String frameNameTwo, String innerFrameNameTwo)
    {
        if (frameSwitching)
        {
            if (!this.switchToInnerSintelligentFrame(frameNameOne, innerFrameNameOne))
            {
                return "Failed to switch to the sintelligent iframe :'" + frameNameOne + "' and frame :'" + innerFrameNameOne + "'";
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterDropDownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterDropDownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(filterDropDownXpath, filterDropDown))
        {
            return "Failed to select Value '" + filterDropDown + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterSearchBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(filterSearchBoxXpath, filterSearchText))
        {
            return "Failed to enter '" + filterSearchText + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        if (frameSwitching)
        {
            if (!this.switchToInnerSintelligentFrame(frameNameTwo, innerFrameNameTwo))
            {
                return "Failed to switch to the sintelligent iframe :'" + frameNameTwo + "' and frame :'" + innerFrameNameTwo + "'";
            }
        }

        //Previous
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(prevPageXpath))
        {
            return "Failed to wait for the previous page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(prevPageXpath))
        {
            return "Failed to wait for the previous page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(prevPageXpath))
        {
            return "Failed to click the previous page button";
        }

        //Next
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(nextPageXpath))
        {
            return "Failed to wait for the next page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(nextPageXpath))
        {
            return "Failed to wait for the next page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(nextPageXpath))
        {
            return "Failed to click the next page button";
        }

        //First Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(firstPageXpath))
        {
            return "Failed to wait for the first page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(firstPageXpath))
        {
            return "Failed to wait for the first page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(firstPageXpath))
        {
            return "Failed to click the first page button";
        }

        //Last Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(lastPageXpath))
        {
            return "Failed to wait for the last page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(lastPageXpath))
        {
            return "Failed to wait for the last page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(lastPageXpath))
        {
            return "Failed to click the last page button";
        }

        return "";
    }

    public String pagingMethodWithWait()
    {
        //Previous
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("prevpage")))
        {
            return "Failed to wait for the previous page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("prevpage")))
        {
            return "Failed to wait for the previous page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("prevpage")))
        {
            return "Failed to click the previous page button";
        }

        String firstWait = WaitForLoaderIconNotVisible();
        if (!firstWait.equals(""))
        {
            return firstWait;
        }

        //Next
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("nextpage")))
        {
            return "Failed to wait for the next page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("nextpage")))
        {
            return "Failed to wait for the next page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("nextpage")))
        {
            return "Failed to click the next page button";
        }

        String secondWait = WaitForLoaderIconNotVisible();
        if (!secondWait.equals(""))
        {
            return secondWait;
        }

        //Last Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("lastpage")))
        {
            return "Failed to wait for the last page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("lastpage")))
        {
            return "Failed to wait for the last page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("lastpage")))
        {
            return "Failed to click the last page button";
        }

        String finalWait = WaitForLoaderIconNotVisible();
        if (!finalWait.equals(""))
        {
            return finalWait;
        }

        //First Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.TdWithId("firstpage")))
        {
            return "Failed to wait for the first page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("firstpage")))
        {
            return "Failed to wait for the first page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("firstpage")))
        {
            return "Failed to click the first page button";
        }
        return "";
    }

    public String sortingMethod(String filterDropDownXpath, String filterDropDown, String filterSearchBoxXpath, String filterSearchText, String searchButtonXpath,
            String columnXpath, String columnIndex, String columHeaderXpath, String type)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(filterDropDownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(filterDropDownXpath, filterDropDown))
        {
            return "Failed to select Value '" + filterDropDown + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterSearchBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(filterSearchBoxXpath, filterSearchText))
        {
            return "Failed to enter '" + filterSearchText + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.stableClickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        if (!SeleniumDriverInstance.stableClickElementByXpath(columHeaderXpath))
        {
            return "Failed to sort the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            ArrayList list = new ArrayList();
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                list.add(values.get(i).getText().toString().toLowerCase());
            }

            if (list.size() != values.size())
            {
                return "Failed to change the front end list to a alphabetical list - front end list size did not match Expected: " + values.size() + " - Actual: " + list.size();
            }

            boolean sorted = false;

            if (type.equalsIgnoreCase("asc"))
            {
                sorted = Ordering.natural().isOrdered(list);
            }
            else if (type.equalsIgnoreCase("desc"))
            {
                sorted = Ordering.natural().reverse().isOrdered(list);
            }

            if (!sorted)
            {
                return "Failed to validate the list for row index " + columnIndex + " was in reverse-alphabetical order";
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values and validate the columns where in " + type + " order";
        }

        return "";
    }

    public String sortingMethodFrameSwitchingWithWaits(String filterDropDownXpath, String filterDropDown, String filterSearchBoxXpath, String filterSearchText, String searchButtonXpath,
            String columnXpath, String columnIndex, String columHeaderXpath, String type, boolean frameSwitching,
            String frameOne, String innerFrameOne, String frameTwo, String innerFrameTwo)
    {
        if (frameSwitching)
        {
            if (frameOne.length() > 0)
            {
                if (!this.switchToInnerSintelligentFrame(frameOne))
                {
                    return "Failed to switch to the sintelligent frame one";
                }
            }

            if (innerFrameOne.length() > 0)
            {
                if (!this.switchToFrameById(innerFrameOne))
                {
                    return "Failed to switch to the inner sintelligent frame one";
                }
            }
        }
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
          SeleniumDriverInstance.pause(3000);
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterDropDownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(filterDropDownXpath, filterDropDown))
        {
            return "Failed to select Value '" + filterDropDown + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterSearchBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(filterSearchBoxXpath, filterSearchText))
        {
            return "Failed to enter '" + filterSearchText + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }
        boolean isLoading = false;
        isLoading = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Loading..."), 1);
        while (isLoading)
        {
            isLoading = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Loading..."), 1);
            this.pause(100);
        }
        if (frameSwitching)
        {
            if (frameTwo.length() > 0)
            {
                if (!this.switchToInnerSintelligentFrame(frameTwo))
                {
                    return "Failed to switch to the sintelligent frame two";
                }
            }

            if (innerFrameTwo.length() > 0)
            {
                if (!this.switchToFrameById(innerFrameTwo))
                {
                    return "Failed to switch to the inner sintelligent frame two";
                }
            }
        }

        if (!SeleniumDriverInstance.stableClickElementByXpath(columHeaderXpath))
        {
            return "Failed to sort the '" + columnIndex + "' column";
        }
        isLoading = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Loading..."), 1);
        while (isLoading)
        {
            isLoading = SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.SpanContainsTextXpath("Loading..."), 1);
            this.pause(100);
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(columnXpath))
        {
            return "Failed to wait for the column list";
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(columnXpath))
        {
            return "Failed to wait for the column list to be visible";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            ArrayList list = new ArrayList();
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                list.add(values.get(i).getText().toString().toLowerCase());
            }

            if (list.size() != values.size())
            {
                return "Failed to change the front end list to a alphabetical list - front end list size did not match Expected: " + values.size() + " - Actual: " + list.size();
            }

            boolean sorted = false;

            if (type.equalsIgnoreCase("asc"))
            {
                sorted = Ordering.natural().isOrdered(list);
            }
            else if (type.equalsIgnoreCase("desc"))
            {
                sorted = Ordering.natural().reverse().isOrdered(list);
            }

            if (!sorted)
            {
                return "Failed to validate the list for row index " + columnIndex + " was in reverse-alphabetical order";
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values and validate the columns where in " + type + " order";
        }

        return "";
    }

    public String sortingMethodFrameSwitchingWithLoaderIconWait(String filterDropDownXpath, String filterDropDown, String filterSearchBoxXpath, String filterSearchText, String searchButtonXpath,
            String columnXpath, String columnIndex, String columHeaderXpath, String type, boolean frameSwitching,
            String frameOne, String innerFrameOne, String frameTwo, String innerFrameTwo)
    {
        if (frameSwitching)
        {
            if (frameOne.length() > 0)
            {
                if (!this.switchToInnerSintelligentFrame(frameOne))
                {
                    return "Failed to switch to the sintelligent frame one";
                }
            }

            if (innerFrameOne.length() > 0)
            {
                if (!this.switchToFrameById(innerFrameOne))
                {
                    return "Failed to switch to the inner sintelligent frame one";
                }
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterDropDownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(filterDropDownXpath, filterDropDown))
        {
            return "Failed to select Value '" + filterDropDown + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterSearchBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(filterSearchBoxXpath, filterSearchText))
        {
            return "Failed to enter '" + filterSearchText + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        String firstWait = WaitForLoaderIconNotVisible();
        if (!firstWait.equals(""))
        {
            return firstWait;
        }
        if (frameSwitching)
        {
            if (frameTwo.length() > 0)
            {
                if (!this.switchToInnerSintelligentFrame(frameTwo))
                {
                    return "Failed to switch to the sintelligent frame two";
                }
            }

            if (innerFrameTwo.length() > 0)
            {
                if (!this.switchToFrameById(innerFrameTwo))
                {
                    return "Failed to switch to the inner sintelligent frame two";
                }
            }
        }

        firstWait = WaitForLoaderIconNotVisible();
        if (!firstWait.equals(""))
        {
            return firstWait;
        }

        if (!SeleniumDriverInstance.clickElementByXpath(columHeaderXpath))
        {
            return "Failed to sort the '" + columnIndex + "' column";
        }

        firstWait = WaitForLoaderIconNotVisible();
        if (!firstWait.equals(""))
        {
            return firstWait;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(columnXpath))
        {
            return "Failed to wait for the column list";
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(columnXpath))
        {
            return "Failed to wait for the column list to be visible";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            ArrayList list = new ArrayList();
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                list.add(values.get(i).getText().toString().toLowerCase());
            }

            if (list.size() != values.size())
            {
                return "Failed to change the front end list to a alphabetical list - front end list size did not match Expected: " + values.size() + " - Actual: " + list.size();
            }

            boolean sorted = false;

            if (type.equalsIgnoreCase("asc"))
            {
                sorted = Ordering.natural().isOrdered(list);
            }
            else if (type.equalsIgnoreCase("desc"))
            {
                sorted = Ordering.natural().reverse().isOrdered(list);
            }

            if (!sorted)
            {
                return "Failed to validate the list for row index " + columnIndex + " was in reverse-alphabetical order";
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values and validate the columns where in " + type + " order";
        }

        return "";
    }

    /**
     * Used to sorted grids
     *
     * @param filterDropDownXpath requires drop down Xpath
     * @param filterDropDown requires filter drop option to select
     * @param filterSearchBoxXpath requires search text
     * @param filterSearchText requires search text box Xpath
     * @param searchButtonXpath requires search button Xpath
     * @param columnXpath requires current column xpath
     * @param columnIndex requires column index
     * @param columHeaderXpath requires column headers
     * @param type requires sorting eg: Asc or desc
     * @param frameSwitching boolean stating in frame switching is required
     * @param frameNameOne Main frame 1
     * @param innerFrameNameOne inner main frame 1
     * @param frameNameTwo mainframe 2
     * @param innerFrameNameTwo inner main frame 2
     * @return
     */
    public String sortingMethodFrameSwitchingByFrameName(String filterDropDownXpath, String filterDropDown, String filterSearchBoxXpath, String filterSearchText, String searchButtonXpath,
            String columnXpath, String columnIndex, String columHeaderXpath, String type, boolean frameSwitching,
            String frameNameOne, String innerFrameNameOne, String frameNameTwo, String innerFrameNameTwo)
    {
        if (frameSwitching)
        {

            if (!this.switchToInnerSintelligentFrame(frameNameOne, innerFrameNameOne))
            {
                return "Failed to switch to the sintelligent iframe :'" + frameNameOne + "' and frame :'" + innerFrameNameOne + "'";
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterDropDownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterDropDownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(filterDropDownXpath, filterDropDown))
        {
            return "Failed to select Value '" + filterDropDown + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterSearchBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(filterSearchBoxXpath, filterSearchText))
        {
            return "Failed to enter '" + filterSearchText + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        if (frameSwitching)
        {
            if (!this.switchToInnerSintelligentFrame(frameNameTwo, innerFrameNameTwo))
            {
                return "Failed to switch to the sintelligent iframe :'" + frameNameTwo + "' and frame :'" + innerFrameNameTwo + "'";
            }
        }
        //Added stability
        if (!SeleniumDriverInstance.waitForElementByXpath(columHeaderXpath))
        {
            return "Failed to wait for '" + columnIndex + "' column";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(columHeaderXpath))
        {
            return "Failed to wait for '" + columnIndex + "' column to be clickable";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(columHeaderXpath))
        {
            return "Failed to sort the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            ArrayList list = new ArrayList();
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                list.add(values.get(i).getText().toString().toLowerCase());
            }

            if (list.size() != values.size())
            {
                return "Failed to change the front end list to a alphabetical list - front end list size did not match Expected: " + values.size() + " - Actual: " + list.size();
            }

            boolean sorted = false;

            if (type.equalsIgnoreCase("asc"))
            {
                sorted = Ordering.natural().isOrdered(list);
            }
            else if (type.equalsIgnoreCase("desc"))
            {
                sorted = Ordering.natural().reverse().isOrdered(list);
            }

            if (!sorted)
            {
                return "Failed to validate the list for row index " + columnIndex + " was in reverse-alphabetical order";
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values and validate the columns where in " + type + " order";
        }

        return "";
    }

    public String sortingMethodWithWait(String filterDropDownXpath, String filterDropDown, String filterSearchBoxXpath, String filterSearchText, String searchButtonXpath,
            String columnXpath, String columnIndex, String columHeaderXpath, String type)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(filterDropDownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(filterDropDownXpath, filterDropDown))
        {
            return "Failed to select Value '" + filterDropDown + "' from the Filter dropdown";
        }
        String selectWait = WaitForLoaderIconNotVisible();
        if (!selectWait.equals(""))
        {
            return selectWait;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(filterSearchBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(filterSearchBoxXpath, filterSearchText))
        {
            return "Failed to enter '" + filterSearchText + "' into the filter search text box";
        }
        String filterWait = WaitForLoaderIconNotVisible();
        if (!filterWait.equals(""))
        {
            return filterWait;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }
        String firstWait = WaitForLoaderIconNotVisible();
        if (!firstWait.equals(""))
        {
            return firstWait;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(columHeaderXpath))
        {
            return "Failed to sort the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            ArrayList list = new ArrayList();
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                list.add(values.get(i).getText().toString().toLowerCase());
            }

            if (list.size() != values.size())
            {
                return "Failed to change the front end list to a alphabetical list - front end list size did not match Expected: " + values.size() + " - Actual: " + list.size();
            }

            boolean sorted = false;

            if (type.equalsIgnoreCase("asc"))
            {
                sorted = Ordering.natural().isOrdered(list);
            }
            else if (type.equalsIgnoreCase("desc"))
            {
                sorted = Ordering.natural().reverse().isOrdered(list);
            }

            if (!sorted)
            {
                return "Failed to validate the list for row index " + columnIndex + " was in reverse-alphabetical order";
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values and validate the columns where in " + type + " order";
        }

        return "";
    }

    public String sortingMethodFrameSwitchingByFrameNameWithWait(String filterDropDownXpath, String filterDropDown, String filterSearchBoxXpath, String filterSearchText, String searchButtonXpath,
            String columnXpath, String columnIndex, String columHeaderXpath, String type, boolean frameSwitching,
            String frameNameOne, String innerFrameNameOne, String frameNameTwo, String innerFrameNameTwo)
    {
        if (frameSwitching)
        {

            if (!this.switchToInnerSintelligentFrame(frameNameOne, innerFrameNameOne))
            {
                return "Failed to switch to the sintelligent iframe :'" + frameNameOne + "' and frame :'" + innerFrameNameOne + "'";
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterDropDownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterDropDownXpath))
        {
            return "Failed to wait for the Filter dropdown";
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(filterDropDownXpath, filterDropDown))
        {
            return "Failed to select Value '" + filterDropDown + "' from the Filter dropdown";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(filterSearchBoxXpath))
        {
            return "Failed to wait for the filter search box";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(filterSearchBoxXpath, filterSearchText))
        {
            return "Failed to enter '" + filterSearchText + "' into the filter search text box";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(searchButtonXpath))
        {
            return "Failed to wait for the filter search button";
        }

        if (!SeleniumDriverInstance.clickElementByXpath(searchButtonXpath))
        {
            return "Failed to click the filter search button";
        }

        if (frameSwitching)
        {
            if (!this.switchToInnerSintelligentFrame(frameNameTwo, innerFrameNameTwo))
            {
                return "Failed to switch to the sintelligent iframe :'" + frameNameTwo + "' and frame :'" + innerFrameNameTwo + "'";
            }
        }
        //Added stability
        if (!SeleniumDriverInstance.waitForElementByXpath(columHeaderXpath))
        {
            return "Failed to wait for '" + columnIndex + "' column";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(columHeaderXpath))
        {
            return "Failed to wait for '" + columnIndex + "' column to be clickable";
        }

        String finalWait = WaitForLoaderIconNotVisible();
        if (!finalWait.equals(""))
        {
            return finalWait;
        }

        if (!SeleniumDriverInstance.clickElementByXpath(columHeaderXpath))
        {
            return "Failed to sort the '" + columnIndex + "' column";
        }

        try
        {
            List<WebElement> values = SeleniumDriverInstance.Driver.findElements(By.xpath(columnXpath));
            ArrayList list = new ArrayList();
            if (values.isEmpty())
            {
                return "Failed to extract the '" + columnIndex + "' column data";
            }

            for (int i = 0; i < values.size(); i++)
            {
                list.add(values.get(i).getText().toString().toLowerCase());
            }

            if (list.size() != values.size())
            {
                return "Failed to change the front end list to a alphabetical list - front end list size did not match Expected: " + values.size() + " - Actual: " + list.size();
            }

            boolean sorted = false;

            if (type.equalsIgnoreCase("asc"))
            {
                sorted = Ordering.natural().isOrdered(list);
            }
            else if (type.equalsIgnoreCase("desc"))
            {
                sorted = Ordering.natural().reverse().isOrdered(list);
            }

            if (!sorted)
            {
                return "Failed to validate the list for row index " + columnIndex + " was in reverse-alphabetical order";
            }
        }
        catch (Exception e)
        {
            return "Failed to extract the column '" + columnIndex + "''s values and validate the columns where in " + type + " order";
        }

        return "";
    }

    public boolean SintelligentMainMenuFilterDropDownSearch(String searchDropDownSelect, String searchTextFieldEnter, String returnError)
    {
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility((SintelligentPageObject.MainSubnetsFilter())))
        {
            returnError = "Could not wait for '" + searchDropDownSelect + "' from the filter dropwdown to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath((SintelligentPageObject.MainSubnetsFilter())))
        {
            returnError = "Could not wait for '" + searchDropDownSelect + "' from the filter dropwdown to be clickable";
            return false;
        }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath((SintelligentPageObject.MainSubnetsFilter()), searchDropDownSelect))
        {
            returnError = "Could not select '" + searchDropDownSelect + "' from the filter dropwdown";
            return false;
        }

        //enter 'searchTextFieldEnter' [test pack] into filter field
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility((SintelligentPageObject.MainSubnetsFilterTextField())))
        {
            returnError = "Could not wait for filter text field to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath((SintelligentPageObject.MainSubnetsFilterTextField())))
        {
            returnError = "Could not wait for filter text field to be clickable";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath((SintelligentPageObject.MainSubnetsFilterTextField()), searchTextFieldEnter))
        {
            returnError = "Could not enter '" + searchTextFieldEnter + "' into the filter text box";
            return false;
        }

        //click filter
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.ImgWithIdXpath("btnGo")))
        {
            returnError = "Could not wait for 'Filter' button to become visible";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgWithIdXpath("btnGo")))
        {
            returnError = "Could not wait for 'Filter' button to become clickable";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgWithIdXpath("btnGo")))
        {
            returnError = "Could not click 'Filter' button";
            return false;
        }

        //no dynamic elements to wait for
        this.pause(300);

        return true;
    }

    public boolean removeDirectory(File dir)
    {
        try
        {
            if (dir.isDirectory())
            {
                File[] files = dir.listFiles();

                if (files != null && files.length > 0)
                {
                    for (File aFile : files)
                    {
                        removeDirectory(aFile);
                    }
                }
                dir.delete();
            }
            else
            {
                dir.delete();
            }
            return true;
        }
        catch (Exception ex)
        {
            System.err.println("[Error] Fail to delete file - " + ex.getMessage());
            return false;
        }
    }
    public boolean cleanDownloadsDirectory()
    {
         String zipfilePath =  System.getProperty("user.home") + "\\Downloads\\";      

        try
        {
        File downloads = new File(zipfilePath);
        FileUtils.cleanDirectory(downloads);
                }
                
        catch (Exception e)
        {
             
            return false;
//            return "Failed to delete '"+downloadedFileName+"' file";
        }
        return true;
    }
    public File getTheNewestFile(String ext)
    {
        try
        {

            SeleniumDriverInstance.pause(10000);
            String file = new File(System.getProperty("user.home") + "/Downloads/").toString();

            File theNewestFile = null;
            File dir = new File(file);
            FileFilter fileFilter = new WildcardFileFilter("*." + ext);
            File[] files = dir.listFiles(fileFilter);

            if (files.length > 0)
            {
                // The newest file comes first /
                Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
                theNewestFile = files[0];
            }
            return theNewestFile;
        }
        catch (Exception ex)
        {
            System.err.println("[Error] Fail to get The Newest File - " + ex.getMessage());
            return null;
        }
    }

    public String getTheNewestFilePathString(String ext)
    {
        try
        {
            String parentHandle = SeleniumDriverInstance.Driver.getWindowHandle(); // get the current window handle

            //wait for download to complete
            this.pause(5000);
            String file = new File(System.getProperty("user.home") + "/Downloads/").toString();

            File theNewestFile = null;
            File dir = new File(file);
            String temp = "*." + ext;
            FileFilter fileFilter = new WildcardFileFilter(temp);
            File[] files = dir.listFiles(fileFilter);

            if (files.length > 0)
            {
                // The newest file comes first /
                Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
                theNewestFile = files[0];
            }

            //close popup if chrome
            if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.Chrome)
            {
                //switch window
                int tryCount = 0;
                while ((parentHandle.equalsIgnoreCase(SeleniumDriverInstance.Driver.getWindowHandle()) && tryCount != 10))
                {
                    for (String winHandle : SeleniumDriverInstance.Driver.getWindowHandles())
                    {
                        SeleniumDriverInstance.Driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
                        tryCount++;
                    }
                }

                SeleniumDriverInstance.Driver.close();//close pop up window

                SeleniumDriverInstance.Driver.switchTo().window(parentHandle); // switch back to the original window
            }
            String returnFile = theNewestFile.toString().replace(System.getProperty("user.home") + "/Downloads/", "");
            return returnFile;
        }
        catch (Exception ex)
        {
            System.err.println("[Error] Fail to get The Newest File - " + ex.getMessage());
            return null;
        }
    }

    public String getTheNewestFilePathStringNew(String enclosingFolderPath, String ext, String isDownload)
    {
        try
        {
            String parentHandle = SeleniumDriverInstance.Driver.getWindowHandle(); // get the current window handle

            //wait for download to complete (if download)
            if (isDownload == "Yes")
            {
                this.pause(5000);
            }

            String file = new File(enclosingFolderPath).toString();

            File theNewestFile = null;
            File dir = new File(file);
            String temp = "*." + ext;
            FileFilter fileFilter = new WildcardFileFilter(temp);
            File[] files = dir.listFiles(fileFilter);

            if (files.length > 0)
            {
                // The newest file comes first /
                Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
                theNewestFile = files[0];
            }

            if (isDownload == "Yes")
            {
                //close popup if chrome
                if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.Chrome)
                {
                    //switch window
                    int tryCount = 0;
                    while ((parentHandle.equalsIgnoreCase(SeleniumDriverInstance.Driver.getWindowHandle()) && tryCount != 10))
                    {
                        for (String winHandle : SeleniumDriverInstance.Driver.getWindowHandles())
                        {
                            SeleniumDriverInstance.Driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
                            tryCount++;
                        }
                    }

                    SeleniumDriverInstance.Driver.close();//close pop up window

                    SeleniumDriverInstance.Driver.switchTo().window(parentHandle); // switch back to the original window

                }
            }

            String returnFile = theNewestFile.toString().replace(System.getProperty("user.home") + "/Downloads/", "");
            return returnFile;
        }
        catch (Exception ex)
        {
            System.err.println("[ERROR] - Failed to get The Newest File - " + ex.getMessage());
            return null;
        }
    }

    public boolean checkElementIsDisplayedById(String elementId)
    {
        try
        {

            Narrator.logDebug("Attempting to check element IsDisplayed by ID - " + elementId);
            // waitForElementById(elementId);
            //Pause is needed in some cases, do not remove
            SeleniumDriverInstance.pause(1000);
            WebElement elementToCheck = Driver.findElement(By.id(elementId));
            elementToCheck.isDisplayed();
            Narrator.logDebug("Element checked successfully...proceeding");
            return true;
        }
        catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean checkElementIsDisplayedByXpath(String elementXpath)
    {
        try
        {

            Narrator.logDebug("Attempting to check element IsDisplayed by xpath - " + elementXpath);
            //Pause is needed in some cases, do not remove
            waitForElementByXpath(elementXpath, 5);
            WebElement elementToCheck = Driver.findElement(By.xpath(elementXpath));
            elementToCheck.isDisplayed();
            Narrator.logDebug("Element checked successfully...proceeding");
            return true;
        }
        catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean TestSintelligentModalFramePagers(String returnError)
    {
        //check the pager buttons are clickable
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("nextpage")))
        {
            returnError = "Could not wait for 'next page' pager button to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("nextpage")))
        {
            returnError = "Could not click 'next page' pager";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("lastpage")))
        {
            returnError = "Could not wait for 'last page' pager button to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("lastpage")))
        {
            returnError = "Could not click 'last page' pager";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("firstpage")))
        {
            returnError = "Could not wait for 'first page' pager button to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("firstpage")))
        {
            returnError = "Could not click 'first page' pager";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.TdWithId("prevpage")))
        {
            returnError = "Could not wait for 'prev page' pager button to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.TdWithId("prevpage")))
        {
            returnError = "Could not click 'prev page' pager";
            return false;
        }

        return true;
    }

    public boolean SintelligentPageButtonsTest(String returnError)//Uses ImgContainsSrc Objects
    {
        //check the pager buttons are clickable
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgContainsSrc("next")))
        {
            returnError = "Could not wait for 'next page' pager button to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgContainsSrc("next")))
        {
            returnError = "Could not click 'next page' pager";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgContainsSrc("last")))
        {
            returnError = "Could not wait for 'last page' pager button to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgContainsSrc("last")))
        {
            returnError = "Could not click 'last page' pager";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgContainsSrc("previous")))
        {
            returnError = "Could not wait for 'prev page' pager button to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgContainsSrc("previous")))
        {
            returnError = "Could not click 'prev page' pager";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.ImgContainsSrc("first")))
        {
            returnError = "Could not wait for 'first page' pager button to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.ImgContainsSrc("first")))
        {
            returnError = "Could not click 'first page' pager";
            return false;
        }

        return true;
    }
    public boolean CompareMultivaluedSintelligentCSVFileWithWebList(List<String> CSVList, List<WebElement> frontEndItemsList, int stringSplitLocation, String returnError)
    {
        //compares the csv file list with the webElements list found on the filtered page
        for (int i = 0; i < CSVList.size(); i++)
        {
            try
            {
                String tempCSVStr = CSVList.get(i).replace("\"", "");
                tempCSVStr = CSVList.get(i).replace("-", "");
                String[] split = tempCSVStr.split(",");

                String tempSearchStr = frontEndItemsList.get(i).getText().replace("\"", "").toString();
                tempSearchStr = tempSearchStr.replace("-", "");
                String tempCompare = split[stringSplitLocation].replace("\"", "");
                if (!tempCompare.contentEquals(tempSearchStr))
                {
                    returnError = "The items in the CSV file  do not match the filtered search";
                    return false;
                }
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
                return false;
            }
        }

        return true;
    }

    public boolean CompareMultivaluedSintelligentCSVFileWithWebListWithoutSpaces(List<String> CSVList, List<WebElement> frontEndItemsList, int stringSplitLocation, String returnError)
    {
        //compares the csv file list with the webElements list found on the filtered page
        for (int i = 0; i < CSVList.size(); i++)
        {
            try
            {
                String tempCSVStr = CSVList.get(i).replace("\"", "");
                tempCSVStr = CSVList.get(i).replace("-", "");
                tempCSVStr = tempCSVStr.replace(" ", "");
                String[] split = tempCSVStr.split(",");

                String tempSearchStr = frontEndItemsList.get(i).getText().replace("\"", "").toString();
                tempSearchStr = tempSearchStr.replace("-", "");
                tempSearchStr = tempSearchStr.replace(" ", "");

                String tempCompare = split[stringSplitLocation].replace("\"", "");
                if (!tempCompare.contentEquals(tempSearchStr))
                {
                    returnError = "The items in the CSV file  do not match the filtered search";
                    return false;
                }
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
                return false;
            }
        }

        return true;
    }

    public String DeleteCSVZipFileFromDownloadLocation(String dowloadedCSVFileIdentifierObject, String returnError)
    {
        String dowloadedCSVName = "";
        try
        {
            dowloadedCSVName = SeleniumDriverInstance.GetTheFileName("", dowloadedCSVFileIdentifierObject);

            if (dowloadedCSVName.equals(""))
            {
                return "";
            }
        }
        catch (Exception e)
        {
            Narrator.logError("[ERROR] - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            returnError = "Could not find  CSV file: '" + dowloadedCSVName + "'";
            return "";
        }

        String downloadedFilePath = System.getProperty("user.home") + "/Downloads/" + dowloadedCSVName;

        File CSV = new File(downloadedFilePath);

        if (!SeleniumDriverInstance.removeDirectory(CSV))
        {
            returnError = "Could not delete the file '" + dowloadedCSVName + "'";
            return "";
        }

        //return deleted file
        return dowloadedCSVName;
    }

    public boolean RefreshSubnetSintelligentFramesToModalIframeId1(String returnError)
    {
        //switch iframes
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            returnError = "Failed to switch to default iFrame.";
            return false;
        }

        //switch to inner iFrame
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.SubnetsFrameXpath()))
        {
            returnError = "Failed to switch to the inner iFrame.";
            return false;
        }

        //switch to ModalIframeId1Frame
        if (!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ModalIframeId1Frame()))
        {
            returnError = "Could not switch to inner " + SintelligentPageObject.ModalIframeId1Frame() + " iframe";
            return false;
        }

        return true;
    }

    public boolean RefreshReportsSintelligentFramesToModalIframeId1(String returnError)
    {
        //switch iframes
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            returnError = "Failed to switch to default iFrame.";
            return false;
        }

        //switch to inner iFrame
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ReportsIframe()))
        {
            returnError = "Failed to switch to the inner iFrame.";
            return false;
        }

        //switch to ModalIframeId1Frame
        if (!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ModalIframeId1Frame()))
        {
            returnError = "Could not switch to inner " + SintelligentPageObject.ModalIframeId1Frame() + " iframe";
            return false;
        }

        return true;
    }

    public boolean RefreshSubnetSintelligentFramesToModalIframeId2(String returnError)
    {
        //switch to inner Sintelligent frame
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            returnError = "Could not switch to the default iframe";
            return false;
        }

        //switch to inner iFrame
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.SubnetsFrameXpath()))
        {
            returnError = "Failed to switch to the inner iFrame.";
            return false;
        }

        //switch to ModalIframeId2Frame
        if (!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ModalIframeId2Frame()))
        {
            returnError = "Could not switch to inner " + SintelligentPageObject.ModalIframeId2Frame() + " iframe";
            return false;
        }

        return true;
    }

    public boolean RefreshReportsSintelligentFramesToModalIframeId2(String returnError)
    {
        //switch to inner Sintelligent frame
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            returnError = "Could not switch to the default iframe";
            return false;
        }

        //switch to inner iFrame
        if (!SeleniumDriverInstance.switchToInnerSintelligentFrame(SintelligentPageObject.ReportsIframe(), "workSpace"))
        {
            returnError = "Failed to switch to the inner iFrame.";
            return false;
        }

        //switch to ModalIframeId2Frame
        if (!SeleniumDriverInstance.switchToFrameById(SintelligentPageObject.ModalIframeId2Frame()))
        {
            returnError = "Could not switch to inner " + SintelligentPageObject.ModalIframeId2Frame() + " iframe";
            return false;
        }

        return true;
    }

    private CookieStore seleniumCookiesToCookieStore()
    {
        try
        {
            Set<Cookie> seleniumCookies = SeleniumDriverInstance.Driver.manage().getCookies();
            CookieStore cookieStore = new BasicCookieStore();
            for (Cookie seleniumCookie : seleniumCookies)
            {
                BasicClientCookie basicClientCookie = new BasicClientCookie(seleniumCookie.getName(), seleniumCookie.getValue());
                basicClientCookie.setDomain(seleniumCookie.getDomain());
                basicClientCookie.setExpiryDate(seleniumCookie.getExpiry());
                basicClientCookie.setPath(seleniumCookie.getPath());
                cookieStore.addCookie(basicClientCookie);
            }
            return cookieStore;
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public boolean downloadFile(String downloadUrl, String outputFilePath)
    {
        try
        {
            CookieStore cookieStore = seleniumCookiesToCookieStore();
            DefaultHttpClient httpClient = new DefaultHttpClient();
            httpClient.setCookieStore(cookieStore);

            HttpGet httpGet = new HttpGet(downloadUrl);
            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();

            if (entity != null)
            {
                File outputFile = new File(outputFilePath);
                InputStream inputStream = entity.getContent();
                FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
                int read = 0;
                byte[] bytes = new byte[1024];
                while ((read = inputStream.read(bytes)) != -1)
                {
                    fileOutputStream.write(bytes, 0, read);
                }
                fileOutputStream.close();
                return true;
            }
            else
            {
                System.out.println("File download failed!");
                return false;
            }
        }
        catch (Exception e)
        {
            System.out.println("File download failed!");
            return false;
        }
    }

    public String SintelligentShiftSelect(String xpathItem1ToSelect, String xpathItem2ToSelect)
    {
        try
        {

            Actions action = new Actions(SeleniumDriverInstance.Driver);

            action.keyDown(Keys.SHIFT).build().perform();
            SeleniumDriverInstance.waitForElementByXpath(xpathItem1ToSelect);
            SeleniumDriverInstance.waitForElementToBeClickableByXpath(xpathItem1ToSelect);
            SeleniumDriverInstance.clickElementByXpath(xpathItem1ToSelect);
            SeleniumDriverInstance.waitForElementByXpath(xpathItem2ToSelect);
            SeleniumDriverInstance.waitForElementToBeClickableByXpath(xpathItem2ToSelect);
            SeleniumDriverInstance.clickElementByXpath(xpathItem2ToSelect);

            action.keyUp(Keys.SHIFT).build().perform();

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return "Could not perform the Shift select ";
        }

        return "";
    }

    public String SintelligentControlSelect(String xpathItem1ToSelect, String xpathItem2ToSelect)
    {
        try
        {

            Actions action = new Actions(SeleniumDriverInstance.Driver);

            action.keyDown(Keys.CONTROL).build().perform();
            SeleniumDriverInstance.waitForElementByXpath(xpathItem1ToSelect);
            SeleniumDriverInstance.waitForElementToBeClickableByXpath(xpathItem1ToSelect);
            SeleniumDriverInstance.clickElementByXpath(xpathItem1ToSelect);
            SeleniumDriverInstance.waitForElementByXpath(xpathItem2ToSelect);
            SeleniumDriverInstance.waitForElementToBeClickableByXpath(xpathItem2ToSelect);
            SeleniumDriverInstance.clickElementByXpath(xpathItem2ToSelect);

            action.keyUp(Keys.CONTROL).build().perform();

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return "Could not perform the Shift select ";
        }

        return "";
    }

    public String SintelligentControlSelectSingle(String xpathItem1ToSelect)
    {
        try
        {

            Actions action = new Actions(SeleniumDriverInstance.Driver);

            action.keyDown(Keys.CONTROL).build().perform();
            SeleniumDriverInstance.waitForElementByXpath(xpathItem1ToSelect);
            SeleniumDriverInstance.waitForElementToBeClickableByXpath(xpathItem1ToSelect);
            SeleniumDriverInstance.clickElementByXpath(xpathItem1ToSelect);

            action.keyUp(Keys.CONTROL).build().perform();

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return "Could not perform the Control select ";
        }

        return "";
    }

    //Copy file from source to destination
    //CopyFileFromSourceToDestination(Source location of file plus file name,destination location plus file name)
    //e.g CopyFileFromSourceToDestination(C:\\folder\\file.csv , C:\\folder\\file.csv )
    public boolean CopyFileFromSourceToDestination(String fromSource, String toDestination)
    {
        try
        {
            File sourceFile = new File(fromSource);
            File destFile = new File(toDestination);
            //Validate the source file
            if (!sourceFile.exists())
            {
                return false;
            }
            //Check if the directory is created if not it will create it
            if (!destFile.exists())
            {
                destFile.createNewFile();
            }
            //Copy the file from source to directory
            FileChannel source = null;
            FileChannel destination = null;
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            if (destination != null && source != null)
            {
                destination.transferFrom(source, 0, source.size());
            }
            //Validate and close source file
            if (source != null)
            {
                source.close();
            }
            //Validate and close destination file
            if (destination != null)
            {
                destination.close();
            }
            return true;
        }
        catch (Exception e)
        {
            System.out.println("Failed to copy file from '" + fromSource + "' to " + toDestination);
            return false;
        }
    }

    //Copy file from source to destination
    //MoveFileFromSourceToDestination(Source location of file plus file name,destination location plus file name)
    //e.g MoveFileFromSourceToDestination(C:\\folder\\file.csv , C:\\folder\\file.csv )
    public boolean MoveFileFromSourceToDestination(String fromSource, String toDestination)
    {
        try
        {
            File sourceFile = new File(fromSource);
            File destFile = new File(toDestination);

            //Validate the source file
            if (!sourceFile.exists())
            {
                SeleniumDriverInstance.pause(1000);
                if (!sourceFile.exists())
                {
                    return false;
                }

            }
            //Check if the directory is created if not it will create it
            if (!destFile.exists())
            {
                destFile.createNewFile();
            }
            //Copy the file from source to directory
            FileChannel source = null;
            FileChannel destination = null;
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            if (destination != null && source != null)
            {
                destination.transferFrom(source, 0, source.size());
            }
            //Validate and close source file
            if (source != null)
            {
                source.close();
            }
            //Validate and close destination file
            if (destination != null)
            {
                destination.close();
            }
            sourceFile.delete();
            return true;
        }
        catch (Exception e)
        {
            System.out.println("Failed to move file from '" + fromSource + "' to " + toDestination);
            return false;
        }
    }

    public List<WebElement> getElementsByXpath(String elementXpath)
    {
        List<WebElement> elementsList = null;
        try
        {
            elementsList = Driver.findElements(By.xpath(elementXpath));
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to retrieve elements by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return null;
        }

        return elementsList;
    }

    /**
     * Initialize record and start recording test.
     */
    public void startRecording()
    {
        if (ApplicationConfig.RecordingTest().equals("true"))
        {
            File file = new File(reportDirectory + "\\\\");

            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

            GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();

            int taskbarheight = Toolkit.getDefaultToolkit().getScreenSize().height - GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height;
            int width = screenSize.width;
            int height = screenSize.height - taskbarheight;

            Rectangle captureSize = new Rectangle(0, 0, width, height);
            Format fileFormat = new Format(MediaTypeKey, FormatKeys.MediaType.FILE, MimeTypeKey, MIME_AVI);
            Format screenFormat = new Format(MediaTypeKey, FormatKeys.MediaType.VIDEO, EncodingKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
                    CompressorNameKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
                    DepthKey, 24, FrameRateKey, Rational.valueOf(15),
                    QualityKey, 1.0f,
                    KeyFrameIntervalKey, 15 * 60);
            Format mouseFormat = new Format(MediaTypeKey, FormatKeys.MediaType.VIDEO, EncodingKey, "White",
                    FrameRateKey, Rational.valueOf(30));
            Format audioFormat = null;
            String fileName = resolveScenarioName().split("_")[0];

            try
            {
                this.screenRecorder = new SpecializedScreenRecorder(gc, captureSize, fileFormat, screenFormat, mouseFormat, audioFormat, file, fileName);
                this.screenRecorder.start();
            }
            catch (Exception e)
            {
                Narrator.logError("Failed to Start the recording - " + e.getMessage());
            }
        }

    }

    /**
     * Stop recording
     */
    public void stopRecording()
    {
        if (ApplicationConfig.RecordingTest().equals("true"))
        {
            try
            {
                this.screenRecorder.stop();
            }
            catch (Exception e)
            {
                Narrator.logError("Failed to Stop the recording - " + e.getMessage());
            }
        }

    }
   public boolean stableClickElementByXpath(String elementXpath, int wait)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(elementXpath,wait))
        {
//			returnError = "Failed to wait for the " + WaitedElement;
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elementXpath,wait))
        {
//			error4 = "Failed to wait for the " + WaitedElement + " to be visble";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementXpath))
        {
//			error4 = "Failed to wait for " + WaitedElement + " to be clickable";
            return false;
        }
        
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
            try
            {
                try 
                {
                    WebElement elementToClick1 = Driver.findElement(By.xpath(elementXpath));            

                    if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
                    {
                        if (elementToClick1.getAttribute("href").contains("tabs"))
                        {
                            return clickElementByXpathFireFox(elementXpath);  
                        }
                    }
                }
                catch (Exception e) 
                {
                    try
                    {
                        WebElement elementToClick2 = Driver.findElement(By.xpath(elementXpath+"//.."));

                        if (elementToClick2.getAttribute("onmouseout").contains("ImageGreyMain();"))
                        {
                            return clickElementByXpathFireFox(elementXpath);  
                        }
                    }
                    catch (Exception es) 
                    {
                        WebElement we = this.Driver.findElement(By.xpath(elementXpath));

//                        JavascriptExecutor executor = (JavascriptExecutor) Driver;
//                        executor.executeScript("arguments[0].click();", we);
                        we.click();

                    }

                }  
            }
            
            catch (Exception e)
            {
                Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
                this.DriverExceptionDetail = e.getMessage();
                return false;
            }

        } 
        else
        {
            if (!SeleniumDriverInstance.clickElementByXpath_Actions(elementXpath))
            {
    //			error4 = "Failed to click on " + WaitedElement;
                return false;
            }
        }

        return true;
    }
    /**
     * This method is a stable click method that will be use to clicking on
     * element within any browser
     *
     * @param elementXpath
     * @param WaitedElement
     * @return
     */
    public boolean stableClickElementByXpath(String elementXpath)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(elementXpath))
        {
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elementXpath))
        {
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementXpath))
        {
            return false;
        }
        
        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
           if (!SeleniumDriverInstance.clickElementByXpath(elementXpath))
            {
                return false;
            }
        } 
        else
        {
            if (!SeleniumDriverInstance.clickElementByXpath_Actions(elementXpath))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * This method is a stable double click method that will be use to clicking
     * on element within any browser
     *
     * @param elementXpath
     * @param WaitedElement
     * @return
     */
    public boolean stableDoubleClickElementByXpath(String elementXpath, String WaitedElement)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(elementXpath))
        {
//			error4 = "Failed to wait for the " + WaitedElement;
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elementXpath))
        {
//			error4 = "Failed to wait for the " + WaitedElement + " to be visble";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementXpath))
        {
//			error4 = "Failed to wait for " + WaitedElement + " to be clickable";
            return false;
        }

        if (!SeleniumDriverInstance.doubleClickElementbyActionXpath(elementXpath))
        {
//			error4 = "Failed to double click on " + WaitedElement;
            return false;
        }

        if (!SeleniumDriverInstance.doubleClickElementbyActionXpath(elementXpath))
        {
//				error4 = "Failed to double click on " + WaitedElement;
            return false;
        }

        return true;
    }

    /**
     * This method is a stable expand Navigation Menu Item for any Browser
     *
     * @param elementXpath
     * @param waitedElement
     * @return
     */
    public boolean stableExpandNavigationMenuItemByXpath(String elementXpath, String waitedElement)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(elementXpath))
        {
//			error4 = "Failed to wait for the " + waitedElement + " container";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elementXpath))
        {
//			error4 = "Failed to wait for the " + waitedElement + " container to be visble";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementXpath))
        {
//			error4 = "Failed to wait for " + waitedElement + " container to be clickable";
            return false;
        }

        if (!SeleniumDriverInstance.expandNavigationMenuItemByXpath(elementXpath))
        {
//			error4 = "Failed to click on " + waitedElement + "  containers expand icon";
            return false;
        }
        return true;
    }

    /**
     * This method is method is a more stable enter text on WebElement for any
     * browser.
     *
     * @param elementXpath
     * @param WaitedElement
     * @param textToEnter
     * @return
     */
    public boolean stableEnterTextByXpath(String elementXpath, String textToEnter)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(elementXpath))
        {
//			error4 = "Failed to wait for the " + WaitedElement;
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elementXpath))
        {
//			error4 = "Failed to wait for the " + WaitedElement + " to be visble";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementXpath))
        {
//			error4 = "Failed to wait for " + WaitedElement + " to be clickable";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(elementXpath, textToEnter))
        {
//			error4 = "Failed to enter text in the " + WaitedElement;
            return false;
        }

        return true;
    }

    /**
     * This method is method is a more stable enter text on WebElement for any
     * browser.
     *
     * @param elemetXpath
     * @param WaitedElement
     * @param textToEnter
     * @return
     */
    public boolean stableEnterElementByXpath(String elemetXpath, String textToEnter)
    {
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elemetXpath))
        {
//			error3 = "Failed to wait for the '" + WaitedElement + "' to be visble";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elemetXpath))
        {
//			error3 = "Failed to wait for '" + WaitedElement + "' to be clickable";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(elemetXpath, textToEnter))
        {
//			error3 = "Failed to enter text in the '" + WaitedElement + "'";
            return false;
        }

        return true;
    }

    /**
     * This method is a more stable Check box method to check boxes to any
     * browser
     *
     * @param elemetXpath
     * @param WaitedElement
     * @param isSelected
     * @return
     */
    public boolean stableCheckBoxSelectionByXpath(String elemetXpath, boolean isSelected)
    {
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elemetXpath))
        {
//			error3 = "Failed to wait for the '" + WaitedElement + "' checkbox to be visble";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elemetXpath))
        {
//			error3 = "Failed to wait for the '" + WaitedElement + "' checkbox to be clickable";
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionByXpath(elemetXpath, isSelected))
        {
//			error3 = "Failed to check check box of '" + WaitedElement + "'";
            return false;
        }

        return true;
    }

    public boolean stableRightClickElementByXpath(String elementXpath)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(elementXpath))
        {
//			error4 = "Failed to wait for the " + WaitedElement;
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elementXpath))
        {
//			error4 = "Failed to wait for the " + WaitedElement + " to be visble";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementXpath))
        {
//			error4 = "Failed to wait for " + WaitedElement + " to be clickable";
            return false;
        }

        if (!SeleniumDriverInstance.rightClickElementByXpath(elementXpath))
        {
//			error4 = "Failed to right click on " + WaitedElement;
            return false;
        }

        return true;
    }

    /**
     * This method is more stable of SelectByTextFromDropDownListUsingXpath.
     *
     * @param elementXpath
     * @param WaitedElement
     * @param valueToSelect
     * @return
     */
    public boolean stableSelectByTextFromDropDownListUsingXpath(String elementXpath, String valueToSelect)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(elementXpath))
        {
//			error4 = "Failed to wait for the " + WaitedElement;
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elementXpath))
        {
//			error4 = "Failed to wait for the " + WaitedElement + " to be visble";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementXpath))
        {
//			error4 = "Failed to wait for the " + WaitedElement + " checkbox to be clickable";
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(elementXpath, valueToSelect))
        {
//			error4 = "Failed to select " + valueToSelect + " from " + WaitedElement;
            return false;
        }

        return true;
    }

    /**
     * This method allows to select element on custom drop down menu
     *
     * @param dropDownXpath Xpath of the dropdown icon
     * @param DropDownName Drop drown name
     * @param valueToSelectXpath Value to select from the dropDown
     * @return
     */
    public boolean NavigateDropDownMenuItem(String dropDownXpath, String valueToSelectXpath)
    {
        if (!SeleniumDriverInstance.stableClickElementByXpath(dropDownXpath))
        {
//			error2 = "Failed to Stable hover Over - '" + DropDownName + "'";
            return false;
        }

        if (!SeleniumDriverInstance.stableClickElementByXpath(valueToSelectXpath))
        {
//			error2 = "Failed to Stable click on '" + SubDropDownOptionString + "' - ";
            return false;
        }

        return true;
    }

    public String ReturnClipboard()
    {
        String result = "";
        try
        {
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Clipboard clipboard = toolkit.getSystemClipboard();
            result = (String) clipboard.getData(DataFlavor.stringFlavor);
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to copy from clipboard" + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return result = null;
        }

        return result;
    }

    public boolean ValidateClipboard(String clipBoard, String toFind)
    {
        boolean stringFound = false;
        try
        {
            stringFound = clipBoard.toLowerCase().contains(toFind.toLowerCase());
            if (stringFound = false)
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to locate substring" + toFind + " :" + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

        return true;
    }

    public boolean waitForPageLoaded(int timeTowait, WebDriver driver)
    {

        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>()
        {
            public Boolean apply(WebDriver driver)
            {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }
        };

        Wait<WebDriver> wait = new WebDriverWait(driver, timeTowait);
        try
        {
            wait.until(expectation);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to wait for the page to load - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

//    public boolean TakeElementScreenshotByXpath(String elementXpath,String toSaveScreenShotLocation)
//    {
//        WebElement ele = Driver.findElement(By.id("hplogo"));
//        //Get entire page screenshot
//        File screenshot = ((TakesScreenshot)Driver).getScreenshotAs(OutputType.FILE);
//        BufferedImage  fullImg = ImageIO.read(screenshot);
//        //Get the location of element on the page
//        Point point = ele.getLocation();
//        //Get width and height of the element
//        int eleWidth = ele.getSize().getWidth();
//        int eleHeight = ele.getSize().getHeight();
//        //Crop the entire page screenshot to get only element screenshot
//        BufferedImage eleScreenshot= fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
//            eleHeight);
//        ImageIO.write(eleScreenshot, "png", screenshot);
//        //Copy the element screenshot to disk
//        File screenshotLocation = new File("C:\\images\\GoogleLogo_screenshot.png");
//        FileUtils.copyFile(screen, screenshotLocation);
//      return true;
//    }
    public String pagingMethod(String prevpageBtnXpath, String nextpageBtnXpath, String lastpageBtnXpath, String firstpageBtnXpath)
    {
        //Previous
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(prevpageBtnXpath))
        {
            return "Failed to wait for the previous page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(prevpageBtnXpath))
        {
            return "Failed to wait for the previous page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(prevpageBtnXpath))
        {
            return "Failed to click the previous page button";
        }

        //Next
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(nextpageBtnXpath))
        {
            return "Failed to wait for the next page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(nextpageBtnXpath))
        {
            return "Failed to wait for the next page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(nextpageBtnXpath))
        {
            return "Failed to click the next page button";
        }

        //Last Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(lastpageBtnXpath))
        {
            return "Failed to wait for the last page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(lastpageBtnXpath))
        {
            return "Failed to wait for the last page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(lastpageBtnXpath))
        {
            return "Failed to click the last page button";
        }

        //First Page
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(firstpageBtnXpath))
        {
            return "Failed to wait for the first page button to be visible";
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(firstpageBtnXpath))
        {
            return "Failed to wait for the first page button to be clickable";
        }
        if (!SeleniumDriverInstance.clickElementByXpath(firstpageBtnXpath))
        {
            return "Failed to click the first page button";
        }
        return "";
    }

    public boolean KeyPress(Keys keys) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
      public void clickAndSaveFileIE1(String xpath) throws InterruptedException{
    try {
           Robot robot = new Robot();
                //get the focus on the element..don't use click since it stalls the driver          

             //wait for the modal dialog to open            
            Thread.sleep(6000);
           //press s key to save            
           robot.keyPress(KeyEvent.VK_S);
           robot.keyRelease(KeyEvent.VK_S);
           Thread.sleep(2000);
          if (IERetryDownload(xpath)) 
            {
            
            }
          else
          {
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
                Thread.sleep(2000);
                robot.keyPress(KeyEvent.VK_ESCAPE);
                robot.keyRelease(KeyEvent.VK_ESCAPE);
                Thread.sleep(2000);
                robot.keyPress(KeyEvent.VK_ESCAPE);
                robot.keyRelease(KeyEvent.VK_ESCAPE);
                JavascriptExecutor jsExecutor = (JavascriptExecutor)SeleniumDriverInstance.Driver;
                String currentFrame = jsExecutor.executeScript("return self.name").toString();
              if (currentFrame.equals("")) 
              {
            
                 robot.keyPress(KeyEvent.VK_ALT);
                 robot.keyPress(KeyEvent.VK_F4);
                 Thread.sleep(2000);
                 robot.keyRelease(KeyEvent.VK_F4);
                 robot.keyRelease(KeyEvent.VK_ALT);
                }
          }
             
        } catch (AWTException e) {

            e.printStackTrace();
        }
  }
   public void clickAndSaveFileIE(String xpath) throws InterruptedException{
    try {
        Robot robot = new Robot();
         //wait for the modal dialog to open            
        Thread.sleep(2000);
        //press s key to save            
        robot.keyPress(KeyEvent.VK_S);
        robot.keyRelease(KeyEvent.VK_S);
        Thread.sleep(2000);
        IERetryDownload(xpath);


    } 
    catch (AWTException e) {

            e.printStackTrace();
        }
  }  
  public void IESaveImagefile(String xpath) throws InterruptedException{
    try {

           
          Robot robot = new Robot();
          if (!xpath.contains("Download")) 
          {
              JavascriptExecutor jsExecutor = (JavascriptExecutor)SeleniumDriverInstance.Driver;
              String currentFrame = jsExecutor.executeScript("return self.name").toString();
              if (currentFrame.equals("")) 
                 {

                     robot.keyPress(KeyEvent.VK_ALT);
                     robot.keyPress(KeyEvent.VK_F4);
                     Thread.sleep(2000);
                     robot.keyRelease(KeyEvent.VK_F4);
                     robot.keyRelease(KeyEvent.VK_ALT);


                 }
          }
           Thread.sleep(2000);         
           robot.keyPress(KeyEvent.VK_CONTROL);
           robot.keyPress(KeyEvent.VK_J);
           robot.keyRelease(KeyEvent.VK_J);
           robot.keyRelease(KeyEvent.VK_CONTROL);

           Thread.sleep(2000);
           if (count == 0) 
            {
               robot.keyPress(KeyEvent.VK_TAB);
               robot.keyRelease(KeyEvent.VK_TAB);
               count=1;
            }
           robot.keyPress(KeyEvent.VK_ENTER);
           robot.keyRelease(KeyEvent.VK_ENTER);
           Thread.sleep(2000);
           robot.keyPress(KeyEvent.VK_ESCAPE);
           robot.keyRelease(KeyEvent.VK_ESCAPE);
           Thread.sleep(2000);

    

 } catch (AWTException e) {

            e.printStackTrace();
        }
  } 
  
  public boolean IERetryDownload(String xpath)
  {
      try
       {  SeleniumDriverInstance.pause(5000);        
          boolean saved = false;
          int count = 0;
          String downloadLocation;
          Robot robot = new Robot();

        while (count < 3 && saved == false)
        {
            downloadLocation = new File(System.getProperty("user.home") + "/Downloads/").toString();
            File dir = new File(downloadLocation);
            try
            {
                if (dir.isDirectory())
                {
                    File[] children = dir.listFiles();
                    if (children != null && children.length!=0)
                    {
                        saved = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
               
            }
            if (!saved)
            {
                SeleniumDriverInstance.pause(2000);
                 //press s key to save   
                robot.keyPress(KeyEvent.VK_S);
                robot.keyRelease(KeyEvent.VK_S);
                count++;
            }
        }
        if (!saved) 
        {

            IESaveImagefile(xpath);
            return true;
        }
    }
    catch(Exception e)
    {

    }
     return false;
  }
  public boolean clickElementByXpathFireFox(String elementXpath)
    {
         if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
        {
            try
            {
            Actions action = new Actions(SeleniumDriverInstance.Driver);
            action.release().build().perform();
            }
            catch(Exception ex)
            {
                
            }
        }
        try
        {

            waitForElementByXpath(elementXpath);
            waitForElementToBeClickableByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
//            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));

            WebElement we = this.Driver.findElement(By.xpath(elementXpath));
            
            JavascriptExecutor executor = (JavascriptExecutor) Driver;
            executor.executeScript("arguments[0].click();", we);
            
//           JavascriptExecutor js = (JavascriptExecutor) Driver;
//            js.executeScript("var evt = document.createEvent('MouseEvents');" + "evt.initMouseEvent('click',true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0,null);" + "arguments[0].dispatchEvent(evt);", elementToClick);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
  
  public boolean clickElementByXpathFireFox2(String elementXpath)
    {
        try
        {
//            JavascriptExecutor js = (JavascriptExecutor)Driver;
           WebElement button =Driver.findElement(By.id(elementXpath));					
//            js.executeScript("document.getElementById('"+elementXpath+"').click();", button);
            Actions builder = new Actions(Driver);
            builder.moveToElement(button).build().perform();
            
//           JavascriptExecutor js = (JavascriptExecutor) Driver;
//           js.executeScript("var evt = document.createEvent('MouseEvents');" + "evt.initMouseEvent('click',true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0,null);" + "arguments[0].dispatchEvent(evt);", elementToClick);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
  

    public boolean SintelligentModalFrameColumnResizeRight2(String columnName, int resizeRightAmount) 
    {
        try
        {
            WebElement columnDivider = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@class]//div[contains(text(), '" + columnName + "')]"));
            Actions action = new Actions(SeleniumDriverInstance.Driver);

            action.clickAndHold(columnDivider).build().perform();

            action.moveByOffset((resizeRightAmount), 0).build().perform();

            action.release().build().perform();

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean SintelligentModalFrameColumnResizeLeft2(String columnName, int resizeLeftAmount)
    {
        try
        {
            WebElement columnDivider = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[contains(text(), '" + columnName + "')]//..//div[2]"));
            Actions action = new Actions(SeleniumDriverInstance.Driver);

            action.clickAndHold(columnDivider).build().perform();

            action.moveByOffset((-1 * resizeLeftAmount), 0).build().perform();

            action.release().build().perform();

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return false;
        }

        return true;
    }
    
    public boolean SintelligentColumnResizeLeft2(String columnName, int resizeLeftAmount)
    {
        try
        {
            waitForElementByXpath("//div[contains(text(), '" + columnName + "')]//..//div[2]");
            waitForElementByXpathVisibility("//div[contains(text(), '" + columnName + "')]//..//div[2]");
            waitForElementToBeClickableByXpath("//div[contains(text(), '" + columnName + "')]//..//div[2]");
            WebElement columnDivider = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[contains(text(), '" + columnName + "')]//..//div[2]"));
            Actions action = new Actions(SeleniumDriverInstance.Driver);

            action.clickAndHold(columnDivider).build().perform();

            action.moveByOffset((-1 * resizeLeftAmount), 0).build().perform();

            action.release().build().perform();

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean SintelligentColumnResizeRight2(String columnName, int resizeRightAmount)
    {
        try
        {
            waitForElementByXpath("//div[contains(text(), '" + columnName + "')]");
            waitForElementByXpathVisibility("//div[contains(text(), '" + columnName + "')]");
            waitForElementToBeClickableByXpath("//div[contains(text(), '" + columnName + "')]");
            WebElement columnDivider = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[contains(text(), '" + columnName + "')]"));
            Actions action = new Actions(SeleniumDriverInstance.Driver);

            action.clickAndHold(columnDivider).build().perform();

            action.moveByOffset((resizeRightAmount), 0).build().perform();

            action.release().build().perform();

        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean stableDoubleClickElementByXpath(String elementXpath) 
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(elementXpath))
        {
//			error4 = "Failed to wait for the " + WaitedElement;
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elementXpath))
        {
//			error4 = "Failed to wait for the " + WaitedElement + " to be visble";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementXpath))
        {
//			error4 = "Failed to wait for " + WaitedElement + " to be clickable";
            return false;
        }

        if (!SeleniumDriverInstance.doubleClickElementbyActionXpath(elementXpath))
        {
//			error4 = "Failed to double click on " + WaitedElement;
            return false;
        }

        if (!SeleniumDriverInstance.doubleClickElementbyActionXpath(elementXpath))
        {
//				error4 = "Failed to double click on " + WaitedElement;
            return false;
        }

        return true;
    
    }
    
    public boolean CustomMenuNavigation(String Name1, String Name2)
    {
        
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
           
            return false;
        }

        if (!SeleniumDriverInstance.stableClickElementByXpath(SintelligentPageObject.MainMenuButtonXpath()))
        {
            
            return false;
        }
        //Using a pause to give the menue bar enough time to expand
        //Helps with stability using firefox and Chrome
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.AWithTextXpath(Name1)))
        {
            
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AWithTextXpath(Name1)))
        {
            
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AWithTextXpath(Name1)))
        {
            
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AWithTextXpath(Name1)))
        {
            
            return false;
        }

        //Using a pause to give the menue bar enough time to expand
        //Helps with stability using firefox and Chrome
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.waitForElementByXpath(SintelligentPageObject.AWithTextXpath(Name2)))
        {
            
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(SintelligentPageObject.AWithTextXpath(Name2)))
        {
            
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(SintelligentPageObject.AWithTextXpath(Name2)))
        {
            
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByXpath(SintelligentPageObject.AWithTextXpath(Name2)))
        {
            
            return false;
        }
        
        return true;
    }
    public boolean IEClickElementByXpath(String elementXpath) 
    {
        try
        {
        SeleniumDriverInstance.pause(4000);
         Actions action = new Actions(Driver);
        WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
        action.moveToElement(elementToClick).build().perform();
        action.click(elementToClick).build().perform();
        return true;
         }
        catch (Exception e)
        {
            Narrator.logError(" double clicking element by actions xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        
    }
 
}
