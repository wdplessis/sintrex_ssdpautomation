/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;


import KeywordDrivenTestFramework.Core.BaseClass;
import autoitx4java.AutoItX;
import com.jacob.com.LibraryLoader;
import java.io.File;

/**
 *
 * @author ynaidoo
 */
public class AutoItDriverUtility extends BaseClass
{
    public static void main(String[] args) throws InterruptedException 
    {
        String activeWindowHandle;
        String activeWindowTitle;
        String jacobDllVersionToUse;
        Float pixelColour;
        
        
        //Checks java version and uses the relevant jacob and autoit dlls
        if (jvmBitVersion().contains("32"))
        {
            jacobDllVersionToUse = "jacob-1.18-x86.dll";

        }
        else 
        {
            jacobDllVersionToUse = "jacob-1.18-x64.dll";

        }

        //
        File file = new File("Dependencies\\libs", jacobDllVersionToUse);
        System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());

        AutoItX x = new AutoItX();
        
        activeWindowHandle  = x.winGetHandle("[Class:IEFrame]", "");
        activeWindowTitle   = x.winGetTitle("[HANDLE:" + activeWindowHandle + "]");
        pixelColour         = x.pixelGetColor(x.controlGetPosX(activeWindowTitle, "", "[Class:DirectUIHWND;INSTANCE:1]"),x.controlGetPosY(activeWindowTitle, "", "[Class:DirectUIHWND;INSTANCE:1]"));
        
        //TO DO - exit criteria
        while (pixelColour < 0)
        {
            activeWindowTitle   = x.winGetTitle("[HANDLE:" + activeWindowHandle + "]");
            pixelColour         = x.pixelGetColor(x.controlGetPosX(activeWindowTitle, "", "[Class:DirectUIHWND;INSTANCE:1]"),x.controlGetPosY(activeWindowTitle, "", "[Class:DirectUIHWND;INSTANCE:1]"));
            Thread.sleep(500);
        }
        
        //IeSave
        x.winActivate(activeWindowTitle, "");        
        x.controlGetHandle("[Class:IEFrame]", "", "[ClassNN:DirectUIHWND1]");
        Thread.sleep(1000);
        x.controlSend("[Class:IEFrame]","", "[ClassNN:DirectUIHWND1]", "{TAB}");
        Thread.sleep(1000);
        x.controlSend("[Class:IEFrame]","", "[ClassNN:DirectUIHWND1]", "{TAB}");
        Thread.sleep(1000);
        x.controlSend("[Class:IEFrame]","", "[ClassNN:DirectUIHWND1]", "{enter}");
        Thread.sleep(1000);

        Thread.sleep(1000);
        
        //IeOpen
    }
    
    public boolean IESave()
    {
        try
        {
            String activeWindowHandle;
            String activeWindowTitle;
            String jacobDllVersionToUse;
            Float pixelColour;

            //Checks java version and uses the relevant jacob and autoit dlls
            if (jvmBitVersion().contains("32"))
            {
                jacobDllVersionToUse = "jacob-1.18-x86.dll";
            }
            else 
            {
                jacobDllVersionToUse = "jacob-1.18-x64.dll";
            }

            File file = new File("Dependencies\\libs", jacobDllVersionToUse);
            System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());

            AutoItX x = new AutoItX();

            activeWindowHandle  = x.winGetHandle("[Class:IEFrame]", "");
            activeWindowTitle   = x.winGetTitle("[HANDLE:" + activeWindowHandle + "]");
            pixelColour         = x.pixelGetColor(x.controlGetPosX(activeWindowTitle, "", "[Class:DirectUIHWND;INSTANCE:1]"),x.controlGetPosY(activeWindowTitle, "", "[Class:DirectUIHWND;INSTANCE:1]"));

            //TO DO - exit criteria
            while (pixelColour < 0)
            {
                activeWindowTitle   = x.winGetTitle("[HANDLE:" + activeWindowHandle + "]");
                pixelColour         = x.pixelGetColor(x.controlGetPosX(activeWindowTitle, "", "[Class:DirectUIHWND;INSTANCE:1]"),x.controlGetPosY(activeWindowTitle, "", "[Class:DirectUIHWND;INSTANCE:1]"));
                Thread.sleep(500);
            }

            //IeSave
            x.winActivate(activeWindowTitle, "");        
            x.controlGetHandle("[Class:IEFrame]", "", "[ClassNN:DirectUIHWND1]");
            Thread.sleep(1000);
            x.controlSend("[Class:IEFrame]","", "[ClassNN:DirectUIHWND1]", "{TAB}");
            Thread.sleep(1000);
            x.controlSend("[Class:IEFrame]","", "[ClassNN:DirectUIHWND1]", "{TAB}");
            Thread.sleep(1000);
            x.controlSend("[Class:IEFrame]","", "[ClassNN:DirectUIHWND1]", "{enter}");
            Thread.sleep(1000);
        }
        catch(Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return false;
        }
        
        return true;
    }

    //Returns if the JVM is 32 or 64 bit version
    public static String jvmBitVersion()
    {
        return System.getProperty("sun.arch.data.model");
    }
}

//; sleep statements are added only to illustrate the focus on buttons
//;read arguments
//Local $pathToSave=$CmdLine[1]
//
//; get the handle of main window
///////////////////Local $windHandle=WinGetHandle("[Class:IEFrame]", "")
//Local $winTitle = "[HANDLE:" & $windHandle & "]"; 
//;get coordinates of default HWND 
//Local $ctlText=ControlGetPos ($winTitle, "", "[Class:DirectUIHWND;INSTANCE:1]")
//
//; wait till the notification bar is displayed
//Local $color= PixelGetColor ($ctlText[0],$ctlText[1])
//while $color <> 0
//sleep(500)
//$ctlText=ControlGetPos ($winTitle, "", "[Class:DirectUIHWND;INSTANCE:1]")
//$color= PixelGetColor ($ctlText[0],$ctlText[1])
//
//wend
//; Select save as option
//WinActivate ($winTitle, "")
//Send("{F6}")
//sleep(500)
//Send("{TAB}")
//sleep(500)
//Send("{DOWN}")
//sleep(500)
//Send("a")
//
//
//; Save as dialog
//; wait for Save As window
//WinWait("Save As")
//; activate Save As window
//WinActivate("Save As")
//; path to save the file is passed as command line arugment
//ControlFocus("Save As","","[CLASS:Edit;INSTANCE:1]")
//Send($pathToSave)
//sleep(500)
//;click on save button
//ControlClick("Save As","","[TEXT:&Save]")
//
//; wait till the download completes
//Local $sAttribute = FileGetAttrib($pathToSave);
//while $sAttribute = ""
//sleep(500)
//$sAttribute = FileGetAttrib($pathToSave)
//wend
//sleep(2000)
//;close the notification bar
//WinActivate ($winTitle, "")
//Send("{F6}")
//sleep(300)
//Send("{TAB}")
//sleep(300)
//Send("{TAB}")
//sleep(300)
//Send("{TAB}")
//sleep(300)
//Send("{ENTER}")



//
//        //x.winGetHandle("[Class:IEFrame]");
//        x.controlGetHandle("[Class:IEFrame]", "", "[ClassNN:DirectUIHWND1]");
//        Thread.sleep(1000);
//        x.controlSend("[Class:IEFrame]","", "[ClassNN:DirectUIHWND1]", "{TAB}");
//        Thread.sleep(1000);
//        x.controlSend("[Class:IEFrame]","", "[ClassNN:DirectUIHWND1]", "{TAB}");
//        Thread.sleep(1000);
//        x.controlSend("[Class:IEFrame]","", "[ClassNN:DirectUIHWND1]", "{enter}");
//        Thread.sleep(1000);