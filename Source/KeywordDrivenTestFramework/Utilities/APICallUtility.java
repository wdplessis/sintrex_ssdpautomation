/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author szeuch
 */
public class APICallUtility 
{
    public boolean sendPost(String url, String call)
    {
        try
        {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(url);

            StringEntity entity = new StringEntity(call);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
          
            CloseableHttpResponse response = client.execute(httpPost);
            int responseValue = response.getStatusLine().getStatusCode(); 
            if (responseValue != 200) 
            {
                System.out.println(response.getStatusLine().toString());
                return false;
            }
            System.out.println(response.getStatusLine().toString());
            client.close();
        }
        catch(Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
            return false;
        }
        
        return true;
    }
    
    public String getSelectResponse(String url, String call, String columnName)
    {
        String responseContent = "";
        String description = "";
        try
        {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(url);

            StringEntity entity = new StringEntity(call);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
          
            CloseableHttpResponse response = client.execute(httpPost);
            int responseValue = response.getStatusLine().getStatusCode(); 
            if (responseValue != 200) 
            {
                return response.getStatusLine().toString();
            }
            System.out.println(response.getStatusLine().toString());
            responseContent = EntityUtils.toString(response.getEntity());
            JSONParser parser = new JSONParser();
            Object resultObject = parser.parse(responseContent);
            if (resultObject instanceof JSONObject) 
            {
                JSONObject obj =(JSONObject)resultObject;
                String dataDesc = obj.get("data").toString();
                Object resultObject2 = parser.parse(dataDesc);
                JSONArray obj2 =(JSONArray)resultObject2;
                for (Object object : obj2) 
                {
                    JSONObject obj3 =(JSONObject)object;
                    responseContent = obj3.get(columnName).toString();
                    System.out.println(columnName + " : " + responseContent);
                }
            }
            
            client.close();
            return responseContent;
        }
        catch(Exception e)
        {
            return "[ERROR] - " + e.getMessage();
        }
    }
}
