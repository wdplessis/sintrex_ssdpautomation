/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import com.sun.mail.imap.IMAPFolder;
import java.util.ArrayList;
import java.util.Properties;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;

/**
 *
 * @author szeuch
 */
public class EmailUtility 
{

    public ArrayList readAllEmails(String sender, String subject)
    {
        ArrayList data = new ArrayList();
        Folder folder;        
        Properties props;
        Session session;
        Store store;
        
        try
        {
            props = System.getProperties();
            props.setProperty("mail.store.protocol", "imaps");
            session = Session.getDefaultInstance(props, null);
            store = session.getStore("imaps");
            store.connect("imap.gmail.com", "sintrexautomatiotestingdvt@gmail.com","dvtautomationtesting");
        
            folder = (IMAPFolder) store.getFolder("INBOX");
            folder.open(Folder.READ_WRITE);

            Message[] messages = null;
              
            messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false),folder.getMessages());

            for (Message mail : messages)
            {
                if (mail.getFrom()[0].toString().contains(sender) && mail.getSubject().toString().contains(subject))
                {
                    String temp = mail.getContent().toString();
//                    temp.
//                    data.add(mail.getContent().toString());
                    mail.setFlag(Flags.Flag.SEEN, true);
                }
            }
            
            messages = folder.getMessages();
            
            for (Message mail : messages)
            {
                mail.setFlag(Flags.Flag.SEEN, true);
            }
        }
        catch(Exception e)
        {
            System.err.println("[Error]Failed to validate the email - " + e.getMessage());
            return null;
        }        
        return data;
    }
    
    public boolean deleteAllEmails()
    {
        Folder folder;        
        Properties props;
        Session session;
        Store store;
        
        try 
        {
            props = System.getProperties();
            props.setProperty("mail.store.protocol", "imaps");
            session = Session.getDefaultInstance(props, null);
            store = session.getStore("imaps");
            store.connect("imap.gmail.com", "sintrexautomatiotestingdvt@gmail.com","dvtautomationtesting");
        
            folder = (IMAPFolder) store.getFolder("INBOX");
            folder.open(Folder.READ_WRITE);

            Message[] messages = null;
            
            for (int i = 0; i <= 5; i++) 
            {                
                messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false),folder.getMessages());
            }
            
            messages = folder.getMessages();
            
            for (Message mail : messages)
            {
                mail.setFlag(Flags.Flag.DELETED, true);
            }
        } 
        catch (Exception e) 
        {
            System.out.println("[Error] - Failed t");
            return false;
        }
        
        return true;
    }
    
}
