/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Reporting;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author trichardson
 */
public class CSVBatchReporter extends BaseClass
{
    TestResult testResult;
    TestEntity testData;
    String csvName = "";
    String outputPath = "";
    String environment = "";
    long testTime = 0;
    String testStatus = "";
    
    File csvReportFile;
    
    public CSVBatchReporter(String outputPath, String csvName)
    {
        this.outputPath = outputPath;
        this.csvName = csvName;
    }
    
    public CSVBatchReporter(String csvName, String outputPath,TestEntity testData, TestResult testResult, long totalTestTime, String environment) 
    {
        this.testData = testData;
        this.environment = environment;
        this.testResult = testResult;
        this.csvName = csvName;
        this.outputPath = outputPath;
        this.testTime = totalTestTime;
        this.testStatus = testResult.testStatus.toString();
    }
    
    public void setCSVName(String input)
    {
        csvName = input;
    }
    
    //create or overwrite the file
    public void createRegressionReportCSV()
    {
        csvReportFile = new File(outputPath+"//"+csvName+".csv");
        
        try
        {
            CSVReader csvReader;
            
            //if file exists - read + overwrite
            if(csvReportFile.exists())
            {
                String toWrite = testResult.testData.TestMethod +","+ testStatus.toString()+","+this.testResult.errorMessage+","+  ""+this.testResult.testDuration+","+ environment;

                csvReader = new CSVReader(new FileReader(csvReportFile));
                List<String[]> list = csvReader.readAll();
                ArrayList <String> currentTestIDs = new ArrayList <String>();
                
                for (int i = 0; i < list.size(); i++) 
                {
                    currentTestIDs.add(list.get(i)[0]);
                }
                
                if(!currentTestIDs.toString().contains(testData.TestMethod.toString()))
                {
                    try
                    {
                        StringBuilder sb = new StringBuilder();

                        //read csv contents
                        ArrayList<String> currentCSVContentsLines = new ArrayList();
                        
                        BufferedReader counterReader;
                        BufferedReader returnInfo;

                        counterReader = new BufferedReader(new FileReader(outputPath+"//"+csvName+".csv"));
                        //reads first line in csv file - metadata

                        returnInfo = new BufferedReader(new FileReader(outputPath+"//"+csvName+".csv"));
                        //reads first line in csv file - metadata

                        String currentLine;

                        while (counterReader.readLine() != null)
                        {
                            currentLine = returnInfo.readLine();
                            currentCSVContentsLines.add(currentLine);
                        }

                        counterReader.close();
                        returnInfo.close();

                        //append the current lines
                        for (int i = 0; i < currentCSVContentsLines.size(); i++)
                        {
                            sb.append(currentCSVContentsLines.get(i).toString() + "\n");
                        }

                        //declare new pw
                        PrintWriter pw = new PrintWriter(new File(outputPath+"//"+csvName+".csv"));

                        //append new lines
                        sb.append(toWrite + '\n');

                        //pw.write
                        pw.write(sb.toString());

                        //pw close
                        pw.close();
                    }
                    catch (Exception e)
                    {
                        System.out.println("[ERROR] - " + e.getMessage());
                    }
                }
            }
            else
            {
                //create the CSV output template
                String [] tempArray = {"TEST CASE ID","STATUS","SCRIPT RETURNED MESSAGE","TIME TAKEN","ENVIRONMENT"};
                String [] toWrite = {testData.TestMethod, testStatus.toString(), this.testResult.errorMessage, ""+this.testResult.testDuration, environment};
                
                String [][] csvData = {tempArray,toWrite};
                
                CSVWriter(csvData, outputPath, csvName+".csv");
            }
        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
        }
    }
    
    public void CSVWriter(String[][] CSVData, String outputDirectory, String outputFileName)
    {
        try
        {
            String[] toWrite;
            for (String[] CSVData1 : CSVData)
            {
                String temp = "";
                for (int j = 0; j < CSVData1.length; j++)
                {
                    temp += CSVData1[j];
                    if (j + 1 != CSVData1.length)
                    {
                        temp += ",";
                    }
                }
                toWrite = temp.split(",");
                String output = outputDirectory + "\\" + outputFileName;
                try (CSVWriter writer = new CSVWriter(new FileWriter(output, true), ',', CSVWriter.NO_QUOTE_CHARACTER))
                {
                    writer.writeNext(toWrite,false);
                }
            }
        }
        catch (Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
        }
    }
    
    public void generateStats()
    {
        csvReportFile = new File(outputPath+"//"+csvName+".csv");
        
        try
        {
            CSVReader csvReader;
            double totalTests = 0;
            double totalPass = 0;
            double totalFail = 0;
            
            //if file exists - generate stats
            if(csvReportFile.exists())
            {
                csvReader = new CSVReader(new FileReader(csvReportFile));

                List<String[]> list = csvReader.readAll();
                
                
                for (int i = 1; i < list.size(); i++) 
                {
                    if(list.get(i)[1].equalsIgnoreCase("PASS"))
                    {
                        totalPass++;
                    }
                    else if (list.get(i)[1].equalsIgnoreCase("FAIL"))
                    {
                        totalFail++;
                    }
                    totalTests++;
                }
                
                //generate stats template
                double passPercentage = (totalPass/totalTests)*100;
                String [] toWrite = {
                                    "_____________"+","+"_____________"+","+"_____________"+","+"_____________"+","+"_____________"+",",
                                    "TOTAL TESTS" +","+ "TOTAL PASS"+","+"TOTAL FAIL"+","+"PASS PERCENTAGE"+",",
                                    ""+totalTests+","+""+totalPass+","+""+totalFail+","+""+passPercentage,
                                    "_____________"+","+"_____________"+","+"_____________"+","+"_____________"+","+"_____________"+",",
                                    };

                //write to file
                for (int i = 0; i < toWrite.length; i++) 
                {
                    StringBuilder sb = new StringBuilder();

                    //read csv contents
                    ArrayList<String> currentCSVContentsLines = new ArrayList();

                    BufferedReader counterReader;
                    BufferedReader returnInfo;

                    counterReader = new BufferedReader(new FileReader(outputPath+"//"+csvName+".csv"));
                    //reads first line in csv file - metadata

                    returnInfo = new BufferedReader(new FileReader(outputPath+"//"+csvName+".csv"));
                    //reads first line in csv file - metadata

                    String currentLine;

                    while (counterReader.readLine() != null)
                    {
                        currentLine = returnInfo.readLine();
                        currentCSVContentsLines.add(currentLine);
                    }

                    counterReader.close();
                    returnInfo.close();

                    //append the current lines
                    for (int j = 0; j < currentCSVContentsLines.size(); j++)
                    {
                        sb.append(currentCSVContentsLines.get(j).toString() + "\n");
                    }

                    //declare new pw
                    PrintWriter pw = new PrintWriter(new File(outputPath+"//"+csvName+".csv"));

                    //append new lines
                    sb.append(toWrite[i] + '\n');

                    //pw.write
                    pw.write(sb.toString());

                    //pw close
                    pw.close();
                }

            }
            else
            {
                System.out.println("[ERROR] - Failed to generate batch stats - Regression file '"+csvName+"' did not exist");
            }
            
        }
        catch(Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
        }
        
    }
    
}
