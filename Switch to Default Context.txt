//switch to default context


        //select impact from the context dropdown
        if (!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(SintelligentPageObject.SelectWithIdXpath("cboContext"), "Host Groups"))
        {
            error = "Failed to select 'Host Groups' from the context dropdown menu on the Alarm Viewer page";
            return false;
        }

        if (!SeleniumDriverInstance.stableClickElementByXpath(AlarmViewer_ContextMenuAndCustomPageObject.ExpandHostGroupXpath(testData.getData("HostgroupToExpand")))) 
        {
            error = "Failed to  expand host group: "+testData.getData("HostgroupToExpand");
            return false;
        }

        if (!SeleniumDriverInstance.doubleClickElementbyActionXpath(AlarmViewer_ContextMenuAndCustomPageObject.HostGroupsContexMenuXpath(testData.getData("Rule4GroupLookup"))))
        {
            error = "Failed to double click on container '" + testData.getData("HostGroup") + "' on the 'Host Groups' context ";
            return false;
        }