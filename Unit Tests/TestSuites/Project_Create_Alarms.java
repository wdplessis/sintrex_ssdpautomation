/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author bkabona
 */
public class Project_Create_Alarms {
    
  static TestMarshall instance;
    
    public Project_Create_Alarms()
    {
       ApplicationConfig appConfig = new ApplicationConfig();
       //This is where you setup the Environment/Box you wish to run your Script on. (Ctrl Left-Click to be taken to the page where you can add your own Boxes.)
       TestMarshall.currentEnvironment = Enums.Environment.SSDPAutomation2_71;        
    }

    //This Script will create a fresh environment within Sintelligent for your Alarms. (Recommended to run the day before exams to not waste unnesseccery time, due to this process taking between 30 to 45 minutes.)   
    @Test
    public void RunProject_Sintelligent_RebuildChrome() throws FileNotFoundException
    {
        Narrator.logDebug("SINTelligent Project_Create_Alarms - Test Pack");
        instance = new TestMarshall("TestPacks\\Project_Sintelligent_Rebuild.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //To run this seperately without, please comment out the script below.

    //This Script will create your Alarms as instructed by you in the Excel Spreadsheet.
    @Test
    public void RunProject_Create_AlarmsChrome() throws FileNotFoundException
    {
        Narrator.logDebug("SINTelligent Project_Create_Alarms - Test Pack");
        instance = new TestMarshall("TestPacks\\Project_Create_Alarms.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }      

}

