/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author szeuch
 */
public class GeneralUtilityTest {

    @Test
    public void RunBasicUnitTest() 
    {
        System.out.println("Hello World!");
    }
    
//    @Test
//    public void ConnectToVpnTest()
//    {
//        VPNConnect con = new VPNConnect();
//        con.connectToVPN();
//    }
    
    @Test
    public void Xpath()
    {
        //Webelement
        
        //List Webelement
    }
    
    @Test
    public void DatePicker()
    {
        ArrayList months = new ArrayList();
        months.add("Jan");
        months.add("Feb");
        months.add("March");
        months.add("April");
        months.add("May");
        months.add("June");
        months.add("July");
        months.add("Aug");
        months.add("Sept");
        months.add("Oct");
        months.add("Nov");
        months.add("Dec");
        
        String frontEndMonth = "March";
        int fonrEndDay = 23;
        int frontEndYear = 2016;
        int fonrEndMonthInt = 0;
        
        for (int i = 0; i < months.size(); i++)
        {
            if (frontEndMonth == months.get(i)) 
            {
                fonrEndMonthInt = i + 1;
            }
        }
        
        String dt = frontEndYear + "-" + fonrEndMonthInt + "-" + fonrEndDay;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dt));
        } catch (ParseException ex) {
            Logger.getLogger(GeneralUtilityTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        c.add(Calendar.DATE, 1);  // number of days to add
        dt = sdf.format(c.getTime());
        String [] split = dt.split("-");
        
        while(!split[0].equals(frontEndYear))
        {
            //click month pager >>
        }
        
        while(!split[1].equals(months.get(fonrEndMonthInt-1)))
        {
            //click month pager >>
        }
        
        //click 'split[2]'       
    }
    
    @Test public void Disabled()
    {
        WebElement button = null;
        try
        {
            button = SeleniumDriverInstance.Driver.findElement(By.xpath("//button[@id='btnNext']"));
        }
        catch(Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
        }
        String temp = button.getAttribute("disabled");
        
        if (temp.equals("true")) 
        {
            //return it is disabled
        }
        else
        {
            //is it enabled
        }
        
    }
    
    @Test
    public void dbConnect()
    {
        DataBaseUtility db = new DataBaseUtility();
        db.insertIntoReportingDB("Sintrex - Regression Pack - Chrome", "2016-05-04 01-02-00", "Chrome", 15, 0, "5 Minutes");
        System.out.println("[Info] Connection to db: sintrex");
    }
    
    @Test
    public void SintelligentAPIPostCall()
    {
        APICallUtility api = new APICallUtility();
        try
        {
            api.sendPost("http://10.0.4.195/sintrex_api/api/test/query",
                "{\n" +
                "\"type\":\"UPDATE\",\n" +
                "\"statement\":\"UPDATE Fault.Alarm SET isAcknowledge = 0 WHERE id = 5955\"\n" +
                "}");
        } 
        catch (Exception ex)
        {
            Logger.getLogger(GeneralUtilityTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    @Test
    public void deleteAllEmails()
    {
        EmailUtility email = new EmailUtility();
        if (!email.deleteAllEmails()) 
        {
            System.out.println("[ERROR] - ");
        }
    }
    
    @Test
    public void readAllEmails()
    {
        EmailUtility email = new EmailUtility();
        ArrayList emails = new ArrayList();
        emails = email.readAllEmails("Stefan Zeuch", "1");
        
        System.out.println(emails);
        
    }
}
