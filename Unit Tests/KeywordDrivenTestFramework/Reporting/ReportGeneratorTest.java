/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Reporting;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 *
 * @author Ferdi
 */
public class ReportGeneratorTest 
{
    static ReportGenerator generateReport;
    ApplicationConfig config;
    public ReportGeneratorTest() 
    {
        
    }
    
    @Before
    public  void  setupTestParameters()
    {
            config = new ApplicationConfig();
            generateReport = new ReportGenerator(config.InputFileName(),config.ReportFileDirectory());

    }

    @Test
    public void parameterToStringMethodTest() 
    {
            Duration testDuration = new Duration(new DateTime(), new DateTime().plusSeconds(20));
            
//            TestEntity test1 = new TestEntity("GSQ1","Google Search Query");
//            test1.addParameter("Search Query", "Chicken");
//            
//            TestEntity test2 = new TestEntity("GSR1","Google Search Results");
//
//            TestResult result1 = new TestResult(test1, false, "Failed to locate search button", testDuration.getStandardSeconds());
//            generateReport.addResult(result1);
//            
//            TestResult result2 = new TestResult(test2, true, "Test Passed", testDuration.getStandardSeconds() + 12);
//            generateReport.addResult(result2);

            Assert.assertTrue("Generate report failed", generateReport.generateTestReport());

    }
}